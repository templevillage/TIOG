package com.kingdee.eas.custom.tiog.connector.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;

/**
 * @author Jambin
 * @description 封装http响应信息
 * @copyright 天津金蝶软件有限公司
 * @date Created in 2020/3/24
 */
public class HttpClientResult implements Serializable {

    private static final long serialVersionUID = 2168152194164783950L;

    // 响应状态码
    private int code;

    //响应数据
    private String content;

    //文件流
    private InputStream inputStream;

    public HttpClientResult(int code, String content) {
        this.code = code;
        this.content = content;
    }

    public HttpClientResult(int code) {
        this.code = code;
    }

    public HttpClientResult(String content) {
        this.content = content;
    }

    public HttpClientResult(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public HttpClientResult(int code, String content, InputStream inputStream) {
        this.code = code;
        this.content = content;
        this.inputStream = inputStream;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public String toString() {
        return "HttpClientResult{" +
                "code=" + code +
                ", content='" + content + '\'' +
                ", inputStream=" + inputStream +
                '}';
    }
}