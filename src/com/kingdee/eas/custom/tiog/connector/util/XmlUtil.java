package com.kingdee.eas.custom.tiog.connector.util;


import org.apache.commons.lang.StringUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * @author Jambin
 * @copyright 天津金蝶软件有限公司
 * @description XML和javaBean相互转换的工具类
 * @date Created in 2020/3/19
 */
public class XmlUtil {
    /**
     * @param obj 要转换的对象
     * @return java.lang.String
     * @title objectToXml
     * @description 将java对象转换为xml字符串
     * @author Jambin
     * @date 2020/3/19
     */
    public static String objectToXml(Object obj) {
        String result = null;
        try {
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
            StringWriter writer = new StringWriter();
            marshaller.marshal(obj, writer);
            result = writer.toString();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    /**
     * @param clazz 要转换的对象
     * @param xml   要转换的xml
     * @return java.lang.Object
     * @title xmlToObject
     * @description 将xml字符串转换成java对象
     * @author Jambin
     * @date 2020/3/19
     */
    public static Object xmlToObject(Class clazz, String xml) {
        Object xmlObject = null;
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            // 进行将Xml转成对象的核心接口
            Unmarshaller unmarshaller = context.createUnmarshaller();
            StringReader sr = new StringReader(xml);
            xmlObject = unmarshaller.unmarshal(sr);
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
        return xmlObject;
    }

    /**
     * @param obj      要转换的java对象
     * @param filePath 要生成的xml文件存放路径
     * @return void
     * @title generateXML
     * @description 根据java对象生成xml文件
     * @author Jambin
     * @date 2020/3/19
     */
    public static void generateXML(Object obj, String filePath) {
        //判断路径
        if (StringUtils.isBlank(filePath)) {
            return;
        }
        if (!filePath.endsWith(".xml")) {
            return;
        }
        File file = new File(filePath);
        if (file.exists()) {
            return;
        }
        try {
            //根据Person类生成上下文对象
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            //从上下文中获取Marshaller对象，用作将bean编组(转换)为xml
            Marshaller ma = context.createMarshaller();
            //以下是为生成xml做的一些配置
            //格式化输出，即按标签自动换行，否则就是一行输出
            ma.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            //设置编码（默认编码就是utf-8）
            ma.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            //是否省略xml头信息，默认不省略（false）
            ma.setProperty(Marshaller.JAXB_FRAGMENT, false);
            //编组
            ma.marshal(obj, file);
        } catch (JAXBException e) {
            e.printStackTrace();
            return;
        }
    }

    /**
     * @param obj      要转换的对象
     * @param filePath xml所属路径
     * @return java.lang.Object
     * @title generateBean
     * @description 根据xml文件生成java对象
     * @author Jambin
     * @date 2020/3/19
     */
    public static Object generateBean(Object obj, String filePath) {
        Object result = null;
        File file = new File(filePath);
        try {
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            Unmarshaller unmarshaller = context.createUnmarshaller();
            result = unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }
}
