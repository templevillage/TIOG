package com.kingdee.eas.custom.tiog.connector.callinterface;

import com.kingdee.eas.custom.tiog.connector.util.HttpClientResult;
import com.kingdee.eas.custom.tiog.connector.util.HttpClientUtil;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jambin
 * @description 反馈数据下载接口
 * @copyright 天津金蝶软件有限公司
 * @date Created in 2020/3/23
 */
public class DownLoadResFile {

    public static void downLoadResFile(String url, String user, String passWord) throws Exception {
        downLoadResFile(url, "SZ01", "0003", user, passWord);
    }

    public static void downLoadResFile(String url, String apiCode, String businessType, String user,
            String passWord) throws Exception {
        // 处理请求头
        Map<String, String> headerParam = new HashMap<String, String>();
        headerParam.put("Charset", "UTF-8");

        // 处理请求参数
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("API_CODE", apiCode);// 接口编码
        paramMap.put("BUSINESS_TYPE", businessType);// 业务类型编码
        paramMap.put("USER", user);// 调用接口用户名
        paramMap.put("PASSWORD", passWord);// 调用接口密码

        // 通过工具类调用接口
        HttpClientResult result = HttpClientUtil.doPost(url, headerParam, paramMap);
        InputStream inputStream = result.getInputStream();
        // String xml = FileZipUtil.getInstance().readZipFromInputStream(inputStream, ".xml");
        // System.out.println(xml);
    }

}
