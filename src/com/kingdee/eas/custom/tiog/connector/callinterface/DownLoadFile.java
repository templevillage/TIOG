package com.kingdee.eas.custom.tiog.connector.callinterface;

import com.kingdee.eas.custom.tiog.connector.util.FileZipUtil;
import com.kingdee.eas.custom.tiog.connector.util.HttpClientResult;
import com.kingdee.eas.custom.tiog.connector.util.HttpClientUtil;
import com.kingdee.eas.custom.tiog.connector.util.XmlUtil;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.*;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jambin
 * @copyright 天津金蝶软件有限公司
 * @description 静态文件下载接口
 * @date Created in 2020/3/20
 */
public class DownLoadFile {

    /**
     * @param url 接口地址
     * @param user 用户名
     * @param passWord 用户密码
     * @return Map<String, Object> 通过xml转换成的对象map集合
     * @description 调用国资委接口下载静态数据（当接口地址和编码没有传时调用此方法）
     * @title downLoadFile
     * @author Jambin
     * @date 2020/3/23
     */
    public static Map<String, Object> downLoadFile(String url, String user, String passWord) throws Exception {
        return downLoadFile(url, "SZ01", "0002", user, passWord);
    }

    /**
     * @param url 接口地址
     * @param apiCode 接口编码
     * @param businessType 业务类型编码
     * @param user 用户名
     * @param passWord 用户密码
     * @return Map<String, Object> 通过xml转换成的对象map集合
     * @description 调用国资委接口下载静态数据
     * @title downLoadFile
     * @author Jambin
     * @date 2020/3/23
     */
    public static Map<String, Object> downLoadFile(String url, String apiCode, String businessType,
            String user, String passWord) throws Exception {
        System.out.println("※※※※※※※※※静态数据下载接口开始调用※※※※※※※※※");

        // 组装请求头和请求参数，之后调用工具类发送静态数据下载请求
        HttpClientResult result = assemblyAndCall(url, apiCode, businessType, user, passWord);
        System.out.println("=========静态数据下载接口返回值" + result.toString() + "=========");

        // TODO: 通过返回值的类型， 来判断请求是否成功

        // 获取国资委接口返回的zip文件数据流
        InputStream inputStream = result.getInputStream();

        // 将zip文件流解析为xml
        Map<String, String> xmlMap = FileZipUtil.readZipFromInputStream(inputStream, ".xml");

        // 新建返回map集合，将xml转换为javaBean
        Map<String, Object> objectMap = conversion(xmlMap);

        System.out.println("※※※※※※※※※静态数据下载接口调用结束※※※※※※※※※");

        inputStream.close();
        return objectMap;
    }

    /**
     * @param xmlMap xml集合
     * @return 转换后的对象集合
     * @description 将xml转换为javaBean对象
     * @title conversion
     * @author Jambin
     * @date 2020/3/30
     */
    private static Map<String, Object> conversion(Map<String, String> xmlMap) {
        // 新建返回map集合
        Map<String, Object> objectMap = new HashMap<String, Object>();
        // 当数据文件的map集合不为空时遍历，将xml进行对象转换
        if (xmlMap.size() > 0) {
            for (Map.Entry<String, String> entry : xmlMap.entrySet()) {
                String fileName = entry.getKey();
                String fileContent = entry.getValue().trim();
                // 事项目录
                if ("catalog.xml".equals(fileName)) {
                    // 将xm l转换成对象
                    CatalogList catalogList = (CatalogList) XmlUtil.xmlToObject(CatalogList.class,
                            fileContent);
                    // 将对象存放进map集合
                    objectMap.put(fileName, catalogList);
                }
                // 会议类型
                if ("meeting_type.xml".equals(fileName)) {
                    MeetingTypeList meetingTypeList = (MeetingTypeList) XmlUtil.xmlToObject(
                            MeetingTypeList.class, fileContent);
                    objectMap.put(fileName, meetingTypeList);
                }
                // 制度类型
                if ("regulation_type.xml".equals(fileName)) {
                    RegulationTypeList regulationTypeList = (RegulationTypeList) XmlUtil.xmlToObject(
                            RegulationTypeList.class, fileContent);
                    objectMap.put(fileName, regulationTypeList);
                }
                // 会议形式
                if ("meeting_form.xml".equals(fileName)) {
                    MeetingFormList meetingFormList = (MeetingFormList) XmlUtil.xmlToObject(
                            MeetingFormList.class, fileContent);
                    objectMap.put(fileName, meetingFormList);
                }
                // 任务来源
                if ("source.xml".equals(fileName)) {
                    SourceList sourceList = (SourceList) XmlUtil.xmlToObject(SourceList.class, fileContent);
                    objectMap.put(fileName, sourceList);
                }
                // 专项名称
                if ("special.xml".equals(fileName)) {
                    SpecialList specialList = (SpecialList) XmlUtil.xmlToObject(SpecialList.class,
                            fileContent);
                    objectMap.put(fileName, specialList);
                }
                // 表决方式
                if ("vote_mode.xml".equals(fileName)) {
                    VoteModeList voteModeList = (VoteModeList) XmlUtil.xmlToObject(VoteModeList.class,
                            fileContent);
                    objectMap.put(fileName, voteModeList);
                }
            }
        }
        return objectMap;
    }

    /**
     * @param url 接口地址
     * @param apiCode 接口编码
     * @param businessType 业务类型编码
     * @param user 用户名
     * @param passWord 用户密码
     * @return HttpClientResult 请求返回值（对象vo）
     * @description 组装请求头和参数并发送请求
     * @title assemblyAndCall
     * @author Jambin
     * @date 2020/3/30
     */
    private static HttpClientResult assemblyAndCall(String url, String apiCode, String businessType,
            String user, String passWord) throws Exception {
        // 处理请求头
        Map<String, String> headerParam = new HashMap<String, String>();
        headerParam.put("Charset", "UTF-8");

        // 处理请求参数
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("API_CODE", apiCode);// 接口编码
        paramMap.put("BUSINESS_TYPE", businessType);// 业务类型编码
        paramMap.put("USER", user);// 调用接口用户名
        paramMap.put("PASSWORD", passWord);// 调用接口密码

        // 通过工具类调用接口
        return HttpClientUtil.doPost(url, headerParam, paramMap);
    }
}
