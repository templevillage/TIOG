package com.kingdee.eas.custom.tiog.connector.callinterface;

import com.kingdee.eas.custom.tiog.connector.util.HttpClientResult;
import com.kingdee.eas.custom.tiog.connector.util.HttpClientUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jambin
 * @description 数据报送接口
 * @copyright 天津金蝶软件有限公司
 * @date Created in 2020/3/23
 */
public class UploadFile {

    /**
     * @param url 接口地址
     * @param businessType 业务类型编码
     * @param fileName 文件名称含后缀
     * @param filePath 文件路径
     * @param user 用户名
     * @param passWord 用户密码
     * @return void
     * @description 当接口编码没有传时调用此方法
     * @title uploadFile
     * @author Jambin
     * @date 2020/3/23
     */
    public static String uploadFile(String url, String businessType, String fileName, String filePath,
            String user, String passWord) throws Exception {
        return uploadFile(url, "SZ01", businessType, fileName, filePath, user, passWord);
    }

    /**
     * @param url 接口地址
     * @param apiCode 接口编码
     * @param businessType 业务类型编码
     * @param fileName 文件名称含后缀
     * @param filePath 文件路径
     * @param user 用户名
     * @param passWord 用户密码
     * @return void
     * @description 调用国资委接口上传文件
     * @title uploadFile
     * @author Jambin
     * @date 2020/3/23
     */
    public static String uploadFile(String url, String apiCode, String businessType, String fileName,
            String filePath, String user, String passWord) throws Exception {
        System.out.println("※※※※※※※※※数据报送接口开始调用※※※※※※※※※");
        // 组装请求头、请求参数并发送请求
        HttpClientResult result = assemblyAndCall(url, apiCode, businessType, fileName, filePath, user,
                passWord);

        if (result == null)
            return null;
        result.getContent();
        System.out.println(result);

        System.out.println("※※※※※※※※※数据报送接口调用结束※※※※※※※※※");
        return result.getContent();
    }

    /**
     * @param url 请求地址
     * @param apiCode 接口编码
     * @param businessType 业务类型编码
     * @param fileName 文件名称含后缀
     * @param filePath 文件路径
     * @param user 用户名
     * @param passWord 用户密码
     * @return com.kingdee.eas.custom.tiog.connector.util.HttpClientResult
     * @description 组装请求头、请求参数并发送请求
     * @title assemblyAndCall
     * @author Jambin
     * @date 2020/4/9
     */
    private static HttpClientResult assemblyAndCall(String url, String apiCode, String businessType,
            String fileName, String filePath, String user, String passWord) throws Exception {
        // 处理请求头
        Map<String, String> headerParam = new HashMap<String, String>();
        headerParam.put("files", fileName);
        headerParam.put("Accept-Charset", "UTF-8");
        // headerParam.put("Content-Type", "multipart/form-data");

        // 处理请求参数
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("API_CODE", apiCode);// 接口编码
        paramMap.put("BUSINESS_TYPE", businessType);// 业务类型编码
        paramMap.put("FILE_NAME", fileName);// 文件名，含文件后缀
        paramMap.put("USER", user);// 调用接口用户名
        paramMap.put("PASSWORD", passWord);// 调用接口密码

        // 处理文件
        List<File> fileList = new ArrayList<File>();
        File file = new File(filePath);
        if (!file.exists()) {
            return null;
        }
        fileList.add(file);

        // 通过工具类调用接口
        return HttpClientUtil.doPost(url, headerParam, paramMap, fileList);
    }
}
