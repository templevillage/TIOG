package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class ExecuteStatusInfo extends AbstractExecuteStatusInfo implements Serializable 
{
    public ExecuteStatusInfo()
    {
        super();
    }
    protected ExecuteStatusInfo(String pkField)
    {
        super(pkField);
    }
}