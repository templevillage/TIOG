package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class VoteModeInfo extends AbstractVoteModeInfo implements Serializable 
{
    public VoteModeInfo()
    {
        super();
    }
    protected VoteModeInfo(String pkField)
    {
        super(pkField);
    }
}