package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.tiog.basedata.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class MeetingForm extends DataBase implements IMeetingForm
{
    public MeetingForm()
    {
        super();
        registerInterface(IMeetingForm.class, this);
    }
    public MeetingForm(Context ctx)
    {
        super(ctx);
        registerInterface(IMeetingForm.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("536B4A08");
    }
    private MeetingFormController getController() throws BOSException
    {
        return (MeetingFormController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public MeetingFormInfo getMeetingFormInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getMeetingFormInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public MeetingFormInfo getMeetingFormInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getMeetingFormInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public MeetingFormInfo getMeetingFormInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getMeetingFormInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public MeetingFormCollection getMeetingFormCollection() throws BOSException
    {
        try {
            return getController().getMeetingFormCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public MeetingFormCollection getMeetingFormCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getMeetingFormCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public MeetingFormCollection getMeetingFormCollection(String oql) throws BOSException
    {
        try {
            return getController().getMeetingFormCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}