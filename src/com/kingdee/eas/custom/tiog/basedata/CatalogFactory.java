package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class CatalogFactory
{
    private CatalogFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalog getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalog)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("02BD1102") ,com.kingdee.eas.custom.tiog.basedata.ICatalog.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.ICatalog getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalog)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("02BD1102") ,com.kingdee.eas.custom.tiog.basedata.ICatalog.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalog getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalog)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("02BD1102"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalog getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalog)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("02BD1102"));
    }
}