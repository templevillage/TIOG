package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class TaskSourceFactory
{
    private TaskSourceFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.ITaskSource getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ITaskSource)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("673ACC77") ,com.kingdee.eas.custom.tiog.basedata.ITaskSource.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.ITaskSource getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ITaskSource)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("673ACC77") ,com.kingdee.eas.custom.tiog.basedata.ITaskSource.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.ITaskSource getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ITaskSource)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("673ACC77"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.ITaskSource getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ITaskSource)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("673ACC77"));
    }
}