package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class DataSourceCollection extends AbstractObjectCollection 
{
    public DataSourceCollection()
    {
        super(DataSourceInfo.class);
    }
    public boolean add(DataSourceInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(DataSourceCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(DataSourceInfo item)
    {
        return removeObject(item);
    }
    public DataSourceInfo get(int index)
    {
        return(DataSourceInfo)getObject(index);
    }
    public DataSourceInfo get(Object key)
    {
        return(DataSourceInfo)getObject(key);
    }
    public void set(int index, DataSourceInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(DataSourceInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(DataSourceInfo item)
    {
        return super.indexOf(item);
    }
}