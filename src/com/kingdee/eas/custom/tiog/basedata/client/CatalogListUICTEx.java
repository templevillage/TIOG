package com.kingdee.eas.custom.tiog.basedata.client;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.ui.face.IUIFactory;
import com.kingdee.bos.ui.face.IUIWindow;
import com.kingdee.bos.ui.face.UIFactory;
import com.kingdee.eas.base.core.fm.ContextHelperFactory;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.common.client.OprtState;
import com.kingdee.eas.custom.tiog.basedata.CatalogFactory;
import com.kingdee.eas.custom.tiog.basedata.CatalogInfo;
import com.kingdee.eas.custom.tiog.basedata.ICatalog;
import com.kingdee.eas.custom.tiog.bizbill.BillState;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListFactory;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.eas.custom.tiog.bizbill.ICategoryList;
import com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemFactory;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.eas.util.SysUtil;
import com.kingdee.eas.util.client.KDTableUtil;
import com.kingdee.eas.util.client.MsgBox;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.basedata.org.CompanyOrgUnitInfo;
import com.kingdee.eas.fi.ar.client.util.CommonClientUtils;

public class CatalogListUICTEx extends CatalogListUI {

	public CatalogListUICTEx() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onLoad() throws Exception {
		// TODO Auto-generated method stub
		super.onLoad();
		this.genList.setEnabled(true);
		this.genPolitical.setEnabled(true);
	}
	
	@Override
	public void actionGenList_actionPerformed(ActionEvent e) throws Exception {
		// 
		super.actionGenList_actionPerformed(e);
		ICategoryList iCategoryList = CategoryListFactory.getRemoteInstance();
		//获取选中列表
		List<String> selectedIDs = getSelectedIDs();

		//01 new
		CategoryListInfo categoryListInfo = new CategoryListInfo();
		//02 初始化赋值
		initCategoryList(selectedIDs, categoryListInfo);
		//03保存
		IObjectPK iObjectPK = iCategoryList.addnew(categoryListInfo);
		//06 打开界面
	    HashMap uiCtx = new HashMap();
	    uiCtx.put("Owner", this);
	    uiCtx.put("ID", iObjectPK.toString());
	    IUIFactory uiFactory = UIFactory.createUIFactory("com.kingdee.eas.base.uiframe.client.UIModelDialogFactory");
	    IUIWindow tempVouUpdateDlg = uiFactory.create("com.kingdee.eas.custom.tiog.bizbill.client.CategoryListEditUI", uiCtx, null, OprtState.EDIT);
	    tempVouUpdateDlg.show();

	}

	@Override
	public void actionGenPolitical_actionPerformed(ActionEvent e)
			throws Exception {
		// TODO Auto-generated method stub
		super.actionGenPolitical_actionPerformed(e);
		IPoliticalSystem iPoliticalSystem = PoliticalSystemFactory.getRemoteInstance();
		//获取选中列表
		List<String> selectedIDs = getSelectedIDs();

		//01 new
		PoliticalSystemInfo politicalSystemInfo = new PoliticalSystemInfo();
		//02 初始化赋值
		initPoliticalSystem(selectedIDs, politicalSystemInfo);
		
		//03保存
		IObjectPK iObjectPK = iPoliticalSystem.addnew(politicalSystemInfo);
		//06 打开界面
	    HashMap uiCtx = new HashMap();
	    uiCtx.put("Owner", this);
	    uiCtx.put("ID", iObjectPK.toString());
	    IUIFactory uiFactory = UIFactory.createUIFactory("com.kingdee.eas.base.uiframe.client.UIModelDialogFactory");
	    IUIWindow tempVouUpdateDlg = uiFactory.create("com.kingdee.eas.custom.tiog.bizbill.client.PoliticalSystemEditUI", uiCtx, null, OprtState.EDIT);
	    tempVouUpdateDlg.show();
	}
	
	/**
	 * 初始化事项清单*/
	public void initCategoryList(List<String> selectedIDs,
			CategoryListInfo categoryListInfo) {
		// TODO Auto-generated method stub
		UserInfo userInfo = ContextHelperFactory.getRemoteInstance().getCurrentUser();
		CompanyOrgUnitInfo company = ContextHelperFactory.getRemoteInstance().getCurrentCompany();;
		try {
			ICatalog iCatalog = CatalogFactory.getRemoteInstance();
			categoryListInfo.setCategoryListName("");
			categoryListInfo.setNumber("SXQD-20200323-");
			categoryListInfo.setCompany(company);
			categoryListInfo.setListVersion("V1.0");
			categoryListInfo.setEffectiveTS(new Date());
			categoryListInfo.setIsUpload(false);
			categoryListInfo.setState(BillState.Saved);
			
			CategoryListEntryCollection categoryListEntryColl = categoryListInfo.getEntrys();
			for(String selectedID : selectedIDs){
				CategoryListEntryInfo categoryListEntryInfo = new CategoryListEntryInfo();
				CatalogInfo catalogInfo = iCatalog.getCatalogInfo(new ObjectUuidPK(selectedID));
				categoryListEntryInfo.setCatalog(catalogInfo);
				categoryListEntryInfo.setCatalogNumber(catalogInfo.getNumber());
				categoryListEntryInfo.setLegalFlag(true);
				categoryListEntryColl.add(categoryListEntryInfo);
			}
		} catch (EASBizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BOSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 初始化决策制度*/
	public void initPoliticalSystem(List<String> selectedIDs,
			PoliticalSystemInfo politicalSystemInfo) {
		// TODO Auto-generated method stub
		UserInfo userInfo = ContextHelperFactory.getRemoteInstance().getCurrentUser();
		CompanyOrgUnitInfo company = ContextHelperFactory.getRemoteInstance().getCurrentCompany();;
		try {
			ICatalog iCatalog = CatalogFactory.getRemoteInstance();
			politicalSystemInfo.setPoliticalSystemName("");
			politicalSystemInfo.setNumber("JCZD-20200323-");
			politicalSystemInfo.setCompany(company);
			politicalSystemInfo.setEffectDate(new Date());
			politicalSystemInfo.setIsUpload(false);
			politicalSystemInfo.setState(BillState.Saved);
			
			PoliticalSystemEntryCollection politicalSystemEntryColl = politicalSystemInfo.getEntrys();
			for(String selectedID : selectedIDs){
				PoliticalSystemEntryInfo politicalSystemEntryInfo = new PoliticalSystemEntryInfo();
				CatalogInfo catalogInfo = iCatalog.getCatalogInfo(new ObjectUuidPK(selectedID));
				politicalSystemEntryInfo.setCatalog(catalogInfo);
				politicalSystemEntryInfo.setCatalogNum(catalogInfo.getNumber());
			}
		} catch (EASBizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BOSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private List<String> getSelectedIDs() {
		// TODO Auto-generated method stub
		List selectedIDs = null;
		int[] selectIds = KDTableUtil.getSelectedRows(this.tblMain);
		if (selectIds.length < 0) {
			MsgBox.showError(this, "请选择单据！");
			SysUtil.abort();
		} else {
			selectedIDs = new ArrayList();
		}
		for (int i = 0; i < selectIds.length; i++) {
			String id = tblMain.getRow(selectIds[i]).getCell("id").getValue()
					.toString();
			if (null != id && !"".equals(id))
				selectedIDs.add(id);
		}

		return selectedIDs;
	}

}
