package com.kingdee.eas.custom.tiog.basedata.client;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.ui.face.IUIFactory;
import com.kingdee.bos.ui.face.IUIWindow;
import com.kingdee.bos.ui.face.UIFactory;
import com.kingdee.eas.base.codingrule.CodingRuleManagerFactory;
import com.kingdee.eas.base.codingrule.ICodingRuleManager;
import com.kingdee.eas.base.core.fm.ContextHelperFactory;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.basedata.org.AdminOrgUnitFactory;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.basedata.org.CompanyOrgUnitInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.common.client.OprtState;
import com.kingdee.eas.custom.tiog.basedata.CatalogFactory;
import com.kingdee.eas.custom.tiog.basedata.CatalogInfo;
import com.kingdee.eas.custom.tiog.basedata.CatalogMainFactory;
import com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo;
import com.kingdee.eas.custom.tiog.basedata.ICatalog;
import com.kingdee.eas.custom.tiog.basedata.ICatalogMain;
import com.kingdee.eas.custom.tiog.bizbill.BillState;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListFactory;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.eas.custom.tiog.bizbill.ICategoryList;
import com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemFactory;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.eas.util.SysUtil;
import com.kingdee.eas.util.client.KDTableUtil;
import com.kingdee.eas.util.client.MsgBox;

public class CatalogMainListUICTEx extends CatalogMainListUI{

	public CatalogMainListUICTEx() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onLoad() throws Exception {
		// TODO Auto-generated method stub
		super.onLoad();
		this.genList.setEnabled(true);
		this.genPolitical.setEnabled(true);
	}
	
	@Override
	protected String getEditUIModal() {
	    return "com.kingdee.eas.base.uiframe.client.UINewTabFactory";
	}
	
	@Override
	public void actionGenList_actionPerformed(ActionEvent e) throws Exception {
		// 
		super.actionGenList_actionPerformed(e);
		ICategoryList iCategoryList = CategoryListFactory.getRemoteInstance();
		//获取选中列表
		List<String> selectedIDs = getSelectedIDs();

		//01 new
		CategoryListInfo categoryListInfo = new CategoryListInfo();
		//02 初始化赋值
		initCategoryList(selectedIDs, categoryListInfo);
		//03保存
		IObjectPK iObjectPK = iCategoryList.addnew(categoryListInfo);
		//06 打开界面
	    HashMap uiCtx = new HashMap();
	    uiCtx.put("Owner", this);
	    //uiCtx.put("canResize", this);
	    uiCtx.put("ID", iObjectPK.toString());
	    //com.kingdee.bos.ui.face.UIInternalFrameFactory
	    IUIFactory uiFactory = UIFactory.createUIFactory("com.kingdee.eas.base.uiframe.client.UIModelDialogFactory");
	    //IUIFactory uiFactory = UIFactory.createUIFactory("com.kingdee.bos.ui.face.UIInternalFrameFactory");
	    IUIWindow tempVouUpdateDlg = uiFactory.create("com.kingdee.eas.custom.tiog.bizbill.client.CategoryListEditUI", uiCtx, null, OprtState.EDIT);
	    tempVouUpdateDlg.show();

	}

	@Override
	public void actionGenPolitical_actionPerformed(ActionEvent e)
			throws Exception {
		// TODO Auto-generated method stub
		super.actionGenPolitical_actionPerformed(e);
		IPoliticalSystem iPoliticalSystem = PoliticalSystemFactory.getRemoteInstance();
		//获取选中列表
		List<String> selectedIDs = getSelectedIDs();

		//01 new
		PoliticalSystemInfo politicalSystemInfo = new PoliticalSystemInfo();
		//02 初始化赋值
		initPoliticalSystem(selectedIDs, politicalSystemInfo);
		
		//03保存
		IObjectPK iObjectPK = iPoliticalSystem.addnew(politicalSystemInfo);
		//06 打开界面
	    HashMap uiCtx = new HashMap();
	    uiCtx.put("Owner", this);
	    //uiCtx.put("canResize", this);
	    uiCtx.put("ID", iObjectPK.toString());
	    IUIFactory uiFactory = UIFactory.createUIFactory("com.kingdee.eas.base.uiframe.client.UIModelDialogFactory");
	    IUIWindow tempVouUpdateDlg = uiFactory.create("com.kingdee.eas.custom.tiog.bizbill.client.PoliticalSystemEditUI", uiCtx, null, OprtState.EDIT);
	    tempVouUpdateDlg.show();
	}
	
	/**
	 * 初始化事项清单*/
	public void initCategoryList(List<String> selectedIDs,
			CategoryListInfo categoryListInfo) {
		// TODO Auto-generated method stub
		UserInfo userInfo = ContextHelperFactory.getRemoteInstance().getCurrentUser();
		CompanyOrgUnitInfo company = ContextHelperFactory.getRemoteInstance().getCurrentCompany();
		try {
			AdminOrgUnitInfo adminOrgUnitInfo = AdminOrgUnitFactory.getRemoteInstance()
				.getAdminOrgUnitInfo(new ObjectUuidPK(company.getId().toString()));
			ICatalogMain iCatalogMain = CatalogMainFactory.getRemoteInstance();
			categoryListInfo.setAdminOrgUnit(adminOrgUnitInfo);
			categoryListInfo.setCategoryListName("");
			categoryListInfo.setNumber("SXQD-20200323-");
                        ICodingRuleManager iCodingRuleManager = CodingRuleManagerFactory.getRemoteInstance();    
                        if (iCodingRuleManager.isExist(categoryListInfo, company.getId().toString())) {    
                            categoryListInfo.setString("number", iCodingRuleManager.getNumber(categoryListInfo, company.getId().toString()));    
                        }   
			categoryListInfo.setCompany(company);
			categoryListInfo.setCompanyNumber(company.getNumber());
			categoryListInfo.setBizDate(new Date());
			categoryListInfo.setListVersion("V1.0");
			categoryListInfo.setEffectiveTS(new Date());
			categoryListInfo.setIsUpload(false);
			categoryListInfo.setState(BillState.Saved);
			
			CategoryListEntryCollection categoryListEntryColl = categoryListInfo.getEntrys();
			for(String selectedID : selectedIDs){
				CategoryListEntryInfo categoryListEntryInfo = new CategoryListEntryInfo();
				CatalogMainInfo catalogMainInfo = iCatalogMain.getCatalogMainInfo(new ObjectUuidPK(selectedID));
				categoryListEntryInfo.setCatalogMain(catalogMainInfo);
				categoryListEntryInfo.setCatalogMainNumber(catalogMainInfo.getNumber());
				categoryListEntryInfo.setLegalFlag(true);
				categoryListEntryColl.add(categoryListEntryInfo);
			}
		} catch (EASBizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BOSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 初始化决策制度*/
	public void initPoliticalSystem(List<String> selectedIDs,
			PoliticalSystemInfo politicalSystemInfo) {
		// TODO Auto-generated method stub
		UserInfo userInfo = ContextHelperFactory.getRemoteInstance().getCurrentUser();
		CompanyOrgUnitInfo company = ContextHelperFactory.getRemoteInstance().getCurrentCompany();;
		try {
			ICatalogMain iCatalogMain = CatalogMainFactory.getRemoteInstance();
			AdminOrgUnitInfo adminOrgUnitInfo = AdminOrgUnitFactory.getRemoteInstance()
				.getAdminOrgUnitInfo(new ObjectUuidPK(company.getId().toString()));
			politicalSystemInfo.setAdminOrgUnit(adminOrgUnitInfo);
			politicalSystemInfo.setPoliticalSystemName("");
//			politicalSystemInfo.setNumber("JCZD-20200323-");
			politicalSystemInfo.setCompany(company);
			politicalSystemInfo.setEffectDate(new Date());
			politicalSystemInfo.setIsUpload(false);
			politicalSystemInfo.setState(BillState.Saved);
			
			PoliticalSystemEntryCollection politicalSystemEntryColl = politicalSystemInfo.getEntrys();
			for(String selectedID : selectedIDs){
				PoliticalSystemEntryInfo politicalSystemEntryInfo = new PoliticalSystemEntryInfo();
				CatalogMainInfo catalogMainInfo = iCatalogMain.getCatalogMainInfo(new ObjectUuidPK(selectedID));
				politicalSystemEntryInfo.setCatalogMain(catalogMainInfo);
				politicalSystemEntryInfo.setCatalogMainNumber(catalogMainInfo.getNumber());
				politicalSystemEntryColl.add(politicalSystemEntryInfo);
			}
		} catch (EASBizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BOSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private List<String> getSelectedIDs() {
		// TODO Auto-generated method stub
		List selectedIDs = null;
		int[] selectIds = KDTableUtil.getSelectedRows(this.tblMain);
		if (selectIds.length < 0) {
			MsgBox.showError(this, "请选择单据！");
			SysUtil.abort();
		} else {
			selectedIDs = new ArrayList();
		}
		for (int i = 0; i < selectIds.length; i++) {
			String id = tblMain.getRow(selectIds[i]).getCell("id").getValue()
					.toString();
			if (null != id && !"".equals(id))
				selectedIDs.add(id);
		}

		return selectedIDs;
	}

}
