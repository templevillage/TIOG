package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class CatalogMainTreeCollection extends AbstractObjectCollection 
{
    public CatalogMainTreeCollection()
    {
        super(CatalogMainTreeInfo.class);
    }
    public boolean add(CatalogMainTreeInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(CatalogMainTreeCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(CatalogMainTreeInfo item)
    {
        return removeObject(item);
    }
    public CatalogMainTreeInfo get(int index)
    {
        return(CatalogMainTreeInfo)getObject(index);
    }
    public CatalogMainTreeInfo get(Object key)
    {
        return(CatalogMainTreeInfo)getObject(key);
    }
    public void set(int index, CatalogMainTreeInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(CatalogMainTreeInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(CatalogMainTreeInfo item)
    {
        return super.indexOf(item);
    }
}