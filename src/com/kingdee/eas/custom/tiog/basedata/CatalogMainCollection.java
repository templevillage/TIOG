package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class CatalogMainCollection extends AbstractObjectCollection 
{
    public CatalogMainCollection()
    {
        super(CatalogMainInfo.class);
    }
    public boolean add(CatalogMainInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(CatalogMainCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(CatalogMainInfo item)
    {
        return removeObject(item);
    }
    public CatalogMainInfo get(int index)
    {
        return(CatalogMainInfo)getObject(index);
    }
    public CatalogMainInfo get(Object key)
    {
        return(CatalogMainInfo)getObject(key);
    }
    public void set(int index, CatalogMainInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(CatalogMainInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(CatalogMainInfo item)
    {
        return super.indexOf(item);
    }
}