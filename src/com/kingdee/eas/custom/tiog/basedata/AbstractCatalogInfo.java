package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractCatalogInfo extends com.kingdee.eas.framework.TreeBaseInfo implements Serializable 
{
    public AbstractCatalogInfo()
    {
        this("id");
    }
    protected AbstractCatalogInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 事项登记 's 父节点 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.basedata.CatalogInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 事项登记 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    /**
     * Object: 事项登记 's 默认表决方式 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.VoteModeInfo getVoteMode()
    {
        return (com.kingdee.eas.custom.tiog.basedata.VoteModeInfo)get("voteMode");
    }
    public void setVoteMode(com.kingdee.eas.custom.tiog.basedata.VoteModeInfo item)
    {
        put("voteMode", item);
    }
    /**
     * Object: 事项登记 's 事项状态 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogStatusInfo getStatus()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogStatusInfo)get("status");
    }
    public void setStatus(com.kingdee.eas.custom.tiog.basedata.CatalogStatusInfo item)
    {
        put("status", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("02BD1102");
    }
}