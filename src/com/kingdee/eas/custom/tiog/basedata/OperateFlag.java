package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.tiog.basedata.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class OperateFlag extends DataBase implements IOperateFlag
{
    public OperateFlag()
    {
        super();
        registerInterface(IOperateFlag.class, this);
    }
    public OperateFlag(Context ctx)
    {
        super(ctx);
        registerInterface(IOperateFlag.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("8F892F79");
    }
    private OperateFlagController getController() throws BOSException
    {
        return (OperateFlagController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public OperateFlagInfo getOperateFlagInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getOperateFlagInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public OperateFlagInfo getOperateFlagInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getOperateFlagInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public OperateFlagInfo getOperateFlagInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getOperateFlagInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public OperateFlagCollection getOperateFlagCollection() throws BOSException
    {
        try {
            return getController().getOperateFlagCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public OperateFlagCollection getOperateFlagCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getOperateFlagCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public OperateFlagCollection getOperateFlagCollection(String oql) throws BOSException
    {
        try {
            return getController().getOperateFlagCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}