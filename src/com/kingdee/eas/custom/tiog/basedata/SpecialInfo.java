package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class SpecialInfo extends AbstractSpecialInfo implements Serializable 
{
    public SpecialInfo()
    {
        super();
    }
    protected SpecialInfo(String pkField)
    {
        super(pkField);
    }
}