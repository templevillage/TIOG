package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractVoteModeInfo extends com.kingdee.eas.framework.DataBaseInfo implements Serializable 
{
    public AbstractVoteModeInfo()
    {
        this("id");
    }
    protected AbstractVoteModeInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 表决方式 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("EF5CE7E4");
    }
}