package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class CatalogCollection extends AbstractObjectCollection 
{
    public CatalogCollection()
    {
        super(CatalogInfo.class);
    }
    public boolean add(CatalogInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(CatalogCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(CatalogInfo item)
    {
        return removeObject(item);
    }
    public CatalogInfo get(int index)
    {
        return(CatalogInfo)getObject(index);
    }
    public CatalogInfo get(Object key)
    {
        return(CatalogInfo)getObject(key);
    }
    public void set(int index, CatalogInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(CatalogInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(CatalogInfo item)
    {
        return super.indexOf(item);
    }
}