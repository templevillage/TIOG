package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class CatalogStatusCollection extends AbstractObjectCollection 
{
    public CatalogStatusCollection()
    {
        super(CatalogStatusInfo.class);
    }
    public boolean add(CatalogStatusInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(CatalogStatusCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(CatalogStatusInfo item)
    {
        return removeObject(item);
    }
    public CatalogStatusInfo get(int index)
    {
        return(CatalogStatusInfo)getObject(index);
    }
    public CatalogStatusInfo get(Object key)
    {
        return(CatalogStatusInfo)getObject(key);
    }
    public void set(int index, CatalogStatusInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(CatalogStatusInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(CatalogStatusInfo item)
    {
        return super.indexOf(item);
    }
}