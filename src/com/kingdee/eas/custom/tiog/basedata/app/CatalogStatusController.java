package com.kingdee.eas.custom.tiog.basedata.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.tiog.basedata.CatalogStatusInfo;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.custom.tiog.basedata.CatalogStatusCollection;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.framework.app.DataBaseController;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface CatalogStatusController extends DataBaseController
{
    public CatalogStatusInfo getCatalogStatusInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public CatalogStatusInfo getCatalogStatusInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public CatalogStatusInfo getCatalogStatusInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public CatalogStatusCollection getCatalogStatusCollection(Context ctx) throws BOSException, RemoteException;
    public CatalogStatusCollection getCatalogStatusCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public CatalogStatusCollection getCatalogStatusCollection(Context ctx, String oql) throws BOSException, RemoteException;
}