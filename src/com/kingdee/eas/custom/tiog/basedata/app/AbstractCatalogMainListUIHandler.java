/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.basedata.app;

import com.kingdee.bos.Context;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.eas.framework.batchHandler.ResponseContext;


/**
 * output class name
 */
public abstract class AbstractCatalogMainListUIHandler extends com.kingdee.eas.framework.app.TreeDetailListUIHandler

{
	public void handleActionGenPolitical(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionGenPolitical(request,response,context);
	}
	protected void _handleActionGenPolitical(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionGenList(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionGenList(request,response,context);
	}
	protected void _handleActionGenList(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
}