package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class UpReportStatusFactory
{
    private UpReportStatusFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.IUpReportStatus getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IUpReportStatus)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("9FF8D378") ,com.kingdee.eas.custom.tiog.basedata.IUpReportStatus.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.IUpReportStatus getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IUpReportStatus)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("9FF8D378") ,com.kingdee.eas.custom.tiog.basedata.IUpReportStatus.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.IUpReportStatus getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IUpReportStatus)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("9FF8D378"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.IUpReportStatus getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IUpReportStatus)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("9FF8D378"));
    }
}