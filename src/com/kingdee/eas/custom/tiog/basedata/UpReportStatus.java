package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.tiog.basedata.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class UpReportStatus extends DataBase implements IUpReportStatus
{
    public UpReportStatus()
    {
        super();
        registerInterface(IUpReportStatus.class, this);
    }
    public UpReportStatus(Context ctx)
    {
        super(ctx);
        registerInterface(IUpReportStatus.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("9FF8D378");
    }
    private UpReportStatusController getController() throws BOSException
    {
        return (UpReportStatusController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public UpReportStatusInfo getUpReportStatusInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getUpReportStatusInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public UpReportStatusInfo getUpReportStatusInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getUpReportStatusInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public UpReportStatusInfo getUpReportStatusInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getUpReportStatusInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public UpReportStatusCollection getUpReportStatusCollection() throws BOSException
    {
        try {
            return getController().getUpReportStatusCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public UpReportStatusCollection getUpReportStatusCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getUpReportStatusCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public UpReportStatusCollection getUpReportStatusCollection(String oql) throws BOSException
    {
        try {
            return getController().getUpReportStatusCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}