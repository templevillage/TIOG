package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractCatalogMainTreeInfo extends com.kingdee.eas.framework.TreeBaseInfo implements Serializable 
{
    public AbstractCatalogMainTreeInfo()
    {
        this("id");
    }
    protected AbstractCatalogMainTreeInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 分类事项组别 's 父节点 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogMainTreeInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogMainTreeInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.basedata.CatalogMainTreeInfo item)
    {
        put("parent", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("ACFEBC19");
    }
}