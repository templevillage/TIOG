package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingTypeFactory
{
    private MeetingTypeFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.IMeetingType getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IMeetingType)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("5371CC7E") ,com.kingdee.eas.custom.tiog.basedata.IMeetingType.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.IMeetingType getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IMeetingType)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("5371CC7E") ,com.kingdee.eas.custom.tiog.basedata.IMeetingType.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.IMeetingType getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IMeetingType)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("5371CC7E"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.IMeetingType getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IMeetingType)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("5371CC7E"));
    }
}