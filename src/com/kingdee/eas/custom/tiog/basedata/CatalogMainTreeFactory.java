package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class CatalogMainTreeFactory
{
    private CatalogMainTreeFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogMainTree getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogMainTree)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("ACFEBC19") ,com.kingdee.eas.custom.tiog.basedata.ICatalogMainTree.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogMainTree getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogMainTree)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("ACFEBC19") ,com.kingdee.eas.custom.tiog.basedata.ICatalogMainTree.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogMainTree getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogMainTree)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("ACFEBC19"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogMainTree getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogMainTree)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("ACFEBC19"));
    }
}