package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class CatalogStatusInfo extends AbstractCatalogStatusInfo implements Serializable 
{
    public CatalogStatusInfo()
    {
        super();
    }
    protected CatalogStatusInfo(String pkField)
    {
        super(pkField);
    }
}