package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingFormFactory
{
    private MeetingFormFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.IMeetingForm getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IMeetingForm)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("536B4A08") ,com.kingdee.eas.custom.tiog.basedata.IMeetingForm.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.IMeetingForm getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IMeetingForm)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("536B4A08") ,com.kingdee.eas.custom.tiog.basedata.IMeetingForm.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.IMeetingForm getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IMeetingForm)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("536B4A08"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.IMeetingForm getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IMeetingForm)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("536B4A08"));
    }
}