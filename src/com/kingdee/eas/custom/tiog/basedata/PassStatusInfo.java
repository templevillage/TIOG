package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class PassStatusInfo extends AbstractPassStatusInfo implements Serializable 
{
    public PassStatusInfo()
    {
        super();
    }
    protected PassStatusInfo(String pkField)
    {
        super(pkField);
    }
}