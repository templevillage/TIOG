package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class VoteModeCollection extends AbstractObjectCollection 
{
    public VoteModeCollection()
    {
        super(VoteModeInfo.class);
    }
    public boolean add(VoteModeInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(VoteModeCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(VoteModeInfo item)
    {
        return removeObject(item);
    }
    public VoteModeInfo get(int index)
    {
        return(VoteModeInfo)getObject(index);
    }
    public VoteModeInfo get(Object key)
    {
        return(VoteModeInfo)getObject(key);
    }
    public void set(int index, VoteModeInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(VoteModeInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(VoteModeInfo item)
    {
        return super.indexOf(item);
    }
}