package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.tiog.basedata.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class CatalogStatus extends DataBase implements ICatalogStatus
{
    public CatalogStatus()
    {
        super();
        registerInterface(ICatalogStatus.class, this);
    }
    public CatalogStatus(Context ctx)
    {
        super(ctx);
        registerInterface(ICatalogStatus.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("88EB8394");
    }
    private CatalogStatusController getController() throws BOSException
    {
        return (CatalogStatusController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public CatalogStatusInfo getCatalogStatusInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getCatalogStatusInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public CatalogStatusInfo getCatalogStatusInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getCatalogStatusInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public CatalogStatusInfo getCatalogStatusInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getCatalogStatusInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public CatalogStatusCollection getCatalogStatusCollection() throws BOSException
    {
        try {
            return getController().getCatalogStatusCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public CatalogStatusCollection getCatalogStatusCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getCatalogStatusCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public CatalogStatusCollection getCatalogStatusCollection(String oql) throws BOSException
    {
        try {
            return getController().getCatalogStatusCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}