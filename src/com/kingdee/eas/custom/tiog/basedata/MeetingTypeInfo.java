package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class MeetingTypeInfo extends AbstractMeetingTypeInfo implements Serializable 
{
    public MeetingTypeInfo()
    {
        super();
    }
    protected MeetingTypeInfo(String pkField)
    {
        super(pkField);
    }
}