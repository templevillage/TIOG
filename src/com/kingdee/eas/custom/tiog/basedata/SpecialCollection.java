package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class SpecialCollection extends AbstractObjectCollection 
{
    public SpecialCollection()
    {
        super(SpecialInfo.class);
    }
    public boolean add(SpecialInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(SpecialCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(SpecialInfo item)
    {
        return removeObject(item);
    }
    public SpecialInfo get(int index)
    {
        return(SpecialInfo)getObject(index);
    }
    public SpecialInfo get(Object key)
    {
        return(SpecialInfo)getObject(key);
    }
    public void set(int index, SpecialInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(SpecialInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(SpecialInfo item)
    {
        return super.indexOf(item);
    }
}