package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class ExecuteStatusCollection extends AbstractObjectCollection 
{
    public ExecuteStatusCollection()
    {
        super(ExecuteStatusInfo.class);
    }
    public boolean add(ExecuteStatusInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(ExecuteStatusCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(ExecuteStatusInfo item)
    {
        return removeObject(item);
    }
    public ExecuteStatusInfo get(int index)
    {
        return(ExecuteStatusInfo)getObject(index);
    }
    public ExecuteStatusInfo get(Object key)
    {
        return(ExecuteStatusInfo)getObject(key);
    }
    public void set(int index, ExecuteStatusInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(ExecuteStatusInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(ExecuteStatusInfo item)
    {
        return super.indexOf(item);
    }
}