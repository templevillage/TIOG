package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class UpReportStatusCollection extends AbstractObjectCollection 
{
    public UpReportStatusCollection()
    {
        super(UpReportStatusInfo.class);
    }
    public boolean add(UpReportStatusInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(UpReportStatusCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(UpReportStatusInfo item)
    {
        return removeObject(item);
    }
    public UpReportStatusInfo get(int index)
    {
        return(UpReportStatusInfo)getObject(index);
    }
    public UpReportStatusInfo get(Object key)
    {
        return(UpReportStatusInfo)getObject(key);
    }
    public void set(int index, UpReportStatusInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(UpReportStatusInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(UpReportStatusInfo item)
    {
        return super.indexOf(item);
    }
}