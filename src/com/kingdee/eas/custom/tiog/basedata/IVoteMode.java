package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IVoteMode extends IDataBase
{
    public VoteModeInfo getVoteModeInfo(IObjectPK pk) throws BOSException, EASBizException;
    public VoteModeInfo getVoteModeInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public VoteModeInfo getVoteModeInfo(String oql) throws BOSException, EASBizException;
    public VoteModeCollection getVoteModeCollection() throws BOSException;
    public VoteModeCollection getVoteModeCollection(EntityViewInfo view) throws BOSException;
    public VoteModeCollection getVoteModeCollection(String oql) throws BOSException;
}