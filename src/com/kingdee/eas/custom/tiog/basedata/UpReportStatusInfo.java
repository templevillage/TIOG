package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class UpReportStatusInfo extends AbstractUpReportStatusInfo implements Serializable 
{
    public UpReportStatusInfo()
    {
        super();
    }
    protected UpReportStatusInfo(String pkField)
    {
        super(pkField);
    }
}