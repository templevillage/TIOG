package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class VoteModeFactory
{
    private VoteModeFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.IVoteMode getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IVoteMode)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("EF5CE7E4") ,com.kingdee.eas.custom.tiog.basedata.IVoteMode.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.IVoteMode getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IVoteMode)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("EF5CE7E4") ,com.kingdee.eas.custom.tiog.basedata.IVoteMode.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.IVoteMode getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IVoteMode)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("EF5CE7E4"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.IVoteMode getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IVoteMode)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("EF5CE7E4"));
    }
}