package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class PassStatusFactory
{
    private PassStatusFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.IPassStatus getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IPassStatus)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("3D8449DA") ,com.kingdee.eas.custom.tiog.basedata.IPassStatus.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.IPassStatus getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IPassStatus)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("3D8449DA") ,com.kingdee.eas.custom.tiog.basedata.IPassStatus.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.IPassStatus getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IPassStatus)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("3D8449DA"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.IPassStatus getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IPassStatus)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("3D8449DA"));
    }
}