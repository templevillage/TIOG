package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingFormCollection extends AbstractObjectCollection 
{
    public MeetingFormCollection()
    {
        super(MeetingFormInfo.class);
    }
    public boolean add(MeetingFormInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingFormCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingFormInfo item)
    {
        return removeObject(item);
    }
    public MeetingFormInfo get(int index)
    {
        return(MeetingFormInfo)getObject(index);
    }
    public MeetingFormInfo get(Object key)
    {
        return(MeetingFormInfo)getObject(key);
    }
    public void set(int index, MeetingFormInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingFormInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingFormInfo item)
    {
        return super.indexOf(item);
    }
}