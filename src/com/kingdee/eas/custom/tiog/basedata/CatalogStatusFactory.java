package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class CatalogStatusFactory
{
    private CatalogStatusFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogStatus getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogStatus)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("88EB8394") ,com.kingdee.eas.custom.tiog.basedata.ICatalogStatus.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogStatus getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogStatus)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("88EB8394") ,com.kingdee.eas.custom.tiog.basedata.ICatalogStatus.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogStatus getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogStatus)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("88EB8394"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogStatus getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogStatus)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("88EB8394"));
    }
}