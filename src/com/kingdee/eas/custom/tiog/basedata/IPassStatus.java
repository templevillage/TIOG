package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IPassStatus extends IDataBase
{
    public PassStatusInfo getPassStatusInfo(IObjectPK pk) throws BOSException, EASBizException;
    public PassStatusInfo getPassStatusInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public PassStatusInfo getPassStatusInfo(String oql) throws BOSException, EASBizException;
    public PassStatusCollection getPassStatusCollection() throws BOSException;
    public PassStatusCollection getPassStatusCollection(EntityViewInfo view) throws BOSException;
    public PassStatusCollection getPassStatusCollection(String oql) throws BOSException;
}