package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingTypeCollection extends AbstractObjectCollection 
{
    public MeetingTypeCollection()
    {
        super(MeetingTypeInfo.class);
    }
    public boolean add(MeetingTypeInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingTypeCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingTypeInfo item)
    {
        return removeObject(item);
    }
    public MeetingTypeInfo get(int index)
    {
        return(MeetingTypeInfo)getObject(index);
    }
    public MeetingTypeInfo get(Object key)
    {
        return(MeetingTypeInfo)getObject(key);
    }
    public void set(int index, MeetingTypeInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingTypeInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingTypeInfo item)
    {
        return super.indexOf(item);
    }
}