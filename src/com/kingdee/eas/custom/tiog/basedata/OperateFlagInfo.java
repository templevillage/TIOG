package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class OperateFlagInfo extends AbstractOperateFlagInfo implements Serializable 
{
    public OperateFlagInfo()
    {
        super();
    }
    protected OperateFlagInfo(String pkField)
    {
        super(pkField);
    }
}