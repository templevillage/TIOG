package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractUpReportStatusInfo extends com.kingdee.eas.framework.DataBaseInfo implements Serializable 
{
    public AbstractUpReportStatusInfo()
    {
        this("id");
    }
    protected AbstractUpReportStatusInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 是否需报国资委审批 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("9FF8D378");
    }
}