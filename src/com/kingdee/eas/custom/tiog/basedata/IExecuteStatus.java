package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IExecuteStatus extends IDataBase
{
    public ExecuteStatusInfo getExecuteStatusInfo(IObjectPK pk) throws BOSException, EASBizException;
    public ExecuteStatusInfo getExecuteStatusInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public ExecuteStatusInfo getExecuteStatusInfo(String oql) throws BOSException, EASBizException;
    public ExecuteStatusCollection getExecuteStatusCollection() throws BOSException;
    public ExecuteStatusCollection getExecuteStatusCollection(EntityViewInfo view) throws BOSException;
    public ExecuteStatusCollection getExecuteStatusCollection(String oql) throws BOSException;
}