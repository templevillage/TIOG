package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class ExecuteStatusFactory
{
    private ExecuteStatusFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.IExecuteStatus getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IExecuteStatus)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("1A1525F0") ,com.kingdee.eas.custom.tiog.basedata.IExecuteStatus.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.IExecuteStatus getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IExecuteStatus)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("1A1525F0") ,com.kingdee.eas.custom.tiog.basedata.IExecuteStatus.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.IExecuteStatus getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IExecuteStatus)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("1A1525F0"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.IExecuteStatus getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IExecuteStatus)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("1A1525F0"));
    }
}