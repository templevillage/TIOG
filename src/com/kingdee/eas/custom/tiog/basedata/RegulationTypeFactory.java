package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class RegulationTypeFactory
{
    private RegulationTypeFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.IRegulationType getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IRegulationType)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("770330FB") ,com.kingdee.eas.custom.tiog.basedata.IRegulationType.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.IRegulationType getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IRegulationType)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("770330FB") ,com.kingdee.eas.custom.tiog.basedata.IRegulationType.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.IRegulationType getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IRegulationType)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("770330FB"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.IRegulationType getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IRegulationType)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("770330FB"));
    }
}