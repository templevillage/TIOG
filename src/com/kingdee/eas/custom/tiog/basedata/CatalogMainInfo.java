package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class CatalogMainInfo extends AbstractCatalogMainInfo implements Serializable 
{
    public CatalogMainInfo()
    {
        super();
    }
    protected CatalogMainInfo(String pkField)
    {
        super(pkField);
    }
}