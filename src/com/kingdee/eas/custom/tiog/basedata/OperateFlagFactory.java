package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class OperateFlagFactory
{
    private OperateFlagFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.IOperateFlag getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IOperateFlag)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("8F892F79") ,com.kingdee.eas.custom.tiog.basedata.IOperateFlag.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.IOperateFlag getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IOperateFlag)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("8F892F79") ,com.kingdee.eas.custom.tiog.basedata.IOperateFlag.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.IOperateFlag getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IOperateFlag)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("8F892F79"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.IOperateFlag getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IOperateFlag)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("8F892F79"));
    }
}