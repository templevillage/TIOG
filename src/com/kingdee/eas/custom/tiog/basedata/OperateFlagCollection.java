package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class OperateFlagCollection extends AbstractObjectCollection 
{
    public OperateFlagCollection()
    {
        super(OperateFlagInfo.class);
    }
    public boolean add(OperateFlagInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(OperateFlagCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(OperateFlagInfo item)
    {
        return removeObject(item);
    }
    public OperateFlagInfo get(int index)
    {
        return(OperateFlagInfo)getObject(index);
    }
    public OperateFlagInfo get(Object key)
    {
        return(OperateFlagInfo)getObject(key);
    }
    public void set(int index, OperateFlagInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(OperateFlagInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(OperateFlagInfo item)
    {
        return super.indexOf(item);
    }
}