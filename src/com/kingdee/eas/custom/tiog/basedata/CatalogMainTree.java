package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.tiog.basedata.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.eas.framework.ITreeBase;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.framework.TreeBase;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class CatalogMainTree extends TreeBase implements ICatalogMainTree
{
    public CatalogMainTree()
    {
        super();
        registerInterface(ICatalogMainTree.class, this);
    }
    public CatalogMainTree(Context ctx)
    {
        super(ctx);
        registerInterface(ICatalogMainTree.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("ACFEBC19");
    }
    private CatalogMainTreeController getController() throws BOSException
    {
        return (CatalogMainTreeController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public CatalogMainTreeInfo getCatalogMainTreeInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getCatalogMainTreeInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public CatalogMainTreeInfo getCatalogMainTreeInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getCatalogMainTreeInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public CatalogMainTreeInfo getCatalogMainTreeInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getCatalogMainTreeInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public CatalogMainTreeCollection getCatalogMainTreeCollection() throws BOSException
    {
        try {
            return getController().getCatalogMainTreeCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public CatalogMainTreeCollection getCatalogMainTreeCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getCatalogMainTreeCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public CatalogMainTreeCollection getCatalogMainTreeCollection(String oql) throws BOSException
    {
        try {
            return getController().getCatalogMainTreeCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}