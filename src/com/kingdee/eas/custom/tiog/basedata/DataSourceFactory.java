package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class DataSourceFactory
{
    private DataSourceFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.IDataSource getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IDataSource)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("798696DC") ,com.kingdee.eas.custom.tiog.basedata.IDataSource.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.IDataSource getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IDataSource)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("798696DC") ,com.kingdee.eas.custom.tiog.basedata.IDataSource.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.IDataSource getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IDataSource)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("798696DC"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.IDataSource getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IDataSource)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("798696DC"));
    }
}