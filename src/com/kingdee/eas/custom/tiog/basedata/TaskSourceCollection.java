package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class TaskSourceCollection extends AbstractObjectCollection 
{
    public TaskSourceCollection()
    {
        super(TaskSourceInfo.class);
    }
    public boolean add(TaskSourceInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(TaskSourceCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(TaskSourceInfo item)
    {
        return removeObject(item);
    }
    public TaskSourceInfo get(int index)
    {
        return(TaskSourceInfo)getObject(index);
    }
    public TaskSourceInfo get(Object key)
    {
        return(TaskSourceInfo)getObject(key);
    }
    public void set(int index, TaskSourceInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(TaskSourceInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(TaskSourceInfo item)
    {
        return super.indexOf(item);
    }
}