package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface ITaskSource extends IDataBase
{
    public TaskSourceInfo getTaskSourceInfo(IObjectPK pk) throws BOSException, EASBizException;
    public TaskSourceInfo getTaskSourceInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public TaskSourceInfo getTaskSourceInfo(String oql) throws BOSException, EASBizException;
    public TaskSourceCollection getTaskSourceCollection() throws BOSException;
    public TaskSourceCollection getTaskSourceCollection(EntityViewInfo view) throws BOSException;
    public TaskSourceCollection getTaskSourceCollection(String oql) throws BOSException;
}