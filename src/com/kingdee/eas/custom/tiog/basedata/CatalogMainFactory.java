package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class CatalogMainFactory
{
    private CatalogMainFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogMain getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogMain)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("4C0D39DB") ,com.kingdee.eas.custom.tiog.basedata.ICatalogMain.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogMain getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogMain)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("4C0D39DB") ,com.kingdee.eas.custom.tiog.basedata.ICatalogMain.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogMain getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogMain)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("4C0D39DB"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.ICatalogMain getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ICatalogMain)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("4C0D39DB"));
    }
}