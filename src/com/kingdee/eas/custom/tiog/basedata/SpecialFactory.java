package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class SpecialFactory
{
    private SpecialFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.ISpecial getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ISpecial)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("69E6F1A2") ,com.kingdee.eas.custom.tiog.basedata.ISpecial.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.ISpecial getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ISpecial)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("69E6F1A2") ,com.kingdee.eas.custom.tiog.basedata.ISpecial.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.ISpecial getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ISpecial)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("69E6F1A2"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.ISpecial getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.ISpecial)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("69E6F1A2"));
    }
}