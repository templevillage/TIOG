package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class RegulationTypeCollection extends AbstractObjectCollection 
{
    public RegulationTypeCollection()
    {
        super(RegulationTypeInfo.class);
    }
    public boolean add(RegulationTypeInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(RegulationTypeCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(RegulationTypeInfo item)
    {
        return removeObject(item);
    }
    public RegulationTypeInfo get(int index)
    {
        return(RegulationTypeInfo)getObject(index);
    }
    public RegulationTypeInfo get(Object key)
    {
        return(RegulationTypeInfo)getObject(key);
    }
    public void set(int index, RegulationTypeInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(RegulationTypeInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(RegulationTypeInfo item)
    {
        return super.indexOf(item);
    }
}