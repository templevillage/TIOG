package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.eas.framework.ITreeBase;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface ICatalog extends ITreeBase
{
    public CatalogInfo getCatalogInfo(IObjectPK pk) throws BOSException, EASBizException;
    public CatalogInfo getCatalogInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public CatalogInfo getCatalogInfo(String oql) throws BOSException, EASBizException;
    public CatalogCollection getCatalogCollection() throws BOSException;
    public CatalogCollection getCatalogCollection(EntityViewInfo view) throws BOSException;
    public CatalogCollection getCatalogCollection(String oql) throws BOSException;
}