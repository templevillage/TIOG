package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class MeetingFormInfo extends AbstractMeetingFormInfo implements Serializable 
{
    public MeetingFormInfo()
    {
        super();
    }
    protected MeetingFormInfo(String pkField)
    {
        super(pkField);
    }
}