package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.tiog.basedata.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class ExecuteStatus extends DataBase implements IExecuteStatus
{
    public ExecuteStatus()
    {
        super();
        registerInterface(IExecuteStatus.class, this);
    }
    public ExecuteStatus(Context ctx)
    {
        super(ctx);
        registerInterface(IExecuteStatus.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("1A1525F0");
    }
    private ExecuteStatusController getController() throws BOSException
    {
        return (ExecuteStatusController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public ExecuteStatusInfo getExecuteStatusInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getExecuteStatusInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public ExecuteStatusInfo getExecuteStatusInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getExecuteStatusInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public ExecuteStatusInfo getExecuteStatusInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getExecuteStatusInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public ExecuteStatusCollection getExecuteStatusCollection() throws BOSException
    {
        try {
            return getController().getExecuteStatusCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public ExecuteStatusCollection getExecuteStatusCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getExecuteStatusCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public ExecuteStatusCollection getExecuteStatusCollection(String oql) throws BOSException
    {
        try {
            return getController().getExecuteStatusCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}