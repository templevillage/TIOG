package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractCatalogMainInfo extends com.kingdee.eas.framework.DataBaseInfo implements Serializable 
{
    public AbstractCatalogMainInfo()
    {
        this("id");
    }
    protected AbstractCatalogMainInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 分类事项 's 组别 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogMainTreeInfo getTreeid()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogMainTreeInfo)get("treeid");
    }
    public void setTreeid(com.kingdee.eas.custom.tiog.basedata.CatalogMainTreeInfo item)
    {
        put("treeid", item);
    }
    /**
     * Object: 分类事项 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    /**
     * Object: 分类事项 's 事项状态 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogStatusInfo getStatus()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogStatusInfo)get("status");
    }
    public void setStatus(com.kingdee.eas.custom.tiog.basedata.CatalogStatusInfo item)
    {
        put("status", item);
    }
    /**
     * Object: 分类事项 's 表决方式 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.VoteModeInfo getVoteMode()
    {
        return (com.kingdee.eas.custom.tiog.basedata.VoteModeInfo)get("voteMode");
    }
    public void setVoteMode(com.kingdee.eas.custom.tiog.basedata.VoteModeInfo item)
    {
        put("voteMode", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("4C0D39DB");
    }
}