package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class PassStatusCollection extends AbstractObjectCollection 
{
    public PassStatusCollection()
    {
        super(PassStatusInfo.class);
    }
    public boolean add(PassStatusInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(PassStatusCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(PassStatusInfo item)
    {
        return removeObject(item);
    }
    public PassStatusInfo get(int index)
    {
        return(PassStatusInfo)getObject(index);
    }
    public PassStatusInfo get(Object key)
    {
        return(PassStatusInfo)getObject(key);
    }
    public void set(int index, PassStatusInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(PassStatusInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(PassStatusInfo item)
    {
        return super.indexOf(item);
    }
}