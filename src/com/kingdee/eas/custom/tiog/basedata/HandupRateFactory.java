package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class HandupRateFactory
{
    private HandupRateFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.basedata.IHandupRate getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IHandupRate)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("B41AB5C1") ,com.kingdee.eas.custom.tiog.basedata.IHandupRate.class);
    }
    
    public static com.kingdee.eas.custom.tiog.basedata.IHandupRate getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IHandupRate)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("B41AB5C1") ,com.kingdee.eas.custom.tiog.basedata.IHandupRate.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.basedata.IHandupRate getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IHandupRate)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("B41AB5C1"));
    }
    public static com.kingdee.eas.custom.tiog.basedata.IHandupRate getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.basedata.IHandupRate)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("B41AB5C1"));
    }
}