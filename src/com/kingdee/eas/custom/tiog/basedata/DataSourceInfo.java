package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class DataSourceInfo extends AbstractDataSourceInfo implements Serializable 
{
    public DataSourceInfo()
    {
        super();
    }
    protected DataSourceInfo(String pkField)
    {
        super(pkField);
    }
}