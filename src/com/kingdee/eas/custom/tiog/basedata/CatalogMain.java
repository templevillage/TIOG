package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.tiog.basedata.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class CatalogMain extends DataBase implements ICatalogMain
{
    public CatalogMain()
    {
        super();
        registerInterface(ICatalogMain.class, this);
    }
    public CatalogMain(Context ctx)
    {
        super(ctx);
        registerInterface(ICatalogMain.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("4C0D39DB");
    }
    private CatalogMainController getController() throws BOSException
    {
        return (CatalogMainController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public CatalogMainInfo getCatalogMainInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getCatalogMainInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public CatalogMainInfo getCatalogMainInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getCatalogMainInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public CatalogMainInfo getCatalogMainInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getCatalogMainInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public CatalogMainCollection getCatalogMainCollection() throws BOSException
    {
        try {
            return getController().getCatalogMainCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public CatalogMainCollection getCatalogMainCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getCatalogMainCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public CatalogMainCollection getCatalogMainCollection(String oql) throws BOSException
    {
        try {
            return getController().getCatalogMainCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}