package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class CatalogMainTreeInfo extends AbstractCatalogMainTreeInfo implements Serializable 
{
    public CatalogMainTreeInfo()
    {
        super();
    }
    protected CatalogMainTreeInfo(String pkField)
    {
        super(pkField);
    }
}