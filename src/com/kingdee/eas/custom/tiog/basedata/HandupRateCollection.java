package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class HandupRateCollection extends AbstractObjectCollection 
{
    public HandupRateCollection()
    {
        super(HandupRateInfo.class);
    }
    public boolean add(HandupRateInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(HandupRateCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(HandupRateInfo item)
    {
        return removeObject(item);
    }
    public HandupRateInfo get(int index)
    {
        return(HandupRateInfo)getObject(index);
    }
    public HandupRateInfo get(Object key)
    {
        return(HandupRateInfo)getObject(key);
    }
    public void set(int index, HandupRateInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(HandupRateInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(HandupRateInfo item)
    {
        return super.indexOf(item);
    }
}