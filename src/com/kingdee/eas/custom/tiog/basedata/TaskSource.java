package com.kingdee.eas.custom.tiog.basedata;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.tiog.basedata.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class TaskSource extends DataBase implements ITaskSource
{
    public TaskSource()
    {
        super();
        registerInterface(ITaskSource.class, this);
    }
    public TaskSource(Context ctx)
    {
        super(ctx);
        registerInterface(ITaskSource.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("673ACC77");
    }
    private TaskSourceController getController() throws BOSException
    {
        return (TaskSourceController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public TaskSourceInfo getTaskSourceInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getTaskSourceInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public TaskSourceInfo getTaskSourceInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getTaskSourceInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public TaskSourceInfo getTaskSourceInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getTaskSourceInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public TaskSourceCollection getTaskSourceCollection() throws BOSException
    {
        try {
            return getController().getTaskSourceCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public TaskSourceCollection getTaskSourceCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getTaskSourceCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public TaskSourceCollection getTaskSourceCollection(String oql) throws BOSException
    {
        try {
            return getController().getTaskSourceCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}