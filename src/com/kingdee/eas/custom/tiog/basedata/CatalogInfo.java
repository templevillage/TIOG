package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class CatalogInfo extends AbstractCatalogInfo implements Serializable 
{
    public CatalogInfo()
    {
        super();
    }
    protected CatalogInfo(String pkField)
    {
        super(pkField);
    }
}