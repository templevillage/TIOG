package com.kingdee.eas.custom.tiog.basedata;

import java.io.Serializable;

public class RegulationTypeInfo extends AbstractRegulationTypeInfo implements Serializable 
{
    public RegulationTypeInfo()
    {
        super();
    }
    protected RegulationTypeInfo(String pkField)
    {
        super(pkField);
    }
}