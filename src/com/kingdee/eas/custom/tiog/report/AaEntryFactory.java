package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AaEntryFactory
{
    private AaEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.report.IAaEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("EA49B114") ,com.kingdee.eas.custom.tiog.report.IAaEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.report.IAaEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("EA49B114") ,com.kingdee.eas.custom.tiog.report.IAaEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.report.IAaEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("EA49B114"));
    }
    public static com.kingdee.eas.custom.tiog.report.IAaEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("EA49B114"));
    }
}