package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class AaEntryDEntryCollection extends AbstractObjectCollection 
{
    public AaEntryDEntryCollection()
    {
        super(AaEntryDEntryInfo.class);
    }
    public boolean add(AaEntryDEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(AaEntryDEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AaEntryDEntryInfo item)
    {
        return removeObject(item);
    }
    public AaEntryDEntryInfo get(int index)
    {
        return(AaEntryDEntryInfo)getObject(index);
    }
    public AaEntryDEntryInfo get(Object key)
    {
        return(AaEntryDEntryInfo)getObject(key);
    }
    public void set(int index, AaEntryDEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(AaEntryDEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AaEntryDEntryInfo item)
    {
        return super.indexOf(item);
    }
}