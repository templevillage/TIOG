package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AaFactory
{
    private AaFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.report.IAa getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAa)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("B1E675FE") ,com.kingdee.eas.custom.tiog.report.IAa.class);
    }
    
    public static com.kingdee.eas.custom.tiog.report.IAa getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAa)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("B1E675FE") ,com.kingdee.eas.custom.tiog.report.IAa.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.report.IAa getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAa)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("B1E675FE"));
    }
    public static com.kingdee.eas.custom.tiog.report.IAa getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAa)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("B1E675FE"));
    }
}