/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.report.client;

import org.apache.log4j.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.BorderFactory;
import javax.swing.event.*;
import javax.swing.KeyStroke;

import com.kingdee.bos.ctrl.swing.*;
import com.kingdee.bos.ctrl.kdf.table.*;
import com.kingdee.bos.ctrl.kdf.data.event.*;
import com.kingdee.bos.dao.*;
import com.kingdee.bos.dao.query.*;
import com.kingdee.bos.metadata.*;
import com.kingdee.bos.metadata.entity.*;
import com.kingdee.bos.ui.face.*;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.enums.EnumUtils;
import com.kingdee.bos.ui.face.UIRuleUtil;
import com.kingdee.bos.ctrl.swing.event.*;
import com.kingdee.bos.ctrl.kdf.table.event.*;
import com.kingdee.bos.ctrl.extendcontrols.*;
import com.kingdee.bos.ctrl.kdf.util.render.*;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.uistatemanage.ActionStateConst;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.appframework.uip.UINavigator;


/**
 * output class name
 */
public abstract class AbstractCatalogLifecycleAnalyzeCdtUI extends com.kingdee.eas.framework.report.client.CommRptBaseConditionUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractCatalogLifecycleAnalyzeCdtUI.class);
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer kDLabelContainer1;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer kDLabelContainer2;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton1;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton2;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton3;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton4;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton5;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton6;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker startDate;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker endDate;
    /**
     * output class constructor
     */
    public AbstractCatalogLifecycleAnalyzeCdtUI() throws Exception
    {
        super();
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractCatalogLifecycleAnalyzeCdtUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        this.kDLabelContainer1 = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDLabelContainer2 = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDRadioButton1 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDRadioButton2 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDRadioButton3 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDRadioButton4 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDRadioButton5 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDRadioButton6 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.startDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.endDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.kDLabelContainer1.setName("kDLabelContainer1");
        this.kDLabelContainer2.setName("kDLabelContainer2");
        this.kDRadioButton1.setName("kDRadioButton1");
        this.kDRadioButton2.setName("kDRadioButton2");
        this.kDRadioButton3.setName("kDRadioButton3");
        this.kDRadioButton4.setName("kDRadioButton4");
        this.kDRadioButton5.setName("kDRadioButton5");
        this.kDRadioButton6.setName("kDRadioButton6");
        this.startDate.setName("startDate");
        this.endDate.setName("endDate");
        // CustomerQueryPanel
        // kDLabelContainer1		
        this.kDLabelContainer1.setBoundLabelText(resHelper.getString("kDLabelContainer1.boundLabelText"));		
        this.kDLabelContainer1.setBoundLabelLength(100);		
        this.kDLabelContainer1.setBoundLabelUnderline(true);
        // kDLabelContainer2		
        this.kDLabelContainer2.setBoundLabelText(resHelper.getString("kDLabelContainer2.boundLabelText"));		
        this.kDLabelContainer2.setBoundLabelLength(100);		
        this.kDLabelContainer2.setBoundLabelUnderline(true);
        // kDRadioButton1		
        this.kDRadioButton1.setText(resHelper.getString("kDRadioButton1.text"));
        // kDRadioButton2		
        this.kDRadioButton2.setText(resHelper.getString("kDRadioButton2.text"));
        // kDRadioButton3		
        this.kDRadioButton3.setText(resHelper.getString("kDRadioButton3.text"));
        // kDRadioButton4		
        this.kDRadioButton4.setText(resHelper.getString("kDRadioButton4.text"));
        // kDRadioButton5		
        this.kDRadioButton5.setText(resHelper.getString("kDRadioButton5.text"));
        // kDRadioButton6		
        this.kDRadioButton6.setText(resHelper.getString("kDRadioButton6.text"));
        // startDate
        // endDate
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(10, 10, 400, 400));
        this.setLayout(null);
        kDLabelContainer1.setBounds(new Rectangle(34, 72, 307, 19));
        this.add(kDLabelContainer1, null);
        kDLabelContainer2.setBounds(new Rectangle(31, 146, 306, 19));
        this.add(kDLabelContainer2, null);
        kDRadioButton1.setBounds(new Rectangle(32, 208, 140, 19));
        this.add(kDRadioButton1, null);
        kDRadioButton2.setBounds(new Rectangle(208, 208, 140, 19));
        this.add(kDRadioButton2, null);
        kDRadioButton3.setBounds(new Rectangle(32, 264, 140, 19));
        this.add(kDRadioButton3, null);
        kDRadioButton4.setBounds(new Rectangle(208, 264, 140, 19));
        this.add(kDRadioButton4, null);
        kDRadioButton5.setBounds(new Rectangle(32, 320, 140, 19));
        this.add(kDRadioButton5, null);
        kDRadioButton6.setBounds(new Rectangle(208, 320, 140, 19));
        this.add(kDRadioButton6, null);
        //kDLabelContainer1
        kDLabelContainer1.setBoundEditor(startDate);
        //kDLabelContainer2
        kDLabelContainer2.setBoundEditor(endDate);

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {


    }

	//Regiester control's property binding.
	private void registerBindings(){		
	}
	//Regiester UI State
	private void registerUIState(){		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.tiog.report.app.CatalogLifecycleAnalyzeCdtUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }



	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
    }

    /**
     * output loadFields method
     */
    public void loadFields()
    {
        dataBinder.loadFields();
    }
    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
    }


    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.tiog.report.client", "CatalogLifecycleAnalyzeCdtUI");
    }




}