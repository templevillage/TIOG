/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.report.client;
import com.kingdee.eas.common.client.OprtState;
import com.kingdee.eas.common.client.SysContext;

import java.awt.Color;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.ui.face.CoreUIObject;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.bos.ui.face.IUIFactory;
import com.kingdee.bos.ui.face.IUIObject;
import com.kingdee.bos.ui.face.IUIWindow;
import com.kingdee.bos.ui.face.ItemAction;
import com.kingdee.bos.ui.face.UIFactory;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.custom.tiog.bizbill.client.AbstractCategoryListEditUI;
import com.kingdee.eas.custom.tiog.report.CatalogLifecycleAnalyzeFacadeFactory;
import com.kingdee.eas.framework.*;
import com.kingdee.eas.framework.*;
import com.kingdee.eas.framework.report.ICommRptBase;
import com.kingdee.eas.framework.report.client.CommRptBaseConditionUI;
import com.kingdee.eas.framework.report.util.RptParams;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.ctrl.kdf.table.ICell;
import com.kingdee.bos.ctrl.kdf.table.IRow;
import com.kingdee.bos.ctrl.kdf.table.KDTable;
import com.kingdee.bos.ctrl.kdf.table.event.KDTDataRequestEvent;
import com.kingdee.bos.ctrl.kdf.table.event.KDTDataRequestListener;
import com.kingdee.bos.ctrl.kdf.table.event.KDTSelectEvent;
import com.kingdee.bos.ctrl.kdf.headfootdesigner.HeadFootModel;
import com.kingdee.bos.ctrl.kdf.headfootdesigner.HeadFootParser;
import com.kingdee.bos.ctrl.kdf.headfootdesigner.HeadFootRow;
import com.kingdee.eas.framework.report.util.KDTableUtil;
import com.kingdee.eas.framework.report.util.RptParams;
import com.kingdee.eas.framework.report.util.RptRowSet;
import com.kingdee.eas.framework.report.util.RptTableColumn;
import com.kingdee.eas.framework.report.util.RptTableHeader;
import com.kingdee.bos.ctrl.print.IVariantParser;
import com.kingdee.bos.ctrl.print.extend.KDTableImagePrinter;
import com.kingdee.bos.ctrl.print.ui.component.PainterInfo;
import com.kingdee.bos.ctrl.swing.KDWorkButton;
import com.kingdee.bos.ctrl.kdf.form.Page;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.basedata.assistant.PeriodInfo;
import org.jdom.Element;

/**
 * output class name
 */
public class CatalogLifecycleAnalyzeRptUI extends AbstractCatalogLifecycleAnalyzeRptUI
{
    
    private static final Logger logger = CoreUIObject.getLogger(CatalogLifecycleAnalyzeRptUI.class);
    List<String> list_Header0 = new ArrayList<String>();               
    List<String> list_Header1 = new ArrayList<String>();               
    Map<Integer, CatalogLifecycleInfo> map = null;
    protected ActionEnter2Bill actionEnter2Bill = null;
    CatalogLifecycleInfo catalogLifecycleInfo = null;
    KDWorkButton curBtn = null;
    /**
     * output class constructor
     */
    public CatalogLifecycleAnalyzeRptUI() throws Exception
    {
        super();
        this.tbl.checkParsed();
        this.tbl.getDataRequestManager().addDataRequestListener((KDTDataRequestListener) this);
        enableExportExcel(this.tbl);
        tbl.setEditable(false);
        //this.kDRadioButton1.setBackground(new Color(0, 255, 0));
    }

    
    public void onLoad() throws Exception {
        getUIContext().put("checkLicense", "false");
        super.onLoad();
        initActionEnter2Bill();
        //this.kDRadioButton1.setBackground(new Color(0, 255, 0));
    }

    /**
     * output storeFields method
     */
    public void storeFields()
    {
        super.storeFields();
    }
    
    public void onShow() throws Exception {
        super.onShow();
        if(tbl.getSortMange() != null)
            tbl.getSortMange().setSortAuto(true);
    }
    
    //参数初始化
    protected RptParams getParamsForInit() {
        return params;
    }
    
    //加载过滤界面
    protected CommRptBaseConditionUI getQueryDialogUserPanel() throws Exception {
        return new CatalogLifecycleAnalyzeCdtUI();
    }
    //获取远程服务实例
    protected ICommRptBase getRemoteInstance() throws BOSException {
        return  (ICommRptBase) CatalogLifecycleAnalyzeFacadeFactory.getRemoteInstance();
    }
    
    //获取主界面表格对象
    protected KDTable getTableForPrintSetting() {
        return tbl;
    }

    protected KDTable getTable() {
        return tbl;
    }
    
    protected void query() {
        tbl.removeColumns(); 
        tbl.removeRows();//触发取数根
    }
    //获取过滤后的报表值并对界面表格填充值
    public void tableDataRequest(KDTDataRequestEvent e) {
        //in
        RptParams para_ReqPre = new RptParams();
        //中文的第二行表头
        params.setObject("list_Fields", list_Header1);
        try {
            int from = e.getFirstRow();
            int end = e.getLastRow();
            int len = end - from + 1;
            RptParams rptParam;
            ArrayList<String> tempTable_Columns = new ArrayList<String>(); 
            if (from == 0) {
                para_ReqPre.putAll(params.toMap());
                para_ReqPre.setString("tempName", this.getTempTable());
                rptParam = getRemoteInstance().createTempTable(para_ReqPre);
                this.setTempTable(rptParam.getString("tempName"));
                this.tbl.setRowCount(rptParam.getInt("count"));
                tempTable_Columns = (ArrayList<String>) rptParam.getObject("tempTable_Columns");
                RptTableHeader header = this.getHeader(tempTable_Columns);
                KDTableUtil.setHeader(header, tbl);
            }
            RptParams rpt = new RptParams();
            rpt.putAll(params.toMap());
            rpt.setString("tempName", this.getTempTable());
            rpt.setObject("tempTable_Colums", tempTable_Columns);
            RptParams selectRpt = getRemoteInstance().query(rpt, from, len);
            this.tbl.setRowCount(selectRpt.getInt("count"));
            RptRowSet rs = (RptRowSet) selectRpt.getObject("rowset");
            KDTableUtil.insertRows(rs, from, tbl);
            list_Header0.clear();
            list_Header1.clear();
            
            map = transTable2CLInfo(tbl, tempTable_Columns);
            
        } catch (Exception ex) {
                ex.printStackTrace();
        }
    }
    
    private Map<Integer, CatalogLifecycleInfo> transTable2CLInfo(KDTable tbl, ArrayList<String> tempTable_Columns) {
        // TODO Auto-generated method stub
        Map<Integer, CatalogLifecycleInfo> catalogLifecycleInfos = null;
        if(tbl.getRowCount() > 0){
            catalogLifecycleInfos = new HashMap<Integer, CatalogLifecycleInfo>();
            for(int index = 0, count = tbl.getRowCount(); index < count; index++){
                IRow iRow = tbl.getRow(index);
                CatalogLifecycleInfo catalogLifecycleInfo = new CatalogLifecycleInfo();
                int pop = 1;
                //立项
                ICell iCell = iRow.getCell(tempTable_Columns.get(pop));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setId_CatalogMain(iCell.getValue().toString());
                iCell = iRow.getCell(tempTable_Columns.get(pop + 4));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setState_CatalogMain(iCell.getValue().toString());
                pop = pop + 5;
                //清单
                iCell = iRow.getCell(tempTable_Columns.get(pop));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setId_CategoryList(iCell.getValue().toString());
                iCell = iRow.getCell(tempTable_Columns.get(pop + 4));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setState_CategoryList(iCell.getValue().toString());
                pop = pop + 5;
                //制度
                iCell = iRow.getCell(tempTable_Columns.get(pop));
                System.out.println("index=>" + index + " tempTable_Colums=>" + tempTable_Columns.get(pop));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setId_PoliticalSystem(iCell.getValue().toString());
                iCell = iRow.getCell(tempTable_Columns.get(pop + 4));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setState_PoliticalSystem(iCell.getValue().toString());
                pop = pop + 5;
                //议题
                iCell = iRow.getCell(tempTable_Columns.get(pop));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setId_Subject(iCell.getValue().toString());
                iCell = iRow.getCell(tempTable_Columns.get(pop + 4));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setState_Subject(iCell.getValue().toString());
                pop = pop + 5;
                //会议
                iCell = iRow.getCell(tempTable_Columns.get(pop));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setId_MeetingMan(iCell.getValue().toString());
                iCell = iRow.getCell(tempTable_Columns.get(pop + 4));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setState_MeetingMan(iCell.getValue().toString());
                pop = pop + 5;
                //实施
                iCell = iRow.getCell(tempTable_Columns.get(pop));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setId_Execution(iCell.getValue().toString());
                iCell = iRow.getCell(tempTable_Columns.get(pop + 4));
                if(null != iCell.getValue())
                    catalogLifecycleInfo.setState_Execution(iCell.getValue().toString());
                //
                catalogLifecycleInfos.put(index, catalogLifecycleInfo);
            }
        }
        return catalogLifecycleInfos;
    }


    /**
     * output ActionUnAudit class
     */     
    protected class ActionEnter2Bill extends ItemAction {     
    
        public ActionEnter2Bill()
        {
            this(null);
        }

        public ActionEnter2Bill(IUIObject uiObject)
        {     
            super(uiObject);     
            String _tempStr = "enter2Bill";
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
            getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", CatalogLifecycleAnalyzeRptUI.this, "Enter2Bill", "actionEnter2Bill_actionPerformed", e);
        }
    }

    public void actionEnter2Bill_actionPerformed(ActionEvent e) throws Exception {
        HashMap uiCtx = new HashMap();
        uiCtx.put("Owner", this);
        //uiCtx.put("canResize", this);
        //com.kingdee.eas.base.uiframe.client.UINewFrameFactory
        //com.kingdee.eas.base.uiframe.client.UIModelDialogFactory
        //e.get
        IUIFactory uiFactory = UIFactory.createUIFactory("com.kingdee.eas.base.uiframe.client.UIModelDialogFactory");
        String ui = "CatalogMain";
        curBtn = (com.kingdee.bos.ctrl.swing.KDWorkButton)e.getSource();
        if(curBtn.getName().equals("kDWorkButton1")){
            ui = "com.kingdee.eas.custom.tiog.basedata.client.CatalogMainEditUI";
            uiCtx.put("ID", this.catalogLifecycleInfo.getId_CatalogMain());
        } else if (curBtn.getName().equals("kDWorkButton2")) {
            ui = "com.kingdee.eas.custom.tiog.bizbill.client.CategoryListEditUI";
            uiCtx.put("ID", this.catalogLifecycleInfo.getId_CategoryList());
        } else if (curBtn.getName().equals("kDWorkButton3")) {
            ui = "com.kingdee.eas.custom.tiog.bizbill.client.PoliticalSystemEditUI";
            uiCtx.put("ID", this.catalogLifecycleInfo.getId_PoliticalSystem());
        } else if (curBtn.getName().equals("kDWB4")) {
            ui = "com.kingdee.eas.custom.tiog.bizbill.client.SubjectEditUI";
            uiCtx.put("ID", this.catalogLifecycleInfo.getId_Subject());
        } else if (curBtn.getName().equals("kDWB5")) {
            ui = "com.kingdee.eas.custom.tiog.bizbill.client.MeetingManEditUI";
            uiCtx.put("ID", this.catalogLifecycleInfo.getId_MeetingMan());
        } else if (curBtn.getName().equals("kDWorkButton6")) {
            ui = "com.kingdee.eas.custom.tiog.bizbill.client.ExecutionEditUI";
            uiCtx.put("ID", this.catalogLifecycleInfo.getId_Execution());
        }
        IUIWindow billDlg = uiFactory.create(ui, uiCtx, null, OprtState.VIEW);
        billDlg.show();
    }

    
    public void initActionEnter2Bill(){
        actionEnter2Bill = new ActionEnter2Bill(this);
        getActionManager().registerAction("actionEnter2Bill", actionEnter2Bill);
        actionEnter2Bill.setBindWorkFlow(true);
        actionEnter2Bill.setExtendProperty("canForewarn", "true");
        actionEnter2Bill.setExtendProperty("userDefined", "true");
        actionEnter2Bill.setExtendProperty("isObjectUpdateLock", "false");
        actionEnter2Bill.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        actionEnter2Bill.addService(new com.kingdee.eas.framework.client.service.ForewarnService());
        actionEnter2Bill.addService(new com.kingdee.eas.framework.client.service.WorkFlowService());
        kDWorkButton1.setAction((IItemAction)ActionProxyFactory.getProxy(actionEnter2Bill, new Class[] { IItemAction.class }, getServiceContext()));                
        kDWorkButton2.setAction((IItemAction)ActionProxyFactory.getProxy(actionEnter2Bill, new Class[] { IItemAction.class }, getServiceContext()));                
        kDWorkButton3.setAction((IItemAction)ActionProxyFactory.getProxy(actionEnter2Bill, new Class[] { IItemAction.class }, getServiceContext()));                
        kDWB4.setAction((IItemAction)ActionProxyFactory.getProxy(actionEnter2Bill, new Class[] { IItemAction.class }, getServiceContext()));                
        kDWB5.setAction((IItemAction)ActionProxyFactory.getProxy(actionEnter2Bill, new Class[] { IItemAction.class }, getServiceContext()));                
        kDWorkButton6.setAction((IItemAction)ActionProxyFactory.getProxy(actionEnter2Bill, new Class[] { IItemAction.class }, getServiceContext()));                

    }

    
    
    @Override
    protected void tbl_tableSelectChanged(KDTSelectEvent e) throws Exception {
        // TODO Auto-generated method stub
        super.tbl_tableSelectChanged(e);
        
        if(map.size() > 0){
            
            System.out.println("Background:" + kDRadioButton1.getBackground() + "Foreground:" + kDRadioButton1.getForeground());
            int[] selectIds = KDTableUtil.getSelectedRows(tbl);
            //全局
            catalogLifecycleInfo = map.get(selectIds[0]);
            if(null != catalogLifecycleInfo){
                //立项
                if(null != catalogLifecycleInfo.getId_CatalogMain()){
                    kDWorkButton1.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("image_item_selected"));
                    kDWorkButton1.setEnabled(true);
                } else {
                    kDWorkButton1.setIcon(null);
                    kDWorkButton1.setEnabled(false);
                }
                kDRadioButton2.setEnabled(false);
                if(null != catalogLifecycleInfo.getState_CatalogMain()){
                    kDRadioButton2.setSelected(true);
                } else {
                    kDRadioButton2.setSelected(false);
                }
                //清单
                if(null != catalogLifecycleInfo.getId_CategoryList()){
                    kDWorkButton2.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("image_item_selected"));
                    kDWorkButton2.setEnabled(true);
                } else {
                    kDWorkButton2.setIcon(null);
                    kDWorkButton2.setEnabled(false);
                }
                kDRadioButton3.setEnabled(false);
                if(null != catalogLifecycleInfo.getState_CategoryList()){
                    kDRadioButton3.setSelected(true);
                } else {
                    kDRadioButton3.setSelected(false);
                }
                //制度
                if(null != catalogLifecycleInfo.getId_PoliticalSystem()){
                    kDWorkButton3.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("image_item_selected"));
                    kDWorkButton3.setEnabled(true);
                } else {
                    kDWorkButton3.setIcon(null);
                    kDWorkButton3.setEnabled(false);
                }
                kDRadioButton4.setEnabled(false);
                if(null != catalogLifecycleInfo.getState_PoliticalSystem()){
                    kDRadioButton4.setSelected(true);
                } else {
                    kDRadioButton4.setSelected(false);
                }
                //议题
                if(null != catalogLifecycleInfo.getId_Subject()){
                    kDWB4.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("image_item_selected"));
                    kDWB4.setEnabled(true);
                } else {
                    kDWB4.setIcon(null);
                    kDWB4.setEnabled(false);
                }
                kDRB4.setEnabled(false);
                if(null != catalogLifecycleInfo.getState_Subject()){
                    kDRB4.setSelected(true);
                } else {
                    kDRB4.setSelected(false);
                }
                //会议
                if(null != catalogLifecycleInfo.getId_MeetingMan()){
                    kDWB5.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("image_item_selected"));
                    kDWB5.setEnabled(true);
                } else {
                    kDWB5.setIcon(null);
                    kDWB5.setEnabled(false);
                }
                kDRadioButton5.setEnabled(false);
                if(null != catalogLifecycleInfo.getState_MeetingMan()){
                    kDRadioButton5.setSelected(true);
                } else {
                    kDRadioButton5.setSelected(false);
                }
                //立项
                if(null != catalogLifecycleInfo.getId_Execution()){
                    kDWorkButton6.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("image_item_selected"));
                    kDWorkButton6.setEnabled(true);
                } else {
                    kDWorkButton6.setIcon(null);
                    kDWorkButton6.setEnabled(false);
                }
                kDRadioButton6.setEnabled(false);
                if(null != catalogLifecycleInfo.getState_Execution()){
                    kDRadioButton6.setSelected(true);
                } else {
                    kDRadioButton6.setSelected(false);
                }
            }
//            this.kDRadioButton1.setBackground(new Color(0, 255, 0));
//            this.kDRadioButton1.setForeground(Color.YELLOW);
//            kDRadioButton1.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgWf_event_event_running"));
//            System.out.println("Background:" + kDRadioButton1.getBackground() + "Foreground:" + kDRadioButton1.getForeground());
//            kDRadioButton1.setSelected(true);
//            //image_item_hover   
//            //imgWf_event_event_running imgLight_green
//            kDWorkButton1.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("image_item_selected"));
//            kDWorkButton2.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("image_item_hover"));
//            this.kDWorkButton1.setForeground(new Color(0, 255, 0));
            //kDRadioButton1.
        }
        this.repaint();
//        this.
    }
    
    // 自定义:表头_明细
    private RptTableHeader getHeader(ArrayList<String> tempTable_Columns) {
        AdminOrgUnitInfo adminOrg = (AdminOrgUnitInfo)params.getObject("prmtDept");
        RptTableHeader header = new RptTableHeader();
        RptTableColumn col = null;
        //{"立项", "立项", "立项", "立项", "事项清单", "事项清单", "事项清单", "事项清单", "事项清单"}
        //, "决策制度", "决策制度", "决策制度", "决策制度", "决策制度", "审议议题", "审议议题", "审议议题", "审议议题", "审议议题"}
        //, "会议决策", "会议决策", "会议决策", "会议决策", "会议决策", "组织实施", "组织实施", "组织实施", "组织实施", "组织实施"}
        //{"ID", "编码", "名称", "日期", "ID", "编码", "名称", "日期", "状态"}
        //, "ID", "编码", "名称", "日期", "状态", "ID", "编码", "名称", "日期", "状态"}
        //, "ID", "编码", "名称", "日期", "状态", "ID", "编码", "名称", "日期", "状态"}
        col = new RptTableColumn("SEQ");
        list_Header0.add("序号");
        list_Header1.add("序号");
        col.setHided(true);
        header.addColumn(col);
        
        //顺序输出
        int pop = 1;
        
        String[] states = new String[] {"立项", "事项清单", "决策制度", "审议议题", "会议决策", "组织实施"};
        for( int i = 0, count = states.length; i < count; i++){
            //id
            System.out.println("i=>" + i + " tempTable_Colums=>" + tempTable_Columns.get(pop));
            col = new RptTableColumn(tempTable_Columns.get(pop++));
            list_Header0.add(states[i]);
            list_Header1.add("ID");
            col.setHided(true);
            header.addColumn(col);
            //number
            col = new RptTableColumn(tempTable_Columns.get(pop++));
            list_Header0.add(states[i]);
            list_Header1.add("编码");
            col.setWidth(100);
            col.setAligment(RptTableColumn.HALIGN_LEFT);
            header.addColumn(col);
            //name
            col = new RptTableColumn(tempTable_Columns.get(pop++));
            list_Header0.add(states[i]);
            list_Header1.add("名称");
            col.setWidth(100);
            col.setAligment(RptTableColumn.HALIGN_LEFT);
            header.addColumn(col);
            //date
            col = new RptTableColumn(tempTable_Columns.get(pop++));
            list_Header0.add(states[i]);
            list_Header1.add("日期");
            col.setWidth(100);
            col.setAligment(RptTableColumn.HALIGN_LEFT);
            header.addColumn(col);
            //state 不搞特殊了，把状态输出来吧
/*            if(!"立项".equals(states[i])){
            }
*/            
            col = new RptTableColumn(tempTable_Columns.get(pop++));
            list_Header0.add(states[i]);
            list_Header1.add("状态");
            col.setWidth(100);
            col.setAligment(RptTableColumn.HALIGN_LEFT);
            header.addColumn(col);
        }

        header.setLabels(new Object[][] {list_Header0.toArray(), list_Header1.toArray()}, true);
        return header;
    }
    
    public void actionPrintPreview_actionPerformed(ActionEvent es) throws Exception {
            KDTable tbl = getTableForPrintSetting();
            preparePrintPage(tbl);
            KDTableImagePrinter tableImagePrinter = null;
            tableImagePrinter = new KDTableImagePrinter();
            tableImagePrinter.setSpace(20);
            tableImagePrinter.addDataParser(com.kingdee.bos.ctrl.kdf.table.KDTable.class, KDTableImagePrinter.TABLE_PARSER);
            tableImagePrinter.addDataParser(java.awt.Image.class, KDTableImagePrinter.IMAGE_PARSER);
            tableImagePrinter.setParent(this);

            java.util.List pList = new ArrayList();
            pList.add(tbl);
            tableImagePrinter.setPrintData(pList.toArray());
            tableImagePrinter.getPrinter().setVariantParser(new IVariantParser() {
                    public String parse(String name, PainterInfo info){
                            return String.valueOf(getPrintExtVarProvider().requestVarData(name).getValue());
                    }
            }
            );
            Page page = getTableForPrintSetting().getPrintManager().getHeader();
            HeadFootModel header = HeadFootParser.parseHeadFootPage2Model(page);                    
            tableImagePrinter.setHeader(header);
            HeadFootModel footer = HeadFootParser.parseHeadFootPage2Model(getTableForPrintSetting().getPrintManager().getFooter());
            tableImagePrinter.setFooter(HeadFootParser.parseHeadFootPage2Model(getTableForPrintSetting().getPrintManager().getFooter()));
            String configName = tbl.getPrintManager().getNewPrintManager().getPrinter().getPrintConfig().createXmlTrans().getName();

            org.jdom.Element e = getKDFForTable(tbl).getKDTableNewPrintConfig(configName);

            if(e != null)
                    tableImagePrinter.fromXmlElement(e);

            List list = header.getRowList();
            List rsList = new ArrayList();
            HeadFootRow obj = null;
            PeriodInfo p = (PeriodInfo)params.getObject("prmtPeriod");
            //AdminOrgUnitInfo adm = (AdminOrgUnitInfo)params.getObject("prmtDept");
            for(int i=0;i < list.size();i++){
                    obj = (HeadFootRow)list.get(i);
                    String temp = obj.getText();
                    if(temp != null&&p!=null){
                            if(temp.indexOf("期间")>=0){
                                    obj.setText("期间："+p.getNumber() + " ");
                            }
                    }
                    rsList.add(obj);
            }
            header.getRowList().clear();
            header.setRowList(rsList);
            tableImagePrinter.setHeader(header);
            
            List flist = footer.getRowList();
            List frsList = new ArrayList();
            HeadFootRow fobj = null;
            UserInfo userInfo = (UserInfo)SysContext.getSysContext().getCurrentUserInfo();
            for(int i=0;i < flist.size();i++){
                    fobj = (HeadFootRow)flist.get(i);
                    String temp = fobj.getText();
                    if(temp != null&&userInfo!=null){
                            if(temp.indexOf("打印日期")>=0){
                                    fobj.setText("打印日期：&[Date]&|打印人："+userInfo.getName()+"&|科室负责人：        &|页数：&[Page]/&[PageCount]");
                            }
                    }
                    frsList.add(fobj);
            }
            footer.getRowList().clear();
            footer.setRowList(frsList);
            tableImagePrinter.setFooter(footer);
            tableImagePrinter.printPreview();
            tbl.getPrintManager().fromXmlElement(tableImagePrinter.getPrinter().getPrintConfig().toXmlElement());
    }
    
    public void actionPrint_actionPerformed(ActionEvent es) throws Exception {
            KDTable tbl = getTableForPrintSetting();
            preparePrintPage(tbl);
            KDTableImagePrinter tableImagePrinter = null;
            tableImagePrinter = new KDTableImagePrinter();
            tableImagePrinter.setSpace(20);
            tableImagePrinter.addDataParser(com.kingdee.bos.ctrl.kdf.table.KDTable.class, KDTableImagePrinter.TABLE_PARSER);
            tableImagePrinter.addDataParser(java.awt.Image.class, KDTableImagePrinter.IMAGE_PARSER);
            tableImagePrinter.setParent(this);

            java.util.List pList = new ArrayList();
            pList.add(tbl);
            tableImagePrinter.setPrintData(pList.toArray());
            tableImagePrinter.getPrinter().setVariantParser(new IVariantParser() {
                    public String parse(String name, PainterInfo info){
                            return String.valueOf(getPrintExtVarProvider().requestVarData(name).getValue());
                    }
            }
            );
            Page page = getTableForPrintSetting().getPrintManager().getHeader();
            HeadFootModel header = HeadFootParser.parseHeadFootPage2Model(page);                    
            tableImagePrinter.setHeader(header);
            HeadFootModel footer = HeadFootParser.parseHeadFootPage2Model(getTableForPrintSetting().getPrintManager().getFooter());
            tableImagePrinter.setFooter(HeadFootParser.parseHeadFootPage2Model(getTableForPrintSetting().getPrintManager().getFooter()));
            String configName = tbl.getPrintManager().getNewPrintManager().getPrinter().getPrintConfig().createXmlTrans().getName();

            org.jdom.Element e = getKDFForTable(tbl).getKDTableNewPrintConfig(configName);

            if(e != null)
                    tableImagePrinter.fromXmlElement(e);

            List list = header.getRowList();
            List rsList = new ArrayList();
            HeadFootRow obj = null;
            PeriodInfo p = (PeriodInfo)params.getObject("prmtPeriod");
            //AdminOrgUnitInfo adm = (AdminOrgUnitInfo)params.getObject("prmtDept");
            for(int i=0;i < list.size();i++){
                    obj = (HeadFootRow)list.get(i);
                    String temp = obj.getText();
                    /**
                    if(temp != null&&p!=null && adm!=null){
                            if(temp.indexOf("期间")>=0){
                                    obj.setText("期间："+p.getNumber()+"     部门："+adm.getName());
                            }
                    }
                     * */
                    if(temp != null&&p!=null){
                            if(temp.indexOf("期间")>=0){
                                    obj.setText("期间："+p.getNumber() + " ");
                            }
                    }
                    rsList.add(obj);
            }
            header.getRowList().clear();
            header.setRowList(rsList);
            tableImagePrinter.setHeader(header);
            
            List flist = footer.getRowList();
            List frsList = new ArrayList();
            HeadFootRow fobj = null;
            UserInfo userInfo = (UserInfo)SysContext.getSysContext().getCurrentUserInfo();
            for(int i=0;i < flist.size();i++){
                    fobj = (HeadFootRow)flist.get(i);
                    String temp = fobj.getText();
                    if(temp != null&&userInfo!=null){
                            if(temp.indexOf("打印日期")>=0){
                                    fobj.setText("打印日期：&[Date]&|打印人："+userInfo.getName()+"&|科室负责人：        &|页数：&[Page]/&[PageCount]");
                            }
                    }
                    frsList.add(fobj);
            }
            footer.getRowList().clear();
            footer.setRowList(frsList);
            tableImagePrinter.setFooter(footer);
            Element el = tableImagePrinter.getPrinter().getPrintConfig().toXmlElement();
            tableImagePrinter.print();
            tbl.getPrintManager().fromXmlElement(tableImagePrinter.getPrinter().getPrintConfig().toXmlElement());
    }

}