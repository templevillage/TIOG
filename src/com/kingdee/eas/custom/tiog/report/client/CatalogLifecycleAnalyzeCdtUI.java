/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.report.client;

import java.awt.event.*;
import org.apache.log4j.Logger;
import com.kingdee.bos.ui.face.CoreUIObject;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.eas.framework.*;
import com.kingdee.eas.framework.report.util.RptParams;
import com.kingdee.bos.ui.face.UIRuleUtil;

/**
 * output class name
 */
public class CatalogLifecycleAnalyzeCdtUI extends AbstractCatalogLifecycleAnalyzeCdtUI
{
    private static final Logger logger = CoreUIObject.getLogger(CatalogLifecycleAnalyzeCdtUI.class);
    
    /**
     * output class constructor
     */
    public CatalogLifecycleAnalyzeCdtUI() throws Exception
    {
        super();
    }

    /**
     * output storeFields method
     */
    public void storeFields()
    {
        super.storeFields();
    }

        @Override
        public RptParams getCustomCondition() {
                RptParams pp = new RptParams();
                //起始日期
                if(UIRuleUtil.isNull(startDate.getValue())){
                        pp.setObject("startDate", null);
                }else{
                        pp.setObject("startDate", this.startDate.getValue());
                }
                //起始日期
                if(UIRuleUtil.isNull(endDate.getValue())){
                        pp.setObject("endDate", null);
                }else{
                        pp.setObject("endDate", this.endDate.getValue());
                }
                return pp;
        }

        @Override
        public void onInit(RptParams arg0) throws Exception {
                // TODO Auto-generated method stub
                
        }

        @Override
        public void setCustomCondition(RptParams rptP) {
            startDate.setValue(rptP.getObject("startDate"));
            endDate.setValue(rptP.getObject("endDate"));
        }

}