/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.report.client;

import org.apache.log4j.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.BorderFactory;
import javax.swing.event.*;
import javax.swing.KeyStroke;

import com.kingdee.bos.ctrl.swing.*;
import com.kingdee.bos.ctrl.kdf.table.*;
import com.kingdee.bos.ctrl.kdf.data.event.*;
import com.kingdee.bos.dao.*;
import com.kingdee.bos.dao.query.*;
import com.kingdee.bos.metadata.*;
import com.kingdee.bos.metadata.entity.*;
import com.kingdee.bos.ui.face.*;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.enums.EnumUtils;
import com.kingdee.bos.ui.face.UIRuleUtil;
import com.kingdee.bos.ctrl.swing.event.*;
import com.kingdee.bos.ctrl.kdf.table.event.*;
import com.kingdee.bos.ctrl.extendcontrols.*;
import com.kingdee.bos.ctrl.kdf.util.render.*;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.uistatemanage.ActionStateConst;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.appframework.uip.UINavigator;


/**
 * output class name
 */
public abstract class AbstractCatalogLifecycleAnalyzeRptUI extends com.kingdee.eas.framework.report.client.VirtualRptBaseUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractCatalogLifecycleAnalyzeRptUI.class);
    protected com.kingdee.bos.ctrl.kdf.table.KDTable tbl;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDL1;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton1;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kDWorkButton1;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton2;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kDWorkButton2;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kDWorkButton3;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton3;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kDWB4;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRB4;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kDWB5;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton5;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kDWorkButton6;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton6;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDL2;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDL3;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDL4;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDL5;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDL6;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDL7;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton4;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDLabel1;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDLabel2;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDLabel3;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDLabel4;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDLabel5;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDLabel6;
    /**
     * output class constructor
     */
    public AbstractCatalogLifecycleAnalyzeRptUI() throws Exception
    {
        super();
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractCatalogLifecycleAnalyzeRptUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        this.tbl = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.kDL1 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDRadioButton1 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDWorkButton1 = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDRadioButton2 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDWorkButton2 = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDWorkButton3 = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDRadioButton3 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDWB4 = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDRB4 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDWB5 = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDRadioButton5 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDWorkButton6 = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDRadioButton6 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDL2 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDL3 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDL4 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDL5 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDL6 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDL7 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDRadioButton4 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDLabel1 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDLabel2 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDLabel3 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDLabel4 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDLabel5 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDLabel6 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.tbl.setName("tbl");
        this.kDL1.setName("kDL1");
        this.kDRadioButton1.setName("kDRadioButton1");
        this.kDWorkButton1.setName("kDWorkButton1");
        this.kDRadioButton2.setName("kDRadioButton2");
        this.kDWorkButton2.setName("kDWorkButton2");
        this.kDWorkButton3.setName("kDWorkButton3");
        this.kDRadioButton3.setName("kDRadioButton3");
        this.kDWB4.setName("kDWB4");
        this.kDRB4.setName("kDRB4");
        this.kDWB5.setName("kDWB5");
        this.kDRadioButton5.setName("kDRadioButton5");
        this.kDWorkButton6.setName("kDWorkButton6");
        this.kDRadioButton6.setName("kDRadioButton6");
        this.kDL2.setName("kDL2");
        this.kDL3.setName("kDL3");
        this.kDL4.setName("kDL4");
        this.kDL5.setName("kDL5");
        this.kDL6.setName("kDL6");
        this.kDL7.setName("kDL7");
        this.kDRadioButton4.setName("kDRadioButton4");
        this.kDLabel1.setName("kDLabel1");
        this.kDLabel2.setName("kDLabel2");
        this.kDLabel3.setName("kDLabel3");
        this.kDLabel4.setName("kDLabel4");
        this.kDLabel5.setName("kDLabel5");
        this.kDLabel6.setName("kDLabel6");
        // CoreUI
        // tbl
        this.tbl.addKDTSelectListener(new com.kingdee.bos.ctrl.kdf.table.event.KDTSelectListener() {
            public void tableSelectChanged(com.kingdee.bos.ctrl.kdf.table.event.KDTSelectEvent e) {
                try {
                    tbl_tableSelectChanged(e);
                } catch (Exception exc) {
                    handUIException(exc);
                } finally {
                }
            }
        });

        

        // kDL1		
        this.kDL1.setText(resHelper.getString("kDL1.text"));
        // kDRadioButton1		
        this.kDRadioButton1.setBackground(new java.awt.Color(0,255,0));		
        this.kDRadioButton1.setEnabled(false);		
        this.kDRadioButton1.setForeground(new java.awt.Color(0,255,0));
        // kDWorkButton1		
        this.kDWorkButton1.setText(resHelper.getString("kDWorkButton1.text"));		
        this.kDWorkButton1.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("image_item_hover"));
        // kDRadioButton2		
        this.kDRadioButton2.setEnabled(false);
        // kDWorkButton2		
        this.kDWorkButton2.setText(resHelper.getString("kDWorkButton2.text"));
        // kDWorkButton3		
        this.kDWorkButton3.setText(resHelper.getString("kDWorkButton3.text"));
        this.kDWorkButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                beforeActionPerformed(e);
                try {
                    kDWorkButton3_actionPerformed(e);
                } catch (Exception exc) {
                    handUIException(exc);
                } finally {
                    afterActionPerformed(e);
                }
            }
        });
        // kDRadioButton3		
        this.kDRadioButton3.setEnabled(false);
        // kDWB4		
        this.kDWB4.setText(resHelper.getString("kDWB4.text"));
        // kDRB4		
        this.kDRB4.setEnabled(false);
        // kDWB5		
        this.kDWB5.setText(resHelper.getString("kDWB5.text"));
        // kDRadioButton5		
        this.kDRadioButton5.setText(resHelper.getString("kDRadioButton5.text"));		
        this.kDRadioButton5.setEnabled(false);
        // kDWorkButton6		
        this.kDWorkButton6.setText(resHelper.getString("kDWorkButton6.text"));
        // kDRadioButton6		
        this.kDRadioButton6.setText(resHelper.getString("kDRadioButton6.text"));		
        this.kDRadioButton6.setEnabled(false);
        // kDL2		
        this.kDL2.setText(resHelper.getString("kDL2.text"));
        // kDL3		
        this.kDL3.setText(resHelper.getString("kDL3.text"));
        // kDL4		
        this.kDL4.setText(resHelper.getString("kDL4.text"));
        // kDL5		
        this.kDL5.setText(resHelper.getString("kDL5.text"));
        // kDL6		
        this.kDL6.setText(resHelper.getString("kDL6.text"));
        // kDL7		
        this.kDL7.setText(resHelper.getString("kDL7.text"));
        // kDRadioButton4		
        this.kDRadioButton4.setEnabled(false);		
        this.kDRadioButton4.setText(resHelper.getString("kDRadioButton4.text"));
        // kDLabel1		
        this.kDLabel1.setText(resHelper.getString("kDLabel1.text"));
        // kDLabel2		
        this.kDLabel2.setText(resHelper.getString("kDLabel2.text"));
        // kDLabel3		
        this.kDLabel3.setText(resHelper.getString("kDLabel3.text"));
        // kDLabel4		
        this.kDLabel4.setText(resHelper.getString("kDLabel4.text"));
        // kDLabel5		
        this.kDLabel5.setText(resHelper.getString("kDLabel5.text"));
        // kDLabel6		
        this.kDLabel6.setText(resHelper.getString("kDLabel6.text"));
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(10, 10, 1016, 600));
        this.setLayout(new KDLayout());
        this.putClientProperty("OriginalBounds", new Rectangle(10, 10, 1016, 600));
        tbl.setBounds(new Rectangle(10, 103, 993, 483));
        this.add(tbl, new KDLayout.Constraints(10, 103, 993, 483, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        kDL1.setBounds(new Rectangle(42, 40, 30, 19));
        this.add(kDL1, new KDLayout.Constraints(42, 40, 30, 19, 0));
        kDRadioButton1.setBounds(new Rectangle(80, 40, 20, 19));
        this.add(kDRadioButton1, new KDLayout.Constraints(80, 40, 20, 19, 0));
        kDWorkButton1.setBounds(new Rectangle(96, 40, 150, 19));
        this.add(kDWorkButton1, new KDLayout.Constraints(96, 40, 150, 19, 0));
        kDRadioButton2.setBounds(new Rectangle(246, 40, 20, 19));
        this.add(kDRadioButton2, new KDLayout.Constraints(246, 40, 20, 19, 0));
        kDWorkButton2.setBounds(new Rectangle(262, 30, 150, 19));
        this.add(kDWorkButton2, new KDLayout.Constraints(262, 30, 150, 19, 0));
        kDWorkButton3.setBounds(new Rectangle(262, 50, 150, 19));
        this.add(kDWorkButton3, new KDLayout.Constraints(262, 50, 150, 19, 0));
        kDRadioButton3.setBounds(new Rectangle(412, 30, 20, 19));
        this.add(kDRadioButton3, new KDLayout.Constraints(412, 30, 20, 19, 0));
        kDWB4.setBounds(new Rectangle(428, 40, 150, 19));
        this.add(kDWB4, new KDLayout.Constraints(428, 40, 150, 19, 0));
        kDRB4.setBounds(new Rectangle(578, 40, 20, 19));
        this.add(kDRB4, new KDLayout.Constraints(578, 40, 20, 19, 0));
        kDWB5.setBounds(new Rectangle(594, 40, 150, 19));
        this.add(kDWB5, new KDLayout.Constraints(594, 40, 150, 19, 0));
        kDRadioButton5.setBounds(new Rectangle(744, 40, 20, 19));
        this.add(kDRadioButton5, new KDLayout.Constraints(744, 40, 20, 19, 0));
        kDWorkButton6.setBounds(new Rectangle(762, 40, 150, 19));
        this.add(kDWorkButton6, new KDLayout.Constraints(762, 40, 150, 19, 0));
        kDRadioButton6.setBounds(new Rectangle(912, 40, 100, 19));
        this.add(kDRadioButton6, new KDLayout.Constraints(912, 40, 100, 19, 0));
        kDL2.setBounds(new Rectangle(78, 10, 43, 19));
        this.add(kDL2, new KDLayout.Constraints(78, 10, 43, 19, 0));
        kDL3.setBounds(new Rectangle(231, 10, 50, 19));
        this.add(kDL3, new KDLayout.Constraints(231, 10, 50, 19, 0));
        kDL4.setBounds(new Rectangle(400, 10, 50, 19));
        this.add(kDL4, new KDLayout.Constraints(400, 10, 50, 19, 0));
        kDL5.setBounds(new Rectangle(563, 10, 50, 19));
        this.add(kDL5, new KDLayout.Constraints(563, 10, 50, 19, 0));
        kDL6.setBounds(new Rectangle(729, 10, 50, 19));
        this.add(kDL6, new KDLayout.Constraints(729, 10, 50, 19, 0));
        kDL7.setBounds(new Rectangle(897, 10, 50, 19));
        this.add(kDL7, new KDLayout.Constraints(897, 10, 50, 19, 0));
        kDRadioButton4.setBounds(new Rectangle(412, 50, 20, 19));
        this.add(kDRadioButton4, new KDLayout.Constraints(412, 50, 20, 19, 0));
        kDLabel1.setBounds(new Rectangle(133, 20, 82, 19));
        this.add(kDLabel1, new KDLayout.Constraints(133, 20, 82, 19, 0));
        kDLabel2.setBounds(new Rectangle(301, 10, 82, 19));
        this.add(kDLabel2, new KDLayout.Constraints(301, 10, 82, 19, 0));
        kDLabel3.setBounds(new Rectangle(302, 68, 82, 19));
        this.add(kDLabel3, new KDLayout.Constraints(302, 68, 82, 19, 0));
        kDLabel4.setBounds(new Rectangle(463, 20, 82, 19));
        this.add(kDLabel4, new KDLayout.Constraints(463, 20, 82, 19, 0));
        kDLabel5.setBounds(new Rectangle(633, 20, 82, 19));
        this.add(kDLabel5, new KDLayout.Constraints(633, 20, 82, 19, 0));
        kDLabel6.setBounds(new Rectangle(799, 20, 82, 19));
        this.add(kDLabel6, new KDLayout.Constraints(799, 20, 82, 19, 0));

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {
        this.menuBar.add(menuFile);
        this.menuBar.add(menuView);
        this.menuBar.add(menuTool);
        this.menuBar.add(MenuService);
        this.menuBar.add(menuHelp);
        //menuFile
        menuFile.add(menuItemPageSetup);
        menuFile.add(menuItemPrint);
        menuFile.add(menuItemCloudFeed);
        menuFile.add(menuItemPrintPreview);
        menuFile.add(kDSeparator1);
        menuFile.add(menuItemCloudScreen);
        menuFile.add(menuItemCloudShare);
        menuFile.add(menuItemExitCurrent);
        menuFile.add(kdSeparatorFWFile1);
        //menuView
        menuView.add(menuItemQuery);
        menuView.add(menuItemRefresh);
        menuView.add(kDSeparator2);
        menuView.add(menuItemChart);
        //menuTool
        menuTool.add(menuItemSendMessage);
        menuTool.add(menuItemCalculator);
        menuTool.add(menuItemToolBarCustom);
        //MenuService
        MenuService.add(MenuItemKnowStore);
        MenuService.add(MenuItemAnwser);
        MenuService.add(SepratorService);
        MenuService.add(MenuItemRemoteAssist);
        //menuHelp
        menuHelp.add(menuItemHelp);
        menuHelp.add(kDSeparator12);
        menuHelp.add(menuItemRegPro);
        menuHelp.add(menuItemPersonalSite);
        menuHelp.add(helpseparatorDiv);
        menuHelp.add(menuitemProductval);
        menuHelp.add(kDSeparatorProduct);
        menuHelp.add(menuItemAbout);

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {
        this.toolBar.add(btnPageSetup);
        this.toolBar.add(btnCloud);
        this.toolBar.add(btnRefresh);
        this.toolBar.add(btnXunTong);
        this.toolBar.add(btnQuery);
        this.toolBar.add(kDSeparatorCloud);
        this.toolBar.add(separator1);
        this.toolBar.add(btnPrint);
        this.toolBar.add(btnPrintPreview);
        this.toolBar.add(separator2);
        this.toolBar.add(btnChart);


    }

	//Regiester control's property binding.
	private void registerBindings(){		
	}
	//Regiester UI State
	private void registerUIState(){		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.tiog.report.app.CatalogLifecycleAnalyzeRptUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }



	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
    }

    /**
     * output loadFields method
     */
    public void loadFields()
    {
        dataBinder.loadFields();
    }
    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
    }

    /**
     * output tbl_tableSelectChanged method
     */
    protected void tbl_tableSelectChanged(com.kingdee.bos.ctrl.kdf.table.event.KDTSelectEvent e) throws Exception
    {
    }

    /**
     * output kDWorkButton3_actionPerformed method
     */
    protected void kDWorkButton3_actionPerformed(java.awt.event.ActionEvent e) throws Exception
    {
    }


    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.tiog.report.client", "CatalogLifecycleAnalyzeRptUI");
    }




}