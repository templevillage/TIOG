package com.kingdee.eas.custom.tiog.report.client;

public class CatalogLifecycleInfo {
    
    //事项
    public String id_CatalogMain;
    public String state_CatalogMain;
    //事项清单
    public String id_CategoryList;
    public String state_CategoryList;
    //决策制度
    public String id_PoliticalSystem;
    public String state_PoliticalSystem;
    //审议议题
    public String id_Subject;
    public String state_Subject;
    //会议决策
    public String id_MeetingMan;
    public String state_MeetingMan;
    //组织实施
    public String id_Execution;
    public String state_Execution;
    public String getId_CatalogMain() {
        return id_CatalogMain;
    }
    public void setId_CatalogMain(String id_CatalogMain) {
        this.id_CatalogMain = id_CatalogMain;
    }
    public String getState_CatalogMain() {
        return state_CatalogMain;
    }
    public void setState_CatalogMain(String state_CatalogMain) {
        this.state_CatalogMain = state_CatalogMain;
    }
    public String getId_CategoryList() {
        return id_CategoryList;
    }
    public void setId_CategoryList(String id_CategoryList) {
        this.id_CategoryList = id_CategoryList;
    }
    public String getState_CategoryList() {
        return state_CategoryList;
    }
    public void setState_CategoryList(String state_CategoryList) {
        this.state_CategoryList = state_CategoryList;
    }
    public String getId_PoliticalSystem() {
        return id_PoliticalSystem;
    }
    public void setId_PoliticalSystem(String id_PoliticalSystem) {
        this.id_PoliticalSystem = id_PoliticalSystem;
    }
    public String getState_PoliticalSystem() {
        return state_PoliticalSystem;
    }
    public void setState_PoliticalSystem(String state_PoliticalSystem) {
        this.state_PoliticalSystem = state_PoliticalSystem;
    }
    public String getId_Subject() {
        return id_Subject;
    }
    public void setId_Subject(String id_Subject) {
        this.id_Subject = id_Subject;
    }
    public String getState_Subject() {
        return state_Subject;
    }
    public void setState_Subject(String state_Subject) {
        this.state_Subject = state_Subject;
    }
    public String getId_MeetingMan() {
        return id_MeetingMan;
    }
    public void setId_MeetingMan(String id_MeetingMan) {
        this.id_MeetingMan = id_MeetingMan;
    }
    public String getState_MeetingMan() {
        return state_MeetingMan;
    }
    public void setState_MeetingMan(String state_MeetingMan) {
        this.state_MeetingMan = state_MeetingMan;
    }
    public String getId_Execution() {
        return id_Execution;
    }
    public void setId_Execution(String id_Execution) {
        this.id_Execution = id_Execution;
    }
    public String getState_Execution() {
        return state_Execution;
    }
    public void setState_Execution(String state_Execution) {
        this.state_Execution = state_Execution;
    }

}
