package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class AaCollection extends AbstractObjectCollection 
{
    public AaCollection()
    {
        super(AaInfo.class);
    }
    public boolean add(AaInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(AaCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AaInfo item)
    {
        return removeObject(item);
    }
    public AaInfo get(int index)
    {
        return(AaInfo)getObject(index);
    }
    public AaInfo get(Object key)
    {
        return(AaInfo)getObject(key);
    }
    public void set(int index, AaInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(AaInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AaInfo item)
    {
        return super.indexOf(item);
    }
}