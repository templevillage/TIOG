package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AaF1Factory
{
    private AaF1Factory()
    {
    }
    public static com.kingdee.eas.custom.tiog.report.IAaF1 getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaF1)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("D220F729") ,com.kingdee.eas.custom.tiog.report.IAaF1.class);
    }
    
    public static com.kingdee.eas.custom.tiog.report.IAaF1 getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaF1)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("D220F729") ,com.kingdee.eas.custom.tiog.report.IAaF1.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.report.IAaF1 getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaF1)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("D220F729"));
    }
    public static com.kingdee.eas.custom.tiog.report.IAaF1 getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaF1)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("D220F729"));
    }
}