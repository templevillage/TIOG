package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class CatalogLifecycleAnalyzeFacadeFactory
{
    private CatalogLifecycleAnalyzeFacadeFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.report.ICatalogLifecycleAnalyzeFacade getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.ICatalogLifecycleAnalyzeFacade)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("DED95497") ,com.kingdee.eas.custom.tiog.report.ICatalogLifecycleAnalyzeFacade.class);
    }
    
    public static com.kingdee.eas.custom.tiog.report.ICatalogLifecycleAnalyzeFacade getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.ICatalogLifecycleAnalyzeFacade)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("DED95497") ,com.kingdee.eas.custom.tiog.report.ICatalogLifecycleAnalyzeFacade.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.report.ICatalogLifecycleAnalyzeFacade getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.ICatalogLifecycleAnalyzeFacade)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("DED95497"));
    }
    public static com.kingdee.eas.custom.tiog.report.ICatalogLifecycleAnalyzeFacade getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.ICatalogLifecycleAnalyzeFacade)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("DED95497"));
    }
}