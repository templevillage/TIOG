package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class AaEntryCollection extends AbstractObjectCollection 
{
    public AaEntryCollection()
    {
        super(AaEntryInfo.class);
    }
    public boolean add(AaEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(AaEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AaEntryInfo item)
    {
        return removeObject(item);
    }
    public AaEntryInfo get(int index)
    {
        return(AaEntryInfo)getObject(index);
    }
    public AaEntryInfo get(Object key)
    {
        return(AaEntryInfo)getObject(key);
    }
    public void set(int index, AaEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(AaEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AaEntryInfo item)
    {
        return super.indexOf(item);
    }
}