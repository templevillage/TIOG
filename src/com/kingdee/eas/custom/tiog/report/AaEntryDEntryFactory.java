package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AaEntryDEntryFactory
{
    private AaEntryDEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.report.IAaEntryDEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntryDEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("188734A2") ,com.kingdee.eas.custom.tiog.report.IAaEntryDEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.report.IAaEntryDEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntryDEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("188734A2") ,com.kingdee.eas.custom.tiog.report.IAaEntryDEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.report.IAaEntryDEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntryDEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("188734A2"));
    }
    public static com.kingdee.eas.custom.tiog.report.IAaEntryDEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntryDEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("188734A2"));
    }
}