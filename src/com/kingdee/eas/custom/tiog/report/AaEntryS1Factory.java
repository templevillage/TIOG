package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AaEntryS1Factory
{
    private AaEntryS1Factory()
    {
    }
    public static com.kingdee.eas.custom.tiog.report.IAaEntryS1 getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntryS1)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("7EA1C652") ,com.kingdee.eas.custom.tiog.report.IAaEntryS1.class);
    }
    
    public static com.kingdee.eas.custom.tiog.report.IAaEntryS1 getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntryS1)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("7EA1C652") ,com.kingdee.eas.custom.tiog.report.IAaEntryS1.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.report.IAaEntryS1 getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntryS1)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("7EA1C652"));
    }
    public static com.kingdee.eas.custom.tiog.report.IAaEntryS1 getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.report.IAaEntryS1)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("7EA1C652"));
    }
}