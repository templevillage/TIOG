package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class AaEntryS1Collection extends AbstractObjectCollection 
{
    public AaEntryS1Collection()
    {
        super(AaEntryS1Info.class);
    }
    public boolean add(AaEntryS1Info item)
    {
        return addObject(item);
    }
    public boolean addCollection(AaEntryS1Collection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AaEntryS1Info item)
    {
        return removeObject(item);
    }
    public AaEntryS1Info get(int index)
    {
        return(AaEntryS1Info)getObject(index);
    }
    public AaEntryS1Info get(Object key)
    {
        return(AaEntryS1Info)getObject(key);
    }
    public void set(int index, AaEntryS1Info item)
    {
        setObject(index, item);
    }
    public boolean contains(AaEntryS1Info item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AaEntryS1Info item)
    {
        return super.indexOf(item);
    }
}