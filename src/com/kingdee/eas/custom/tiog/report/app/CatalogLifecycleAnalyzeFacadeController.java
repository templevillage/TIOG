package com.kingdee.eas.custom.tiog.report.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import java.util.HashSet;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.report.app.CommRptBaseController;
import com.kingdee.bos.util.*;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface CatalogLifecycleAnalyzeFacadeController extends CommRptBaseController
{
    public HashSet getBizDept(Context ctx, String userId) throws BOSException, RemoteException;
}