package com.kingdee.eas.custom.tiog.report.app;

import javax.ejb.*;
import java.rmi.RemoteException;
import com.kingdee.bos.*;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.metadata.IMetaDataPK;
import com.kingdee.bos.metadata.rule.RuleExecutor;
import com.kingdee.bos.metadata.MetaDataPK;
//import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.framework.ejb.AbstractEntityControllerBean;
import com.kingdee.bos.framework.ejb.AbstractBizControllerBean;
//import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.bos.dao.IObjectCollection;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.bos.service.IServiceContext;
import com.kingdee.eas.framework.Result;
import com.kingdee.eas.framework.LineResult;
import com.kingdee.eas.framework.exception.EASMultiException;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;

import com.kingdee.eas.framework.report.app.CommRptBaseControllerBean;
import java.util.HashSet;
import java.lang.String;



public abstract class AbstractCatalogLifecycleAnalyzeFacadeControllerBean extends CommRptBaseControllerBean implements CatalogLifecycleAnalyzeFacadeController
{
    protected AbstractCatalogLifecycleAnalyzeFacadeControllerBean()
    {
    }

    protected BOSObjectType getBOSType()
    {
        return new BOSObjectType("DED95497");
    }

    public HashSet getBizDept(Context ctx, String userId) throws BOSException
    {
        try {
            ServiceContext svcCtx = createServiceContext(new MetaDataPK("3fbdccb7-e18d-49fb-96e6-a4ea1478c1f8"), new Object[]{ctx, userId});
            invokeServiceBefore(svcCtx);
            if(!svcCtx.invokeBreak()) {
            HashSet retValue = (HashSet)_getBizDept(ctx, userId);
            svcCtx.setMethodReturnValue(retValue);
            }
            invokeServiceAfter(svcCtx);
            return (HashSet)svcCtx.getMethodReturnValue();
        } catch (BOSException ex) {
            throw ex;
        } finally {
            super.cleanUpServiceState();
        }
    }
    protected HashSet _getBizDept(Context ctx, String userId) throws BOSException
    {    	
        return null;
    }

}