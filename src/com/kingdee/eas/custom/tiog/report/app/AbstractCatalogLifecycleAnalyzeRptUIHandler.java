/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.report.app;

import com.kingdee.bos.Context;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.eas.framework.batchHandler.ResponseContext;


/**
 * output class name
 */
public abstract class AbstractCatalogLifecycleAnalyzeRptUIHandler extends com.kingdee.eas.framework.report.app.VirtualRptBaseUIHandler

{
}