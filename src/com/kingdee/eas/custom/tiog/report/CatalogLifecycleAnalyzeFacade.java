package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.framework.report.CommRptBase;
import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.eas.framework.report.ICommRptBase;
import java.util.HashSet;
import java.lang.String;
import com.kingdee.eas.custom.tiog.report.app.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;

public class CatalogLifecycleAnalyzeFacade extends CommRptBase implements ICatalogLifecycleAnalyzeFacade
{
    public CatalogLifecycleAnalyzeFacade()
    {
        super();
        registerInterface(ICatalogLifecycleAnalyzeFacade.class, this);
    }
    public CatalogLifecycleAnalyzeFacade(Context ctx)
    {
        super(ctx);
        registerInterface(ICatalogLifecycleAnalyzeFacade.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("DED95497");
    }
    private CatalogLifecycleAnalyzeFacadeController getController() throws BOSException
    {
        return (CatalogLifecycleAnalyzeFacadeController)getBizController();
    }
    /**
     *获取业务科室范围-User defined method
     *@param userId 用户ID
     *@return
     */
    public HashSet getBizDept(String userId) throws BOSException
    {
        try {
            return getController().getBizDept(getContext(), userId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}