package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IAa extends ICoreBillBase
{
    public AaCollection getAaCollection() throws BOSException;
    public AaCollection getAaCollection(EntityViewInfo view) throws BOSException;
    public AaCollection getAaCollection(String oql) throws BOSException;
    public AaInfo getAaInfo(IObjectPK pk) throws BOSException, EASBizException;
    public AaInfo getAaInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public AaInfo getAaInfo(String oql) throws BOSException, EASBizException;
}