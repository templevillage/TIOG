package com.kingdee.eas.custom.tiog.report;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractAaF1Info extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractAaF1Info()
    {
        this("id");
    }
    protected AbstractAaF1Info(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: F1 's null property 
     */
    public com.kingdee.eas.custom.tiog.report.AaInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.report.AaInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.report.AaInfo item)
    {
        put("parent", item);
    }
    /**
     * Object:F1's aproperty 
     */
    public String getA()
    {
        return getString("a");
    }
    public void setA(String item)
    {
        setString("a", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("D220F729");
    }
}