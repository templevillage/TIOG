package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.framework.CoreBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.custom.tiog.report.app.*;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class Aa extends CoreBillBase implements IAa
{
    public Aa()
    {
        super();
        registerInterface(IAa.class, this);
    }
    public Aa(Context ctx)
    {
        super(ctx);
        registerInterface(IAa.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("B1E675FE");
    }
    private AaController getController() throws BOSException
    {
        return (AaController)getBizController();
    }
    /**
     *取集合-System defined method
     *@return
     */
    public AaCollection getAaCollection() throws BOSException
    {
        try {
            return getController().getAaCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param view 取集合
     *@return
     */
    public AaCollection getAaCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getAaCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param oql 取集合
     *@return
     */
    public AaCollection getAaCollection(String oql) throws BOSException
    {
        try {
            return getController().getAaCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@return
     */
    public AaInfo getAaInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getAaInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@param selector 取值
     *@return
     */
    public AaInfo getAaInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getAaInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param oql 取值
     *@return
     */
    public AaInfo getAaInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getAaInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}