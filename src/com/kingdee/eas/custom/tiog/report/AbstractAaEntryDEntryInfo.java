package com.kingdee.eas.custom.tiog.report;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractAaEntryDEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractAaEntryDEntryInfo()
    {
        this("id");
    }
    protected AbstractAaEntryDEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��ϸ��¼ 's null property 
     */
    public com.kingdee.eas.custom.tiog.report.AaEntryInfo getParent1()
    {
        return (com.kingdee.eas.custom.tiog.report.AaEntryInfo)get("parent1");
    }
    public void setParent1(com.kingdee.eas.custom.tiog.report.AaEntryInfo item)
    {
        put("parent1", item);
    }
    /**
     * Object:��ϸ��¼'s aproperty 
     */
    public String getA()
    {
        return getString("a");
    }
    public void setA(String item)
    {
        setString("a", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("188734A2");
    }
}