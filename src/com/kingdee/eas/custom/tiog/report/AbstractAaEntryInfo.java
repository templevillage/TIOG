package com.kingdee.eas.custom.tiog.report;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractAaEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractAaEntryInfo()
    {
        this("id");
    }
    protected AbstractAaEntryInfo(String pkField)
    {
        super(pkField);
        put("S1", new com.kingdee.eas.custom.tiog.report.AaEntryS1Collection());
        put("DEntrys", new com.kingdee.eas.custom.tiog.report.AaEntryDEntryCollection());
    }
    /**
     * Object: 分录 's 单据头 property 
     */
    public com.kingdee.eas.custom.tiog.report.AaInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.report.AaInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.report.AaInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 分录 's 明细分录 property 
     */
    public com.kingdee.eas.custom.tiog.report.AaEntryDEntryCollection getDEntrys()
    {
        return (com.kingdee.eas.custom.tiog.report.AaEntryDEntryCollection)get("DEntrys");
    }
    /**
     * Object: 分录 's S1 property 
     */
    public com.kingdee.eas.custom.tiog.report.AaEntryS1Collection getS1()
    {
        return (com.kingdee.eas.custom.tiog.report.AaEntryS1Collection)get("S1");
    }
    /**
     * Object:分录's aproperty 
     */
    public String getA()
    {
        return getString("a");
    }
    public void setA(String item)
    {
        setString("a", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("EA49B114");
    }
}