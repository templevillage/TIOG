package com.kingdee.eas.custom.tiog.report;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractAaInfo extends com.kingdee.eas.framework.CoreBillBaseInfo implements Serializable 
{
    public AbstractAaInfo()
    {
        this("id");
    }
    protected AbstractAaInfo(String pkField)
    {
        super(pkField);
        put("F1", new com.kingdee.eas.custom.tiog.report.AaF1Collection());
        put("entrys", new com.kingdee.eas.custom.tiog.report.AaEntryCollection());
    }
    /**
     * Object: Aa 's 分录 property 
     */
    public com.kingdee.eas.custom.tiog.report.AaEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.tiog.report.AaEntryCollection)get("entrys");
    }
    /**
     * Object:Aa's 是否生成凭证property 
     */
    public boolean isFivouchered()
    {
        return getBoolean("Fivouchered");
    }
    public void setFivouchered(boolean item)
    {
        setBoolean("Fivouchered", item);
    }
    /**
     * Object: Aa 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    /**
     * Object: Aa 's F1 property 
     */
    public com.kingdee.eas.custom.tiog.report.AaF1Collection getF1()
    {
        return (com.kingdee.eas.custom.tiog.report.AaF1Collection)get("F1");
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("B1E675FE");
    }
}