package com.kingdee.eas.custom.tiog.report;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractAaEntryS1Info extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractAaEntryS1Info()
    {
        this("id");
    }
    protected AbstractAaEntryS1Info(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: S1 's null property 
     */
    public com.kingdee.eas.custom.tiog.report.AaEntryInfo getParent1()
    {
        return (com.kingdee.eas.custom.tiog.report.AaEntryInfo)get("parent1");
    }
    public void setParent1(com.kingdee.eas.custom.tiog.report.AaEntryInfo item)
    {
        put("parent1", item);
    }
    /**
     * Object:S1's aproperty 
     */
    public String getA()
    {
        return getString("a");
    }
    public void setA(String item)
    {
        setString("a", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("7EA1C652");
    }
}