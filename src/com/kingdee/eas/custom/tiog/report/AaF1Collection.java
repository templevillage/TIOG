package com.kingdee.eas.custom.tiog.report;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class AaF1Collection extends AbstractObjectCollection 
{
    public AaF1Collection()
    {
        super(AaF1Info.class);
    }
    public boolean add(AaF1Info item)
    {
        return addObject(item);
    }
    public boolean addCollection(AaF1Collection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AaF1Info item)
    {
        return removeObject(item);
    }
    public AaF1Info get(int index)
    {
        return(AaF1Info)getObject(index);
    }
    public AaF1Info get(Object key)
    {
        return(AaF1Info)getObject(key);
    }
    public void set(int index, AaF1Info item)
    {
        setObject(index, item);
    }
    public boolean contains(AaF1Info item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AaF1Info item)
    {
        return super.indexOf(item);
    }
}