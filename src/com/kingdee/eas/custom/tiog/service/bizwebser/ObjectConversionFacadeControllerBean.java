package com.kingdee.eas.custom.tiog.service.bizwebser;

import org.apache.log4j.Logger;
import javax.ejb.*;
import java.rmi.RemoteException;
import com.kingdee.bos.*;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.metadata.IMetaDataPK;
import com.kingdee.bos.metadata.rule.RuleExecutor;
import com.kingdee.bos.metadata.MetaDataPK;
//import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.framework.ejb.AbstractEntityControllerBean;
import com.kingdee.bos.framework.ejb.AbstractBizControllerBean;
//import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.bos.dao.IObjectCollection;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.bos.service.IServiceContext;

import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMeetingBO;
import com.kingdee.eas.custom.tiog.middleware.bo.OrganizationAndImplementationVO;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMakingSystemBO;
import com.kingdee.eas.custom.tiog.middleware.bo.ListofMattersBO;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo;

public class ObjectConversionFacadeControllerBean extends AbstractObjectConversionFacadeControllerBean
{
    private static Logger logger =
        Logger.getLogger("com.kingdee.eas.custom.tiog.service.bizwebser.ObjectConversionFacadeControllerBean");
}