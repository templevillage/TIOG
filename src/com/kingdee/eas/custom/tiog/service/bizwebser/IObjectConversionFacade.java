package com.kingdee.eas.custom.tiog.service.bizwebser;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.tiog.middleware.bo.OrganizationAndImplementationVO;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.ListofMattersBO;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMakingSystemBO;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMeetingBO;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;

public interface IObjectConversionFacade extends IBizCtrl
{
    public ListofMattersBO convertToListofMattersBO(CategoryListInfo billInfo) throws BOSException, EASBizException;
    public DecisionMakingSystemBO convertToDecisionMakingSystemBO(PoliticalSystemInfo billInfo) throws BOSException, EASBizException;
    public DecisionMeetingBO convertToDecisionMeetingBO(MeetingInfo billInfo) throws BOSException, EASBizException;
    public OrganizationAndImplementationVO convertToOrganizationAndImplementationVO(ExecutionInfo billInfo) throws BOSException, EASBizException;
    public DecisionMeetingBO convertToDecisionMeetingManBO(MeetingManInfo billInfo) throws BOSException, EASBizException;
}