package com.kingdee.eas.custom.tiog.service.bizwebser;

import javax.ejb.*;
import java.rmi.RemoteException;
import com.kingdee.bos.*;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.metadata.IMetaDataPK;
import com.kingdee.bos.metadata.rule.RuleExecutor;
import com.kingdee.bos.metadata.MetaDataPK;
//import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.framework.ejb.AbstractEntityControllerBean;
import com.kingdee.bos.framework.ejb.AbstractBizControllerBean;
//import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.bos.dao.IObjectCollection;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.bos.service.IServiceContext;
import com.kingdee.eas.framework.Result;
import com.kingdee.eas.framework.LineResult;
import com.kingdee.eas.framework.exception.EASMultiException;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;

import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMeetingBO;
import com.kingdee.eas.custom.tiog.middleware.bo.OrganizationAndImplementationVO;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMakingSystemBO;
import com.kingdee.eas.custom.tiog.middleware.bo.ListofMattersBO;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo;



public abstract class AbstractObjectConversionFacadeControllerBean extends AbstractBizControllerBean implements ObjectConversionFacadeController
{
    protected AbstractObjectConversionFacadeControllerBean()
    {
    }

    protected BOSObjectType getBOSType()
    {
        return new BOSObjectType("C95DAB20");
    }

    public ListofMattersBO convertToListofMattersBO(Context ctx, CategoryListInfo billInfo) throws BOSException, EASBizException
    {
        try {
            ServiceContext svcCtx = createServiceContext(new MetaDataPK("aa173feb-1ac0-4887-bfdc-5b36e5c02ec2"), new Object[]{ctx, billInfo});
            invokeServiceBefore(svcCtx);
            if(!svcCtx.invokeBreak()) {
            ListofMattersBO retValue = (ListofMattersBO)_convertToListofMattersBO(ctx, billInfo);
            svcCtx.setMethodReturnValue(retValue);
            }
            invokeServiceAfter(svcCtx);
            return (ListofMattersBO)svcCtx.getMethodReturnValue();
        } catch (BOSException ex) {
            throw ex;
        } catch (EASBizException ex0) {
            throw ex0;
        } finally {
            super.cleanUpServiceState();
        }
    }
    protected ListofMattersBO _convertToListofMattersBO(Context ctx, CategoryListInfo billInfo) throws BOSException, EASBizException
    {    	
        return null;
    }

    public DecisionMakingSystemBO convertToDecisionMakingSystemBO(Context ctx, PoliticalSystemInfo billInfo) throws BOSException, EASBizException
    {
        try {
            ServiceContext svcCtx = createServiceContext(new MetaDataPK("ab5b7c62-f27f-43ce-b493-48f1d84da850"), new Object[]{ctx, billInfo});
            invokeServiceBefore(svcCtx);
            if(!svcCtx.invokeBreak()) {
            DecisionMakingSystemBO retValue = (DecisionMakingSystemBO)_convertToDecisionMakingSystemBO(ctx, billInfo);
            svcCtx.setMethodReturnValue(retValue);
            }
            invokeServiceAfter(svcCtx);
            return (DecisionMakingSystemBO)svcCtx.getMethodReturnValue();
        } catch (BOSException ex) {
            throw ex;
        } catch (EASBizException ex0) {
            throw ex0;
        } finally {
            super.cleanUpServiceState();
        }
    }
    protected DecisionMakingSystemBO _convertToDecisionMakingSystemBO(Context ctx, PoliticalSystemInfo billInfo) throws BOSException, EASBizException
    {    	
        return null;
    }

    public DecisionMeetingBO convertToDecisionMeetingBO(Context ctx, MeetingInfo billInfo) throws BOSException, EASBizException
    {
        try {
            ServiceContext svcCtx = createServiceContext(new MetaDataPK("aa311b49-cbd3-4eb0-ba8c-18511c4a0722"), new Object[]{ctx, billInfo});
            invokeServiceBefore(svcCtx);
            if(!svcCtx.invokeBreak()) {
            DecisionMeetingBO retValue = (DecisionMeetingBO)_convertToDecisionMeetingBO(ctx, billInfo);
            svcCtx.setMethodReturnValue(retValue);
            }
            invokeServiceAfter(svcCtx);
            return (DecisionMeetingBO)svcCtx.getMethodReturnValue();
        } catch (BOSException ex) {
            throw ex;
        } catch (EASBizException ex0) {
            throw ex0;
        } finally {
            super.cleanUpServiceState();
        }
    }
    protected DecisionMeetingBO _convertToDecisionMeetingBO(Context ctx, MeetingInfo billInfo) throws BOSException, EASBizException
    {    	
        return null;
    }

    public OrganizationAndImplementationVO convertToOrganizationAndImplementationVO(Context ctx, ExecutionInfo billInfo) throws BOSException, EASBizException
    {
        try {
            ServiceContext svcCtx = createServiceContext(new MetaDataPK("d8a73900-d514-4702-818c-b7ec28ac7bad"), new Object[]{ctx, billInfo});
            invokeServiceBefore(svcCtx);
            if(!svcCtx.invokeBreak()) {
            OrganizationAndImplementationVO retValue = (OrganizationAndImplementationVO)_convertToOrganizationAndImplementationVO(ctx, billInfo);
            svcCtx.setMethodReturnValue(retValue);
            }
            invokeServiceAfter(svcCtx);
            return (OrganizationAndImplementationVO)svcCtx.getMethodReturnValue();
        } catch (BOSException ex) {
            throw ex;
        } catch (EASBizException ex0) {
            throw ex0;
        } finally {
            super.cleanUpServiceState();
        }
    }
    protected OrganizationAndImplementationVO _convertToOrganizationAndImplementationVO(Context ctx, ExecutionInfo billInfo) throws BOSException, EASBizException
    {    	
        return null;
    }

    public DecisionMeetingBO convertToDecisionMeetingManBO(Context ctx, MeetingManInfo billInfo) throws BOSException, EASBizException
    {
        try {
            ServiceContext svcCtx = createServiceContext(new MetaDataPK("13c47f26-0e57-4e8a-a051-d5711c7f8a90"), new Object[]{ctx, billInfo});
            invokeServiceBefore(svcCtx);
            if(!svcCtx.invokeBreak()) {
            DecisionMeetingBO retValue = (DecisionMeetingBO)_convertToDecisionMeetingManBO(ctx, billInfo);
            svcCtx.setMethodReturnValue(retValue);
            }
            invokeServiceAfter(svcCtx);
            return (DecisionMeetingBO)svcCtx.getMethodReturnValue();
        } catch (BOSException ex) {
            throw ex;
        } catch (EASBizException ex0) {
            throw ex0;
        } finally {
            super.cleanUpServiceState();
        }
    }
    protected DecisionMeetingBO _convertToDecisionMeetingManBO(Context ctx, MeetingManInfo billInfo) throws BOSException, EASBizException
    {    	
        return null;
    }

}