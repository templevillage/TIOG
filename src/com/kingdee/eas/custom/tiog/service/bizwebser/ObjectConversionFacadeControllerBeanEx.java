package com.kingdee.eas.custom.tiog.service.bizwebser;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMakingSystemBO;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMeetingBO;
import com.kingdee.eas.custom.tiog.middleware.bo.ListofMattersBO;
import com.kingdee.eas.custom.tiog.middleware.bo.OrganizationAndImplementationVO;
import com.kingdee.eas.custom.tiog.service.bizwebser.util.CategoryListDataUtil;
import com.kingdee.eas.custom.tiog.service.bizwebser.util.ExecutionDataUtil;
import com.kingdee.eas.custom.tiog.service.bizwebser.util.MeetingDataUtil;
import com.kingdee.eas.custom.tiog.service.bizwebser.util.PoliticalSystemDataUtil;

/**
 * 
 * @title ObjectConversionFacadeControllerBeanEx
 * @description 中间层对象转换Facade
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-24
 */
public class ObjectConversionFacadeControllerBeanEx extends
        com.kingdee.eas.custom.tiog.service.bizwebser.ObjectConversionFacadeControllerBean {
    private static Logger logger = Logger
            .getLogger("com.kingdee.eas.custom.tiog.service.bizwebser.ObjectConversionFacadeControllerBeanEx");

    /**
     * （非 Javadoc）
     * 
     * @title _convertToListofMattersBO
     * @description EAS事项清单对象转换成事项清单中间层对象
     * @param ctx 上下文
     * @param billInfo EAS事项清单单据对象
     * @return ListofMattersBO 事项清单中间层对象
     * @throws BOSException
     * @throws EASBizException
     * @see com.kingdee.eas.custom.tiog.service.bizwebser.AbstractObjectConversionFacadeControllerBean#_convertToListofMattersBO(com.kingdee.bos.Context,
     *      com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo)
     */
    protected ListofMattersBO _convertToListofMattersBO(Context ctx, CategoryListInfo billInfo)
            throws BOSException, EASBizException {
        if (billInfo == null) {
            throw new EASBizException(EASBizException.CHECKBLANK,
                    new Object[] { "事项清单 CategoryListInfo 为空！" });
        }

        return CategoryListDataUtil.convertInfo(billInfo);
    }

    /**
     * （非 Javadoc）
     * 
     * @title _convertToDecisionMakingSystemBO
     * @description EAS决策制度单据对象转换决策制度中间层对象
     * @param ctx 上下文
     * @param billInfo EAS决策制度单据对象
     * @return DecisionMakingSystemBO 决策制度中间层对象
     * @throws BOSException
     * @throws EASBizException
     * @see com.kingdee.eas.custom.tiog.service.bizwebser.AbstractObjectConversionFacadeControllerBean#_convertToDecisionMakingSystemBO(com.kingdee.bos.Context,
     *      com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo)
     */
    protected DecisionMakingSystemBO _convertToDecisionMakingSystemBO(Context ctx,
            PoliticalSystemInfo billInfo) throws BOSException, EASBizException {
        if (billInfo == null) {
            throw new EASBizException(EASBizException.CHECKBLANK,
                    new Object[] { "决策制度 PoliticalSystemInfo 为空！" });
        }
        return PoliticalSystemDataUtil.convertInfo(billInfo);
    }

    /**
     * （非 Javadoc）
     * 
     * @title _convertToDecisionMeetingManBO
     * @description EAS决策会议单据对象转换决策会议中间层对象
     * @param ctx 上下文
     * @param billInfo EAS决策会议单据对象
     * @return DecisionMeetingBO 决策会议中间层对象
     * @throws BOSException
     * @throws EASBizException
     * @see com.kingdee.eas.custom.tiog.service.bizwebser.AbstractObjectConversionFacadeControllerBean#_convertToDecisionMeetingManBO(com.kingdee.bos.Context,
     *      com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo)
     */
    @Override
    protected DecisionMeetingBO _convertToDecisionMeetingManBO(Context ctx, MeetingManInfo billInfo)
            throws BOSException, EASBizException {
        if (billInfo == null) {
            throw new EASBizException(EASBizException.CHECKBLANK, new Object[] { "决策会议 MeetingInfo 为空！" });
        }

        return MeetingDataUtil.convertInfo(ctx, billInfo);
    }

    /**
     * （非 Javadoc）
     * 
     * @title _convertToOrganizationAndImplementationVO
     * @description EAS组织实施单据对象转换组织实施中间层对象
     * @param ctx 上下文
     * @param billInfo EAS组织实施单据对象
     * @return 转换决策会议中间层对象
     * @throws BOSException
     * @throws EASBizException
     * @see com.kingdee.eas.custom.tiog.service.bizwebser.AbstractObjectConversionFacadeControllerBean#_convertToOrganizationAndImplementationVO(com.kingdee.bos.Context,
     *      com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo)
     */
    protected OrganizationAndImplementationVO _convertToOrganizationAndImplementationVO(Context ctx,
            ExecutionInfo billInfo) throws BOSException, EASBizException {
        if (billInfo == null) {
            throw new EASBizException(EASBizException.CHECKBLANK, new Object[] { "组织实施 ExecutionInfo 为空！" });
        }
        return ExecutionDataUtil.convertInfo(billInfo);
    }

}
