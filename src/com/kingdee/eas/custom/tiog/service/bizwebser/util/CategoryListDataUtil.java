package com.kingdee.eas.custom.tiog.service.bizwebser.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryDEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryDEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.ListofMattersBO;
import com.kingdee.eas.custom.tiog.middleware.vo.ItemMeetingVO;
import com.kingdee.eas.custom.tiog.middleware.vo.ItemVO;
import com.kingdee.eas.custom.tiog.utils.BooleanUtil;
import com.kingdee.eas.custom.tiog.utils.DateUtil;

/**
 * 
 * @title CategoryListDataUtil
 * @description 事项清单工具类
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-25
 */
public class CategoryListDataUtil {
    private static Logger logger = Logger
            .getLogger("com.kingdee.eas.custom.tiog.service.bizwebser.util.CategoryListDataUtil");

    /**
     * 
     * @title convertInfo
     * @description EAS事项清单对象转换成事项清单中间层对象
     * @param billInfo EAS事项清单
     * @return 事项清单中间层对象
     * @throws BOSException
     */
    public static ListofMattersBO convertInfo(CategoryListInfo billInfo) throws BOSException {
        ListofMattersBO info = new ListofMattersBO(); // 中间层对象

        String billId = billInfo.getId().toString();// 单据内码
        String companyName = billInfo.getCompany().getName();// 企业名称
        String companyNumber = billInfo.getCompanyNumber();// 企业编码
        String categoryListName = billInfo.getCategoryListName();// 事项清单名称
        String categoryLisNnumber = billInfo.getNumber();// 事项清单编码
        String listVersion = billInfo.getListVersion();// 版本号
        Date effectiveTS = billInfo.getEffectiveTS();// 生效日期
        Date invalidTS = billInfo.getInvalidTS();// 失效日期
        String description = billInfo.getDescription();// 备注
        String source = billInfo.getSource().getName();// 数据来源
        String operType = billInfo.getOperType().getName();// 操作标识

        info.setList_id(billId);
        info.setCompany_name(companyName);
        info.setCompany_id(companyNumber);
        info.setList_name(categoryListName);
        info.setList_version(listVersion);
        try {
            info.setEffective_date(DateUtil.formatYMD(effectiveTS));
            info.setInvalid_date(DateUtil.formatYMD(invalidTS));
        } catch (ParseException e) {
            e.printStackTrace();
            logger.error("com.kingdee.eas.custom.tiog.utils.DateUtil.formatYMD() 日期转字符串失败！", e);
            throw new BOSException(e.getMessage(), e);
        }
        info.setRemark(description);
        info.setSource(source);
        info.setOper_type(operType);
        info.setItem_list(listItemVO(billInfo)); // 事项清单列表

        return info;
    }

    /**
     * 
     * @title listItem
     * @description 获取事项清单分录中事项列表(中间层对象)
     * @param billInfo EAS事项清单
     * @return
     */
    private static List<ItemVO> listItemVO(CategoryListInfo billInfo) {
        // 事项清单列表
        List<ItemVO> itemVOList = new ArrayList<ItemVO>();
        CategoryListEntryCollection entrys = billInfo.getEntrys();
        if (entrys != null && entrys.size() > 0) {
            for (int i = 0; i < entrys.size(); i++) {
                ItemVO itemVO = new ItemVO();

                CategoryListEntryInfo entryInfo = entrys.get(0); // 事项清单
                String entryId = entryInfo.getId().toString();

                String entryCatalog = entryInfo.getCatalogMain().getName();// 事项名称
                String entryCatalogNumber = entryInfo.getCatalogMain().getNumber();// 事项编码
                boolean legalFlag = entryInfo.isLegalFlag();// 是否经法律审核
                String entryOperType = entryInfo.getOperType().getName();// 操作标识
                String entryRemark = entryInfo.getRemark();// 备注

                itemVO.setItem_id(entryId);
                itemVO.setItem_name(entryCatalog);
                itemVO.setItem_code(entryCatalogNumber);
                itemVO.setLegal_flag(BooleanUtil.booleanConvertStr(legalFlag));
                itemVO.setRemark(entryRemark);
                itemVO.setOper_type(entryOperType);
                itemVO.setItem_meeting_list(listMeetingVO(entryInfo)); // 决策会议及顺序

                itemVOList.add(itemVO);
            }
        }
        return itemVOList;
    }

    /**
     * 
     * @title listMeetingVO
     * @description 获取事项对应的会议列表（中间层对象）
     * @param entryInfo 事项清单分录对象
     * @return List<ItemMeetingVO>
     */
    private static List<ItemMeetingVO> listMeetingVO(CategoryListEntryInfo entryInfo) {
        List<ItemMeetingVO> itemMeetingList = new ArrayList<ItemMeetingVO>();
        CategoryListEntryDEntryCollection dentrys = entryInfo.getDEntrys();
        if (dentrys != null && dentrys.size() > 0) {
            for (int j = 0; j < dentrys.size(); j++) {
                ItemMeetingVO itemMeetingVO = new ItemMeetingVO();// 决策会议

                CategoryListEntryDEntryInfo dentryInfo = dentrys.get(0);// 会议安排
                String denntryId = dentryInfo.getId().toString();
                String meetName = dentryInfo.getMeetingType().getName();// 会议类型
                String meetingTypeNumber = dentryInfo.getMeetingTypeNumber();// 会议类型编码
                String meetingDes = dentryInfo.getMeetingDes();// 会议说明
                String meetRemark = dentryInfo.getRemark();// 备注

                itemMeetingVO.setType_name(meetName);
                itemMeetingVO.setType_code(meetingTypeNumber);
                itemMeetingList.add(itemMeetingVO);
            }
        }
        return itemMeetingList;
    }

}
