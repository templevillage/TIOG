package com.kingdee.eas.custom.tiog.service.bizwebser;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import java.util.List;
import java.lang.String;
import com.kingdee.eas.custom.tiog.service.bizwebser.*;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;

public class AttachmentDownloadFacade extends AbstractBizCtrl implements IAttachmentDownloadFacade
{
    public AttachmentDownloadFacade()
    {
        super();
        registerInterface(IAttachmentDownloadFacade.class, this);
    }
    public AttachmentDownloadFacade(Context ctx)
    {
        super(ctx);
        registerInterface(IAttachmentDownloadFacade.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("5FF05B76");
    }
    private AttachmentDownloadFacadeController getController() throws BOSException
    {
        return (AttachmentDownloadFacadeController)getBizController();
    }
    /**
     *下载附件-User defined method
     *@param billId 单据内码
     *@return
     */
    public List download(String billId) throws BOSException, EASBizException
    {
        try {
            return getController().download(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}