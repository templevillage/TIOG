package com.kingdee.eas.custom.tiog.service.bizwebser;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.tiog.middleware.bo.OrganizationAndImplementationVO;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.ListofMattersBO;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMakingSystemBO;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMeetingBO;
import com.kingdee.eas.custom.tiog.service.bizwebser.*;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;

public class ObjectConversionFacade extends AbstractBizCtrl implements IObjectConversionFacade
{
    public ObjectConversionFacade()
    {
        super();
        registerInterface(IObjectConversionFacade.class, this);
    }
    public ObjectConversionFacade(Context ctx)
    {
        super(ctx);
        registerInterface(IObjectConversionFacade.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("C95DAB20");
    }
    private ObjectConversionFacadeController getController() throws BOSException
    {
        return (ObjectConversionFacadeController)getBizController();
    }
    /**
     *转换成事项清单中间层对象-User defined method
     *@param billInfo 单据对象
     *@return
     */
    public ListofMattersBO convertToListofMattersBO(CategoryListInfo billInfo) throws BOSException, EASBizException
    {
        try {
            return getController().convertToListofMattersBO(getContext(), billInfo);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *转换决策制度中间层对象-User defined method
     *@param billInfo 单据对象
     *@return
     */
    public DecisionMakingSystemBO convertToDecisionMakingSystemBO(PoliticalSystemInfo billInfo) throws BOSException, EASBizException
    {
        try {
            return getController().convertToDecisionMakingSystemBO(getContext(), billInfo);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *转换决策会议中间层对象 废弃-User defined method
     *@param billInfo 单据对象
     *@return
     */
    public DecisionMeetingBO convertToDecisionMeetingBO(MeetingInfo billInfo) throws BOSException, EASBizException
    {
        try {
            return getController().convertToDecisionMeetingBO(getContext(), billInfo);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *转换组织实施中间层对象-User defined method
     *@param billInfo 单据对象
     *@return
     */
    public OrganizationAndImplementationVO convertToOrganizationAndImplementationVO(ExecutionInfo billInfo) throws BOSException, EASBizException
    {
        try {
            return getController().convertToOrganizationAndImplementationVO(getContext(), billInfo);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *转换决策会议中间层对象-User defined method
     *@param billInfo 单据对象
     *@return
     */
    public DecisionMeetingBO convertToDecisionMeetingManBO(MeetingManInfo billInfo) throws BOSException, EASBizException
    {
        try {
            return getController().convertToDecisionMeetingManBO(getContext(), billInfo);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}