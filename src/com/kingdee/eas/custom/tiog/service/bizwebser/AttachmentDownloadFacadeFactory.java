package com.kingdee.eas.custom.tiog.service.bizwebser;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AttachmentDownloadFacadeFactory
{
    private AttachmentDownloadFacadeFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.service.bizwebser.IAttachmentDownloadFacade getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.bizwebser.IAttachmentDownloadFacade)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("5FF05B76") ,com.kingdee.eas.custom.tiog.service.bizwebser.IAttachmentDownloadFacade.class);
    }
    
    public static com.kingdee.eas.custom.tiog.service.bizwebser.IAttachmentDownloadFacade getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.bizwebser.IAttachmentDownloadFacade)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("5FF05B76") ,com.kingdee.eas.custom.tiog.service.bizwebser.IAttachmentDownloadFacade.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.service.bizwebser.IAttachmentDownloadFacade getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.bizwebser.IAttachmentDownloadFacade)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("5FF05B76"));
    }
    public static com.kingdee.eas.custom.tiog.service.bizwebser.IAttachmentDownloadFacade getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.bizwebser.IAttachmentDownloadFacade)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("5FF05B76"));
    }
}