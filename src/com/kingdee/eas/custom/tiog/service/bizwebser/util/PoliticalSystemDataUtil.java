package com.kingdee.eas.custom.tiog.service.bizwebser.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMakingSystemBO;
import com.kingdee.eas.custom.tiog.middleware.vo.VoteModeVO;
import com.kingdee.eas.custom.tiog.utils.BooleanUtil;
import com.kingdee.eas.custom.tiog.utils.DateUtil;

/**
 * 
 * @title PoliticalSystemDataUtil
 * @description 决策制度工具类
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-25
 */
public class PoliticalSystemDataUtil {
    private static Logger logger = Logger
            .getLogger("com.kingdee.eas.custom.tiog.service.bizwebser.util.PoliticalSystemDataUtil");

    /**
     * 
     * @title convertInfo
     * @description EAS决策制度转换决策制度中间层对象
     * @param billInfo
     * @return
     * @throws BOSException
     */
    public static DecisionMakingSystemBO convertInfo(PoliticalSystemInfo billInfo) throws BOSException {
        DecisionMakingSystemBO decisionMakingSystemVO = new DecisionMakingSystemBO(); // 中间层对象

        String id = billInfo.getId().toString();
        String companyName = billInfo.getCompany().getName();// 企业名称
        String companyNumber = billInfo.getCompanyNumber();// 企业编码
        String politicalSystemName = billInfo.getPoliticalSystemName();// 制度名称
        String regulationType = billInfo.getRegulationType().getName();// 制度类型名称
        Date auditTS = billInfo.getAuditTS();// 审批日期
        Date effectDate = billInfo.getEffectDate();// 生效日期
        Date expireDate = billInfo.getExpireDate();// 失效日期
        boolean isReview = billInfo.isIsReview();// 是否经过合法性审查
        String meetTypeName = billInfo.getMeetNum().getName();// 会议类型名称
        String meetTypeNumber = billInfo.getMeetNum().getNumber();// 会议类型编码

        String source = billInfo.getDataSource().getName();// 数据来源
        String operType = billInfo.getOperType().getName();// 操作标识
        String description = billInfo.getDescription();// 备注

        decisionMakingSystemVO.setRegulation_id(id);
        decisionMakingSystemVO.setCompany_name(companyName);
        decisionMakingSystemVO.setCompany_id(companyNumber);
        decisionMakingSystemVO.setRegulation_name(politicalSystemName);
        decisionMakingSystemVO.setRegulation_type_name(regulationType);
        try {
            decisionMakingSystemVO.setApproval_date(DateUtil.formatYMD(auditTS));
            decisionMakingSystemVO.setEffective_date(DateUtil.formatYMD(effectDate));
            decisionMakingSystemVO.setInvalid_date(DateUtil.formatYMD(expireDate));
        } catch (ParseException e) {
            e.printStackTrace();
            logger.error("com.kingdee.eas.custom.tiog.utils.DateUtil.formatYMD() 日期转字符串失败！", e);
            throw new BOSException(e.getMessage(), e);
        }
        decisionMakingSystemVO.setAudit_flag(BooleanUtil.booleanConvertStr(isReview));
        decisionMakingSystemVO.setMeeting_type_code(meetTypeNumber);
        decisionMakingSystemVO.setVote_mode_list(listVoteModeVO(billInfo)); // 表决方式列表
        decisionMakingSystemVO.setRemark(description);
        decisionMakingSystemVO.setSource(source);
        decisionMakingSystemVO.setOper_type(operType);

        return decisionMakingSystemVO;
    }

    /**
     * 
     * @title listVoteModeVO
     * @description 决策制度获取分录 表决方式列表 (中间层对象)
     * @param billInfo d
     * @return List<VoteModeVO>
     */
    private static List<VoteModeVO> listVoteModeVO(PoliticalSystemInfo billInfo) {
        List<VoteModeVO> voteModeVOList = new ArrayList<VoteModeVO>();
        PoliticalSystemEntryCollection entrys = billInfo.getEntrys();
        if (entrys != null && entrys.size() > 0) {
            for (int i = 0; i < entrys.size(); i++) {
                VoteModeVO voteModeVO = new VoteModeVO();

                PoliticalSystemEntryInfo entryInfo = entrys.get(i);
                String entryId = entryInfo.getId().toString();
                String catalogName = entryInfo.getCatalogMain().getName();// 事项名称
                String catalogNumber = entryInfo.getCatalogMain().getNumber();// 事项编码
                String rate = entryInfo.getRate().getName();// 人数占比
                String voteModeName = entryInfo.getVoteMode().getName();// 表决方式

                voteModeVO.setItem_code(catalogNumber);
                voteModeVO.setRate(rate);
                voteModeVO.setVote_mode(voteModeName);
                voteModeVOList.add(voteModeVO);
            }
        }
        return voteModeVOList;
    }

}
