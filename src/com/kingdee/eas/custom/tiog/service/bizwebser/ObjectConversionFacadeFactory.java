package com.kingdee.eas.custom.tiog.service.bizwebser;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class ObjectConversionFacadeFactory
{
    private ObjectConversionFacadeFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.service.bizwebser.IObjectConversionFacade getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.bizwebser.IObjectConversionFacade)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("C95DAB20") ,com.kingdee.eas.custom.tiog.service.bizwebser.IObjectConversionFacade.class);
    }
    
    public static com.kingdee.eas.custom.tiog.service.bizwebser.IObjectConversionFacade getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.bizwebser.IObjectConversionFacade)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("C95DAB20") ,com.kingdee.eas.custom.tiog.service.bizwebser.IObjectConversionFacade.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.service.bizwebser.IObjectConversionFacade getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.bizwebser.IObjectConversionFacade)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("C95DAB20"));
    }
    public static com.kingdee.eas.custom.tiog.service.bizwebser.IObjectConversionFacade getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.bizwebser.IObjectConversionFacade)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("C95DAB20"));
    }
}