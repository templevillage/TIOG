package com.kingdee.eas.custom.tiog.service.bizwebser.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kingdee.eas.custom.tiog.bizbill.ExecutionEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.OrganizationAndImplementationVO;
import com.kingdee.eas.custom.tiog.middleware.vo.DutyVO;

/**
 * 
 * @title ExecutionDataUtil
 * @description 组织实施工具类
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-25
 */
public class ExecutionDataUtil {

    private static Logger logger = Logger
            .getLogger("com.kingdee.eas.custom.tiog.service.bizwebser.util.ExecutionDataUtil");

    /**
     * 
     * @title convertInfo
     * @description EAS组织实施单据对象转换组织实施中间层对象
     * @param billInfo EAS组织实施单据对象
     * @return 组织实施中间层对象
     */
    public static OrganizationAndImplementationVO convertInfo(ExecutionInfo billInfo) {
        OrganizationAndImplementationVO organizationAndImplementationVO = new OrganizationAndImplementationVO(); // 中间层对象

        String id = billInfo.getId().toString();
        String companyName = billInfo.getCompany().getName();// 企业名称
        String companyNumber = billInfo.getCompanyNumber();// 企业编码
        String subjectName = billInfo.getSubject().getSubjectName();// 议题名称
        String subjectNumber = billInfo.getSubjectNumber();// 议题编码
        String effect = billInfo.getEffect();// 预期成效
        String state = billInfo.getState().getName();// 实施状态
        String remark = billInfo.getRemark();// 备注
        String source = billInfo.getDataSource().getName();// 数据来源
        String operType = billInfo.getOperType().getName();// 操作标识

        organizationAndImplementationVO.setExecution_id(id);
        organizationAndImplementationVO.setCompany_name(companyName);
        organizationAndImplementationVO.setCompany_id(companyNumber);
        organizationAndImplementationVO.setSubject_code(subjectNumber);
        organizationAndImplementationVO.setEffect(effect);
        organizationAndImplementationVO.setStatus(state);
        organizationAndImplementationVO.setDescription(remark);
        organizationAndImplementationVO.setSource(source);
        organizationAndImplementationVO.setOper_type(operType);
        organizationAndImplementationVO.setDuty_list(ExecutionDataUtil.listDytyVo(billInfo));

        return organizationAndImplementationVO;
    }

    /**
     * 
     * @title listDytyVo
     * @description 获取组织实施分录中的落实责任信息列表（中间层对象）
     * @param billInfo 组织实施单据对象
     * @return List<DutyVO>
     */
    private static List<DutyVO> listDytyVo(ExecutionInfo billInfo) {
        List<DutyVO> dutyVOList = new ArrayList<DutyVO>(); // 落实责任信息列表
        ExecutionEntryCollection entrys = billInfo.getEntrys();
        if (entrys != null && entrys.size() > 0) {
            for (int i = 0; i < entrys.size(); i++) {
                DutyVO dutyVO = new DutyVO();
                ExecutionEntryInfo entryInfo = entrys.get(i);
                String departName = entryInfo.getResponseDept().getName();// 落实责任部门
                String personName = entryInfo.getResponsePer().getName(); // 责任人

                dutyVO.setDept(departName);
                dutyVO.setName(personName);
                dutyVOList.add(dutyVO);
            }
        }
        return dutyVOList;
    }

}
