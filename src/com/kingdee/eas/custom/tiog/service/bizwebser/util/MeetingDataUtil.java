package com.kingdee.eas.custom.tiog.service.bizwebser.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.tiog.bizbill.ISubject;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManMeetingManAttendeeEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManMeetingManAttendeeEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectAttendanceEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectDeliberationEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectFactory;
import com.kingdee.eas.custom.tiog.bizbill.SubjectInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectRefSubjectEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.SubjectRefSubjectEntryInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMeetingBO;
import com.kingdee.eas.custom.tiog.middleware.vo.AttendanceVO;
import com.kingdee.eas.custom.tiog.middleware.vo.AttendeeVO;
import com.kingdee.eas.custom.tiog.middleware.vo.DeliberationVO;
import com.kingdee.eas.custom.tiog.middleware.vo.ItemVO;
import com.kingdee.eas.custom.tiog.middleware.vo.RelationVO;
import com.kingdee.eas.custom.tiog.middleware.vo.SubjectVO;
import com.kingdee.eas.custom.tiog.utils.BooleanUtil;
import com.kingdee.eas.custom.tiog.utils.DataConvertUtil;
import com.kingdee.eas.custom.tiog.utils.DateUtil;

/**
 * 
 * @title MeetingDataUtil
 * @description 决策会议工具类
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-25
 */
public class MeetingDataUtil {
    private static Logger logger = Logger
            .getLogger("com.kingdee.eas.custom.tiog.service.bizwebser.util.MeetingDataUtil");

    /**
     * 
     * @title convertInfo
     * @description EAS决策会议单据对象转决策会议中间层对象
     * @param ctx 上下文
     * @param billInfo 决策会议单据对象
     * @return 会议中间层对象
     * @throws BOSException
     * @throws EASBizException
     */
    public static DecisionMeetingBO convertInfo(Context ctx, MeetingManInfo billInfo) throws BOSException,
            EASBizException {
        DecisionMeetingBO decisionMeetingBO = new DecisionMeetingBO();// 中间层对象

        String id = billInfo.getId().toString();
        String company = billInfo.getCompany().getName();// 企业名称
        String companyNumber = billInfo.getCompanyNumber();// 企业编码
        String meetingName = billInfo.getMeetingName();// 会议名称
        String meetingNumber = billInfo.getNumber();// 会议编码
        String meetType = billInfo.getMeetingType().getName();// 会议类型
        String meetTypeNumber = billInfo.getMeetingType().getNumber();// 会议类型编码
        String meetForm = billInfo.getMeetingForm().getName();// 会议形式
        Date meetingTS = billInfo.getMeetingTS();// 会议时间
        Date releaseTS = billInfo.getReleaseTS();// 会议纪要发送时间
        String moderator = billInfo.getModerator().getName();// 主持人
        String remark = billInfo.getRemark();// 备注
        String source = billInfo.getDataSource().getName();// 数据来源
        String operType = billInfo.getOperType().getName();// 操作标识

        decisionMeetingBO.setMeeting_id(id);
        decisionMeetingBO.setCompany_name(company);
        decisionMeetingBO.setCompany_id(companyNumber);
        decisionMeetingBO.setMeeting_name(meetingName);
        decisionMeetingBO.setMeeting_code(meetingNumber);
        decisionMeetingBO.setMeeting_type_name(meetType);
        decisionMeetingBO.setMeeting_type_code(meetTypeNumber);
        decisionMeetingBO.setMeeting_form(meetForm);
        try {
            decisionMeetingBO.setMeeting_time(DateUtil.formatYMD(meetingTS));
            decisionMeetingBO.setRelease_time(DateUtil.formatYMD(releaseTS));
        } catch (ParseException e) {
            e.printStackTrace();
            logger.error("com.kingdee.eas.custom.tiog.utils.DateUtil.formatYMD() 日期转字符串失败！", e);
            throw new BOSException(e.getMessage(), e);
        }
        decisionMeetingBO.setModerator(moderator);
        decisionMeetingBO.setRemark(remark);
        decisionMeetingBO.setSource(source);
        decisionMeetingBO.setOper_type(operType);
        decisionMeetingBO.setAttendee_list(listAttendeeVO(billInfo)); // 参会人列表
        decisionMeetingBO.setSubject_list(listSubjectVO(ctx, billInfo));// 议题列表
        return decisionMeetingBO;
    }

    /**
     * 
     * @title listSubjectVO
     * @description EAS决策会议议题分录转议题中间层对象列表
     * @param ctx 上下文
     * @param billInfo EAS决策会议议题
     * @return 议题中间层对象列表
     * @throws BOSException
     * @throws EASBizException
     */
    public static List<SubjectVO> listSubjectVO(Context ctx, MeetingManInfo billInfo) throws BOSException,
            EASBizException {
        List<SubjectVO> subjectVOArrayList = new ArrayList<SubjectVO>();// 议题列表
        MeetingManSubjectEntryCollection subjectEntrys = billInfo.getSubjectEntry();
        if (subjectEntrys != null && subjectEntrys.size() > 0) {
            for (int i = 0; i < subjectEntrys.size(); i++) {
                SubjectVO subjectVO = new SubjectVO();
                MeetingManSubjectEntryInfo subjectEntryInfo = subjectEntrys.get(i);
                String subjectId = subjectEntryInfo.getSubject().getId().toString(); // 议题

                ISubject ISubject = SubjectFactory.getLocalInstance(ctx);
                SubjectInfo subjectInfo = ISubject.getSubjectInfo(new ObjectUuidPK(subjectId)); // 审议议题
                String subjectName = subjectInfo.getSubjectName();// 议题名称
                String subjectNum = subjectInfo.getSubjectNum();// 议题编码
                String taskSource = subjectInfo.getTaskSource().getName();// 任务来源
                String special = subjectInfo.getSpecial().getName();// 专项名称
                String pass = subjectInfo.getPassFlag().getName();// 是否通过
                String approve = subjectInfo.getApprovalFlag().getName();// 是否报国资委审批
                String subjectResult = subjectInfo.getSubjectResult();// 议题决议
                boolean superviseFlag = subjectInfo.isSuperviseFlag();// 是否需要督办
                boolean adoptFlag = subjectInfo.isAdoptFlag();// 是否听取意见
                String subjectOperType = subjectInfo.getOperType().getName();// 操作标识
                String subjectRemark = subjectInfo.getRemark();// 议题备注

                subjectVO.setSubject_id(subjectId);
                subjectVO.setSubject_name(subjectName);
                subjectVO.setSubject_code(subjectNum);
                subjectVO.setSource_name(taskSource);
                subjectVO.setSpecial_name(special);
                subjectVO.setItem_list((String[]) listItemVO(subjectInfo).toArray()); // 议题事项
                subjectVO.setRelation_list(listRelationVO(ctx, subjectInfo)); // 关联议题
                subjectVO.setAttendance_list(listAttendanceVO(subjectEntryInfo)); // 列席情况列表
                subjectVO.setDeliberation_list(listDeliberationVO(subjectEntryInfo));// 审议情况列表
                subjectVO.setPass_flag(pass);
                subjectVO.setApproval_flag(approve);
                subjectVO.setAdopt_flag(BooleanUtil.booleanConvertStr(adoptFlag));
                subjectVO.setSubject_result(subjectResult);
                subjectVO.setSupervise_flag(BooleanUtil.booleanConvertStr(superviseFlag)); // 是否需要督办
                subjectVO.setOper_type(subjectOperType);
                subjectVO.setRemark(subjectRemark);

                subjectVOArrayList.add(subjectVO);
            }
        }
        return subjectVOArrayList;
    }

    /**
     * 
     * @title listDeliberationVO
     * @description 获取EAS会议决策议题的审议情况分录转审议结果中间层对象列表
     * @param subjectEntryInfo 审议情况分录
     * @return 审议结果中间层对象列表
     */
    private static List<DeliberationVO> listDeliberationVO(MeetingManSubjectEntryInfo subjectEntryInfo) {
        List<DeliberationVO> deliberationVOList = new ArrayList<DeliberationVO>();// 审议情况列表
        MeetingManSubjectEntryMeetingManSubjectDeliberationEntryCollection manSubjectDeliberationEntry = subjectEntryInfo
                .getMeetingManSubjectDeliberationEntry();
        if (manSubjectDeliberationEntry != null && manSubjectDeliberationEntry.size() > 0) {
            for (int t = 0; t < manSubjectDeliberationEntry.size(); t++) {
                DeliberationVO deliberationVO = new DeliberationVO();
                MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo entryInfo = manSubjectDeliberationEntry
                        .get(t);
                String personName = entryInfo.getDeliberator().getName();// 审议人
                String result = entryInfo.getDeliberationResult();// 审议结果

                deliberationVO.setDeliberation_personnel(personName);
                deliberationVO.setDeliberation_result(result);
                deliberationVOList.add(deliberationVO);
            }
        }
        return deliberationVOList;
    }

    /**
     * 
     * @title listAttendanceVO
     * @description 获取EAS会议决策议题的列席情况分录转列席人情况中间层对象列表
     * @param subjectEntryInfo 列席情况分录
     * @return 列席人情况中间层对象列表
     */
    private static List<AttendanceVO> listAttendanceVO(MeetingManSubjectEntryInfo subjectEntryInfo) {
        List<AttendanceVO> attendanceVOList = new ArrayList<AttendanceVO>();// 列席情况列表
        MeetingManSubjectEntryMeetingManSubjectAttendanceEntryCollection manSubjectAttendanceEntry = subjectEntryInfo
                .getMeetingManSubjectAttendanceEntry();
        if (manSubjectAttendanceEntry != null && manSubjectAttendanceEntry.size() > 0) {
            for (int j = 0; j < manSubjectAttendanceEntry.size(); j++) {
                AttendanceVO attendanceVO = new AttendanceVO();
                MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo entryInfo = manSubjectAttendanceEntry
                        .get(j);
                String personName = entryInfo.getAttendance().getName();// 列席人
                String positionName = entryInfo.getAttendancePos().getName();// 职位

                attendanceVO.setAttendance_name(personName);
                attendanceVO.setPosition(positionName);
                attendanceVOList.add(attendanceVO);
            }
        }
        return attendanceVOList;
    }

    /**
     * 
     * @title listAttendeeVO
     * @description 获取EAS决策会议分录参会人分录转参会人中间层对象列表
     * @param billInfo EAS决策会议单据
     * @return 参会人中间层对象列表
     */
    private static List<AttendeeVO> listAttendeeVO(MeetingManInfo billInfo) {
        List<AttendeeVO> attendeeVOList = new ArrayList<AttendeeVO>(); // 参会人列表
        MeetingManMeetingManAttendeeEntryCollection attendeeEntrys = billInfo.getMeetingManAttendeeEntry();
        if (attendeeEntrys != null && attendeeEntrys.size() > 0) {
            for (int i = 0; i < attendeeEntrys.size(); i++) {
                AttendeeVO attendeeVO = new AttendeeVO();
                MeetingManMeetingManAttendeeEntryInfo meetingAttendeeEntryInfo = attendeeEntrys.get(i);
                String attendeeName = meetingAttendeeEntryInfo.getAttendee().getName();// 参会人
                String reason = meetingAttendeeEntryInfo.getReason();// 缺席原因

                attendeeVO.setAttendee_name(attendeeName);
                attendeeVO.setReason(reason);
                attendeeVOList.add(attendeeVO);
            }
        }
        return attendeeVOList;
    }

    /**
     * 
     * @title listItemVO
     * @description 获取EAS审议议题议题事项分录转事项清单中间层对象列表
     * @param subjectInfo 议题
     * @return 事项清单中间层对象列表
     */
    private static List<ItemVO> listItemVO(SubjectInfo subjectInfo) {
        List<ItemVO> itemList = new ArrayList<ItemVO>(); // 议题事项列表
        SubjectCatalogEntryCollection catalogEntry = subjectInfo.getCatalogEntry();
        if (catalogEntry != null && catalogEntry.size() > 0) {
            for (int j = 0; j < catalogEntry.size(); j++) {
                ItemVO itemVO = new ItemVO();
                SubjectCatalogEntryInfo subjectCatalogEntryInfo = catalogEntry.get(j);
                String sbjectCatalogName = subjectCatalogEntryInfo.getCatalogMain().getName();// 事项名称
                String sbjectCatalogNum = subjectCatalogEntryInfo.getCatalogMain().getNumber();// 事项编码

                itemVO.setItem_code(sbjectCatalogNum);
                itemList.add(itemVO);
            }
        }
        return itemList;
    }

    /**
     * 
     * @title listRelationVO
     * @description 获取EAS审议议题关联议题分录转议题关联中间层对象列表
     * @param subjectInfo 议题
     * @return 议题关联中间层对象列表
     * @throws BOSException
     * @throws EASBizException
     */
    private static List<RelationVO> listRelationVO(Context ctx, SubjectInfo subjectInfo)
            throws EASBizException, BOSException {
        List<RelationVO> relationVOList = new ArrayList<RelationVO>(); // 关联议题
        SubjectRefSubjectEntryCollection subjectRefEntrys = subjectInfo.getRefSubjectEntry();
        if (subjectRefEntrys != null && subjectRefEntrys.size() > 0) {
            for (int j = 0; j < subjectRefEntrys.size(); j++) {
                RelationVO relationVO = new RelationVO();
                SubjectRefSubjectEntryInfo entryInfo = subjectRefEntrys.get(j);
                String companyId = entryInfo.getCompany().getId().toString();
                String companyName = entryInfo.getCompany().getName();// 企业名称
                String refSubjectNumber = entryInfo.getRefSubjectNumber();// 关联议题编码
                // 公司编码（组织机构代码）
                relationVO.setRel_company_id(DataConvertUtil.findOrgCode(ctx, companyId));
                relationVO.setRel_subject_code(refSubjectNumber);
                relationVOList.add(relationVO);
            }
        }
        return relationVOList;
    }

}
