package com.kingdee.eas.custom.tiog.service.bizwebser;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.tiog.middleware.bo.OrganizationAndImplementationVO;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.ListofMattersBO;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMakingSystemBO;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.tiog.middleware.bo.DecisionMeetingBO;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface ObjectConversionFacadeController extends BizController
{
    public ListofMattersBO convertToListofMattersBO(Context ctx, CategoryListInfo billInfo) throws BOSException, EASBizException, RemoteException;
    public DecisionMakingSystemBO convertToDecisionMakingSystemBO(Context ctx, PoliticalSystemInfo billInfo) throws BOSException, EASBizException, RemoteException;
    public DecisionMeetingBO convertToDecisionMeetingBO(Context ctx, MeetingInfo billInfo) throws BOSException, EASBizException, RemoteException;
    public OrganizationAndImplementationVO convertToOrganizationAndImplementationVO(Context ctx, ExecutionInfo billInfo) throws BOSException, EASBizException, RemoteException;
    public DecisionMeetingBO convertToDecisionMeetingManBO(Context ctx, MeetingManInfo billInfo) throws BOSException, EASBizException, RemoteException;
}