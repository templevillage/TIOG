package com.kingdee.eas.custom.tiog.service.log;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractLogmessageEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractLogmessageEntryInfo()
    {
        this("id");
    }
    protected AbstractLogmessageEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 分录 's 单据头 property 
     */
    public com.kingdee.eas.custom.tiog.service.log.LogmessageInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.service.log.LogmessageInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.service.log.LogmessageInfo item)
    {
        put("parent", item);
    }
    /**
     * Object:分录's 请求信息property 
     */
    public String getREQUESTMAS()
    {
        return getString("REQUESTMAS");
    }
    public void setREQUESTMAS(String item)
    {
        setString("REQUESTMAS", item);
    }
    /**
     * Object:分录's 响应信息property 
     */
    public String getRESPONSEMAS()
    {
        return getString("RESPONSEMAS");
    }
    public void setRESPONSEMAS(String item)
    {
        setString("RESPONSEMAS", item);
    }
    /**
     * Object:分录's 交互来源property 
     */
    public String getFROM()
    {
        return getString("FROM");
    }
    public void setFROM(String item)
    {
        setString("FROM", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("432F1736");
    }
}