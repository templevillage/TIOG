package com.kingdee.eas.custom.tiog.service.log;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.tiog.service.log.app.*;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.framework.ICoreBillEntryBase;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBillEntryBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class LogmessageEntry extends CoreBillEntryBase implements ILogmessageEntry
{
    public LogmessageEntry()
    {
        super();
        registerInterface(ILogmessageEntry.class, this);
    }
    public LogmessageEntry(Context ctx)
    {
        super(ctx);
        registerInterface(ILogmessageEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("432F1736");
    }
    private LogmessageEntryController getController() throws BOSException
    {
        return (LogmessageEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public LogmessageEntryInfo getLogmessageEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getLogmessageEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public LogmessageEntryInfo getLogmessageEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getLogmessageEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public LogmessageEntryInfo getLogmessageEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getLogmessageEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public LogmessageEntryCollection getLogmessageEntryCollection() throws BOSException
    {
        try {
            return getController().getLogmessageEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public LogmessageEntryCollection getLogmessageEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getLogmessageEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public LogmessageEntryCollection getLogmessageEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getLogmessageEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}