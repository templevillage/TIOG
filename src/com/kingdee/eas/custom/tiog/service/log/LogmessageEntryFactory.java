package com.kingdee.eas.custom.tiog.service.log;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class LogmessageEntryFactory
{
    private LogmessageEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.service.log.ILogmessageEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.log.ILogmessageEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("432F1736") ,com.kingdee.eas.custom.tiog.service.log.ILogmessageEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.service.log.ILogmessageEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.log.ILogmessageEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("432F1736") ,com.kingdee.eas.custom.tiog.service.log.ILogmessageEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.service.log.ILogmessageEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.log.ILogmessageEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("432F1736"));
    }
    public static com.kingdee.eas.custom.tiog.service.log.ILogmessageEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.log.ILogmessageEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("432F1736"));
    }
}