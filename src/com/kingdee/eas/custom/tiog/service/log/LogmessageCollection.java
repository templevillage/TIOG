package com.kingdee.eas.custom.tiog.service.log;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class LogmessageCollection extends AbstractObjectCollection 
{
    public LogmessageCollection()
    {
        super(LogmessageInfo.class);
    }
    public boolean add(LogmessageInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(LogmessageCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(LogmessageInfo item)
    {
        return removeObject(item);
    }
    public LogmessageInfo get(int index)
    {
        return(LogmessageInfo)getObject(index);
    }
    public LogmessageInfo get(Object key)
    {
        return(LogmessageInfo)getObject(key);
    }
    public void set(int index, LogmessageInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(LogmessageInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(LogmessageInfo item)
    {
        return super.indexOf(item);
    }
}