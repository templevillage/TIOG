package com.kingdee.eas.custom.tiog.service.log;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.eas.framework.ICoreBillEntryBase;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface ILogmessageEntry extends ICoreBillEntryBase
{
    public LogmessageEntryInfo getLogmessageEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public LogmessageEntryInfo getLogmessageEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public LogmessageEntryInfo getLogmessageEntryInfo(String oql) throws BOSException, EASBizException;
    public LogmessageEntryCollection getLogmessageEntryCollection() throws BOSException;
    public LogmessageEntryCollection getLogmessageEntryCollection(EntityViewInfo view) throws BOSException;
    public LogmessageEntryCollection getLogmessageEntryCollection(String oql) throws BOSException;
}