package com.kingdee.eas.custom.tiog.service.log;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractLogmessageInfo extends com.kingdee.eas.framework.CoreBillBaseInfo implements Serializable 
{
    public AbstractLogmessageInfo()
    {
        this("id");
    }
    protected AbstractLogmessageInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.tiog.service.log.LogmessageEntryCollection());
    }
    /**
     * Object: 交互日志 's 分录 property 
     */
    public com.kingdee.eas.custom.tiog.service.log.LogmessageEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.tiog.service.log.LogmessageEntryCollection)get("entrys");
    }
    /**
     * Object:交互日志's 是否生成凭证property 
     */
    public boolean isFivouchered()
    {
        return getBoolean("Fivouchered");
    }
    public void setFivouchered(boolean item)
    {
        setBoolean("Fivouchered", item);
    }
    /**
     * Object:交互日志's 交互类型property 
     */
    public String getType()
    {
        return getString("type");
    }
    public void setType(String item)
    {
        setString("type", item);
    }
    /**
     * Object:交互日志's 详细时间property 
     */
    public String getMINUTETIME()
    {
        return getString("MINUTETIME");
    }
    public void setMINUTETIME(String item)
    {
        setString("MINUTETIME", item);
    }
    /**
     * Object:交互日志's 交互结果property 
     */
    public String getRESULT()
    {
        return getString("RESULT");
    }
    public void setRESULT(String item)
    {
        setString("RESULT", item);
    }
    /**
     * Object:交互日志's 上传单据编号property 
     */
    public String getYNUMBER()
    {
        return getString("YNUMBER");
    }
    public void setYNUMBER(String item)
    {
        setString("YNUMBER", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("3D9E429C");
    }
}