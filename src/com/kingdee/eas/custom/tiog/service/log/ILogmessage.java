package com.kingdee.eas.custom.tiog.service.log;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface ILogmessage extends ICoreBillBase
{
    public LogmessageCollection getLogmessageCollection() throws BOSException;
    public LogmessageCollection getLogmessageCollection(EntityViewInfo view) throws BOSException;
    public LogmessageCollection getLogmessageCollection(String oql) throws BOSException;
    public LogmessageInfo getLogmessageInfo(IObjectPK pk) throws BOSException, EASBizException;
    public LogmessageInfo getLogmessageInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public LogmessageInfo getLogmessageInfo(String oql) throws BOSException, EASBizException;
    public void logtest() throws BOSException;
}