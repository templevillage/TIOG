package com.kingdee.eas.custom.tiog.service.log.app;

import org.apache.log4j.Logger;
import javax.ejb.*;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kingdee.bos.*;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.metadata.IMetaDataPK;
import com.kingdee.bos.metadata.rule.RuleExecutor;
import com.kingdee.bos.metadata.MetaDataPK;
//import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.framework.ejb.AbstractEntityControllerBean;
import com.kingdee.bos.framework.ejb.AbstractBizControllerBean;
//import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.bos.dao.IObjectCollection;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.bos.service.IServiceContext;

import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.CatalogList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.RegulationTypeList;
import com.kingdee.eas.custom.tiog.middleware.service.LogService;
import com.kingdee.eas.custom.tiog.middleware.vo.basicdata.RegulationTypeVO;
import com.kingdee.eas.custom.tiog.service.log.LogmessageInfo;
import com.kingdee.eas.framework.app.CoreBillBaseControllerBean;
import com.kingdee.eas.framework.SystemEnum;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.framework.ObjectBaseCollection;
import com.kingdee.eas.custom.tiog.service.log.LogmessageCollection;
import java.lang.String;
import com.kingdee.eas.framework.CoreBillBaseCollection;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.Result;
import com.kingdee.eas.cm.common.utils.ArrayMap;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class LogmessageControllerBean extends AbstractLogmessageControllerBean
{
    private static Logger logger =
        Logger.getLogger("com.kingdee.eas.custom.tiog.service.log.app.LogmessageControllerBean");
    @Override
    protected void _logtest(Context ctx) throws BOSException {
    	LogService logService = new LogService(ctx);
    	Map<String,Object> map = new HashMap<String,Object>();
    	RegulationTypeList cata = new RegulationTypeList();
    	RegulationTypeVO ra = new RegulationTypeVO();
    	ra.setFlag("1");
    	ra.setName("测试");
    	List list = new ArrayList();
    	list.add(ra);
    	cata.setRegulation_type_list(list);
    	map.put("regulation_type.xml", cata);
    	logService.saveDownloadLog(map, "测试上传信息");
    	
    }
}