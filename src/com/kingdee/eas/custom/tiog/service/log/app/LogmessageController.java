package com.kingdee.eas.custom.tiog.service.log.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.eas.custom.tiog.service.log.LogmessageInfo;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.custom.tiog.service.log.LogmessageCollection;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.app.CoreBillBaseController;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface LogmessageController extends CoreBillBaseController
{
    public LogmessageCollection getLogmessageCollection(Context ctx) throws BOSException, RemoteException;
    public LogmessageCollection getLogmessageCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public LogmessageCollection getLogmessageCollection(Context ctx, String oql) throws BOSException, RemoteException;
    public LogmessageInfo getLogmessageInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public LogmessageInfo getLogmessageInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public LogmessageInfo getLogmessageInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public void logtest(Context ctx) throws BOSException, RemoteException;
}