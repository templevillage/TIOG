package com.kingdee.eas.custom.tiog.service.log;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class LogmessageFactory
{
    private LogmessageFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.service.log.ILogmessage getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.log.ILogmessage)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("3D9E429C") ,com.kingdee.eas.custom.tiog.service.log.ILogmessage.class);
    }
    
    public static com.kingdee.eas.custom.tiog.service.log.ILogmessage getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.log.ILogmessage)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("3D9E429C") ,com.kingdee.eas.custom.tiog.service.log.ILogmessage.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.service.log.ILogmessage getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.log.ILogmessage)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("3D9E429C"));
    }
    public static com.kingdee.eas.custom.tiog.service.log.ILogmessage getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.log.ILogmessage)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("3D9E429C"));
    }
}