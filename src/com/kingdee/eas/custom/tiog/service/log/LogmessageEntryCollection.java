package com.kingdee.eas.custom.tiog.service.log;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class LogmessageEntryCollection extends AbstractObjectCollection 
{
    public LogmessageEntryCollection()
    {
        super(LogmessageEntryInfo.class);
    }
    public boolean add(LogmessageEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(LogmessageEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(LogmessageEntryInfo item)
    {
        return removeObject(item);
    }
    public LogmessageEntryInfo get(int index)
    {
        return(LogmessageEntryInfo)getObject(index);
    }
    public LogmessageEntryInfo get(Object key)
    {
        return(LogmessageEntryInfo)getObject(key);
    }
    public void set(int index, LogmessageEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(LogmessageEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(LogmessageEntryInfo item)
    {
        return super.indexOf(item);
    }
}