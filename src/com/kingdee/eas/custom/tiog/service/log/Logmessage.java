package com.kingdee.eas.custom.tiog.service.log;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.tiog.service.log.app.*;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.framework.CoreBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class Logmessage extends CoreBillBase implements ILogmessage
{
    public Logmessage()
    {
        super();
        registerInterface(ILogmessage.class, this);
    }
    public Logmessage(Context ctx)
    {
        super(ctx);
        registerInterface(ILogmessage.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("3D9E429C");
    }
    private LogmessageController getController() throws BOSException
    {
        return (LogmessageController)getBizController();
    }
    /**
     *取集合-System defined method
     *@return
     */
    public LogmessageCollection getLogmessageCollection() throws BOSException
    {
        try {
            return getController().getLogmessageCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param view 取集合
     *@return
     */
    public LogmessageCollection getLogmessageCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getLogmessageCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param oql 取集合
     *@return
     */
    public LogmessageCollection getLogmessageCollection(String oql) throws BOSException
    {
        try {
            return getController().getLogmessageCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@return
     */
    public LogmessageInfo getLogmessageInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getLogmessageInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@param selector 取值
     *@return
     */
    public LogmessageInfo getLogmessageInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getLogmessageInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param oql 取值
     *@return
     */
    public LogmessageInfo getLogmessageInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getLogmessageInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *logtest-User defined method
     */
    public void logtest() throws BOSException
    {
        try {
            getController().logtest(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}