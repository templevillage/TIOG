package com.kingdee.eas.custom.tiog.service.baseconf;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractBizUpLoadConfInfo extends com.kingdee.eas.framework.DataBaseInfo implements Serializable 
{
    public AbstractBizUpLoadConfInfo()
    {
        this("id");
    }
    protected AbstractBizUpLoadConfInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 业务上传配置 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    /**
     * Object:业务上传配置's 国资委标识property 
     */
    public String getServerFlag()
    {
        return getString("serverFlag");
    }
    public void setServerFlag(String item)
    {
        setString("serverFlag", item);
    }
    /**
     * Object:业务上传配置's BOSTYPEproperty 
     */
    public String getBOSTYPE()
    {
        return getString("BOSTYPE");
    }
    public void setBOSTYPE(String item)
    {
        setString("BOSTYPE", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("9C935754");
    }
}