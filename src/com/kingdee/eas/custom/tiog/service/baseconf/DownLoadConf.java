package com.kingdee.eas.custom.tiog.service.baseconf;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.eas.custom.tiog.service.baseconf.app.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class DownLoadConf extends DataBase implements IDownLoadConf
{
    public DownLoadConf()
    {
        super();
        registerInterface(IDownLoadConf.class, this);
    }
    public DownLoadConf(Context ctx)
    {
        super(ctx);
        registerInterface(IDownLoadConf.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("BFE8EF70");
    }
    private DownLoadConfController getController() throws BOSException
    {
        return (DownLoadConfController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public DownLoadConfInfo getDownLoadConfInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getDownLoadConfInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public DownLoadConfInfo getDownLoadConfInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getDownLoadConfInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public DownLoadConfInfo getDownLoadConfInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getDownLoadConfInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public DownLoadConfCollection getDownLoadConfCollection() throws BOSException
    {
        try {
            return getController().getDownLoadConfCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public DownLoadConfCollection getDownLoadConfCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getDownLoadConfCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public DownLoadConfCollection getDownLoadConfCollection(String oql) throws BOSException
    {
        try {
            return getController().getDownLoadConfCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}