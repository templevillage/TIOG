package com.kingdee.eas.custom.tiog.service.baseconf.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.eas.custom.tiog.service.baseconf.DownLoadConfInfo;
import com.kingdee.eas.custom.tiog.service.baseconf.DownLoadConfCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.framework.app.DataBaseController;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface DownLoadConfController extends DataBaseController
{
    public DownLoadConfInfo getDownLoadConfInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public DownLoadConfInfo getDownLoadConfInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public DownLoadConfInfo getDownLoadConfInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public DownLoadConfCollection getDownLoadConfCollection(Context ctx) throws BOSException, RemoteException;
    public DownLoadConfCollection getDownLoadConfCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public DownLoadConfCollection getDownLoadConfCollection(Context ctx, String oql) throws BOSException, RemoteException;
}