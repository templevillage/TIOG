package com.kingdee.eas.custom.tiog.service.baseconf;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractDownLoadConfInfo extends com.kingdee.eas.framework.DataBaseInfo implements Serializable 
{
    public AbstractDownLoadConfInfo()
    {
        this("id");
    }
    protected AbstractDownLoadConfInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��̬�������� 's ������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    /**
     * Object:��̬��������'s xmlproperty 
     */
    public String getXml()
    {
        return getString("xml");
    }
    public void setXml(String item)
    {
        setString("xml", item);
    }
    /**
     * Object:��̬��������'s BOSTYPEproperty 
     */
    public String getBOSTYPE()
    {
        return getString("BOSTYPE");
    }
    public void setBOSTYPE(String item)
    {
        setString("BOSTYPE", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("BFE8EF70");
    }
}