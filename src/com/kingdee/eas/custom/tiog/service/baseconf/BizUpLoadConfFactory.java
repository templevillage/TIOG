package com.kingdee.eas.custom.tiog.service.baseconf;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class BizUpLoadConfFactory
{
    private BizUpLoadConfFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.service.baseconf.IBizUpLoadConf getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.baseconf.IBizUpLoadConf)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("9C935754") ,com.kingdee.eas.custom.tiog.service.baseconf.IBizUpLoadConf.class);
    }
    
    public static com.kingdee.eas.custom.tiog.service.baseconf.IBizUpLoadConf getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.baseconf.IBizUpLoadConf)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("9C935754") ,com.kingdee.eas.custom.tiog.service.baseconf.IBizUpLoadConf.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.service.baseconf.IBizUpLoadConf getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.baseconf.IBizUpLoadConf)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("9C935754"));
    }
    public static com.kingdee.eas.custom.tiog.service.baseconf.IBizUpLoadConf getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.baseconf.IBizUpLoadConf)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("9C935754"));
    }
}