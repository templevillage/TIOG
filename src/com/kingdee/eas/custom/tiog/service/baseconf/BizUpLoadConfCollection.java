package com.kingdee.eas.custom.tiog.service.baseconf;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class BizUpLoadConfCollection extends AbstractObjectCollection 
{
    public BizUpLoadConfCollection()
    {
        super(BizUpLoadConfInfo.class);
    }
    public boolean add(BizUpLoadConfInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(BizUpLoadConfCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(BizUpLoadConfInfo item)
    {
        return removeObject(item);
    }
    public BizUpLoadConfInfo get(int index)
    {
        return(BizUpLoadConfInfo)getObject(index);
    }
    public BizUpLoadConfInfo get(Object key)
    {
        return(BizUpLoadConfInfo)getObject(key);
    }
    public void set(int index, BizUpLoadConfInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(BizUpLoadConfInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(BizUpLoadConfInfo item)
    {
        return super.indexOf(item);
    }
}