package com.kingdee.eas.custom.tiog.service.baseconf;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class DownLoadConfCollection extends AbstractObjectCollection 
{
    public DownLoadConfCollection()
    {
        super(DownLoadConfInfo.class);
    }
    public boolean add(DownLoadConfInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(DownLoadConfCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(DownLoadConfInfo item)
    {
        return removeObject(item);
    }
    public DownLoadConfInfo get(int index)
    {
        return(DownLoadConfInfo)getObject(index);
    }
    public DownLoadConfInfo get(Object key)
    {
        return(DownLoadConfInfo)getObject(key);
    }
    public void set(int index, DownLoadConfInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(DownLoadConfInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(DownLoadConfInfo item)
    {
        return super.indexOf(item);
    }
}