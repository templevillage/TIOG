package com.kingdee.eas.custom.tiog.service.baseconf;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.eas.custom.tiog.service.baseconf.app.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class BizUpLoadConf extends DataBase implements IBizUpLoadConf
{
    public BizUpLoadConf()
    {
        super();
        registerInterface(IBizUpLoadConf.class, this);
    }
    public BizUpLoadConf(Context ctx)
    {
        super(ctx);
        registerInterface(IBizUpLoadConf.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("9C935754");
    }
    private BizUpLoadConfController getController() throws BOSException
    {
        return (BizUpLoadConfController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public BizUpLoadConfInfo getBizUpLoadConfInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getBizUpLoadConfInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public BizUpLoadConfInfo getBizUpLoadConfInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getBizUpLoadConfInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public BizUpLoadConfInfo getBizUpLoadConfInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getBizUpLoadConfInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public BizUpLoadConfCollection getBizUpLoadConfCollection() throws BOSException
    {
        try {
            return getController().getBizUpLoadConfCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public BizUpLoadConfCollection getBizUpLoadConfCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getBizUpLoadConfCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public BizUpLoadConfCollection getBizUpLoadConfCollection(String oql) throws BOSException
    {
        try {
            return getController().getBizUpLoadConfCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}