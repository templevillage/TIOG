package com.kingdee.eas.custom.tiog.service.baseconf;

import java.io.Serializable;

public class BizUpLoadConfInfo extends AbstractBizUpLoadConfInfo implements Serializable 
{
    public BizUpLoadConfInfo()
    {
        super();
    }
    protected BizUpLoadConfInfo(String pkField)
    {
        super(pkField);
    }
}