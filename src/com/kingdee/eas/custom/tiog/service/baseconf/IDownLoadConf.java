package com.kingdee.eas.custom.tiog.service.baseconf;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IDownLoadConf extends IDataBase
{
    public DownLoadConfInfo getDownLoadConfInfo(IObjectPK pk) throws BOSException, EASBizException;
    public DownLoadConfInfo getDownLoadConfInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public DownLoadConfInfo getDownLoadConfInfo(String oql) throws BOSException, EASBizException;
    public DownLoadConfCollection getDownLoadConfCollection() throws BOSException;
    public DownLoadConfCollection getDownLoadConfCollection(EntityViewInfo view) throws BOSException;
    public DownLoadConfCollection getDownLoadConfCollection(String oql) throws BOSException;
}