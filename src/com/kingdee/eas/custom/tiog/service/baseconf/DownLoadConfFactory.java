package com.kingdee.eas.custom.tiog.service.baseconf;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class DownLoadConfFactory
{
    private DownLoadConfFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.service.baseconf.IDownLoadConf getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.baseconf.IDownLoadConf)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("BFE8EF70") ,com.kingdee.eas.custom.tiog.service.baseconf.IDownLoadConf.class);
    }
    
    public static com.kingdee.eas.custom.tiog.service.baseconf.IDownLoadConf getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.baseconf.IDownLoadConf)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("BFE8EF70") ,com.kingdee.eas.custom.tiog.service.baseconf.IDownLoadConf.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.service.baseconf.IDownLoadConf getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.baseconf.IDownLoadConf)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("BFE8EF70"));
    }
    public static com.kingdee.eas.custom.tiog.service.baseconf.IDownLoadConf getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.service.baseconf.IDownLoadConf)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("BFE8EF70"));
    }
}