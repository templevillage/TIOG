/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.service.sasac.app;

import com.kingdee.bos.Context;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.eas.framework.batchHandler.ResponseContext;


/**
 * output class name
 */
public abstract class AbstractDownLoadPlatformUIHandler extends com.kingdee.eas.framework.app.CoreUIHandler

{
	public void handleActionComSelected(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionComSelected(request,response,context);
	}
	protected void _handleActionComSelected(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionTypeSelected(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionTypeSelected(request,response,context);
	}
	protected void _handleActionTypeSelected(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionComClear(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionComClear(request,response,context);
	}
	protected void _handleActionComClear(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionTypeClear(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionTypeClear(request,response,context);
	}
	protected void _handleActionTypeClear(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionSync(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionSync(request,response,context);
	}
	protected void _handleActionSync(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionAntiSync(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionAntiSync(request,response,context);
	}
	protected void _handleActionAntiSync(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionCancel(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionCancel(request,response,context);
	}
	protected void _handleActionCancel(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionExecute(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionExecute(request,response,context);
	}
	protected void _handleActionExecute(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionQuery(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionQuery(request,response,context);
	}
	protected void _handleActionQuery(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionAwaitSelected(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionAwaitSelected(request,response,context);
	}
	protected void _handleActionAwaitSelected(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionAwaitClear(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionAwaitClear(request,response,context);
	}
	protected void _handleActionAwaitClear(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionCancel2(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionCancel2(request,response,context);
	}
	protected void _handleActionCancel2(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionH01(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionH01(request,response,context);
	}
	protected void _handleActionH01(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionH02(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionH02(request,response,context);
	}
	protected void _handleActionH02(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionH03(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionH03(request,response,context);
	}
	protected void _handleActionH03(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
}