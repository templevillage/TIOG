package com.kingdee.eas.custom.tiog.service.sasac.client;
/**
 * 三重一大上传扩展类
 * author:马伟楠   time：20200413
 *  email:13021302062@163.com
 */
import java.awt.event.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.IMetaDataLoader;
import com.kingdee.bos.metadata.MetaDataLoaderFactory;
import com.kingdee.bos.metadata.data.DataTableInfo;
import com.kingdee.bos.metadata.entity.EntityObjectInfo;
import com.kingdee.bos.ui.face.CoreUIObject;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.ctrl.kdf.table.IRow;
import com.kingdee.bos.ctrl.kdf.table.KDTable;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.bos.dao.ormapping.ObjectStringPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.dao.query.SQLExecutorFactory;
import com.kingdee.eas.basedata.org.CompanyOrgUnitInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.cp.dm.CategoryCollection;
import com.kingdee.eas.cp.dm.CategoryFactory;
import com.kingdee.eas.cp.wfm.bill.DemoFactory;
import com.kingdee.eas.cp.wfm.bill.DemoInfo;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListCollection;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryDEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryDEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListFactory;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionCollection;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionFactory;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo;
import com.kingdee.eas.custom.tiog.bizbill.ISubject;
import com.kingdee.eas.custom.tiog.bizbill.MeetingFactory;
import com.kingdee.eas.custom.tiog.bizbill.MeetingInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManCollection;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManFactory;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManMeetingManAttendeeEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManMeetingManAttendeeEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectAttendanceEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectDeliberationEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemCollection;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemFactory;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectCollection;
import com.kingdee.eas.custom.tiog.bizbill.SubjectFactory;
import com.kingdee.eas.custom.tiog.bizbill.SubjectInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectRefSubjectEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.SubjectRefSubjectEntryInfo;
import com.kingdee.eas.custom.tiog.middleware.vo.AttendanceVO;
import com.kingdee.eas.custom.tiog.middleware.vo.AttendeeVO;
import com.kingdee.eas.custom.tiog.middleware.vo.DeliberationVO;
import com.kingdee.eas.custom.tiog.middleware.vo.ItemVO;
import com.kingdee.eas.custom.tiog.middleware.vo.RelationVO;
import com.kingdee.eas.custom.tiog.middleware.vo.SubjectVO;
import com.kingdee.eas.custom.tiog.middleware.vo.VoteModeVO;
import com.kingdee.eas.custom.tiog.service.baseconf.BizUpLoadConfCollection;
import com.kingdee.eas.custom.tiog.service.baseconf.BizUpLoadConfFactory;
import com.kingdee.eas.custom.tiog.service.baseconf.BizUpLoadConfInfo;
import com.kingdee.eas.custom.tiog.service.bizwebser.util.CategoryListDataUtil;
import com.kingdee.eas.custom.tiog.service.bizwebser.util.ExecutionDataUtil;
import com.kingdee.eas.custom.tiog.service.bizwebser.util.MeetingDataUtil;
import com.kingdee.eas.custom.tiog.service.bizwebser.util.PoliticalSystemDataUtil;
import com.kingdee.eas.custom.tiog.utils.BooleanUtil;
import com.kingdee.eas.custom.tiog.utils.DataConvertUtil;
import com.kingdee.eas.custom.tiog.utils.DateUtil;
import com.kingdee.eas.custom.tiog.utils.MsgBoxUtil;
import com.kingdee.eas.framework.*;
import com.kingdee.eas.framework.util.EntityUtility;
import com.kingdee.eas.util.client.MsgBox;
import com.kingdee.jdbc.rowset.IRowSet;
public class UpLoadPlatformUICTEx extends UpLoadPlatformUI{
	private int flag = 1;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private Context ctx;
	List Y_catelist = null;
	List Y_execulist = null;
	List Y_meetlist = null;
	List Y_politlist = null;
	List N_catelist = null;
	List N_execulist = null;
	List N_meetlist = null;
	List N_politlist = null;
	public UpLoadPlatformUICTEx() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * output actionSync_actionPerformed
	 */
	// 同步
	public void actionSync_actionPerformed(ActionEvent e) throws Exception {
		super.actionSync_actionPerformed(e);
		String id = "id";
		List list = getTableColl(kDTableType, id);
		Y_catelist=new ArrayList();
		Y_execulist=new ArrayList();
		Y_meetlist=new ArrayList();
		Y_politlist=new ArrayList();
		N_catelist=new ArrayList();
		N_execulist=new ArrayList();
		N_meetlist=new ArrayList();
		N_politlist=new ArrayList();
		StringBuffer Y=new StringBuffer();
		StringBuffer N=new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			String bos = list.get(i).toString();
			BOSUuid uuid = BOSUuid.read(bos);
			BOSObjectType bostype = uuid.getType();
			if (bostype.toString().equals("9B951826")) {
				// 事项清单
				CategoryListCollection catecoll = CategoryListFactory
						.getRemoteInstance().getCategoryListCollection(
								"select * where id='" + uuid + "'");
				if (catecoll != null && catecoll.size() > 0) {
					CategoryListInfo catelistInfo = null;
					for (int index = 0, coll = catecoll.size(); index < coll; index++) {
						catelistInfo = catecoll.get(index);
						// 对info进行校验
						CategoryListInfo catelistInfo_true = checkCateListInfo(catelistInfo);
						if(catelistInfo_true!=null){
						// 转换成BO
						CategoryListDataUtil.convertInfo(catelistInfo_true);
						// 记录单据名称 :为书写日志
						Y_catelist.add(catelistInfo_true.getNumber());
					}
					}
				}
			} else if (bostype.toString().equals("E27E2DEE")) {
				// 组织实施
				ExecutionCollection execucoll = ExecutionFactory
						.getRemoteInstance().getExecutionCollection(
								"select * where id='" + uuid + "'");
				if (execucoll != null && execucoll.size() > 0) {
					ExecutionInfo execuInfo = null;
					for (int index = 0, coll = execucoll.size(); index < coll; index++) {
						execuInfo = execucoll.get(index);
						// 对info进行校验
						ExecutionInfo execuInfo_true = checkExecuInfo(execuInfo);
						// 转换成BO
						if(execuInfo_true!=null){
						ExecutionDataUtil.convertInfo(execuInfo_true);
						// 记录单据名称 :为书写日志
						Y_execulist.add(execuInfo_true.getNumber());
					}
					}
				}

			} else if (bostype.toString().equals("2B222A91")) {
				// 决策会议
				MeetingManCollection meetcoll = MeetingManFactory
						.getRemoteInstance().getMeetingManCollection(
								"select * where id='" + uuid + "'");
				if (meetcoll != null && meetcoll.size() > 0) {
					MeetingManInfo meetManInfo = null;
					for (int index = 0, coll = meetcoll.size(); index < coll; index++) {
						meetManInfo = meetcoll.get(index);
						// 对info进行校验
						MeetingManInfo meetInfo_true = checkMeetingInfo(meetManInfo);
						// 转换成BO
						if(meetInfo_true!=null){
						MeetingDataUtil.convertInfo(ctx, meetInfo_true);
						// 记录单据名称 :为书写日志
						Y_politlist.add(meetInfo_true.getNumber());
					}
					}
				}
			} else {
				// 会议制度
				PoliticalSystemCollection politcoll = PoliticalSystemFactory
						.getRemoteInstance().getPoliticalSystemCollection(
								"select * where id='" + uuid + "'");
				if (politcoll != null && politcoll.size() > 0) {
					PoliticalSystemInfo politInfo = null;
					for (int index = 0, coll = politcoll.size(); index < coll; index++) {
						politInfo = politcoll.get(index);
						// 对info进行校验
						PoliticalSystemInfo execuInfo_true = checkPolitInfo(politInfo);
						if(execuInfo_true!=null){
						// 转换成BO
						PoliticalSystemDataUtil.convertInfo(politInfo);
						// 记录单据名称 :为书写日志
						Y_meetlist.add(execuInfo_true.getNumber());
						}
					}
				}

			}

		}
		String value=null;
		if(Y_catelist.size()>0){
			Y.append("事项清单："+Y_catelist.toString()+"导入成功");
		}
		if(Y_execulist.size()>0){
			Y.append("组织实施："+Y_execulist.toString()+"导入成功");
		}
		if(Y_meetlist.size()>0){
			Y.append("决策会议："+Y_meetlist.toString()+"导入成功");
		}
		if(Y_politlist.size()>0){
			Y.append("会议制度："+N_politlist.toString()+"导入失败");
		}
		if(N_catelist.size()>0){
			N.append("事项清单："+N_catelist.toString()+"导入失败");
		}
		if(N_execulist.size()>0){
			N.append("组织实施："+N_execulist.toString()+"导入失败");
		}
		if(N_meetlist.size()>0){
			N.append("决策会议："+N_meetlist.toString()+"导入失败");
		}
		if(N_politlist.size()>0){
			N.append("会议制度："+N_politlist.toString()+"导入失败");
		}
		if(Y.length()>0){
		value=Y.toString();
		}
		if(N.length()>0){
			value=N.toString();
			}
		kDTextArea1.setText(value);
	}
	public void actionExecute_actionPerformed(ActionEvent e) throws Exception {
		super.actionExecute_actionPerformed(e);
		// 清空kDTableType
		kDTableType.removeRows();
		String productID = "productID";
		// 获取选中的条件的bostype
		List list = getTableColl(kDTableCom, productID);
		// 起始时间
		Object startobj = kDDatePicker1.getValue();
		// 结束时间
		Object endobj = kDDatePicker2.getValue();
		for (int i = 0; i < list.size(); i++) {
			String bostype = (String) list.get(i).toString();
			IMetaDataLoader metadataloader = MetaDataLoaderFactory
					.getRemoteMetaDataLoader();
			EntityObjectInfo entity = metadataloader
					.getEntity(new BOSObjectType(bostype));
			entity.getTable();
			String controllerBean = entity.getRealFullName();
			String infoPath = controllerBean.replace(".app.", ".") + "Info"; // 对象路径
			Class<?> clazz = Class.forName(infoPath);
			DataTableInfo table = EntityUtility.getBOSEntity(null,
					(IObjectValue) clazz.newInstance()).getTable();
			// 数据库表名
			String name = table.getName();
			initkDTableType(name, startobj, endobj, flag);

		}
	}
	@Override
	public void onLoad() throws Exception {
		// TODO Auto-generated method stub
		super.onLoad();
		// 初始化按钮
		initKDButton();
		// 单据框添加监听
		addRedioButtonListener();
		// 默认设置已审核按钮单选
		kDRadioButton1.setSelected(true);
		flag = 1;
		// 初始化kDTableCom表格
		initkDTableCom();
	}

	/**
	 * 
	 * <p>
	 * Title: checkCateListInfo
	 * </p>
	 * <p>
	 * Description: 校验info必填项是否填写
	 * </p>
	 * 
	 * @param catelistInfo
	 *            事项清单info
	 * @return 事项清单info
	 */
	private CategoryListInfo checkCateListInfo(CategoryListInfo catelistInfo) {
		if (catelistInfo.getId() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "单据内码");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		if (catelistInfo.getCompany().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "企业名称");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		if (catelistInfo.getCompanyNumber() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "企业名称编码");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		if (catelistInfo.getCategoryListName() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "企业名称编码");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		if (catelistInfo.getNumber() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "事项清单编码");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		if (catelistInfo.getListVersion() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "版本号");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		if (catelistInfo.getEffectiveTS() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "生效时间");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		if (catelistInfo.getInvalidTS() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "失效时间");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		if (catelistInfo.getDescription() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "备注信息");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		if (catelistInfo.getSource().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "数据来源");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		if (catelistInfo.getOperType().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(), "操作标识");
			N_catelist.add(catelistInfo.getNumber());
			return null;
		}
		CategoryListEntryCollection catentrycoll = catelistInfo.getEntrys();
		if (catentrycoll != null && catentrycoll.size() > 0) {
			CategoryListEntryInfo catentryInfo = null;
			for (int index = 0, coll = catentrycoll.size(); index < coll; index++) {
				catentryInfo = catentrycoll.get(index);
				if (catentryInfo.getId() == null) {
					MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(),
							"分录id");
					N_catelist.add(catelistInfo.getNumber());
					return null;
				}
				if (catentryInfo.getCatalogMain().getNumber() == null) {
					MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(),
							"事项清单编码");
					N_catelist.add(catelistInfo.getNumber());
					return null;
				}
				if (catentryInfo.getCatalogMain().getName() == null) {
					MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(),
							"事项清单编码");
					N_catelist.add(catelistInfo.getNumber());
					return null;
				}
				if (catentryInfo.isLegalFlag() == false) {
					MsgBoxUtil.showWarning("事项清单编号为："
							+ catelistInfo.getNumber() + "分录法律审核是未审核");
					N_catelist.add(catelistInfo.getNumber());
					return null;
				}
				if (catentryInfo.getOperType() == null) {
					MsgBoxUtil.fieldBlankWarning(catelistInfo.getNumber(),
							"操作标识符");
					N_catelist.add(catelistInfo.getNumber());
					return null;
				}
				if (catentryInfo.getRemark() == null) {
					MsgBoxUtil
							.fieldBlankWarning(catelistInfo.getNumber(), "备注");
					N_catelist.add(catelistInfo.getNumber());
					return null;
				}
				CategoryListEntryDEntryCollection dentrys = catentryInfo
						.getDEntrys();
				if (dentrys != null && dentrys.size() > 0) {
					CategoryListEntryDEntryInfo dentryInfo = null;
					for (int j = 0, d = dentrys.size(); j < d; j++) {
						dentryInfo = dentrys.get(j);
						if (dentryInfo.getId() == null) {
							MsgBoxUtil.fieldBlankWarning(catelistInfo
									.getNumber(), "会议类型");
							N_catelist.add(catelistInfo.getNumber());
							return null;
						}
						if (dentryInfo.getMeetingType().getName() == null) {
							MsgBoxUtil.fieldBlankWarning(catelistInfo
									.getNumber(), "会议类型名称");
							N_catelist.add(catelistInfo.getNumber());
							return null;
						}
						if (dentryInfo.getMeetingTypeNumber() == null) {
							MsgBoxUtil.fieldBlankWarning(catelistInfo
									.getNumber(), "会议类型编码");
							N_catelist.add(catelistInfo.getNumber());
							return null;
						}
						if (dentryInfo.getMeetingDes() == null) {
							MsgBoxUtil.fieldBlankWarning(catelistInfo
									.getNumber(), "会议说明");
							N_catelist.add(catelistInfo.getNumber());
							return null;
						}
						if (dentryInfo.getRemark() == null) {
							MsgBoxUtil.fieldBlankWarning(catelistInfo
									.getNumber(), "会议备注");
							N_catelist.add(catelistInfo.getNumber());
							return null;
						}
					}
				}
			}
		}
		return catelistInfo;
	}

	/**
	 * 
	 * <p>
	 * Title: checkExecuInfo
	 * </p>
	 * <p>
	 * Description: 校验组织实施单据
	 * </p>
	 * 
	 * @param execuInfo
	 * @return 组织实施单据info
	 */
	private ExecutionInfo checkExecuInfo(ExecutionInfo execuInfo) {
		// TODO Auto-generated method stub
		if (execuInfo.getId() == null) {
			MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(), "单据id");
			N_execulist.add(execuInfo.getNumber());
			return null;
		}
		if (execuInfo.getCompany().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(), "企业名称");
			N_execulist.add(execuInfo.getNumber());
			return null;
		}
		if (execuInfo.getCompanyNumber() == null) {
			MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(), "企业名称");
			N_execulist.add(execuInfo.getNumber());
			return null;
		}
		if (execuInfo.getSubject().getNumber() == null) {
			MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(), "议题名称");
			N_execulist.add(execuInfo.getNumber());
			return null;
		}
		if (execuInfo.getSubjectNumber() == null) {
			MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(), "议题编码");
			N_execulist.add(execuInfo.getNumber());
			return null;
		}
		if (execuInfo.getEffect() == null) {
			MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(), "预期成效");
			N_execulist.add(execuInfo.getNumber());
			return null;
		}
		if (execuInfo.getState().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(), "实施状态");
			N_execulist.add(execuInfo.getNumber());
			return null;
		}
		if (execuInfo.getRemark() == null) {
			MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(), "备注");
			N_execulist.add(execuInfo.getNumber());
			return null;
		}
		if (execuInfo.getDataSource().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(), "数据来源");
			N_execulist.add(execuInfo.getNumber());
			return null;
		}
		if (execuInfo.getOperType().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(), "操作标识");
			N_execulist.add(execuInfo.getNumber());
			return null;
		}
		ExecutionEntryCollection execuentrycoll = execuInfo.getEntrys();
		if (execuentrycoll != null && execuentrycoll.size() > 0) {
			ExecutionEntryInfo execuentryInfo = null;
			for (int index = 0, coll = execuentrycoll.size(); index < coll; index++) {
				execuentryInfo = execuentrycoll.get(index);
				if (execuentryInfo.getResponseDept().getName() == null) {
					MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(),
							"落实责任部门");
					N_execulist.add(execuInfo.getNumber());
					return null;
				}
				if (execuentryInfo.getResponsePer().getName() == null) {
					MsgBoxUtil.fieldBlankWarning(execuInfo.getNumber(),
							"落实责任部门");
					N_execulist.add(execuInfo.getNumber());
					return null;
				}
			}

		}

		return execuInfo;
	}

	private MeetingManInfo checkMeetingInfo(MeetingManInfo meetManInfo)
			throws BOSException, EASBizException {
		// TODO Auto-generated method stub
		if (meetManInfo.getId() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "单据内码");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getCompany() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "企业名称");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getCompanyNumber() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "企业编码");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getMeetingName() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "会议名称");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getNumber() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "会议编码");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getMeetingType().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "会议类型");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getMeetingType().getNumber() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "会议类型编码");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getMeetingForm().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "会议形式");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getMeetingTS() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "会议时间");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getReleaseTS() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "会议纪要发送时间");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getModerator().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "主持人");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getRemark() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "备注");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getDataSource().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "数据来源");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		if (meetManInfo.getOperType().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(), "操作标识");
			N_meetlist.add(meetManInfo.getNumber());
			return null;
		}
		MeetingManMeetingManAttendeeEntryCollection attendeeEntrys = meetManInfo
				.getMeetingManAttendeeEntry();
		if (attendeeEntrys != null && attendeeEntrys.size() > 0) {
			for (int i = 0; i < attendeeEntrys.size(); i++) {
				MeetingManMeetingManAttendeeEntryInfo meetingAttendeeEntryInfo = attendeeEntrys
						.get(i);
				if (meetingAttendeeEntryInfo.getAttendee().getName() == null) {
					MsgBoxUtil
							.fieldBlankWarning(meetManInfo.getNumber(), "参会人");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (meetingAttendeeEntryInfo.getReason() == null) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"缺席原因");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}

			}
		}
		MeetingManSubjectEntryCollection subjectEntrys = meetManInfo
				.getSubjectEntry();
		if (subjectEntrys != null && subjectEntrys.size() > 0) {
			for (int i = 0; i < subjectEntrys.size(); i++) {
				MeetingManSubjectEntryInfo subjectEntryInfo = subjectEntrys
						.get(i);
				String subjectId = subjectEntryInfo.getSubject().getId()
						.toString(); // 议题
				ISubject ISubject = SubjectFactory.getLocalInstance(ctx);
				SubjectInfo subjectInfo = ISubject
						.getSubjectInfo(new ObjectUuidPK(subjectId)); // 审议议题
				if (subjectInfo.getSubjectName() == null) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"议题名称");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (subjectInfo.getSubjectNum() == null) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"议题编码");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (subjectInfo.getTaskSource().getName() == null) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"任务来源");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (subjectInfo.getSpecial().getName() == null) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"专项名称");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (subjectInfo.getPassFlag().getName() == null) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"是否通过审议");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (subjectInfo.getApprovalFlag().getName() == null) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"是否报国资委审批");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (subjectInfo.getSubjectResult() == null) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"议题决议");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (subjectInfo.isSuperviseFlag() == false) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"是否需要督办");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (subjectInfo.isAdoptFlag() == false) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"是否听取意见");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (subjectInfo.getOperType().getName() == null) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"操作标识");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				if (subjectInfo.getRemark() == null) {
					MsgBoxUtil.fieldBlankWarning(meetManInfo.getNumber(),
							"议题备注");
					N_meetlist.add(meetManInfo.getNumber());
					return null;
				}
				SubjectCatalogEntryCollection catalogEntry = subjectInfo
						.getCatalogEntry();
				if (catalogEntry != null && catalogEntry.size() > 0) {
					for (int j = 0; j < catalogEntry.size(); j++) {
						SubjectCatalogEntryInfo subjectCatalogEntryInfo = catalogEntry
								.get(j);
						if (subjectCatalogEntryInfo.getCatalogMain().getName() == null) {
							MsgBoxUtil.fieldBlankWarning(meetManInfo
									.getNumber(), "事项名称");
							N_meetlist.add(meetManInfo.getNumber());
							return null;
						}
						if (subjectCatalogEntryInfo.getCatalogMain()
								.getNumber() == null) {
							MsgBoxUtil.fieldBlankWarning(meetManInfo
									.getNumber(), "事项编码");
							N_meetlist.add(meetManInfo.getNumber());
							return null;
						}

					}
				}
				SubjectRefSubjectEntryCollection subjectRefEntrys = subjectInfo
						.getRefSubjectEntry();
				if (subjectRefEntrys != null && subjectRefEntrys.size() > 0) {
					for (int j = 0; j < subjectRefEntrys.size(); j++) {
						RelationVO relationVO = new RelationVO();
						SubjectRefSubjectEntryInfo entryInfo = subjectRefEntrys
								.get(j);
						String companyId = entryInfo.getCompany().getId()
								.toString();
						if (entryInfo.getCompany().getName() == null) {
							MsgBoxUtil.fieldBlankWarning(meetManInfo
									.getNumber(), "企业名称");
							N_meetlist.add(meetManInfo.getNumber());
							return null;
						}
						if (entryInfo.getRefSubjectNumber() == null) {
							MsgBoxUtil.fieldBlankWarning(meetManInfo
									.getNumber(), " 关联议题编码");
							N_meetlist.add(meetManInfo.getNumber());
							return null;
						}
					}
				}
				MeetingManSubjectEntryMeetingManSubjectAttendanceEntryCollection manSubjectAttendanceEntry = subjectEntryInfo
						.getMeetingManSubjectAttendanceEntry();
				if (manSubjectAttendanceEntry != null
						&& manSubjectAttendanceEntry.size() > 0) {
					for (int j = 0; j < manSubjectAttendanceEntry.size(); j++) {
						MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo entryInfo = manSubjectAttendanceEntry
								.get(j);
						if (entryInfo.getAttendance().getName() == null) {
							MsgBoxUtil.fieldBlankWarning(meetManInfo
									.getNumber(), " 列席人");
							N_meetlist.add(meetManInfo.getNumber());
							return null;
						}
						if (entryInfo.getAttendancePos().getName() == null) {
							MsgBoxUtil.fieldBlankWarning(meetManInfo
									.getNumber(), " 职位");
							N_meetlist.add(meetManInfo.getNumber());
							return null;
						}

					}
				}
				MeetingManSubjectEntryMeetingManSubjectDeliberationEntryCollection manSubjectDeliberationEntry = subjectEntryInfo
						.getMeetingManSubjectDeliberationEntry();
				if (manSubjectDeliberationEntry != null
						&& manSubjectDeliberationEntry.size() > 0) {
					for (int t = 0; t < manSubjectDeliberationEntry.size(); t++) {
						DeliberationVO deliberationVO = new DeliberationVO();
						MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo entryInfo = manSubjectDeliberationEntry
								.get(t);
						if (entryInfo.getDeliberator().getName() == null) {
							MsgBoxUtil.fieldBlankWarning(meetManInfo
									.getNumber(), " 审议人");
							N_meetlist.add(meetManInfo.getNumber());
							return null;
						}
						if (entryInfo.getDeliberationResult() == null) {
							MsgBoxUtil.fieldBlankWarning(meetManInfo
									.getNumber(), " 审议结果");
							N_meetlist.add(meetManInfo.getNumber());
							return null;
						}
					}
				}
			}
		}
		return meetManInfo;
	}

	/**
	 * 
	 * <p>
	 * Title: checkPolitInfo
	 * </p>
	 * <p>
	 * Description: 校验会议制度单据
	 * </p>
	 * 
	 * @param politInfo
	 *            会议制度 info
	 * @return 会议制度 info
	 */
	private PoliticalSystemInfo checkPolitInfo(PoliticalSystemInfo politInfo) {
		// TODO Auto-generated method stub
		if (politInfo.getId() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "单据内码");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getCompany().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "企业名称");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getCompanyNumber() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "企业编码");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getPoliticalSystemName() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "制度名称");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getRegulationType().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "制度类型名称");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getAuditor() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "审批日期");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getEffectDate() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "生效日期");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getExpireDate() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "失效日期");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.isIsReview() == false) {
			MsgBoxUtil.showWarning("会议制度编号为：" + politInfo.getNumber()
					+ "是否经过合法性审查是未审核");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getMeetNum().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "会议类型名称");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getMeetNum().getNumber() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "会议类型编码");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getDataSource().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "数据来源");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getOperType().getName() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "操作标识");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		if (politInfo.getDescription() == null) {
			MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "备注");
			N_politlist.add(politInfo.getNumber());
			return null;
		}
		PoliticalSystemEntryCollection entrys = politInfo.getEntrys();
		if (entrys != null && entrys.size() > 0) {
			for (int i = 0; i < entrys.size(); i++) {
				PoliticalSystemEntryInfo entryInfo = entrys.get(i);
				if (entryInfo.getId() == null) {
					MsgBoxUtil.fieldBlankWarning(politInfo.getNumber(), "分录ID");
					N_politlist.add(politInfo.getNumber());
					return null;
				}
				if (entryInfo.getCatalogMain().getName() == null) {
					MsgBoxUtil
							.fieldBlankWarning(politInfo.getNumber(), " 事项名称");
					N_politlist.add(politInfo.getNumber());
					return null;
				}
				if (entryInfo.getCatalogMain().getNumber() == null) {
					MsgBoxUtil
							.fieldBlankWarning(politInfo.getNumber(), " 事项编码");
					N_politlist.add(politInfo.getNumber());
					return null;
				}
				if (entryInfo.getRate().getName() == null) {
					MsgBoxUtil
							.fieldBlankWarning(politInfo.getNumber(), " 人数占比");
					N_politlist.add(politInfo.getNumber());
					return null;
				}
				if (entryInfo.getVoteMode().getName() == null) {
					MsgBoxUtil
							.fieldBlankWarning(politInfo.getNumber(), " 表决方式");
					N_politlist.add(politInfo.getNumber());
					return null;
				}

			}
		}
		return politInfo;
	}

	/**
	 * 
	 * <p>
	 * Title: addRedioButton
	 * </p>
	 * <p>
	 * Description: 对单选框添加监听
	 * </p>
	 */
	private void addRedioButtonListener() {
		// TODO Auto-generated method stub
		kDRadioButton2.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				// 设置已审核按钮为false
				kDRadioButton1.setSelected(false);
				if (flag != 0)
					flag = 0;
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});

		kDRadioButton1.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				// 设置暂存按钮为false
				kDRadioButton2.setSelected(false);
				if (flag != 1)
					flag = 1;
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});
	}

	/**
	 * 
	 * <p>
	 * Title: initKDButton
	 * </p>
	 * <p>
	 * Description:初始化按钮
	 * </p>
	 */
	private void initKDButton() {
		// TODO Auto-generated method stub
		kdwbComSelected.setVisible(true);
		kdwbComSelected.setEnabled(true);

		kdwbComClear.setVisible(true);
		kdwbComClear.setEnabled(true);

		kdwbExecute.setVisible(true);
		kdwbExecute.setEnabled(true);

		kdwbSync.setVisible(true);
		kdwbSync.setEnabled(true);

		kdwbAntiSync.setVisible(true);
		kdwbAntiSync.setEnabled(true);

		kdwbTypeSelected.setVisible(true);
		kdwbTypeSelected.setEnabled(true);

		kdwbTypeClear.setVisible(true);
		kdwbTypeClear.setEnabled(true);

		kdwbAwaitSelected.setVisible(true);
		kdwbAwaitSelected.setEnabled(true);

		kdwbAwaitClear.setVisible(true);
		kdwbAwaitClear.setEnabled(true);

	}

	/**
	 * 
	 * <p>
	 * Title: selectOrCancel
	 * </p>
	 * <p>
	 * Description: 设置全选或者全取消
	 * </p>
	 * 
	 * @param tabOrgTJ
	 * @param b
	 */
	private void selectOrCancel(KDTable tabOrgTJ, boolean b) {
		// TODO Auto-generated method stub
		IRow row;
		for (int i = 0, count = tabOrgTJ.getRowCount(); i < count; i++) {
			row = tabOrgTJ.getRow(i);
			if (row.getStyleAttributes().isHided()) {
				continue;
			} else {
				row.getCell("isSelected").setValue(Boolean.valueOf(b));
			}
		}
	}

	/**
	 * 
	 * <p>
	 * Title: getTableColl
	 * </p>
	 * <p>
	 * Description: 设置表格选中数据bostype
	 * </p>
	 * 
	 * @param table
	 * @return
	 */
	private List getTableColl(KDTable table, String value) {
		// TODO Auto-generated method stub
		List list = new ArrayList();
		IRow row;
		for (int i = 0, count = table.getRowCount(); i < count; i++) {
			row = table.getRow(i);
			Boolean isSelected = false;
			isSelected = (Boolean) row.getCell("isSelected").getValue();
			if (isSelected) {
				list.add(row.getCell("" + value + "").getValue());
			}
		}
		return list;
	}

	/**
	 * 
	 * <p>
	 * Title: initkDTableCom
	 * </p>
	 * <p>
	 * Description: 初始化kDTableCom选择框
	 * </p>
	 * 
	 * @throws BOSException
	 */
	private void initkDTableCom() throws BOSException {
		// TODO Auto-generated method stub
		kDTableCom.checkParsed();
		kDTableCom.removeRows();
		BizUpLoadConfCollection bizcoll = BizUpLoadConfFactory
				.getRemoteInstance().getBizUpLoadConfCollection();
		if (bizcoll != null && bizcoll.size() > 0) {
			BizUpLoadConfInfo bizinfo = null;
			for (int i = 0, count = bizcoll.size(); i < count; i++) {
				bizinfo = bizcoll.get(i);
				IRow row = kDTableCom.addRow();
				row.getCell("isSelected").setValue(Boolean.FALSE);
				row.getCell("isSelected").getStyleAttributes().setLocked(false);
				row.getCell("id").setUserObject(bizinfo.getId());
				row.getCell("companyNumber").setValue(bizinfo.getNumber());
				row.getCell("companyName").setValue(bizinfo.getName());
				row.getCell("productID").setValue(bizinfo.getBOSTYPE());
			}
		}
	}

	/**
	 * 
	 * <p>
	 * Title: initkDTableType
	 * </p>
	 * <p>
	 * Description: 初始化单据界面
	 * </p>
	 * 
	 * @param name
	 *            表名
	 * @param startobj
	 *            开始时间
	 * @param endobj
	 *            结束时间
	 * @param flag
	 *            单据状态 1：已审核 0：暂存
	 * @throws ParseException
	 * @throws BOSException
	 * @throws SQLException
	 * @throws SQLException
	 */
	private void initkDTableType(String name, Object startobj, Object endobj,
			int flag) throws ParseException, BOSException, SQLException {
		// TODO Auto-generated method stub
		if (startobj == null && endobj == null) {
			MsgBox.showInfo("选择时间为空");
			return;
		}

		int value = 0;
		if (flag == 1) {
			value = 5;
		} else if (flag == 0) {
			value = 1;
		}
		SimpleDateFormat sdf1 = new SimpleDateFormat(
				"EEE MMM dd HH:mm:ss Z yyyy", Locale.UK);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = sdf1.parse(endobj.toString());
		String endTime = sdf.format(date);
		Date date1 = sdf1.parse(startobj.toString());
		String startTime = sdf.format(date1);
		String sql = "select * from " + name + " where cfstate=" + value
				+ " and fbizdate BETWEEN {ts'" + startTime + "'} AND {ts'"
				+ endTime + "'}";
		IRowSet rowSet = SQLExecutorFactory.getRemoteInstance(sql).executeSQL();
		while (rowSet.next()) {
			String id = rowSet.getString("fid");
			// 单据编号
			String nubmer = rowSet.getString("fnumber");
			String name2 = rowSet.getString("cf"
					+ name.substring(7, name.length()) + "name");
			String upload = rowSet.getString("cfisupload");
			IRow row = kDTableType.addRow();
			row.getCell("id").setValue(id);
			row.getCell("isSelected").setValue(Boolean.FALSE);
			row.getCell("isSelected").getStyleAttributes().setLocked(false);
			row.getCell("bizNumber").setValue(nubmer);
			row.getCell("bizName").setValue(name2);
			row.getCell("upLoadBizType").setValue(upload);
		}
	}
}
