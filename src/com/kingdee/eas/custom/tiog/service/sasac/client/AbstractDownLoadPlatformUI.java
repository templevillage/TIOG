/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.service.sasac.client;

import org.apache.log4j.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.BorderFactory;
import javax.swing.event.*;
import javax.swing.KeyStroke;

import com.kingdee.bos.ctrl.swing.*;
import com.kingdee.bos.ctrl.kdf.table.*;
import com.kingdee.bos.ctrl.kdf.data.event.*;
import com.kingdee.bos.dao.*;
import com.kingdee.bos.dao.query.*;
import com.kingdee.bos.metadata.*;
import com.kingdee.bos.metadata.entity.*;
import com.kingdee.bos.ui.face.*;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.enums.EnumUtils;
import com.kingdee.bos.ui.face.UIRuleUtil;
import com.kingdee.bos.ctrl.swing.event.*;
import com.kingdee.bos.ctrl.kdf.table.event.*;
import com.kingdee.bos.ctrl.extendcontrols.*;
import com.kingdee.bos.ctrl.kdf.util.render.*;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.uistatemanage.ActionStateConst;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.appframework.uip.UINavigator;


/**
 * output class name
 */
public abstract class AbstractDownLoadPlatformUI extends com.kingdee.eas.framework.client.CoreUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractDownLoadPlatformUI.class);
    protected com.kingdee.bos.ctrl.swing.KDLabel kDLabel1;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kDTableCom;
    protected com.kingdee.bos.ctrl.swing.KDContainer kDContainer1;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kdwbComSelected;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kdwbComClear;
    protected com.kingdee.bos.ctrl.swing.KDContainer kDContainer3;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kdwbAwaitSelected;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kdwbAwaitClear;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kdwbh01;
    protected com.kingdee.bos.ctrl.swing.KDSeparator kDSeparator2;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton1;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton2;
    protected com.kingdee.bos.ctrl.swing.KDRadioButton kDRadioButton3;
    protected com.kingdee.bos.ctrl.swing.KDSeparator kDSeparator4;
    protected com.kingdee.bos.ctrl.swing.KDScrollPane kDScrollPane1;
    protected com.kingdee.bos.ctrl.swing.KDTextArea kDTextArea1;
    protected ActionComSelected actionComSelected = null;
    protected ActionTypeSelected actionTypeSelected = null;
    protected ActionComClear actionComClear = null;
    protected ActionTypeClear actionTypeClear = null;
    protected ActionSync actionSync = null;
    protected ActionAntiSync actionAntiSync = null;
    protected ActionCancel actionCancel = null;
    protected ActionExecute actionExecute = null;
    protected ActionQuery actionQuery = null;
    protected ActionAwaitSelected actionAwaitSelected = null;
    protected ActionAwaitClear actionAwaitClear = null;
    protected ActionCancel2 actionCancel2 = null;
    protected ActionH01 actionH01 = null;
    protected ActionH02 actionH02 = null;
    protected ActionH03 actionH03 = null;
    /**
     * output class constructor
     */
    public AbstractDownLoadPlatformUI() throws Exception
    {
        super();
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractDownLoadPlatformUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        //actionComSelected
        this.actionComSelected = new ActionComSelected(this);
        getActionManager().registerAction("actionComSelected", actionComSelected);
         this.actionComSelected.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionTypeSelected
        this.actionTypeSelected = new ActionTypeSelected(this);
        getActionManager().registerAction("actionTypeSelected", actionTypeSelected);
         this.actionTypeSelected.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionComClear
        this.actionComClear = new ActionComClear(this);
        getActionManager().registerAction("actionComClear", actionComClear);
         this.actionComClear.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionTypeClear
        this.actionTypeClear = new ActionTypeClear(this);
        getActionManager().registerAction("actionTypeClear", actionTypeClear);
         this.actionTypeClear.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionSync
        this.actionSync = new ActionSync(this);
        getActionManager().registerAction("actionSync", actionSync);
         this.actionSync.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionAntiSync
        this.actionAntiSync = new ActionAntiSync(this);
        getActionManager().registerAction("actionAntiSync", actionAntiSync);
         this.actionAntiSync.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionCancel
        this.actionCancel = new ActionCancel(this);
        getActionManager().registerAction("actionCancel", actionCancel);
         this.actionCancel.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionExecute
        this.actionExecute = new ActionExecute(this);
        getActionManager().registerAction("actionExecute", actionExecute);
         this.actionExecute.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionQuery
        this.actionQuery = new ActionQuery(this);
        getActionManager().registerAction("actionQuery", actionQuery);
         this.actionQuery.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionAwaitSelected
        this.actionAwaitSelected = new ActionAwaitSelected(this);
        getActionManager().registerAction("actionAwaitSelected", actionAwaitSelected);
         this.actionAwaitSelected.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionAwaitClear
        this.actionAwaitClear = new ActionAwaitClear(this);
        getActionManager().registerAction("actionAwaitClear", actionAwaitClear);
         this.actionAwaitClear.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionCancel2
        this.actionCancel2 = new ActionCancel2(this);
        getActionManager().registerAction("actionCancel2", actionCancel2);
         this.actionCancel2.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionH01
        this.actionH01 = new ActionH01(this);
        getActionManager().registerAction("actionH01", actionH01);
         this.actionH01.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionH02
        this.actionH02 = new ActionH02(this);
        getActionManager().registerAction("actionH02", actionH02);
         this.actionH02.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionH03
        this.actionH03 = new ActionH03(this);
        getActionManager().registerAction("actionH03", actionH03);
         this.actionH03.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        this.kDLabel1 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.kDTableCom = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.kDContainer1 = new com.kingdee.bos.ctrl.swing.KDContainer();
        this.kdwbComSelected = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kdwbComClear = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDContainer3 = new com.kingdee.bos.ctrl.swing.KDContainer();
        this.kdwbAwaitSelected = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kdwbAwaitClear = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kdwbh01 = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDSeparator2 = new com.kingdee.bos.ctrl.swing.KDSeparator();
        this.kDRadioButton1 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDRadioButton2 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDRadioButton3 = new com.kingdee.bos.ctrl.swing.KDRadioButton();
        this.kDSeparator4 = new com.kingdee.bos.ctrl.swing.KDSeparator();
        this.kDScrollPane1 = new com.kingdee.bos.ctrl.swing.KDScrollPane();
        this.kDTextArea1 = new com.kingdee.bos.ctrl.swing.KDTextArea();
        this.kDLabel1.setName("kDLabel1");
        this.kDTableCom.setName("kDTableCom");
        this.kDContainer1.setName("kDContainer1");
        this.kdwbComSelected.setName("kdwbComSelected");
        this.kdwbComClear.setName("kdwbComClear");
        this.kDContainer3.setName("kDContainer3");
        this.kdwbAwaitSelected.setName("kdwbAwaitSelected");
        this.kdwbAwaitClear.setName("kdwbAwaitClear");
        this.kdwbh01.setName("kdwbh01");
        this.kDSeparator2.setName("kDSeparator2");
        this.kDRadioButton1.setName("kDRadioButton1");
        this.kDRadioButton2.setName("kDRadioButton2");
        this.kDRadioButton3.setName("kDRadioButton3");
        this.kDSeparator4.setName("kDSeparator4");
        this.kDScrollPane1.setName("kDScrollPane1");
        this.kDTextArea1.setName("kDTextArea1");
        // CoreUI		
        this.setAutoscrolls(true);		
        this.btnPageSetup.setVisible(false);		
        this.btnCloud.setVisible(false);		
        this.btnXunTong.setVisible(false);		
        this.kDSeparatorCloud.setVisible(false);		
        this.menuItemPageSetup.setVisible(false);		
        this.menuItemCloudFeed.setVisible(false);		
        this.menuItemCloudScreen.setEnabled(false);		
        this.menuItemCloudScreen.setVisible(false);		
        this.menuItemCloudShare.setVisible(false);		
        this.kdSeparatorFWFile1.setVisible(false);
        // kDLabel1		
        this.kDLabel1.setText(resHelper.getString("kDLabel1.text"));		
        this.kDLabel1.setFont(resHelper.getFont("kDLabel1.font"));
        // kDTableCom
		String kDTableComStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol1\"><c:Protection hidden=\"true\" /></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"isSelected\" t:width=\"50\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"id\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol1\" /><t:Column t:key=\"companyNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"companyName\" t:width=\"150\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"productID\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header1\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{isSelected}</t:Cell><t:Cell>$Resource{id}</t:Cell><t:Cell>$Resource{companyNumber}</t:Cell><t:Cell>$Resource{companyName}</t:Cell><t:Cell>$Resource{productID}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kDTableCom.setFormatXml(resHelper.translateString("kDTableCom",kDTableComStrXML));

        

        // kDContainer1		
        this.kDContainer1.setTitle(resHelper.getString("kDContainer1.title"));
        // kdwbComSelected
        this.kdwbComSelected.setAction((IItemAction)ActionProxyFactory.getProxy(actionComSelected, new Class[] { IItemAction.class }, getServiceContext()));		
        this.kdwbComSelected.setText(resHelper.getString("kdwbComSelected.text"));
        // kdwbComClear
        this.kdwbComClear.setAction((IItemAction)ActionProxyFactory.getProxy(actionComClear, new Class[] { IItemAction.class }, getServiceContext()));		
        this.kdwbComClear.setText(resHelper.getString("kdwbComClear.text"));
        // kDContainer3		
        this.kDContainer3.setTitle(resHelper.getString("kDContainer3.title"));
        // kdwbAwaitSelected
        this.kdwbAwaitSelected.setAction((IItemAction)ActionProxyFactory.getProxy(actionAwaitSelected, new Class[] { IItemAction.class }, getServiceContext()));		
        this.kdwbAwaitSelected.setText(resHelper.getString("kdwbAwaitSelected.text"));
        // kdwbAwaitClear
        this.kdwbAwaitClear.setAction((IItemAction)ActionProxyFactory.getProxy(actionAwaitClear, new Class[] { IItemAction.class }, getServiceContext()));		
        this.kdwbAwaitClear.setText(resHelper.getString("kdwbAwaitClear.text"));
        // kdwbh01
        this.kdwbh01.setAction((IItemAction)ActionProxyFactory.getProxy(actionH01, new Class[] { IItemAction.class }, getServiceContext()));		
        this.kdwbh01.setText(resHelper.getString("kdwbh01.text"));
        // kDSeparator2
        // kDRadioButton1		
        this.kDRadioButton1.setText(resHelper.getString("kDRadioButton1.text"));
        // kDRadioButton2		
        this.kDRadioButton2.setText(resHelper.getString("kDRadioButton2.text"));
        // kDRadioButton3		
        this.kDRadioButton3.setText(resHelper.getString("kDRadioButton3.text"));
        // kDSeparator4		
        this.kDSeparator4.setOrientation(1);
        // kDScrollPane1
        // kDTextArea1
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(10, 10, 1280, 700));
        this.setLayout(new KDLayout());
        this.putClientProperty("OriginalBounds", new Rectangle(10, 10, 1280, 700));
        kDLabel1.setBounds(new Rectangle(479, 15, 297, 27));
        this.add(kDLabel1, new KDLayout.Constraints(479, 15, 297, 27, 0));
        kDTableCom.setBounds(new Rectangle(20, 106, 404, 387));
        this.add(kDTableCom, new KDLayout.Constraints(20, 106, 404, 387, 0));
        kDContainer1.setBounds(new Rectangle(22, 85, 224, 19));
        this.add(kDContainer1, new KDLayout.Constraints(22, 85, 224, 19, 0));
        kdwbComSelected.setBounds(new Rectangle(302, 87, 60, 19));
        this.add(kdwbComSelected, new KDLayout.Constraints(302, 87, 60, 19, 0));
        kdwbComClear.setBounds(new Rectangle(363, 87, 60, 19));
        this.add(kdwbComClear, new KDLayout.Constraints(363, 87, 60, 19, 0));
        kDContainer3.setBounds(new Rectangle(691, 86, 196, 19));
        this.add(kDContainer3, new KDLayout.Constraints(691, 86, 196, 19, 0));
        kdwbAwaitSelected.setBounds(new Rectangle(1107, 85, 60, 19));
        this.add(kdwbAwaitSelected, new KDLayout.Constraints(1107, 85, 60, 19, 0));
        kdwbAwaitClear.setBounds(new Rectangle(1172, 86, 60, 19));
        this.add(kdwbAwaitClear, new KDLayout.Constraints(1172, 86, 60, 19, 0));
        kdwbh01.setBounds(new Rectangle(483, 476, 117, 19));
        this.add(kdwbh01, new KDLayout.Constraints(483, 476, 117, 19, 0));
        kDSeparator2.setBounds(new Rectangle(7, 56, 1225, 10));
        this.add(kDSeparator2, new KDLayout.Constraints(7, 56, 1225, 10, 0));
        kDRadioButton1.setBounds(new Rectangle(480, 175, 140, 19));
        this.add(kDRadioButton1, new KDLayout.Constraints(480, 175, 140, 19, 0));
        kDRadioButton2.setBounds(new Rectangle(480, 359, 140, 19));
        this.add(kDRadioButton2, new KDLayout.Constraints(480, 359, 140, 19, 0));
        kDRadioButton3.setBounds(new Rectangle(480, 267, 140, 19));
        this.add(kDRadioButton3, new KDLayout.Constraints(480, 267, 140, 19, 0));
        kDSeparator4.setBounds(new Rectangle(634, 56, 8, 503));
        this.add(kDSeparator4, new KDLayout.Constraints(634, 56, 8, 503, 0));
        kDScrollPane1.setBounds(new Rectangle(684, 104, 548, 549));
        this.add(kDScrollPane1, new KDLayout.Constraints(684, 104, 548, 549, 0));
        kDContainer1.getContentPane().setLayout(null);        kDContainer3.getContentPane().setLayout(null);        //kDScrollPane1
        kDScrollPane1.getViewport().add(kDTextArea1, null);

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {
        this.menuBar.add(menuFile);
        this.menuBar.add(menuTool);
        this.menuBar.add(MenuService);
        this.menuBar.add(menuHelp);
        //menuFile
        menuFile.add(menuItemPageSetup);
        menuFile.add(kDSeparator1);
        menuFile.add(menuItemCloudFeed);
        menuFile.add(menuItemCloudScreen);
        menuFile.add(menuItemCloudShare);
        menuFile.add(kdSeparatorFWFile1);
        menuFile.add(menuItemExitCurrent);
        //menuTool
        menuTool.add(menuItemSendMessage);
        menuTool.add(menuItemCalculator);
        menuTool.add(menuItemToolBarCustom);
        //MenuService
        MenuService.add(MenuItemKnowStore);
        MenuService.add(MenuItemAnwser);
        MenuService.add(SepratorService);
        MenuService.add(MenuItemRemoteAssist);
        //menuHelp
        menuHelp.add(menuItemHelp);
        menuHelp.add(kDSeparator12);
        menuHelp.add(menuItemRegPro);
        menuHelp.add(menuItemPersonalSite);
        menuHelp.add(helpseparatorDiv);
        menuHelp.add(menuitemProductval);
        menuHelp.add(kDSeparatorProduct);
        menuHelp.add(menuItemAbout);

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {
        this.toolBar.add(btnPageSetup);
        this.toolBar.add(btnCloud);
        this.toolBar.add(btnXunTong);
        this.toolBar.add(kDSeparatorCloud);


    }

	//Regiester control's property binding.
	private void registerBindings(){		
	}
	//Regiester UI State
	private void registerUIState(){		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.tiog.service.sasac.app.DownLoadPlatformUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }



	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
    }

    /**
     * output loadFields method
     */
    public void loadFields()
    {
        dataBinder.loadFields();
    }
    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
    }

    	

    /**
     * output actionComSelected_actionPerformed method
     */
    public void actionComSelected_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionTypeSelected_actionPerformed method
     */
    public void actionTypeSelected_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionComClear_actionPerformed method
     */
    public void actionComClear_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionTypeClear_actionPerformed method
     */
    public void actionTypeClear_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionSync_actionPerformed method
     */
    public void actionSync_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionAntiSync_actionPerformed method
     */
    public void actionAntiSync_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionCancel_actionPerformed method
     */
    public void actionCancel_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionExecute_actionPerformed method
     */
    public void actionExecute_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionQuery_actionPerformed method
     */
    public void actionQuery_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionAwaitSelected_actionPerformed method
     */
    public void actionAwaitSelected_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionAwaitClear_actionPerformed method
     */
    public void actionAwaitClear_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionCancel2_actionPerformed method
     */
    public void actionCancel2_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionH01_actionPerformed method
     */
    public void actionH01_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionH02_actionPerformed method
     */
    public void actionH02_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionH03_actionPerformed method
     */
    public void actionH03_actionPerformed(ActionEvent e) throws Exception
    {
    }
	public RequestContext prepareActionComSelected(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionComSelected() {
    	return false;
    }
	public RequestContext prepareActionTypeSelected(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionTypeSelected() {
    	return false;
    }
	public RequestContext prepareActionComClear(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionComClear() {
    	return false;
    }
	public RequestContext prepareActionTypeClear(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionTypeClear() {
    	return false;
    }
	public RequestContext prepareActionSync(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionSync() {
    	return false;
    }
	public RequestContext prepareActionAntiSync(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionAntiSync() {
    	return false;
    }
	public RequestContext prepareActionCancel(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionCancel() {
    	return false;
    }
	public RequestContext prepareActionExecute(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionExecute() {
    	return false;
    }
	public RequestContext prepareActionQuery(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionQuery() {
    	return false;
    }
	public RequestContext prepareActionAwaitSelected(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionAwaitSelected() {
    	return false;
    }
	public RequestContext prepareActionAwaitClear(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionAwaitClear() {
    	return false;
    }
	public RequestContext prepareActionCancel2(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionCancel2() {
    	return false;
    }
	public RequestContext prepareActionH01(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionH01() {
    	return false;
    }
	public RequestContext prepareActionH02(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionH02() {
    	return false;
    }
	public RequestContext prepareActionH03(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionH03() {
    	return false;
    }

    /**
     * output ActionComSelected class
     */     
    protected class ActionComSelected extends ItemAction {     
    
        public ActionComSelected()
        {
            this(null);
        }

        public ActionComSelected(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionComSelected.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionComSelected.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionComSelected.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionComSelected", "actionComSelected_actionPerformed", e);
        }
    }

    /**
     * output ActionTypeSelected class
     */     
    protected class ActionTypeSelected extends ItemAction {     
    
        public ActionTypeSelected()
        {
            this(null);
        }

        public ActionTypeSelected(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionTypeSelected.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionTypeSelected.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionTypeSelected.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionTypeSelected", "actionTypeSelected_actionPerformed", e);
        }
    }

    /**
     * output ActionComClear class
     */     
    protected class ActionComClear extends ItemAction {     
    
        public ActionComClear()
        {
            this(null);
        }

        public ActionComClear(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionComClear.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionComClear.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionComClear.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionComClear", "actionComClear_actionPerformed", e);
        }
    }

    /**
     * output ActionTypeClear class
     */     
    protected class ActionTypeClear extends ItemAction {     
    
        public ActionTypeClear()
        {
            this(null);
        }

        public ActionTypeClear(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionTypeClear.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionTypeClear.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionTypeClear.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionTypeClear", "actionTypeClear_actionPerformed", e);
        }
    }

    /**
     * output ActionSync class
     */     
    protected class ActionSync extends ItemAction {     
    
        public ActionSync()
        {
            this(null);
        }

        public ActionSync(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionSync.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionSync.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionSync.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionSync", "actionSync_actionPerformed", e);
        }
    }

    /**
     * output ActionAntiSync class
     */     
    protected class ActionAntiSync extends ItemAction {     
    
        public ActionAntiSync()
        {
            this(null);
        }

        public ActionAntiSync(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionAntiSync.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAntiSync.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAntiSync.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionAntiSync", "actionAntiSync_actionPerformed", e);
        }
    }

    /**
     * output ActionCancel class
     */     
    protected class ActionCancel extends ItemAction {     
    
        public ActionCancel()
        {
            this(null);
        }

        public ActionCancel(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionCancel.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionCancel.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionCancel.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionCancel", "actionCancel_actionPerformed", e);
        }
    }

    /**
     * output ActionExecute class
     */     
    protected class ActionExecute extends ItemAction {     
    
        public ActionExecute()
        {
            this(null);
        }

        public ActionExecute(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionExecute.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionExecute.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionExecute.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionExecute", "actionExecute_actionPerformed", e);
        }
    }

    /**
     * output ActionQuery class
     */     
    protected class ActionQuery extends ItemAction {     
    
        public ActionQuery()
        {
            this(null);
        }

        public ActionQuery(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionQuery.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionQuery.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionQuery.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionQuery", "actionQuery_actionPerformed", e);
        }
    }

    /**
     * output ActionAwaitSelected class
     */     
    protected class ActionAwaitSelected extends ItemAction {     
    
        public ActionAwaitSelected()
        {
            this(null);
        }

        public ActionAwaitSelected(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionAwaitSelected.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAwaitSelected.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAwaitSelected.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionAwaitSelected", "actionAwaitSelected_actionPerformed", e);
        }
    }

    /**
     * output ActionAwaitClear class
     */     
    protected class ActionAwaitClear extends ItemAction {     
    
        public ActionAwaitClear()
        {
            this(null);
        }

        public ActionAwaitClear(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionAwaitClear.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAwaitClear.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAwaitClear.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionAwaitClear", "actionAwaitClear_actionPerformed", e);
        }
    }

    /**
     * output ActionCancel2 class
     */     
    protected class ActionCancel2 extends ItemAction {     
    
        public ActionCancel2()
        {
            this(null);
        }

        public ActionCancel2(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionCancel2.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionCancel2.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionCancel2.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionCancel2", "actionCancel2_actionPerformed", e);
        }
    }

    /**
     * output ActionH01 class
     */     
    protected class ActionH01 extends ItemAction {     
    
        public ActionH01()
        {
            this(null);
        }

        public ActionH01(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            _tempStr = resHelper.getString("ActionH01.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionH01.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionH01.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionH01", "actionH01_actionPerformed", e);
        }
    }

    /**
     * output ActionH02 class
     */     
    protected class ActionH02 extends ItemAction {     
    
        public ActionH02()
        {
            this(null);
        }

        public ActionH02(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionH02.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionH02.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionH02.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionH02", "actionH02_actionPerformed", e);
        }
    }

    /**
     * output ActionH03 class
     */     
    protected class ActionH03 extends ItemAction {     
    
        public ActionH03()
        {
            this(null);
        }

        public ActionH03(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionH03.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionH03.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionH03.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractDownLoadPlatformUI.this, "ActionH03", "actionH03_actionPerformed", e);
        }
    }

    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.tiog.service.sasac.client", "DownLoadPlatformUI");
    }




}