package com.kingdee.eas.custom.tiog.service.sasac.client;

import java.awt.Frame;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.ctrl.kdf.table.IRow;
import com.kingdee.bos.ctrl.kdf.table.KDTable;
import com.kingdee.bos.ctrl.swing.KDScrollPane;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.bos.metadata.IMetaDataLoader;
import com.kingdee.bos.metadata.MetaDataLoaderFactory;
import com.kingdee.bos.metadata.entity.EntityObjectInfo;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.base.core.fm.ContextHelperFactory;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.base.permission.client.longtime.ILongTimeTask;
import com.kingdee.eas.base.permission.client.longtime.LongTimeDialog;
import com.kingdee.eas.base.permission.client.util.UITools;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.cp.mrm.MeetingTypeInfo;
import com.kingdee.eas.custom.tiog.basedata.CatalogFactory;
import com.kingdee.eas.custom.tiog.basedata.CatalogInfo;
import com.kingdee.eas.custom.tiog.basedata.CatalogMainFactory;
import com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo;
import com.kingdee.eas.custom.tiog.basedata.CatalogMainTree;
import com.kingdee.eas.custom.tiog.basedata.CatalogMainTreeFactory;
import com.kingdee.eas.custom.tiog.basedata.CatalogMainTreeInfo;
import com.kingdee.eas.custom.tiog.basedata.MeetingFormFactory;
import com.kingdee.eas.custom.tiog.basedata.MeetingFormInfo;
import com.kingdee.eas.custom.tiog.basedata.MeetingTypeFactory;
import com.kingdee.eas.custom.tiog.basedata.RegulationTypeFactory;
import com.kingdee.eas.custom.tiog.basedata.RegulationTypeInfo;
import com.kingdee.eas.custom.tiog.basedata.SpecialFactory;
import com.kingdee.eas.custom.tiog.basedata.SpecialInfo;
import com.kingdee.eas.custom.tiog.basedata.TaskSourceFactory;
import com.kingdee.eas.custom.tiog.basedata.TaskSourceInfo;
import com.kingdee.eas.custom.tiog.basedata.VoteModeFactory;
import com.kingdee.eas.custom.tiog.basedata.VoteModeInfo;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.CatalogList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.MeetingFormList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.MeetingTypeList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.RegulationTypeList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.SourceList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.SpecialList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.VoteModeList;
import com.kingdee.eas.custom.tiog.middleware.service.LogService;
import com.kingdee.eas.custom.tiog.middleware.vo.basicdata.MeetingFormVO;
import com.kingdee.eas.custom.tiog.middleware.vo.basicdata.MeetingTypeVO;
import com.kingdee.eas.custom.tiog.middleware.vo.basicdata.RegulationTypeVO;
import com.kingdee.eas.custom.tiog.middleware.vo.basicdata.VoteModeVO;
import com.kingdee.eas.custom.tiog.service.baseconf.DownLoadConfCollection;
import com.kingdee.eas.custom.tiog.service.baseconf.DownLoadConfFactory;
import com.kingdee.eas.custom.tiog.service.baseconf.DownLoadConfInfo;
import com.kingdee.eas.fm.common.FMIsqlFacadeFactory;
import com.kingdee.eas.util.client.MsgBox;
import com.kingdee.jdbc.rowset.IRowSet;

/**
 * Title: DownLoadPlatformUICTEx
 * Description: 对静态数据同步控制台界面功能扩展
 * @author 冯小红 Email:tjfengxiaohong@kingdee.com
 * @date 2020-3-25
 */


public class DownLoadPlatformUICTEx extends DownLoadPlatformUI{
	private static Logger logger = Logger
    .getLogger("com.kingdee.eas.custom.tiog.service.sasac.client.DownLoadPlatformUICTEx");

	Map<String,String> localDataMap =new HashMap();
	public String str_Result = "";
	//public StringBuffer logMessage =new StringBuffer();
	String logMessage="";
	public DownLoadPlatformUICTEx() throws Exception {
		super();
	}



	/**
	 * （非 Javadoc）
	 * <p>Title: onLoad</p>
	 * <p>Description: 界面更新时对数据清单进行渲染</p>
	 * @throws Exception
	 * @see com.kingdee.eas.framework.client.CoreUI#onLoad()
	 */

	@Override
	public void onLoad() throws Exception {
		super.onLoad();
		//设置按钮可用
		setButtonEnable();
		//初始化 静态资源表格数据
		initKDTableCom();


	}

	/**
	 * 
	 * <p>Title: setButtonEnable</p>
	 * <p>Description:设置按钮可用 </p>
	 */
	private void setButtonEnable() {
		//设置静态数据全选全清按钮状态为可用
		this.kdwbComSelected.setEnabled(true);
		this.kdwbComClear.setEnabled(true);
		//设置结果日志全选全清按钮状态可用
		this.kdwbAwaitClear.setEnabled(true);
		//this.kdwbAwaitSelected.setEnabled(true);

	}

	/**
	 * （非 Javadoc）
	 * <p>Title: actionComClear_actionPerformed</p>
	 * <p>Description: 表格界面全选</p>
	 * @param e
	 * @throws Exception
	 * @see com.kingdee.eas.custom.tiog.service.sasac.client.DownLoadPlatformUI#actionComClear_actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionComSelected_actionPerformed(ActionEvent e)
	throws Exception {
		super.actionComSelected_actionPerformed(e);
		selectOrCancel(kDTableCom,true);
	}
	/**
	 * （非 Javadoc）
	 * <p>Title: actionComSelected_actionPerformed</p>
	 * <p>Description: 表格界面全清</p>
	 * @param e
	 * @throws Exception
	 * @see com.kingdee.eas.custom.tiog.service.sasac.client.DownLoadPlatformUI#actionComSelected_actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionComClear_actionPerformed(ActionEvent e) throws Exception {
		super.actionComClear_actionPerformed(e);
		selectOrCancel(kDTableCom,false);

	}

	/**
	 * 
	 * <p>Title: selectOrCancel</p>
	 * <p>Description:全选全清 </p>
	 * @param tabOrgTJ
	 * @param b
	 */
	private void selectOrCancel(KDTable tabOrgTJ, boolean b) {
		IRow row;
		for (int i = 0, count = tabOrgTJ.getRowCount(); i < count; i++) {
			row = tabOrgTJ.getRow(i);
			if (row.getStyleAttributes().isHided()) {
				continue;
			} else {
				row.getCell("isSelected").setValue(Boolean.valueOf(b));
			}
		}
	}
	
	
	/**
	 * （非 Javadoc）
	 * <p>Title: actionAwaitClear_actionPerformed</p>
	 * <p>Description:日志框全清按钮 </p>
	 * @param e
	 * @throws Exception
	 * @see com.kingdee.eas.custom.tiog.service.sasac.client.DownLoadPlatformUI#actionAwaitClear_actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionAwaitClear_actionPerformed(ActionEvent e)
			throws Exception {
		super.actionAwaitClear_actionPerformed(e);
		this.kDTextArea1.setText("");
	}


	/**
	 * （非 Javadoc）
	 * <p>Title: actionAwaitSelected_actionPerformed</p>
	 * <p>Description:日志框全选按钮 </p>
	 * @param e
	 * @throws Exception
	 * @see com.kingdee.eas.custom.tiog.service.sasac.client.DownLoadPlatformUI#actionAwaitSelected_actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionAwaitSelected_actionPerformed(ActionEvent e)
			throws Exception {
		// TODO Auto-generated method stub
		super.actionAwaitSelected_actionPerformed(e);
	}



	/**
	 * 
	 * <p>Title: initKDTable</p>
	 * <p>Description: 初始化静态数据数据</p>
	 * @throws BOSException 
	 */
	private void initKDTableCom() throws BOSException {

		kDTableCom.checkParsed();
		kDTableCom.removeRows();
		//1.获取静态资源基础资料对象  目前还未有该对象用 事项状态对象来代替
		DownLoadConfCollection co =DownLoadConfFactory.getRemoteInstance().getDownLoadConfCollection();
		if(co !=null && co.size()>0){
			DownLoadConfInfo downLoadConfInfo =null;
			for (int i = 0, count = co.size(); i < count; i++) {
				downLoadConfInfo=co.get(i);
				IRow row = kDTableCom.addRow();
				row.getCell("isSelected").setValue(Boolean.FALSE);
				row.getCell("isSelected").getStyleAttributes().setLocked(false);
				row.getCell("id").setUserObject(downLoadConfInfo); //基础资料实体
				row.getCell("companyNumber").setValue(downLoadConfInfo.getNumber()); //编码
				row.getCell("companyName").setValue(downLoadConfInfo.getName());//名称
				row.getCell("productID").setValue(downLoadConfInfo.getBOSTYPE()); //BOSTYPE
				//用查询到的对象中的属性为此表格进行赋值
			}
		}

	}


	/**
	 * （非 Javadoc）
	 * <p>Title: actionH01_actionPerformed</p>
	 * <p>Description:操作执行更新按钮 </p>
	 * @param e
	 * @throws Exception
	 * @see com.kingdee.eas.custom.tiog.service.sasac.client.DownLoadPlatformUI#actionH01_actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionH01_actionPerformed(ActionEvent e) throws Exception {
		System.out.println("进入此方法");

		//1.获取静态基础资料选中行数据  k:.名称  v:当前行数据.xml名称
		final Map<String,String> selectedLocatMap = getSelectedLocatMap();
		//2.获取国资委下载数据
		final Map<String,Object> testMap =getTestMap();

		//3.调取接口方法，获取国资委下载到的info集合   remoteMap形式 .xml名称  v:info
		//getRemoteDownLoadConfInfoList();
		//4.监听三个按钮的选中状态分别执行三个方法  三个按钮选中状态 组成一个集合
		if(this.kDRadioButton1.isSelected()){
			//4.1编码增量更新方法
			//logMessage.append("\n").append("开始执行编码增量更新方法").append("\n");
			
			//addNewInfo(selectedLocatMap,testMap);
			// int confirm = MsgBox.showConfirm2("将按照您选定的静态数据进行基础资料编码增量更新？");
			// 进度条
			 if(showConfirm("编码增量更新") == 0){
				 
			
            LongTimeDialog dialog = new LongTimeDialog((Frame) SwingUtilities.getWindowAncestor(this));
            dialog.setLongTimeTask(new ILongTimeTask() {

                @Override
                public void afterExec(Object paramObject) throws Exception {
                    UITools.showObject(paramObject);
                }

                @Override
                public Object exec() throws Exception {
                    return addNewInfo(selectedLocatMap,testMap);
                }

            });
            dialog.setTitle("正在同步数据，请稍等......");
            dialog.getContentPane().remove(dialog.getContentPane().getComponent(1));
            dialog.show();
			 }else{
				 MsgBox.showInfo("您取消了本次同步操作.");
			 }
			
		}

		if(this.kDRadioButton2.isSelected()){
			//4.2编码相同覆盖方法
			//updateLocalInfo(selectedLocatMap,testMap);
		}
		if(this.kDRadioButton3.isSelected()){
			//4.3清空覆盖更新
			//initLocalInfo(localList,remoteList);
			//addNewInfo(selectedLocatMap,testMap);
		}

		//4.remoteList.constainsKey(localList)
		//调用骏骏的日志工具类  进行日志保存
		LogService logService =new LogService();
		str_Result="请求成功";
		//logService.saveDownloadLog(testMap,str_Result);
		//界面日志保存方法

		super.actionH01_actionPerformed(e);
	}
	/**
	 * 
	 * <p>Title: showConfirm</p>
	 * <p>Description:确定，取消 </p>
	 * @param string
	 * @return 点[确认]返回：0 点[取消]返回：2
	 */
	private int showConfirm(String string) {
		String msg ="您将对选中静态数据进行编码增量更新操作 ";
		int flag = MsgBox.showConfirm2(this, msg);
		 logger.info("******************************************************基础资料类型个数提醒！"
	                + (flag == 0 ? "确定" : "取消"));
		return flag;
	}



	/**
	 * 
	 * <p>Title: initLogArea</p>
	 * <p>Description: 界面日志数据填充</p>
	 * @param logMessage2
	 */
	private void initLogArea(String logMessage) {
		System.out.println("进入界面日志保存方法");
		StringBuffer sb =new StringBuffer();
		UserInfo currentUser = ContextHelperFactory.getRemoteInstance().getCurrentUser();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = df.format(new Date());
		sb.append("操作人："+currentUser+",操作时间："+date).append(",").append("\n").append(logMessage);
		//this.kDTextArea1.setText(this.kDTextArea1.getText()+"\n"+sb.toString());
		//this.kDTextArea1.append((this.kDTextArea1.getText()+"\n"+sb.toString()));
		this.kDTextArea1.append(sb.toString());
		
	}



	//测试数据
	private Map<String, Object> getTestMap() {
		Map<String,Object> testMap =new HashMap();
		MeetingTypeList meet =new MeetingTypeList();
		List<MeetingTypeVO> meeting_type_list =new ArrayList();
		MeetingTypeVO meetingVO =new MeetingTypeVO();
		meetingVO.setType_code("T");
		meetingVO.setType_name("党委");
		meetingVO.setGroup_type("党委（党组）成员");
		meeting_type_list.add(meetingVO);
		meet.setMeeting_type_list(meeting_type_list);
		testMap.put("meeting_type.xml",meet);
		MeetingTypeVO meetingVO1 =new MeetingTypeVO();
		meetingVO1.setType_code("Te");
		meetingVO1.setType_name("党");
		meetingVO1.setGroup_type("党委（党组）成员");
		meeting_type_list.add(meetingVO1);
		meet.setMeeting_type_list(meeting_type_list);
		testMap.put("meeting_type.xml",meet);
		List<CatalogList> catalog_list =new ArrayList();
		CatalogList catlog =new CatalogList();
		catlog.setCatalog_code("H02");
		catlog.setCatalog_level("3");
		catlog.setCatalog_name("测试人员");
		catlog.setOrder_number("3");
		catlog.setCatalog_list(catalog_list);
		catalog_list.add(catlog);
		catlog =new CatalogList();
		catlog.setCatalog_code("HA");
		catlog.setCatalog_level("2");
		catlog.setCatalog_name("测试中层");
		catlog.setOrder_number("2");
		catlog.setCatalog_list(catalog_list);
		catalog_list.add(catlog);
		catlog =new CatalogList();
		catlog.setCatalog_code("H");
		catlog.setCatalog_level("1");
		catlog.setCatalog_name("测试开始");
		catlog.setOrder_number("1");
		catlog.setCatalog_list(catalog_list);
		catalog_list.add(catlog);
		testMap.put("catalog.xml", catlog);
		RegulationTypeList regList =new RegulationTypeList();
		List<RegulationTypeVO> regulation_type_list =new ArrayList();
		RegulationTypeVO regulationTypeVO =new RegulationTypeVO();
		regulationTypeVO.setFlag("否");
		regulationTypeVO.setName("制度");
		regulation_type_list.add(regulationTypeVO);
		regList.setRegulation_type_list(regulation_type_list);
		testMap.put("regulation_type.xml", regList);
		RegulationTypeVO regulationTypeVO1 =new RegulationTypeVO();
		regulationTypeVO1.setFlag("是");
		regulationTypeVO1.setName("公司");
		regulation_type_list.add(regulationTypeVO1);
		regList.setRegulation_type_list(regulation_type_list);
		testMap.put("regulation_type.xml", regList);
		MeetingFormList  meetingFormList=new MeetingFormList();
		List<MeetingFormVO> meeting_form_list =new ArrayList();
		MeetingFormVO meetingFormVO =new MeetingFormVO();
		meetingFormVO.setForm_code("09");
		meetingFormVO.setForm_name("测试");
		meeting_form_list.add(meetingFormVO);
		MeetingFormVO meetingFormVO1 =new MeetingFormVO();
		meetingFormVO1.setForm_code("test");
		meetingFormVO1.setForm_name("测一侧");
		meeting_form_list.add(meetingFormVO1);
		meetingFormList.setMeeting_form_list(meeting_form_list);
		testMap.put("meeting_form.xml", meetingFormList);
		SourceList sourceList =new SourceList();
		String[] source ={"测试重复","重复测试"};
		sourceList.setSource(source);
		testMap.put("source.xml", sourceList);
		SpecialList specialList =new SpecialList();
		String[] specSource={"测试专项","测试专项数据"};
		specialList.setSpecial(specSource);
		testMap.put("special.xml", specialList);
		VoteModeList voteModeList =new VoteModeList();
		List<VoteModeVO> vote_mode_list=new ArrayList();
		VoteModeVO vo =new VoteModeVO();
		vo.setMode_name("方式");
		vo.setMode_radix("应到");
		vo.setMode_rate("1/9");
		vote_mode_list.add(vo);
		voteModeList.setVote_mode_list(vote_mode_list);
		testMap.put("vote_mode.xml", voteModeList);
		VoteModeVO vo1 =new VoteModeVO();
		vo1.setMode_name("表决");
		vo1.setMode_radix("实到");
		vo1.setMode_rate("1998");
		vote_mode_list.add(vo1);
		voteModeList.setVote_mode_list(vote_mode_list);
		testMap.put("vote_mode.xml", voteModeList);

		return testMap;
	}



	/**
	 * 
	 * <p>Title: updateLocalInfo</p>
	 * <p>Description: 编码相同名称不同进行覆盖</p>
	 * @param selectedLocatMap
	 * @param testMap
	 */
	private void updateLocalInfo(Map<String, String> selectedLocatMap,
			Map<String, Object> testMap) {

	}



	/**
	 * 
	 * <p>Title: addNewInfo</p>
	 * <p>Description:编码增量更新方法 </p>
	 * @param selectedLocatMap
	 * @param testMap
	 */

	private Object addNewInfo(Map<String, String> selectedLocatMap,
			Map<String, Object> testMap) {
		//1.遍历selectedLocatMap 获取到numMap k:num  v:name
		//1.遍历本地查询到的map
		Iterator it=selectedLocatMap.keySet().iterator();
		while(it.hasNext()){
			//取出key
			String key=it.next().toString();
			//若下载数据中包含此key 则获取当前key对应的对象
			if(testMap.containsKey(key)){
				//获取到相应的对象并进行数据同步操作 
				try {
					Object object = testMap.get(key);
					String bosTypeId = selectedLocatMap.get(key);
					System.out.println("输出bosTypeID:"+bosTypeId);
					IMetaDataLoader metadataloader = MetaDataLoaderFactory.getRemoteMetaDataLoader();
					EntityObjectInfo entity = metadataloader.getEntity(new BOSObjectType(bosTypeId));
					String objectName = entity.getName();
					System.out.println("获取到对应的pojo对象名称为："+objectName);
					String tableName = entity.getTable().getName();
					System.out.println("tableName："+tableName);
					String billName = entity.getFullName();
					System.out.println("billName："+billName);
					IObjectValue ov = null;
					String objectClassName = entity.getObjectValueClass();
					Class objectClass = Class.forName(objectClassName);
					if("MeetingType".equals(objectName)){
						setMeetingTypeInfo(key,tableName,testMap);
					}
					if("MeetingForm".equals(objectName)){
						setMeetingFormInfo(key,tableName,testMap);
					}
					if("CatalogMain".equals(objectName)){
						setCatalogInfo(key,tableName,testMap);
					}
					if("VoteMode".equals(objectName)){
						setVoteModeInfo(key,tableName,testMap);
					}
					if("RegulationType".equals(objectName)){
						setRegulationTypeInfo(key,tableName,testMap);
					}
					if("Special".equals(objectName)){
						setSpecialInfo(key,tableName,testMap);
					}
					if("TaskSource".equals(objectName)){
						setSourceInfo(key,tableName,testMap);
					}
					
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("执行编码增量更新任务失败");
					logMessage="执行编码增量更新任务出现错误。"+e.getMessage().toString();
					//logMessage.append("执行编码增量更新任务出现错误。"+e.getMessage().toString());
					initLogArea(logMessage);
				}

			}

		}
		return null;
	}

	/**
	 * 
	 * <p>Title: setSourceInfo</p>
	 * <p>Description:任务来源 待</p>
	 * @param key
	 * @param tableName
	 * @param testMap
	 */
	private void setSourceInfo(String key, String tableName,
			Map<String, Object> testMap) {
		Map<String,String> numMap =new HashMap();
		Map<String,String> nameMap =new HashMap();
		try {
			String sql ="select fnumber as num,fname_l2 as name from "+tableName+"";
			System.out.println("打印setVoteModeInfo方法查询sql:"+sql);
			IRowSet rs = FMIsqlFacadeFactory.getRemoteInstance().executeQuery(sql.toString(), null);
			//查询
			while(rs.next()){
				String num =rs.getString("num");
				String name=rs.getString("name");
				numMap.put(name, num);
			}
			SourceList sourceList = (SourceList) testMap.get(key);

			String[] sourceName = sourceList.getSource();
			for(int j =0;j<sourceName.length;j++){
				String name = sourceName[j];
				if(numMap==null || (numMap!=null && !numMap.containsKey(name))){
					TaskSourceInfo taskSoureInfo =new TaskSourceInfo();
					taskSoureInfo.setName(name);
					TaskSourceFactory.getRemoteInstance().save(taskSoureInfo);
					//if(j==sourceName.length-1){
					//logMessage.append("执行【任务来源】基础资料*编码增量更新*操作成功。").append("\n");
					//logMessage.append("执行【任务来源】编码增量更新，新增名称：").append(name).append(" 操作成功\n");
					String msg ="执行【任务来源】编码增量更新，新增名称："+name+" 操作成功\n";
					initLogArea(msg);
					//}
				}else{
					//logMessage.setLength(0);
					logMessage="执行【任务来源】编码增量更新，新增名称："+name+" 在系统中已经存在，新增失败\n";
					//logMessage.append("执行【任务来源】编码增量更新，新增名称：").append(name).append(" 在系统中已经存在，新增失败\n");
					initLogArea(logMessage);
				}

			} 
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("任务来源数据同步失败");
			logMessage="执行【任务来源】基础资料*编码增量更新*操作失败。"+e.getMessage().toString();
			//logMessage.append("执行【任务来源】基础资料*编码增量更新*操作失败。"+e.getMessage().toString());
			initLogArea(logMessage);
		} 
	}



	/**
	 * 
	 * <p>Title: setSpecialInfo</p>
	 * <p>Description:专项名称 没有编码的处理 </p>
	 * @param key
	 * @param tableName
	 * @param testMap
	 */
	private void setSpecialInfo(String key, String tableName,
			Map<String, Object> testMap) {
		Map<String,String> numMap =new HashMap();
		Map<String,String> nameMap =new HashMap();
		try {
			String sql ="select fnumber as num,fname_l2 as name from "+tableName+"";
			System.out.println("打印setVoteModeInfo方法查询sql:"+sql);
			IRowSet rs = FMIsqlFacadeFactory.getRemoteInstance().executeQuery(sql.toString(), null);
			//查询
			while(rs.next()){
				String num =rs.getString("num");
				String name=rs.getString("name");
				numMap.put(name, num);
			}
			SpecialInfo specialInfo=new SpecialInfo();
			SpecialList specialList = (SpecialList) testMap.get(key);
			String[] special = specialList.getSpecial();
			for(int j =0;j<special.length;j++){
				String name = special[j];
				if(numMap==null ||(numMap!=null && !numMap.containsKey(name))){
					//取出key
					specialInfo.setName(name);
					SpecialFactory.getRemoteInstance().save(specialInfo);
					//if(j==special.length-1){
					//logMessage.setLength(0);
					logMessage="执行【专项名称】编码增量更新，新增名称："+name+" 操作成功\n";
					//logMessage.append("执行【专项名称】编码增量更新，新增名称：").append(name).append(" 操作成功\n");
					initLogArea(logMessage);
					//}
				} else{
					//logMessage.setLength(0);
					logMessage="执行【专项名称】编码增量更新，新增名称："+name+" 在系统中已经存在，新增失败\n";
					//logMessage.append("执行【专项名称】编码增量更新，新增名称：").append(name).append(" 在系统中已经存在，新增失败\n");
					initLogArea(logMessage);
				}

			}

		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("专项名称数据同步失败");
			logMessage="执行【专项名称】基础资料*编码增量更新*操作失败。"+e.getMessage().toString();
			//logMessage.append("执行【专项名称】基础资料*编码增量更新*操作失败。"+e.getMessage().toString());
			initLogArea(logMessage);
		} 
	}



	/**
	 * 
	 * <p>Title: setRegulationTypeInfo</p>
	 * <p>Description:制度类型 无编码 </p>
	 * @param key
	 * @param tableName
	 * @param testMap
	 */
	private void setRegulationTypeInfo(String key, String tableName,
			Map<String, Object> testMap) {
		Map<String,String> numMap =new HashMap();
		Map<String,String> nameMap =new HashMap();
		try {
			String sql ="select fnumber as num,fname_l2 as name from "+tableName+"";
			System.out.println("打印setVoteModeInfo方法查询sql:"+sql);
			IRowSet rs = FMIsqlFacadeFactory.getRemoteInstance().executeQuery(sql.toString(), null);
			//查询
			while(rs.next()){
				String num =rs.getString("num");
				String name=rs.getString("name");
				numMap.put(name, num);
			}
			RegulationTypeInfo regulationTypeInfo=new RegulationTypeInfo();
			RegulationTypeList regulationTypeList = (RegulationTypeList) testMap.get(key);
			List<RegulationTypeVO> regulation_type_list = regulationTypeList.getRegulation_type_list();
			for(int j =0;j<regulation_type_list.size();j++){
				RegulationTypeVO regulationTypeVO = regulation_type_list.get(j);

				String name = regulationTypeVO.getName();
				System.out.println("type_name:"+name);
				String flag = regulationTypeVO.getFlag();//是否报送
				if(numMap==null ||(numMap!=null && !numMap.containsKey(name))){
					regulationTypeInfo.setName(name);
					RegulationTypeFactory.getRemoteInstance().save(regulationTypeInfo);
					//if(j==regulation_type_list.size()-1){
					//logMessage.append("执行【制度类型】基础资料*编码增量更新*操作成功。").append("\n");
					//logMessage.setLength(0);
					logMessage="执行【制度类型】编码增量更新，新增名称："+name+" 操作成功\n";
					//logMessage.append("执行【制度类型】编码增量更新，新增名称：").append(name).append(" 操作成功\n");
					initLogArea(logMessage);
					//}

				} else{
					//logMessage.setLength(0);
					logMessage="执行【制度类型】编码增量更新，新增名称："+name+" 在系统中已经存在，新增失败\n";
					//logMessage.append("执行【制度类型】编码增量更新，新增名称：").append(name).append(" 在系统中已经存在，新增失败\n");
					initLogArea(logMessage);
				}
			}

		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("制度类型数据同步失败");
			logMessage="执行【制度类型】基础资料*编码增量更新*操作失败。"+e.getMessage().toString();
			//logMessage.append("执行【制度类型】基础资料*编码增量更新*操作失败。"+e.getMessage().toString());
			initLogArea(logMessage);
		} 
	}



	/**
	 * 
	 * <p>Title: setVoteModeInfo</p>
	 * <p>Description: 表决方式 下载文件无编码</p>
	 * @param key
	 * @param tableName
	 * @param testMap
	 */
	private void setVoteModeInfo(String key, String tableName,
			Map<String, Object> testMap) {
		Map<String,String> numMap =new HashMap();
		try {
			String sql ="select fnumber as num,fname_l2 as name from "+tableName+"";
			System.out.println("打印setVoteModeInfo方法查询sql:"+sql);
			IRowSet rs = FMIsqlFacadeFactory.getRemoteInstance().executeQuery(sql.toString(), null);
			//查询
			while(rs.next()){
				String num =rs.getString("num");
				String name=rs.getString("name");
				numMap.put(name, num);
			}
			VoteModeInfo voteModeInfo=new VoteModeInfo();
			VoteModeList voteModeList = (VoteModeList) testMap.get(key);
			List<VoteModeVO> meeting_type_list = voteModeList.getVote_mode_list();
			for(int j =0;j<meeting_type_list.size();j++){
				VoteModeVO voteModeVO = meeting_type_list.get(j);

				String mode_name = voteModeVO.getMode_name();//表决方式名称
				System.out.println("type_name:"+mode_name);
				if(numMap==null || (numMap!=null && !numMap.containsKey(mode_name))){
					voteModeInfo.setName(mode_name);
					VoteModeFactory.getRemoteInstance().save(voteModeInfo);

					//logMessage.append("执行【表决方式】基础资料*编码增量更新*操作成功。").append("\n");
					//logMessage.setLength(0);
					logMessage="执行【表决方式】编码增量更新，新增名称："+mode_name+" 操作成功\n";
					//logMessage.append("执行【表决方式】编码增量更新，新增名称：").append(mode_name).append(" 操作成功\n");
					initLogArea(logMessage);
				}else{
					//logMessage.setLength(0);
					logMessage="执行【表决方式】编码增量更新，新增名称："+mode_name+" 在系统中已经存在，新增失败\n";
					//logMessage.append("执行【表决方式】编码增量更新，新增名称：").append(mode_name).append(" 在系统中已经存在，新增失败\n");
					initLogArea(logMessage);
				}

			} 
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("表决方式数据同步失败");
			logMessage="执行【表决方式】基础资料*编码增量更新*操作失败。"+e.getMessage().toString();
			//logMessage.append("执行【表决方式】基础资料*编码增量更新*操作失败。"+e.getMessage().toString());
			initLogArea(logMessage);
		} 
	}


	/**
	 * 
	 * <p>Title: setCatalogInfo</p>
	 * <p>Description: 事项目录 取消</p>
	 * @param key
	 * @param tableName
	 * @param testMap
	 */
	private void setCatalogInfo(String key, String tableName,
			Map<String, Object> testMap) {
		Map<String,String> numMap =new HashMap();
		Map<String,CatalogList> nameMap =new HashMap();
		try {
			String sql ="select fnumber as num,fname_l2 as name from "+tableName+"";
			System.out.println("打印setMeetingFormInfo方法查询sql:"+sql);
			IRowSet rs = FMIsqlFacadeFactory.getRemoteInstance().executeQuery(sql.toString(), null);
			//查询
			while(rs.next()){
				String num =rs.getString("num");
				String name=rs.getString("name");
				numMap.put(num, name);
			}
			CatalogMainTreeInfo catalogMainTreeInfo=new CatalogMainTreeInfo();
			CatalogList catalogList = (CatalogList) testMap.get(key);
			List<CatalogList> catalog_list = catalogList.getCatalog_list();
			for(int j =0;j<catalog_list.size();j++){
				CatalogList catalogList2 = catalog_list.get(j);
				String catalog_code = catalogList2.getCatalog_code();//编码
				nameMap.clear();
				nameMap.put(catalog_code, catalogList2);

				if(numMap==null ||numMap!=null && !numMap.containsKey(catalog_code)){
					Iterator numIt=nameMap.keySet().iterator();
					while(numIt.hasNext()){
						//取出key 设置基础资料编码
						String numKey=numIt.next().toString();
						catalogMainTreeInfo.setNumber(numKey);
						//取出v
						CatalogList catalogList3 = nameMap.get(numKey);
						//设置基础资料名称
						String catalog_name = catalogList3.getCatalog_name();//名称
						catalogMainTreeInfo.setName(catalog_name);
						//设置基础资料treeId
						/*String catalog_level = catalogList3.getCatalog_level(); //级别
						int level = Integer.parseInt(catalog_level);
						CatalogMainTreeInfo treeInfo =new CatalogMainTreeInfo();
						if(level==1){
							treeInfo.setLevel(1);
							treeInfo.setIsLeaf(false);
							treeInfo.setName(catalog_name);
							treeInfo.setNumber(numKey);
							CatalogMainTreeFactory.getRemoteInstance().save(treeInfo);
						}else{

						}*/

						/*treeInfo =CatalogMainTreeFactory.getRemoteInstance().getCatalogMainTreeInfo(" where level="+level);
						String name = treeInfo.getName();
						System.out.println("上级目录名称为："+name);
						//CatalogMainTreeFactory.getRemoteInstance().save(treeInfo);
						catalogMainInfo.setTreeid(treeInfo);*/

						//String order_number = catalogList3.getOrder_number();//排序号
						CatalogMainTreeFactory.getRemoteInstance().save(catalogMainTreeInfo);
						logMessage="执行【事项目录】编码增量更新，新增名称："+catalog_name+" 操作成功\n";
						//logMessage.setLength(0);
						//logMessage.append("执行【事项目录】编码增量更新，新增名称：").append(catalog_name).append(" 操作成功\n");
						initLogArea(logMessage);
					} 
				}else{
					//logMessage.setLength(0);
					logMessage="执行【事项目录】编码增量更新，新增编码："+catalog_code+" 在系统中已经存在，新增失败\n";
					//logMessage.append("执行【事项目录】编码增量更新，新增编码：").append(catalog_code).append(" 在系统中已经存在，新增失败\n");
					initLogArea(logMessage);
				}

			} 
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("事项目录数据同步失败");
			logMessage="执行【事项目录】基础资料*编码增量更新*操作失败。"+e.getMessage().toString();
			//logMessage.append("执行【事项目录】基础资料*编码增量更新*操作失败。"+e.getMessage().toString());
			initLogArea(logMessage);
		} 
	}


	/**
	 * 
	 * <p>Title: setMeetingFormInfo</p>
	 * <p>Description: 会议形式</p>
	 * @param key
	 * @param tableName
	 * @param testMap
	 */
	private void setMeetingFormInfo(String key, String tableName,
			Map<String, Object> testMap) {
		Map<String,String> numMap =new HashMap();
		Map<String,String> nameMap =new HashMap();
		try {
			String sql ="select fnumber as num,fname_l2 as name from "+tableName+"";
			System.out.println("打印setMeetingFormInfo方法查询sql:"+sql);
			IRowSet rs = FMIsqlFacadeFactory.getRemoteInstance().executeQuery(sql.toString(), null);
			//查询
			while(rs.next()){
				String num =rs.getString("num");
				String name=rs.getString("name");
				numMap.put(num, name);
			}
			MeetingFormInfo meetingFormInfo=new MeetingFormInfo();
			MeetingFormList meetingTypeList = (MeetingFormList) testMap.get(key);
			List<MeetingFormVO> meeting_type_list = meetingTypeList.getMeeting_form_list();
			for(int j =0;j<meeting_type_list.size();j++){
				MeetingFormVO meetingFormVO = meeting_type_list.get(j);
				String form_name = meetingFormVO.getForm_name();
				System.out.println("type_name:"+form_name);
				String form_code = meetingFormVO.getForm_code();
				nameMap.clear();
				nameMap.put(form_code, form_name);
				Iterator numIt=nameMap.keySet().iterator();
				if(numMap==null ||numMap!=null && !numMap.containsKey(form_code)){
					while(numIt.hasNext()){
						//取出key
						String numKey=numIt.next().toString();
						meetingFormInfo.setNumber(numKey);
						System.out.println(key);
						//通过key拿到value
						String name=(String) nameMap.get(numKey);
						System.out.println(name);
						meetingFormInfo.setName(name);

						MeetingFormFactory.getRemoteInstance().save(meetingFormInfo);
						logMessage ="执行【会议形式】编码增量更新，新增名称："+name+" 操作成功\n";
						//logMessage.append("执行【会议形式】编码增量更新，新增名称：").append(name).append(" 操作成功\n");
						initLogArea(logMessage);
					}
				}else{
					//logMessage.setLength(0);
					
					logMessage="执行【会议形式】编码增量更新，新增名称："+form_name+" 在系统中已经存在，新增失败\n";
					//logMessage.append("执行【会议形式】编码增量更新，新增名称：").append(form_name).append(" 在系统中已经存在，新增失败\n");
					initLogArea(logMessage);
				}

			} 
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("会议形式数据同步失败");
			logMessage="执行【会议形式】基础资料*编码增量更新*操作失败。"+e.getMessage().toString();
			//logMessage.append("执行【会议形式】基础资料*编码增量更新*操作失败。"+e.getMessage().toString());
			initLogArea(logMessage);
		} 

	}


	/**
	 * 
	 * <p>Title: setMeetingTypeInfo</p>
	 * <p>Description:会议类型</p>
	 * @param key
	 * @param tableName
	 * @param testMap
	 */
	private void setMeetingTypeInfo(String key,String tableName,Map<String, Object> testMap) {
		Map<String,String> numMap =new HashMap();
		Map<String,String> nameMap =new HashMap();
		String msg ="";
		try {
			String sql ="select fnumber as num,fname_l2 as name from "+tableName+"";
			System.out.println("打印setMeetingTypeInfo方法查询sql:"+sql);
			IRowSet rs = FMIsqlFacadeFactory.getRemoteInstance().executeQuery(sql.toString(), null);
			//查询
			while(rs.next()){
				String num =rs.getString("num");
				String name=rs.getString("name");
				numMap.put(num, name);
			}
			MeetingTypeInfo meetingTypeInfo=new MeetingTypeInfo();
			MeetingTypeList meetingTypeList = (MeetingTypeList) testMap.get(key);
			List<MeetingTypeVO> meeting_type_list = meetingTypeList.getMeeting_type_list();
			for(int j =0;j<meeting_type_list.size();j++){
				MeetingTypeVO meetingTypeVO = meeting_type_list.get(j);
				String type_name = meetingTypeVO.getType_name();
				System.out.println("type_name:"+type_name);
				String type_code = meetingTypeVO.getType_code();
				//String group_type = meetingTypeVO.getGroup_type();
				nameMap.clear();
				nameMap.put(type_code, type_name);

				if(numMap==null || numMap!=null && !numMap.containsKey(type_code)){
					Iterator numIt=nameMap.keySet().iterator();
					while(numIt.hasNext()){
						//取出key
						String numKey=numIt.next().toString();
						meetingTypeInfo.setNumber(numKey);
						System.out.println(key);
						//通过key拿到value
						String name=(String) nameMap.get(numKey);
						System.out.println(name);
						meetingTypeInfo.setName(name);

						MeetingTypeFactory.getRemoteInstance().save(meetingTypeInfo);
						//if(j==meeting_type_list.size()-1){
						//logMessage.setLength(0);
						//logMessage.append("执行【会议类型】编码增量更新，新增名称：").append(name).append(" 操作成功\n");
						msg="执行【会议类型】编码增量更新，新增名称："+name+" 操作成功\n";
						initLogArea(msg);
						//}

					}
				}else{
					//logMessage.append("执行【会议类型】编码增量更新，新增名称：").append(type_name).append(" 在系统中已经存在，新增失败\n");
					msg="执行【会议类型】编码增量更新，新增名称："+type_name+" 在系统中已经存在，新增失败\n";
					initLogArea(msg);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("会议类型数据同步失败");
			//logMessage.append("执行会议类型基础资料*编码增量更新*操作失败。"+e.getMessage().toString());
			msg="执行会议类型基础资料*编码增量更新*操作失败。"+e.getMessage().toString();
			initLogArea(msg);
		} 
	}




	/**
	 * 
	 * <p>Title: getSelectedLocatMap</p>
	 * <p>Description:获取选中行的基础资料数据 </p>
	 * @return
	 */
	private Map<String,String> getSelectedLocatMap() {
		KDTable tableCom = this.kDTableCom;
		for (int i = 0; i < tableCom.getRowCount(); i++) {
			IRow row = tableCom.getRow(i);
			String companyNum = row.getCell("companyName").getValue().toString();
			Object cell = row.getCell("isSelected").getValue();
			//获取对应的基础资料
			if (cell.equals(Boolean.TRUE)) {
				DownLoadConfInfo downLoadInfo;
				try {
					downLoadInfo = DownLoadConfFactory.getRemoteInstance().getDownLoadConfInfo("where name='"+companyNum+"'");
					if(downLoadInfo==null){
						MsgBox.showWarning("未找到对应基础资料，请先同步。");
					}else{
						//将符合条件的基础资料数据存储到Map中
						localDataMap.put(downLoadInfo.getXml().toString(), downLoadInfo.getBOSTYPE());

					}
				} catch (EASBizException e) {
					e.printStackTrace();
				} catch (BOSException e) {
					e.printStackTrace();
				}

			}
		}
		return localDataMap;
	}


}
