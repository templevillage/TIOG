/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.service.sasac.app;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.eas.framework.batchHandler.ResponseContext;


/**
 * output class name
 */
public class DownLoadPlatformUIHandler extends AbstractDownLoadPlatformUIHandler
{
	protected void _handleInit(RequestContext request,ResponseContext response, Context context) throws Exception {
		super._handleInit(request,response,context);
	}
	protected void _handleActionComSelected(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionTypeSelected(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionComClear(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionTypeClear(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionSync(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionAntiSync(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionCancel(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionExecute(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionQuery(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionAwaitSelected(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionAwaitClear(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionCancel2(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionH01(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionH02(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	protected void _handleActionH03(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
}