package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractCategoryListInfo extends com.kingdee.eas.framework.CoreBillBaseInfo implements Serializable 
{
    public AbstractCategoryListInfo()
    {
        this("id");
    }
    protected AbstractCategoryListInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryCollection());
    }
    /**
     * Object: 事项清单 's 分录 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryCollection)get("entrys");
    }
    /**
     * Object:事项清单's 是否生成凭证property 
     */
    public boolean isFivouchered()
    {
        return getBoolean("Fivouchered");
    }
    public void setFivouchered(boolean item)
    {
        setBoolean("Fivouchered", item);
    }
    /**
     * Object: 事项清单 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    /**
     * Object: 事项清单 's 公司 property 
     */
    public com.kingdee.eas.basedata.org.CompanyOrgUnitInfo getCompany()
    {
        return (com.kingdee.eas.basedata.org.CompanyOrgUnitInfo)get("company");
    }
    public void setCompany(com.kingdee.eas.basedata.org.CompanyOrgUnitInfo item)
    {
        put("company", item);
    }
    /**
     * Object:事项清单's 企业编码property 
     */
    public String getCompanyNumber()
    {
        return getString("companyNumber");
    }
    public void setCompanyNumber(String item)
    {
        setString("companyNumber", item);
    }
    /**
     * Object:事项清单's 事项清单名称property 
     */
    public String getCategoryListName()
    {
        return getString("categoryListName");
    }
    public void setCategoryListName(String item)
    {
        setString("categoryListName", item);
    }
    /**
     * Object:事项清单's 版本号property 
     */
    public String getListVersion()
    {
        return getString("listVersion");
    }
    public void setListVersion(String item)
    {
        setString("listVersion", item);
    }
    /**
     * Object:事项清单's 生效日期property 
     */
    public java.util.Date getEffectiveTS()
    {
        return getDate("effectiveTS");
    }
    public void setEffectiveTS(java.util.Date item)
    {
        setDate("effectiveTS", item);
    }
    /**
     * Object:事项清单's 失效日期property 
     */
    public java.util.Date getInvalidTS()
    {
        return getDate("invalidTS");
    }
    public void setInvalidTS(java.util.Date item)
    {
        setDate("invalidTS", item);
    }
    /**
     * Object:事项清单's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object:事项清单's 报送标识property 
     */
    public boolean isIsUpload()
    {
        return getBoolean("isUpload");
    }
    public void setIsUpload(boolean item)
    {
        setBoolean("isUpload", item);
    }
    /**
     * Object:事项清单's 单据状态property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.BillState getState()
    {
        return com.kingdee.eas.custom.tiog.bizbill.BillState.getEnum(getString("state"));
    }
    public void setState(com.kingdee.eas.custom.tiog.bizbill.BillState item)
    {
		if (item != null) {
        setString("state", item.getValue());
		}
    }
    /**
     * Object: 事项清单 's 数据来源 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.DataSourceInfo getSource()
    {
        return (com.kingdee.eas.custom.tiog.basedata.DataSourceInfo)get("source");
    }
    public void setSource(com.kingdee.eas.custom.tiog.basedata.DataSourceInfo item)
    {
        put("source", item);
    }
    /**
     * Object: 事项清单 's 操作标识 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo getOperType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo)get("operType");
    }
    public void setOperType(com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo item)
    {
        put("operType", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("9B951826");
    }
}