package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingManEntryCollection extends AbstractObjectCollection 
{
    public MeetingManEntryCollection()
    {
        super(MeetingManEntryInfo.class);
    }
    public boolean add(MeetingManEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingManEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingManEntryInfo item)
    {
        return removeObject(item);
    }
    public MeetingManEntryInfo get(int index)
    {
        return(MeetingManEntryInfo)getObject(index);
    }
    public MeetingManEntryInfo get(Object key)
    {
        return(MeetingManEntryInfo)getObject(key);
    }
    public void set(int index, MeetingManEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingManEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingManEntryInfo item)
    {
        return super.indexOf(item);
    }
}