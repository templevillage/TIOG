package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractPoliticalSystemInfo extends com.kingdee.eas.framework.CoreBillBaseInfo implements Serializable 
{
    public AbstractPoliticalSystemInfo()
    {
        this("id");
    }
    protected AbstractPoliticalSystemInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryCollection());
    }
    /**
     * Object: 决策制度 's 分录 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryCollection)get("entrys");
    }
    /**
     * Object:决策制度's 是否生成凭证property 
     */
    public boolean isFivouchered()
    {
        return getBoolean("Fivouchered");
    }
    public void setFivouchered(boolean item)
    {
        setBoolean("Fivouchered", item);
    }
    /**
     * Object: 决策制度 's 企业 property 
     */
    public com.kingdee.eas.basedata.org.CompanyOrgUnitInfo getCompany()
    {
        return (com.kingdee.eas.basedata.org.CompanyOrgUnitInfo)get("company");
    }
    public void setCompany(com.kingdee.eas.basedata.org.CompanyOrgUnitInfo item)
    {
        put("company", item);
    }
    /**
     * Object:决策制度's 企业编码property 
     */
    public String getCompanyNumber()
    {
        return getString("companyNumber");
    }
    public void setCompanyNumber(String item)
    {
        setString("companyNumber", item);
    }
    /**
     * Object:决策制度's 制度名称property 
     */
    public String getPoliticalSystemName()
    {
        return getString("politicalSystemName");
    }
    public void setPoliticalSystemName(String item)
    {
        setString("politicalSystemName", item);
    }
    /**
     * Object: 决策制度 's 制度类型 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.RegulationTypeInfo getRegulationType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.RegulationTypeInfo)get("regulationType");
    }
    public void setRegulationType(com.kingdee.eas.custom.tiog.basedata.RegulationTypeInfo item)
    {
        put("regulationType", item);
    }
    /**
     * Object:决策制度's 审批日期property 
     */
    public java.util.Date getAuditTS()
    {
        return getDate("auditTS");
    }
    public void setAuditTS(java.util.Date item)
    {
        setDate("auditTS", item);
    }
    /**
     * Object:决策制度's 生效日期property 
     */
    public java.util.Date getEffectDate()
    {
        return getDate("effectDate");
    }
    public void setEffectDate(java.util.Date item)
    {
        setDate("effectDate", item);
    }
    /**
     * Object:决策制度's 失效日期property 
     */
    public java.util.Date getExpireDate()
    {
        return getDate("expireDate");
    }
    public void setExpireDate(java.util.Date item)
    {
        setDate("expireDate", item);
    }
    /**
     * Object:决策制度's 是否经过合法合规性审查property 
     */
    public boolean isIsReview()
    {
        return getBoolean("isReview");
    }
    public void setIsReview(boolean item)
    {
        setBoolean("isReview", item);
    }
    /**
     * Object: 决策制度 's 会议类型 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo getMeetNum()
    {
        return (com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo)get("meetNum");
    }
    public void setMeetNum(com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo item)
    {
        put("meetNum", item);
    }
    /**
     * Object: 决策制度 's 数据来源 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.DataSourceInfo getDataSource()
    {
        return (com.kingdee.eas.custom.tiog.basedata.DataSourceInfo)get("dataSource");
    }
    public void setDataSource(com.kingdee.eas.custom.tiog.basedata.DataSourceInfo item)
    {
        put("dataSource", item);
    }
    /**
     * Object: 决策制度 's 操作标识 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo getOperType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo)get("operType");
    }
    public void setOperType(com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo item)
    {
        put("operType", item);
    }
    /**
     * Object: 决策制度 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    /**
     * Object:决策制度's 报送标识property 
     */
    public boolean isIsUpload()
    {
        return getBoolean("isUpload");
    }
    public void setIsUpload(boolean item)
    {
        setBoolean("isUpload", item);
    }
    /**
     * Object:决策制度's 单据状态property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.BillState getState()
    {
        return com.kingdee.eas.custom.tiog.bizbill.BillState.getEnum(getString("state"));
    }
    public void setState(com.kingdee.eas.custom.tiog.bizbill.BillState item)
    {
		if (item != null) {
        setString("state", item.getValue());
		}
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("C09155E2");
    }
}