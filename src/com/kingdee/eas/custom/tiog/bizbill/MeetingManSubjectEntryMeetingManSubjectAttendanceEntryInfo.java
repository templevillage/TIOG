package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo extends AbstractMeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo implements Serializable 
{
    public MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo()
    {
        super();
    }
    protected MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo(String pkField)
    {
        super(pkField);
    }
}