package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class SubjectCatalogEntryInfo extends AbstractSubjectCatalogEntryInfo implements Serializable 
{
    public SubjectCatalogEntryInfo()
    {
        super();
    }
    protected SubjectCatalogEntryInfo(String pkField)
    {
        super(pkField);
    }
}