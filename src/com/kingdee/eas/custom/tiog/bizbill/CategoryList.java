package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.framework.CoreBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.tiog.bizbill.app.*;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class CategoryList extends CoreBillBase implements ICategoryList
{
    public CategoryList()
    {
        super();
        registerInterface(ICategoryList.class, this);
    }
    public CategoryList(Context ctx)
    {
        super(ctx);
        registerInterface(ICategoryList.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("9B951826");
    }
    private CategoryListController getController() throws BOSException
    {
        return (CategoryListController)getBizController();
    }
    /**
     *取集合-System defined method
     *@return
     */
    public CategoryListCollection getCategoryListCollection() throws BOSException
    {
        try {
            return getController().getCategoryListCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param view 取集合
     *@return
     */
    public CategoryListCollection getCategoryListCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getCategoryListCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param oql 取集合
     *@return
     */
    public CategoryListCollection getCategoryListCollection(String oql) throws BOSException
    {
        try {
            return getController().getCategoryListCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@return
     */
    public CategoryListInfo getCategoryListInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getCategoryListInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@param selector 取值
     *@return
     */
    public CategoryListInfo getCategoryListInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getCategoryListInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param oql 取值
     *@return
     */
    public CategoryListInfo getCategoryListInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getCategoryListInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *审核-User defined method
     *@param model model
     */
    public void audit(CategoryListInfo model) throws BOSException
    {
        try {
            getController().audit(getContext(), model);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *反审核-User defined method
     *@param model model
     */
    public void unAudit(CategoryListInfo model) throws BOSException
    {
        try {
            getController().unAudit(getContext(), model);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}