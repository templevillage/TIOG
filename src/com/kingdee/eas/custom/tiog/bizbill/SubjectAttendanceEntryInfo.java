package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class SubjectAttendanceEntryInfo extends AbstractSubjectAttendanceEntryInfo implements Serializable 
{
    public SubjectAttendanceEntryInfo()
    {
        super();
    }
    protected SubjectAttendanceEntryInfo(String pkField)
    {
        super(pkField);
    }
}