package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingManSubjectEntryFactory
{
    private MeetingManSubjectEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("655BDA4F") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("655BDA4F") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("655BDA4F"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("655BDA4F"));
    }
}