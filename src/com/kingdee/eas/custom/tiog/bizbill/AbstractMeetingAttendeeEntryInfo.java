package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractMeetingAttendeeEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractMeetingAttendeeEntryInfo()
    {
        this("id");
    }
    protected AbstractMeetingAttendeeEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 参会人集合 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.MeetingInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 参会人集合 's 参会人 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getAttendee()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("attendee");
    }
    public void setAttendee(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("attendee", item);
    }
    /**
     * Object:参会人集合's 缺席原因property 
     */
    public String getReason()
    {
        return getString("reason");
    }
    public void setReason(String item)
    {
        setString("reason", item);
    }
    /**
     * Object:参会人集合's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("8ABBA8C7");
    }
}