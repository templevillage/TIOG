package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class SubjectRefSubjectEntryFactory
{
    private SubjectRefSubjectEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectRefSubjectEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectRefSubjectEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("55FC74F7") ,com.kingdee.eas.custom.tiog.bizbill.ISubjectRefSubjectEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectRefSubjectEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectRefSubjectEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("55FC74F7") ,com.kingdee.eas.custom.tiog.bizbill.ISubjectRefSubjectEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectRefSubjectEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectRefSubjectEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("55FC74F7"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectRefSubjectEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectRefSubjectEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("55FC74F7"));
    }
}