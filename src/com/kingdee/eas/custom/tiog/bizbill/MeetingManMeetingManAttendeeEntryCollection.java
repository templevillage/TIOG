package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingManMeetingManAttendeeEntryCollection extends AbstractObjectCollection 
{
    public MeetingManMeetingManAttendeeEntryCollection()
    {
        super(MeetingManMeetingManAttendeeEntryInfo.class);
    }
    public boolean add(MeetingManMeetingManAttendeeEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingManMeetingManAttendeeEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingManMeetingManAttendeeEntryInfo item)
    {
        return removeObject(item);
    }
    public MeetingManMeetingManAttendeeEntryInfo get(int index)
    {
        return(MeetingManMeetingManAttendeeEntryInfo)getObject(index);
    }
    public MeetingManMeetingManAttendeeEntryInfo get(Object key)
    {
        return(MeetingManMeetingManAttendeeEntryInfo)getObject(key);
    }
    public void set(int index, MeetingManMeetingManAttendeeEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingManMeetingManAttendeeEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingManMeetingManAttendeeEntryInfo item)
    {
        return super.indexOf(item);
    }
}