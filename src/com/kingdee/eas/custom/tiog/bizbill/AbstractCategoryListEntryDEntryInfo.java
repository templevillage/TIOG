package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractCategoryListEntryDEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractCategoryListEntryDEntryInfo()
    {
        this("id");
    }
    protected AbstractCategoryListEntryDEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 明细分录 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryInfo getParent1()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryInfo)get("parent1");
    }
    public void setParent1(com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryInfo item)
    {
        put("parent1", item);
    }
    /**
     * Object: 明细分录 's 会议类型 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo getMeetingType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo)get("meetingType");
    }
    public void setMeetingType(com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo item)
    {
        put("meetingType", item);
    }
    /**
     * Object:明细分录's 会议类型编码property 
     */
    public String getMeetingTypeNumber()
    {
        return getString("meetingTypeNumber");
    }
    public void setMeetingTypeNumber(String item)
    {
        setString("meetingTypeNumber", item);
    }
    /**
     * Object:明细分录's 会议说明property 
     */
    public String getMeetingDes()
    {
        return getString("meetingDes");
    }
    public void setMeetingDes(String item)
    {
        setString("meetingDes", item);
    }
    /**
     * Object:明细分录's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("9481297A");
    }
}