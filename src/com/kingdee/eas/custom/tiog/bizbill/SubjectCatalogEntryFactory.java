package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class SubjectCatalogEntryFactory
{
    private SubjectCatalogEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectCatalogEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectCatalogEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("3D79367B") ,com.kingdee.eas.custom.tiog.bizbill.ISubjectCatalogEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectCatalogEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectCatalogEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("3D79367B") ,com.kingdee.eas.custom.tiog.bizbill.ISubjectCatalogEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectCatalogEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectCatalogEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("3D79367B"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectCatalogEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectCatalogEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("3D79367B"));
    }
}