package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IMeetingMan extends ICoreBillBase
{
    public MeetingManCollection getMeetingManCollection() throws BOSException;
    public MeetingManCollection getMeetingManCollection(EntityViewInfo view) throws BOSException;
    public MeetingManCollection getMeetingManCollection(String oql) throws BOSException;
    public MeetingManInfo getMeetingManInfo(IObjectPK pk) throws BOSException, EASBizException;
    public MeetingManInfo getMeetingManInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public MeetingManInfo getMeetingManInfo(String oql) throws BOSException, EASBizException;
    public void audit(MeetingManInfo model) throws BOSException;
    public void unAudit(MeetingManInfo model) throws BOSException;
}