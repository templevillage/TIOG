package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class SubjectOtherEntryFactory
{
    private SubjectOtherEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectOtherEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectOtherEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("0BCF1A64") ,com.kingdee.eas.custom.tiog.bizbill.ISubjectOtherEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectOtherEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectOtherEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("0BCF1A64") ,com.kingdee.eas.custom.tiog.bizbill.ISubjectOtherEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectOtherEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectOtherEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("0BCF1A64"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectOtherEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectOtherEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("0BCF1A64"));
    }
}