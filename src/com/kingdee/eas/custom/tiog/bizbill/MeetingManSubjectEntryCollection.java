package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingManSubjectEntryCollection extends AbstractObjectCollection 
{
    public MeetingManSubjectEntryCollection()
    {
        super(MeetingManSubjectEntryInfo.class);
    }
    public boolean add(MeetingManSubjectEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingManSubjectEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingManSubjectEntryInfo item)
    {
        return removeObject(item);
    }
    public MeetingManSubjectEntryInfo get(int index)
    {
        return(MeetingManSubjectEntryInfo)getObject(index);
    }
    public MeetingManSubjectEntryInfo get(Object key)
    {
        return(MeetingManSubjectEntryInfo)getObject(key);
    }
    public void set(int index, MeetingManSubjectEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingManSubjectEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingManSubjectEntryInfo item)
    {
        return super.indexOf(item);
    }
}