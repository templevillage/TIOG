package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingAttendanceEntryCollection extends AbstractObjectCollection 
{
    public MeetingAttendanceEntryCollection()
    {
        super(MeetingAttendanceEntryInfo.class);
    }
    public boolean add(MeetingAttendanceEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingAttendanceEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingAttendanceEntryInfo item)
    {
        return removeObject(item);
    }
    public MeetingAttendanceEntryInfo get(int index)
    {
        return(MeetingAttendanceEntryInfo)getObject(index);
    }
    public MeetingAttendanceEntryInfo get(Object key)
    {
        return(MeetingAttendanceEntryInfo)getObject(key);
    }
    public void set(int index, MeetingAttendanceEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingAttendanceEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingAttendanceEntryInfo item)
    {
        return super.indexOf(item);
    }
}