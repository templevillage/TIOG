package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IPoliticalSystem extends ICoreBillBase
{
    public PoliticalSystemCollection getPoliticalSystemCollection() throws BOSException;
    public PoliticalSystemCollection getPoliticalSystemCollection(EntityViewInfo view) throws BOSException;
    public PoliticalSystemCollection getPoliticalSystemCollection(String oql) throws BOSException;
    public PoliticalSystemInfo getPoliticalSystemInfo(IObjectPK pk) throws BOSException, EASBizException;
    public PoliticalSystemInfo getPoliticalSystemInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public PoliticalSystemInfo getPoliticalSystemInfo(String oql) throws BOSException, EASBizException;
    public void audit(PoliticalSystemInfo model) throws BOSException;
    public void unAudit(PoliticalSystemInfo model) throws BOSException;
}