package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class ExecutionEntryCollection extends AbstractObjectCollection 
{
    public ExecutionEntryCollection()
    {
        super(ExecutionEntryInfo.class);
    }
    public boolean add(ExecutionEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(ExecutionEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(ExecutionEntryInfo item)
    {
        return removeObject(item);
    }
    public ExecutionEntryInfo get(int index)
    {
        return(ExecutionEntryInfo)getObject(index);
    }
    public ExecutionEntryInfo get(Object key)
    {
        return(ExecutionEntryInfo)getObject(key);
    }
    public void set(int index, ExecutionEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(ExecutionEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(ExecutionEntryInfo item)
    {
        return super.indexOf(item);
    }
}