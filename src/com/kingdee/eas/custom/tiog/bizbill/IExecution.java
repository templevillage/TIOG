package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IExecution extends ICoreBillBase
{
    public ExecutionCollection getExecutionCollection() throws BOSException;
    public ExecutionCollection getExecutionCollection(EntityViewInfo view) throws BOSException;
    public ExecutionCollection getExecutionCollection(String oql) throws BOSException;
    public ExecutionInfo getExecutionInfo(IObjectPK pk) throws BOSException, EASBizException;
    public ExecutionInfo getExecutionInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public ExecutionInfo getExecutionInfo(String oql) throws BOSException, EASBizException;
    public void audit(ExecutionInfo model) throws BOSException;
    public void unAudit(ExecutionInfo model) throws BOSException;
}