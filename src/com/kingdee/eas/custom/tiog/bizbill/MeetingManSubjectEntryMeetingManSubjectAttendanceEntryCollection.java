package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingManSubjectEntryMeetingManSubjectAttendanceEntryCollection extends AbstractObjectCollection 
{
    public MeetingManSubjectEntryMeetingManSubjectAttendanceEntryCollection()
    {
        super(MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo.class);
    }
    public boolean add(MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingManSubjectEntryMeetingManSubjectAttendanceEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo item)
    {
        return removeObject(item);
    }
    public MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo get(int index)
    {
        return(MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo)getObject(index);
    }
    public MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo get(Object key)
    {
        return(MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo)getObject(key);
    }
    public void set(int index, MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo item)
    {
        return super.indexOf(item);
    }
}