package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractCategoryListEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractCategoryListEntryInfo()
    {
        this("id");
    }
    protected AbstractCategoryListEntryInfo(String pkField)
    {
        super(pkField);
        put("DEntrys", new com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryDEntryCollection());
    }
    /**
     * Object: 分录 's 单据头 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 分录 's 明细分录 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryDEntryCollection getDEntrys()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryDEntryCollection)get("DEntrys");
    }
    /**
     * Object: 分录 's 事项 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogInfo getCatalog()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogInfo)get("catalog");
    }
    public void setCatalog(com.kingdee.eas.custom.tiog.basedata.CatalogInfo item)
    {
        put("catalog", item);
    }
    /**
     * Object:分录's 事项编码property 
     */
    public String getCatalogNumber()
    {
        return getString("catalogNumber");
    }
    public void setCatalogNumber(String item)
    {
        setString("catalogNumber", item);
    }
    /**
     * Object:分录's 是否需经法律审核property 
     */
    public boolean isLegalFlag()
    {
        return getBoolean("legalFlag");
    }
    public void setLegalFlag(boolean item)
    {
        setBoolean("legalFlag", item);
    }
    /**
     * Object: 分录 's 数据来源 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.DataSourceInfo getDataSource()
    {
        return (com.kingdee.eas.custom.tiog.basedata.DataSourceInfo)get("dataSource");
    }
    public void setDataSource(com.kingdee.eas.custom.tiog.basedata.DataSourceInfo item)
    {
        put("dataSource", item);
    }
    /**
     * Object: 分录 's 操作标识 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo getOperType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo)get("operType");
    }
    public void setOperType(com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo item)
    {
        put("operType", item);
    }
    /**
     * Object:分录's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: 分录 's 事项 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo getCatalogMain()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo)get("catalogMain");
    }
    public void setCatalogMain(com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo item)
    {
        put("catalogMain", item);
    }
    /**
     * Object:分录's 事项编码property 
     */
    public String getCatalogMainNumber()
    {
        return getString("catalogMainNumber");
    }
    public void setCatalogMainNumber(String item)
    {
        setString("catalogMainNumber", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("92B227EC");
    }
}