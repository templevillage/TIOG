package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingManSubjectEntryMeetingManSubjectDeliberationEntryFactory
{
    private MeetingManSubjectEntryMeetingManSubjectDeliberationEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectDeliberationEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectDeliberationEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("7D43E270") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectDeliberationEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectDeliberationEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectDeliberationEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("7D43E270") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectDeliberationEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectDeliberationEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectDeliberationEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("7D43E270"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectDeliberationEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectDeliberationEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("7D43E270"));
    }
}