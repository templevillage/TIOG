package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class SubjectOtherEntryInfo extends AbstractSubjectOtherEntryInfo implements Serializable 
{
    public SubjectOtherEntryInfo()
    {
        super();
    }
    protected SubjectOtherEntryInfo(String pkField)
    {
        super(pkField);
    }
}