package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class SubjectOtherEntryCollection extends AbstractObjectCollection 
{
    public SubjectOtherEntryCollection()
    {
        super(SubjectOtherEntryInfo.class);
    }
    public boolean add(SubjectOtherEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(SubjectOtherEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(SubjectOtherEntryInfo item)
    {
        return removeObject(item);
    }
    public SubjectOtherEntryInfo get(int index)
    {
        return(SubjectOtherEntryInfo)getObject(index);
    }
    public SubjectOtherEntryInfo get(Object key)
    {
        return(SubjectOtherEntryInfo)getObject(key);
    }
    public void set(int index, SubjectOtherEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(SubjectOtherEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(SubjectOtherEntryInfo item)
    {
        return super.indexOf(item);
    }
}