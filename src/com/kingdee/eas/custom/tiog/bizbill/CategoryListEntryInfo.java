package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class CategoryListEntryInfo extends AbstractCategoryListEntryInfo implements Serializable 
{
    public CategoryListEntryInfo()
    {
        super();
    }
    protected CategoryListEntryInfo(String pkField)
    {
        super(pkField);
    }
}