package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractMeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractMeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo()
    {
        this("id");
    }
    protected AbstractMeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 审议情况 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo getParent1()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo)get("parent1");
    }
    public void setParent1(com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo item)
    {
        put("parent1", item);
    }
    /**
     * Object:审议情况's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: 审议情况 's 审议人员 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getDeliberator()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("deliberator");
    }
    public void setDeliberator(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("deliberator", item);
    }
    /**
     * Object:审议情况's 审议结果property 
     */
    public String getDeliberationResult()
    {
        return getString("deliberationResult");
    }
    public void setDeliberationResult(String item)
    {
        setString("deliberationResult", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("7D43E270");
    }
}