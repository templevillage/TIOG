package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class ExecutionEntryInfo extends AbstractExecutionEntryInfo implements Serializable 
{
    public ExecutionEntryInfo()
    {
        super();
    }
    protected ExecutionEntryInfo(String pkField)
    {
        super(pkField);
    }
}