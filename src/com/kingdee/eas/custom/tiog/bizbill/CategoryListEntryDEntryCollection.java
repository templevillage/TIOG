package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class CategoryListEntryDEntryCollection extends AbstractObjectCollection 
{
    public CategoryListEntryDEntryCollection()
    {
        super(CategoryListEntryDEntryInfo.class);
    }
    public boolean add(CategoryListEntryDEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(CategoryListEntryDEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(CategoryListEntryDEntryInfo item)
    {
        return removeObject(item);
    }
    public CategoryListEntryDEntryInfo get(int index)
    {
        return(CategoryListEntryDEntryInfo)getObject(index);
    }
    public CategoryListEntryDEntryInfo get(Object key)
    {
        return(CategoryListEntryDEntryInfo)getObject(key);
    }
    public void set(int index, CategoryListEntryDEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(CategoryListEntryDEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(CategoryListEntryDEntryInfo item)
    {
        return super.indexOf(item);
    }
}