package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingManMeetingManAttendeeEntryFactory
{
    private MeetingManMeetingManAttendeeEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManMeetingManAttendeeEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManMeetingManAttendeeEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("9C2205B0") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManMeetingManAttendeeEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManMeetingManAttendeeEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManMeetingManAttendeeEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("9C2205B0") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManMeetingManAttendeeEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManMeetingManAttendeeEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManMeetingManAttendeeEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("9C2205B0"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManMeetingManAttendeeEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManMeetingManAttendeeEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("9C2205B0"));
    }
}