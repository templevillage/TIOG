package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class CategoryListCollection extends AbstractObjectCollection 
{
    public CategoryListCollection()
    {
        super(CategoryListInfo.class);
    }
    public boolean add(CategoryListInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(CategoryListCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(CategoryListInfo item)
    {
        return removeObject(item);
    }
    public CategoryListInfo get(int index)
    {
        return(CategoryListInfo)getObject(index);
    }
    public CategoryListInfo get(Object key)
    {
        return(CategoryListInfo)getObject(key);
    }
    public void set(int index, CategoryListInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(CategoryListInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(CategoryListInfo item)
    {
        return super.indexOf(item);
    }
}