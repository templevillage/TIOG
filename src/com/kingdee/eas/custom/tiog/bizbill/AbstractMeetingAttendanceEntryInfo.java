package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractMeetingAttendanceEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractMeetingAttendanceEntryInfo()
    {
        this("id");
    }
    protected AbstractMeetingAttendanceEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 列席情况 二级分录 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingSubjectEntryInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingSubjectEntryInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.MeetingSubjectEntryInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 列席情况 二级分录 's 列席人员 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getAttendancePerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("attendancePerson");
    }
    public void setAttendancePerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("attendancePerson", item);
    }
    /**
     * Object: 列席情况 二级分录 's 列席人职位 property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("41549838");
    }
}