package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class CategoryListEntryCollection extends AbstractObjectCollection 
{
    public CategoryListEntryCollection()
    {
        super(CategoryListEntryInfo.class);
    }
    public boolean add(CategoryListEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(CategoryListEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(CategoryListEntryInfo item)
    {
        return removeObject(item);
    }
    public CategoryListEntryInfo get(int index)
    {
        return(CategoryListEntryInfo)getObject(index);
    }
    public CategoryListEntryInfo get(Object key)
    {
        return(CategoryListEntryInfo)getObject(key);
    }
    public void set(int index, CategoryListEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(CategoryListEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(CategoryListEntryInfo item)
    {
        return super.indexOf(item);
    }
}