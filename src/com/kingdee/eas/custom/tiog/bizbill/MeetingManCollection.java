package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingManCollection extends AbstractObjectCollection 
{
    public MeetingManCollection()
    {
        super(MeetingManInfo.class);
    }
    public boolean add(MeetingManInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingManCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingManInfo item)
    {
        return removeObject(item);
    }
    public MeetingManInfo get(int index)
    {
        return(MeetingManInfo)getObject(index);
    }
    public MeetingManInfo get(Object key)
    {
        return(MeetingManInfo)getObject(key);
    }
    public void set(int index, MeetingManInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingManInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingManInfo item)
    {
        return super.indexOf(item);
    }
}