package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo extends AbstractMeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo implements Serializable 
{
    public MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo()
    {
        super();
    }
    protected MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo(String pkField)
    {
        super(pkField);
    }
}