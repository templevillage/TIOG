package com.kingdee.eas.custom.tiog.bizbill.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionEntryInfo;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.framework.app.CoreBillEntryBaseController;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.custom.tiog.bizbill.ExecutionEntryCollection;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface ExecutionEntryController extends CoreBillEntryBaseController
{
    public ExecutionEntryInfo getExecutionEntryInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public ExecutionEntryInfo getExecutionEntryInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public ExecutionEntryInfo getExecutionEntryInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public ExecutionEntryCollection getExecutionEntryCollection(Context ctx) throws BOSException, RemoteException;
    public ExecutionEntryCollection getExecutionEntryCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public ExecutionEntryCollection getExecutionEntryCollection(Context ctx, String oql) throws BOSException, RemoteException;
}