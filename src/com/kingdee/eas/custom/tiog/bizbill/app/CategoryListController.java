package com.kingdee.eas.custom.tiog.bizbill.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListCollection;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.app.CoreBillBaseController;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface CategoryListController extends CoreBillBaseController
{
    public CategoryListCollection getCategoryListCollection(Context ctx) throws BOSException, RemoteException;
    public CategoryListCollection getCategoryListCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public CategoryListCollection getCategoryListCollection(Context ctx, String oql) throws BOSException, RemoteException;
    public CategoryListInfo getCategoryListInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public CategoryListInfo getCategoryListInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public CategoryListInfo getCategoryListInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public void audit(Context ctx, CategoryListInfo model) throws BOSException, RemoteException;
    public void unAudit(Context ctx, CategoryListInfo model) throws BOSException, RemoteException;
}