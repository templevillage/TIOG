package com.kingdee.eas.custom.tiog.bizbill.app;

import org.apache.log4j.Logger;
import javax.ejb.*;
import java.rmi.RemoteException;
import com.kingdee.bos.*;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.metadata.IMetaDataPK;
import com.kingdee.bos.metadata.rule.RuleExecutor;
import com.kingdee.bos.metadata.MetaDataPK;
//import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.framework.ejb.AbstractEntityControllerBean;
import com.kingdee.bos.framework.ejb.AbstractBizControllerBean;
//import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.bos.dao.IObjectCollection;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.bos.service.IServiceContext;

import com.kingdee.eas.framework.app.CoreBillBaseControllerBean;
import com.kingdee.eas.framework.ObjectBaseCollection;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.custom.tiog.bizbill.BillState;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListFactory;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBillBaseCollection;
import com.kingdee.eas.custom.tiog.bizbill.CategoryListCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class CategoryListControllerBeanEx extends com.kingdee.eas.custom.tiog.bizbill.app.CategoryListControllerBean
{
    private static Logger logger =
        Logger.getLogger("com.kingdee.eas.custom.tiog.bizbill.app.CategoryListControllerBeanEx");
    protected void _audit(Context ctx, IObjectValue model)throws BOSException
    {
        super._audit(ctx, model);
        CategoryListInfo categoryListInfo = (CategoryListInfo) model;
        //model.get("id");
        categoryListInfo.setState(BillState.Audited);
        try {
            CategoryListFactory.getLocalInstance(ctx).update(new ObjectUuidPK(model.get("id").toString()), categoryListInfo);
        } catch (EASBizException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    protected void _unAudit(Context ctx, IObjectValue model)throws BOSException
    {
	super._unAudit(ctx, model);
	CategoryListInfo categoryListInfo = (CategoryListInfo) model;
	categoryListInfo.setState(BillState.Saved);
        try {
            CategoryListFactory.getLocalInstance(ctx).update(new ObjectUuidPK(model.getPKField()), categoryListInfo);
            } catch (EASBizException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }
}				
