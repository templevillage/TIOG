/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.bizbill.client;

import org.apache.log4j.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.BorderFactory;
import javax.swing.event.*;
import javax.swing.KeyStroke;

import com.kingdee.bos.ctrl.swing.*;
import com.kingdee.bos.ctrl.kdf.table.*;
import com.kingdee.bos.ctrl.kdf.data.event.*;
import com.kingdee.bos.dao.*;
import com.kingdee.bos.dao.query.*;
import com.kingdee.bos.metadata.*;
import com.kingdee.bos.metadata.entity.*;
import com.kingdee.bos.ui.face.*;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.enums.EnumUtils;
import com.kingdee.bos.ui.face.UIRuleUtil;
import com.kingdee.bos.ctrl.swing.event.*;
import com.kingdee.bos.ctrl.kdf.table.event.*;
import com.kingdee.bos.ctrl.extendcontrols.*;
import com.kingdee.bos.ctrl.kdf.util.render.*;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.uistatemanage.ActionStateConst;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.appframework.uip.UINavigator;


/**
 * output class name
 */
public abstract class AbstractPoliticalSystemEditUI extends com.kingdee.eas.framework.client.CoreBillEditUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractPoliticalSystemEditUI.class);
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contCreator;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contCreateTime;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contLastUpdateUser;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contLastUpdateTime;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contBizDate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contDescription;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contAuditor;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtEntrys;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtEntrys_detailPanel = null;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcompany;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcompanyNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contpoliticalSystemName;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contregulationType;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contauditTS;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer conteffectDate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contexpireDate;
    protected com.kingdee.bos.ctrl.swing.KDCheckBox chkisReview;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contmeetNum;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contdataSource;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contoperType;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contadminOrgUnit;
    protected com.kingdee.bos.ctrl.swing.KDCheckBox chkisUpload;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contstate;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtCreator;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker kDDateCreateTime;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtLastUpdateUser;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker kDDateLastUpdateTime;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtNumber;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkBizDate;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtDescription;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtAuditor;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtcompany;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtcompanyNumber;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtpoliticalSystemName;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtregulationType;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkauditTS;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkeffectDate;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkexpireDate;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtmeetNum;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtdataSource;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtoperType;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtadminOrgUnit;
    protected com.kingdee.bos.ctrl.swing.KDComboBox state;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton genSubject;
    protected com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo editData = null;
    protected ActionGenSubject actionGenSubject = null;
    protected ActionAudit actionAudit = null;
    protected ActionUnAudit actionUnAudit = null;
    /**
     * output class constructor
     */
    public AbstractPoliticalSystemEditUI() throws Exception
    {
        super();
        this.defaultObjectName = "editData";
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractPoliticalSystemEditUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        //actionSubmit
        String _tempStr = null;
        actionSubmit.setEnabled(true);
        actionSubmit.setDaemonRun(false);

        actionSubmit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl S"));
        _tempStr = resHelper.getString("ActionSubmit.SHORT_DESCRIPTION");
        actionSubmit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.LONG_DESCRIPTION");
        actionSubmit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.NAME");
        actionSubmit.putValue(ItemAction.NAME, _tempStr);
        this.actionSubmit.setBindWorkFlow(true);
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrint
        actionPrint.setEnabled(true);
        actionPrint.setDaemonRun(false);

        actionPrint.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl P"));
        _tempStr = resHelper.getString("ActionPrint.SHORT_DESCRIPTION");
        actionPrint.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.LONG_DESCRIPTION");
        actionPrint.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.NAME");
        actionPrint.putValue(ItemAction.NAME, _tempStr);
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrintPreview
        actionPrintPreview.setEnabled(true);
        actionPrintPreview.setDaemonRun(false);

        actionPrintPreview.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift ctrl P"));
        _tempStr = resHelper.getString("ActionPrintPreview.SHORT_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.LONG_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.NAME");
        actionPrintPreview.putValue(ItemAction.NAME, _tempStr);
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionGenSubject
        this.actionGenSubject = new ActionGenSubject(this);
        getActionManager().registerAction("actionGenSubject", actionGenSubject);
         this.actionGenSubject.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionAudit
        this.actionAudit = new ActionAudit(this);
        getActionManager().registerAction("actionAudit", actionAudit);
        this.actionAudit.setBindWorkFlow(true);
        this.actionAudit.setExtendProperty("canForewarn", "true");
        this.actionAudit.setExtendProperty("userDefined", "true");
        this.actionAudit.setExtendProperty("isObjectUpdateLock", "false");
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.ForewarnService());
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.WorkFlowService());
        //actionUnAudit
        this.actionUnAudit = new ActionUnAudit(this);
        getActionManager().registerAction("actionUnAudit", actionUnAudit);
        this.actionUnAudit.setBindWorkFlow(true);
        this.actionUnAudit.setExtendProperty("canForewarn", "true");
        this.actionUnAudit.setExtendProperty("userDefined", "true");
        this.actionUnAudit.setExtendProperty("isObjectUpdateLock", "false");
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.ForewarnService());
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.WorkFlowService());
        this.contCreator = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contCreateTime = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contLastUpdateUser = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contLastUpdateTime = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contBizDate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contDescription = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contAuditor = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kdtEntrys = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.contcompany = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contcompanyNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contpoliticalSystemName = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contregulationType = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contauditTS = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.conteffectDate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contexpireDate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.chkisReview = new com.kingdee.bos.ctrl.swing.KDCheckBox();
        this.contmeetNum = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contdataSource = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contoperType = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contadminOrgUnit = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.chkisUpload = new com.kingdee.bos.ctrl.swing.KDCheckBox();
        this.contstate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.prmtCreator = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.kDDateCreateTime = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.prmtLastUpdateUser = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.kDDateLastUpdateTime = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.txtNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.pkBizDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.txtDescription = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtAuditor = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtcompany = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.txtcompanyNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtpoliticalSystemName = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtregulationType = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.pkauditTS = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.pkeffectDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.pkexpireDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.prmtmeetNum = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtdataSource = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtoperType = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtadminOrgUnit = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.state = new com.kingdee.bos.ctrl.swing.KDComboBox();
        this.genSubject = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.contCreator.setName("contCreator");
        this.contCreateTime.setName("contCreateTime");
        this.contLastUpdateUser.setName("contLastUpdateUser");
        this.contLastUpdateTime.setName("contLastUpdateTime");
        this.contNumber.setName("contNumber");
        this.contBizDate.setName("contBizDate");
        this.contDescription.setName("contDescription");
        this.contAuditor.setName("contAuditor");
        this.kdtEntrys.setName("kdtEntrys");
        this.contcompany.setName("contcompany");
        this.contcompanyNumber.setName("contcompanyNumber");
        this.contpoliticalSystemName.setName("contpoliticalSystemName");
        this.contregulationType.setName("contregulationType");
        this.contauditTS.setName("contauditTS");
        this.conteffectDate.setName("conteffectDate");
        this.contexpireDate.setName("contexpireDate");
        this.chkisReview.setName("chkisReview");
        this.contmeetNum.setName("contmeetNum");
        this.contdataSource.setName("contdataSource");
        this.contoperType.setName("contoperType");
        this.contadminOrgUnit.setName("contadminOrgUnit");
        this.chkisUpload.setName("chkisUpload");
        this.contstate.setName("contstate");
        this.prmtCreator.setName("prmtCreator");
        this.kDDateCreateTime.setName("kDDateCreateTime");
        this.prmtLastUpdateUser.setName("prmtLastUpdateUser");
        this.kDDateLastUpdateTime.setName("kDDateLastUpdateTime");
        this.txtNumber.setName("txtNumber");
        this.pkBizDate.setName("pkBizDate");
        this.txtDescription.setName("txtDescription");
        this.prmtAuditor.setName("prmtAuditor");
        this.prmtcompany.setName("prmtcompany");
        this.txtcompanyNumber.setName("txtcompanyNumber");
        this.txtpoliticalSystemName.setName("txtpoliticalSystemName");
        this.prmtregulationType.setName("prmtregulationType");
        this.pkauditTS.setName("pkauditTS");
        this.pkeffectDate.setName("pkeffectDate");
        this.pkexpireDate.setName("pkexpireDate");
        this.prmtmeetNum.setName("prmtmeetNum");
        this.prmtdataSource.setName("prmtdataSource");
        this.prmtoperType.setName("prmtoperType");
        this.prmtadminOrgUnit.setName("prmtadminOrgUnit");
        this.state.setName("state");
        this.genSubject.setName("genSubject");
        // CoreUI		
        this.btnTraceUp.setVisible(false);		
        this.btnTraceDown.setVisible(false);		
        this.btnCreateTo.setVisible(true);		
        this.btnAddLine.setVisible(false);		
        this.btnCopyLine.setVisible(false);		
        this.btnInsertLine.setVisible(false);		
        this.btnRemoveLine.setVisible(false);		
        this.btnAuditResult.setVisible(false);		
        this.separator1.setVisible(false);		
        this.menuItemCreateTo.setVisible(true);		
        this.separator3.setVisible(false);		
        this.menuItemTraceUp.setVisible(false);		
        this.menuItemTraceDown.setVisible(false);		
        this.menuTable1.setVisible(false);		
        this.menuItemAddLine.setVisible(false);		
        this.menuItemCopyLine.setVisible(false);		
        this.menuItemInsertLine.setVisible(false);		
        this.menuItemRemoveLine.setVisible(false);		
        this.menuItemViewSubmitProccess.setVisible(false);		
        this.menuItemViewDoProccess.setVisible(false);		
        this.menuItemAuditResult.setVisible(false);
        // contCreator		
        this.contCreator.setBoundLabelText(resHelper.getString("contCreator.boundLabelText"));		
        this.contCreator.setBoundLabelLength(100);		
        this.contCreator.setBoundLabelUnderline(true);		
        this.contCreator.setEnabled(false);
        // contCreateTime		
        this.contCreateTime.setBoundLabelText(resHelper.getString("contCreateTime.boundLabelText"));		
        this.contCreateTime.setBoundLabelLength(100);		
        this.contCreateTime.setBoundLabelUnderline(true);		
        this.contCreateTime.setEnabled(false);
        // contLastUpdateUser		
        this.contLastUpdateUser.setBoundLabelText(resHelper.getString("contLastUpdateUser.boundLabelText"));		
        this.contLastUpdateUser.setBoundLabelLength(100);		
        this.contLastUpdateUser.setBoundLabelUnderline(true);		
        this.contLastUpdateUser.setEnabled(false);		
        this.contLastUpdateUser.setVisible(false);
        // contLastUpdateTime		
        this.contLastUpdateTime.setBoundLabelText(resHelper.getString("contLastUpdateTime.boundLabelText"));		
        this.contLastUpdateTime.setBoundLabelLength(100);		
        this.contLastUpdateTime.setBoundLabelUnderline(true);		
        this.contLastUpdateTime.setEnabled(false);		
        this.contLastUpdateTime.setVisible(false);
        // contNumber		
        this.contNumber.setBoundLabelText(resHelper.getString("contNumber.boundLabelText"));		
        this.contNumber.setBoundLabelLength(100);		
        this.contNumber.setBoundLabelUnderline(true);
        // contBizDate		
        this.contBizDate.setBoundLabelText(resHelper.getString("contBizDate.boundLabelText"));		
        this.contBizDate.setBoundLabelLength(100);		
        this.contBizDate.setBoundLabelUnderline(true);		
        this.contBizDate.setBoundLabelAlignment(7);		
        this.contBizDate.setVisible(true);
        // contDescription		
        this.contDescription.setBoundLabelText(resHelper.getString("contDescription.boundLabelText"));		
        this.contDescription.setBoundLabelLength(100);		
        this.contDescription.setBoundLabelUnderline(true);
        // contAuditor		
        this.contAuditor.setBoundLabelText(resHelper.getString("contAuditor.boundLabelText"));		
        this.contAuditor.setBoundLabelLength(100);		
        this.contAuditor.setBoundLabelUnderline(true);
        // kdtEntrys
		String kdtEntrysStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol2\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol5\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol6\"><c:Protection locked=\"true\" hidden=\"true\" /></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"id\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"catalogMain\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"1\" /><t:Column t:key=\"catalogMainNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"2\" t:styleID=\"sCol2\" /><t:Column t:key=\"rate\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"3\" /><t:Column t:key=\"voteMode\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"4\" /><t:Column t:key=\"catalog\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"5\" t:styleID=\"sCol5\" /><t:Column t:key=\"catalogNum\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"6\" t:styleID=\"sCol6\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{id}</t:Cell><t:Cell>$Resource{catalogMain}</t:Cell><t:Cell>$Resource{catalogMainNumber}</t:Cell><t:Cell>$Resource{rate}</t:Cell><t:Cell>$Resource{voteMode}</t:Cell><t:Cell>$Resource{catalog}</t:Cell><t:Cell>$Resource{catalogNum}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtEntrys.setFormatXml(resHelper.translateString("kdtEntrys",kdtEntrysStrXML));
        kdtEntrys.addKDTEditListener(new KDTEditAdapter() {
		public void editStopped(KDTEditEvent e) {
			try {
				kdtEntrys_Changed(e.getRowIndex(),e.getColIndex());
			}
			catch (Exception exc) {
				handUIException(exc);
			}
		}
	});


                this.kdtEntrys.putBindContents("editData",new String[] {"id","catalogMain","catalogMainNumber","rate","voteMode","catalog","catalogNum"});


        this.kdtEntrys.checkParsed();
        final KDBizPromptBox kdtEntrys_catalogMain_PromptBox = new KDBizPromptBox();
        kdtEntrys_catalogMain_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.CatalogMainQuery");
        kdtEntrys_catalogMain_PromptBox.setVisible(true);
        kdtEntrys_catalogMain_PromptBox.setEditable(true);
        kdtEntrys_catalogMain_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_catalogMain_PromptBox.setEditFormat("$number$");
        kdtEntrys_catalogMain_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_catalogMain_CellEditor = new KDTDefaultCellEditor(kdtEntrys_catalogMain_PromptBox);
        this.kdtEntrys.getColumn("catalogMain").setEditor(kdtEntrys_catalogMain_CellEditor);
        ObjectValueRender kdtEntrys_catalogMain_OVR = new ObjectValueRender();
        kdtEntrys_catalogMain_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("catalogMain").setRenderer(kdtEntrys_catalogMain_OVR);
        			EntityViewInfo evikdtEntrys_catalogMain_PromptBox = new EntityViewInfo ();
		evikdtEntrys_catalogMain_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtEntrys_catalogMain_PromptBox.setEntityViewInfo(evikdtEntrys_catalogMain_PromptBox);
					
        			kdtEntrys_catalogMain_PromptBox.addSelectorListener(new SelectorListener() {
			com.kingdee.eas.custom.tiog.basedata.client.CatalogMainListUI kdtEntrys_catalogMain_PromptBox_F7ListUI = null;
			public void willShow(SelectorEvent e) {
				if (kdtEntrys_catalogMain_PromptBox_F7ListUI == null) {
					try {
						kdtEntrys_catalogMain_PromptBox_F7ListUI = new com.kingdee.eas.custom.tiog.basedata.client.CatalogMainListUI();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					HashMap ctx = new HashMap();
					ctx.put("bizUIOwner",javax.swing.SwingUtilities.getWindowAncestor(kdtEntrys_catalogMain_PromptBox_F7ListUI));
					kdtEntrys_catalogMain_PromptBox_F7ListUI.setF7Use(true,ctx);
					kdtEntrys_catalogMain_PromptBox.setSelector(kdtEntrys_catalogMain_PromptBox_F7ListUI);
				}
			}
		});
					
        KDTextField kdtEntrys_catalogMainNumber_TextField = new KDTextField();
        kdtEntrys_catalogMainNumber_TextField.setName("kdtEntrys_catalogMainNumber_TextField");
        kdtEntrys_catalogMainNumber_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtEntrys_catalogMainNumber_CellEditor = new KDTDefaultCellEditor(kdtEntrys_catalogMainNumber_TextField);
        this.kdtEntrys.getColumn("catalogMainNumber").setEditor(kdtEntrys_catalogMainNumber_CellEditor);
        final KDBizPromptBox kdtEntrys_rate_PromptBox = new KDBizPromptBox();
        kdtEntrys_rate_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.HandupRateQuery");
        kdtEntrys_rate_PromptBox.setVisible(true);
        kdtEntrys_rate_PromptBox.setEditable(true);
        kdtEntrys_rate_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_rate_PromptBox.setEditFormat("$number$");
        kdtEntrys_rate_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_rate_CellEditor = new KDTDefaultCellEditor(kdtEntrys_rate_PromptBox);
        this.kdtEntrys.getColumn("rate").setEditor(kdtEntrys_rate_CellEditor);
        ObjectValueRender kdtEntrys_rate_OVR = new ObjectValueRender();
        kdtEntrys_rate_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("rate").setRenderer(kdtEntrys_rate_OVR);
        			EntityViewInfo evikdtEntrys_rate_PromptBox = new EntityViewInfo ();
		evikdtEntrys_rate_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtEntrys_rate_PromptBox.setEntityViewInfo(evikdtEntrys_rate_PromptBox);
					
        final KDBizPromptBox kdtEntrys_voteMode_PromptBox = new KDBizPromptBox();
        kdtEntrys_voteMode_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.VoteModeQuery");
        kdtEntrys_voteMode_PromptBox.setVisible(true);
        kdtEntrys_voteMode_PromptBox.setEditable(true);
        kdtEntrys_voteMode_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_voteMode_PromptBox.setEditFormat("$number$");
        kdtEntrys_voteMode_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_voteMode_CellEditor = new KDTDefaultCellEditor(kdtEntrys_voteMode_PromptBox);
        this.kdtEntrys.getColumn("voteMode").setEditor(kdtEntrys_voteMode_CellEditor);
        ObjectValueRender kdtEntrys_voteMode_OVR = new ObjectValueRender();
        kdtEntrys_voteMode_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("voteMode").setRenderer(kdtEntrys_voteMode_OVR);
        			EntityViewInfo evikdtEntrys_voteMode_PromptBox = new EntityViewInfo ();
		evikdtEntrys_voteMode_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtEntrys_voteMode_PromptBox.setEntityViewInfo(evikdtEntrys_voteMode_PromptBox);
					
        final KDBizPromptBox kdtEntrys_catalog_PromptBox = new KDBizPromptBox();
        kdtEntrys_catalog_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.CatalogQuery");
        kdtEntrys_catalog_PromptBox.setVisible(true);
        kdtEntrys_catalog_PromptBox.setEditable(true);
        kdtEntrys_catalog_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_catalog_PromptBox.setEditFormat("$number$");
        kdtEntrys_catalog_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_catalog_CellEditor = new KDTDefaultCellEditor(kdtEntrys_catalog_PromptBox);
        this.kdtEntrys.getColumn("catalog").setEditor(kdtEntrys_catalog_CellEditor);
        ObjectValueRender kdtEntrys_catalog_OVR = new ObjectValueRender();
        kdtEntrys_catalog_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("catalog").setRenderer(kdtEntrys_catalog_OVR);
        			EntityViewInfo evikdtEntrys_catalog_PromptBox = new EntityViewInfo ();
		evikdtEntrys_catalog_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtEntrys_catalog_PromptBox.setEntityViewInfo(evikdtEntrys_catalog_PromptBox);
					
        			kdtEntrys_catalog_PromptBox.addSelectorListener(new SelectorListener() {
			com.kingdee.eas.custom.tiog.basedata.client.CatalogListUI kdtEntrys_catalog_PromptBox_F7ListUI = null;
			public void willShow(SelectorEvent e) {
				if (kdtEntrys_catalog_PromptBox_F7ListUI == null) {
					try {
						kdtEntrys_catalog_PromptBox_F7ListUI = new com.kingdee.eas.custom.tiog.basedata.client.CatalogListUI();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					HashMap ctx = new HashMap();
					ctx.put("bizUIOwner",javax.swing.SwingUtilities.getWindowAncestor(kdtEntrys_catalog_PromptBox_F7ListUI));
					kdtEntrys_catalog_PromptBox_F7ListUI.setF7Use(true,ctx);
					kdtEntrys_catalog_PromptBox.setSelector(kdtEntrys_catalog_PromptBox_F7ListUI);
				}
			}
		});
					
        KDTextField kdtEntrys_catalogNum_TextField = new KDTextField();
        kdtEntrys_catalogNum_TextField.setName("kdtEntrys_catalogNum_TextField");
        kdtEntrys_catalogNum_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtEntrys_catalogNum_CellEditor = new KDTDefaultCellEditor(kdtEntrys_catalogNum_TextField);
        this.kdtEntrys.getColumn("catalogNum").setEditor(kdtEntrys_catalogNum_CellEditor);
        // contcompany		
        this.contcompany.setBoundLabelText(resHelper.getString("contcompany.boundLabelText"));		
        this.contcompany.setBoundLabelLength(100);		
        this.contcompany.setBoundLabelUnderline(true);		
        this.contcompany.setVisible(true);
        // contcompanyNumber		
        this.contcompanyNumber.setBoundLabelText(resHelper.getString("contcompanyNumber.boundLabelText"));		
        this.contcompanyNumber.setBoundLabelLength(100);		
        this.contcompanyNumber.setBoundLabelUnderline(true);		
        this.contcompanyNumber.setVisible(true);
        // contpoliticalSystemName		
        this.contpoliticalSystemName.setBoundLabelText(resHelper.getString("contpoliticalSystemName.boundLabelText"));		
        this.contpoliticalSystemName.setBoundLabelLength(100);		
        this.contpoliticalSystemName.setBoundLabelUnderline(true);		
        this.contpoliticalSystemName.setVisible(true);
        // contregulationType		
        this.contregulationType.setBoundLabelText(resHelper.getString("contregulationType.boundLabelText"));		
        this.contregulationType.setBoundLabelLength(100);		
        this.contregulationType.setBoundLabelUnderline(true);		
        this.contregulationType.setVisible(true);
        // contauditTS		
        this.contauditTS.setBoundLabelText(resHelper.getString("contauditTS.boundLabelText"));		
        this.contauditTS.setBoundLabelLength(100);		
        this.contauditTS.setBoundLabelUnderline(true);		
        this.contauditTS.setVisible(true);
        // conteffectDate		
        this.conteffectDate.setBoundLabelText(resHelper.getString("conteffectDate.boundLabelText"));		
        this.conteffectDate.setBoundLabelLength(100);		
        this.conteffectDate.setBoundLabelUnderline(true);		
        this.conteffectDate.setVisible(true);
        // contexpireDate		
        this.contexpireDate.setBoundLabelText(resHelper.getString("contexpireDate.boundLabelText"));		
        this.contexpireDate.setBoundLabelLength(100);		
        this.contexpireDate.setBoundLabelUnderline(true);		
        this.contexpireDate.setVisible(true);
        // chkisReview		
        this.chkisReview.setText(resHelper.getString("chkisReview.text"));		
        this.chkisReview.setVisible(true);		
        this.chkisReview.setHorizontalAlignment(2);
        // contmeetNum		
        this.contmeetNum.setBoundLabelText(resHelper.getString("contmeetNum.boundLabelText"));		
        this.contmeetNum.setBoundLabelLength(100);		
        this.contmeetNum.setBoundLabelUnderline(true);		
        this.contmeetNum.setVisible(true);
        // contdataSource		
        this.contdataSource.setBoundLabelText(resHelper.getString("contdataSource.boundLabelText"));		
        this.contdataSource.setBoundLabelLength(100);		
        this.contdataSource.setBoundLabelUnderline(true);		
        this.contdataSource.setVisible(true);
        // contoperType		
        this.contoperType.setBoundLabelText(resHelper.getString("contoperType.boundLabelText"));		
        this.contoperType.setBoundLabelLength(100);		
        this.contoperType.setBoundLabelUnderline(true);		
        this.contoperType.setVisible(true);
        // contadminOrgUnit		
        this.contadminOrgUnit.setBoundLabelText(resHelper.getString("contadminOrgUnit.boundLabelText"));		
        this.contadminOrgUnit.setBoundLabelLength(100);		
        this.contadminOrgUnit.setBoundLabelUnderline(true);		
        this.contadminOrgUnit.setVisible(true);
        // chkisUpload		
        this.chkisUpload.setText(resHelper.getString("chkisUpload.text"));		
        this.chkisUpload.setVisible(false);		
        this.chkisUpload.setHorizontalAlignment(2);		
        this.chkisUpload.setEnabled(false);
        // contstate		
        this.contstate.setBoundLabelText(resHelper.getString("contstate.boundLabelText"));		
        this.contstate.setBoundLabelLength(100);		
        this.contstate.setBoundLabelUnderline(true);		
        this.contstate.setVisible(false);
        // prmtCreator		
        this.prmtCreator.setEnabled(false);
        // kDDateCreateTime		
        this.kDDateCreateTime.setTimeEnabled(true);		
        this.kDDateCreateTime.setEnabled(false);
        // prmtLastUpdateUser		
        this.prmtLastUpdateUser.setEnabled(false);
        // kDDateLastUpdateTime		
        this.kDDateLastUpdateTime.setTimeEnabled(true);		
        this.kDDateLastUpdateTime.setEnabled(false);
        // txtNumber		
        this.txtNumber.setMaxLength(80);
        // pkBizDate		
        this.pkBizDate.setVisible(true);		
        this.pkBizDate.setEnabled(true);
        // txtDescription		
        this.txtDescription.setMaxLength(80);
        // prmtAuditor		
        this.prmtAuditor.setEnabled(false);
        // prmtcompany		
        this.prmtcompany.setQueryInfo("com.kingdee.eas.basedata.org.app.CompanyOrgUnitQuery");		
        this.prmtcompany.setVisible(true);		
        this.prmtcompany.setEditable(true);		
        this.prmtcompany.setDisplayFormat("$name$");		
        this.prmtcompany.setEditFormat("$number$");		
        this.prmtcompany.setCommitFormat("$number$");		
        this.prmtcompany.setRequired(false);
        // txtcompanyNumber		
        this.txtcompanyNumber.setVisible(true);		
        this.txtcompanyNumber.setHorizontalAlignment(2);		
        this.txtcompanyNumber.setMaxLength(80);		
        this.txtcompanyNumber.setRequired(false);
        // txtpoliticalSystemName		
        this.txtpoliticalSystemName.setVisible(true);		
        this.txtpoliticalSystemName.setHorizontalAlignment(2);		
        this.txtpoliticalSystemName.setMaxLength(255);		
        this.txtpoliticalSystemName.setRequired(false);
        // prmtregulationType		
        this.prmtregulationType.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.RegulationTypeQuery");		
        this.prmtregulationType.setVisible(true);		
        this.prmtregulationType.setEditable(true);		
        this.prmtregulationType.setDisplayFormat("$name$");		
        this.prmtregulationType.setEditFormat("$number$");		
        this.prmtregulationType.setCommitFormat("$number$");		
        this.prmtregulationType.setRequired(false);
        		EntityViewInfo eviprmtregulationType = new EntityViewInfo ();
		eviprmtregulationType.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtregulationType.setEntityViewInfo(eviprmtregulationType);
					
        // pkauditTS		
        this.pkauditTS.setVisible(true);		
        this.pkauditTS.setRequired(false);
        // pkeffectDate		
        this.pkeffectDate.setVisible(true);		
        this.pkeffectDate.setRequired(false);
        // pkexpireDate		
        this.pkexpireDate.setVisible(true);		
        this.pkexpireDate.setRequired(false);
        // prmtmeetNum		
        this.prmtmeetNum.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.MeetingTypeQuery");		
        this.prmtmeetNum.setVisible(true);		
        this.prmtmeetNum.setEditable(true);		
        this.prmtmeetNum.setDisplayFormat("$name$");		
        this.prmtmeetNum.setEditFormat("$number$");		
        this.prmtmeetNum.setCommitFormat("$number$");		
        this.prmtmeetNum.setRequired(false);
        		EntityViewInfo eviprmtmeetNum = new EntityViewInfo ();
		eviprmtmeetNum.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtmeetNum.setEntityViewInfo(eviprmtmeetNum);
					
        // prmtdataSource		
        this.prmtdataSource.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.DataSourceQuery");		
        this.prmtdataSource.setVisible(true);		
        this.prmtdataSource.setEditable(true);		
        this.prmtdataSource.setDisplayFormat("$name$");		
        this.prmtdataSource.setEditFormat("$number$");		
        this.prmtdataSource.setCommitFormat("$number$");		
        this.prmtdataSource.setRequired(false);
        		EntityViewInfo eviprmtdataSource = new EntityViewInfo ();
		eviprmtdataSource.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtdataSource.setEntityViewInfo(eviprmtdataSource);
					
        // prmtoperType		
        this.prmtoperType.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.OperateFlagQuery");		
        this.prmtoperType.setVisible(true);		
        this.prmtoperType.setEditable(true);		
        this.prmtoperType.setDisplayFormat("$name$");		
        this.prmtoperType.setEditFormat("$number$");		
        this.prmtoperType.setCommitFormat("$number$");		
        this.prmtoperType.setRequired(false);
        		EntityViewInfo eviprmtoperType = new EntityViewInfo ();
		eviprmtoperType.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtoperType.setEntityViewInfo(eviprmtoperType);
					
        // prmtadminOrgUnit		
        this.prmtadminOrgUnit.setQueryInfo("com.kingdee.eas.basedata.org.app.AdminItemQuery");		
        this.prmtadminOrgUnit.setVisible(true);		
        this.prmtadminOrgUnit.setEditable(true);		
        this.prmtadminOrgUnit.setDisplayFormat("$name$");		
        this.prmtadminOrgUnit.setEditFormat("$number$");		
        this.prmtadminOrgUnit.setCommitFormat("$number$");		
        this.prmtadminOrgUnit.setRequired(false);
        		setOrgF7(prmtadminOrgUnit,com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"));
					
        // state		
        this.state.setVisible(false);		
        this.state.addItems(EnumUtils.getEnumList("com.kingdee.eas.custom.tiog.bizbill.BillState").toArray());		
        this.state.setRequired(false);		
        this.state.setEnabled(false);
        // genSubject
        this.genSubject.setAction((IItemAction)ActionProxyFactory.getProxy(actionGenSubject, new Class[] { IItemAction.class }, getServiceContext()));		
        this.genSubject.setText(resHelper.getString("genSubject.text"));
        this.setFocusTraversalPolicy(new com.kingdee.bos.ui.UIFocusTraversalPolicy(new java.awt.Component[] {prmtcompany,txtcompanyNumber,txtpoliticalSystemName,prmtregulationType,pkauditTS,chkisReview,prmtmeetNum,prmtdataSource,prmtoperType,prmtadminOrgUnit,chkisUpload,state}));
        this.setFocusCycleRoot(true);
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(0, 0, 1013, 989));
        this.setLayout(new KDLayout());
        this.putClientProperty("OriginalBounds", new Rectangle(0, 0, 1013, 989));
        contCreator.setBounds(new Rectangle(366, 932, 270, 19));
        this.add(contCreator, new KDLayout.Constraints(366, 932, 270, 19, 0));
        contCreateTime.setBounds(new Rectangle(697, 932, 270, 19));
        this.add(contCreateTime, new KDLayout.Constraints(697, 932, 270, 19, 0));
        contLastUpdateUser.setBounds(new Rectangle(35, 956, 270, 19));
        this.add(contLastUpdateUser, new KDLayout.Constraints(35, 956, 270, 19, 0));
        contLastUpdateTime.setBounds(new Rectangle(366, 956, 270, 19));
        this.add(contLastUpdateTime, new KDLayout.Constraints(366, 956, 270, 19, 0));
        contNumber.setBounds(new Rectangle(39, 21, 270, 19));
        this.add(contNumber, new KDLayout.Constraints(39, 21, 270, 19, 0));
        contBizDate.setBounds(new Rectangle(666, 59, 270, 19));
        this.add(contBizDate, new KDLayout.Constraints(666, 59, 270, 19, 0));
        contDescription.setBounds(new Rectangle(40, 285, 601, 19));
        this.add(contDescription, new KDLayout.Constraints(40, 285, 601, 19, 0));
        contAuditor.setBounds(new Rectangle(35, 932, 270, 19));
        this.add(contAuditor, new KDLayout.Constraints(35, 932, 270, 19, 0));
        kdtEntrys.setBounds(new Rectangle(34, 352, 951, 542));
        kdtEntrys_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtEntrys,new com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryInfo(),null,false);
        this.add(kdtEntrys_detailPanel, new KDLayout.Constraints(34, 352, 951, 542, 0));
        contcompany.setBounds(new Rectangle(362, 21, 270, 19));
        this.add(contcompany, new KDLayout.Constraints(362, 21, 270, 19, 0));
        contcompanyNumber.setBounds(new Rectangle(362, 59, 270, 19));
        this.add(contcompanyNumber, new KDLayout.Constraints(362, 59, 270, 19, 0));
        contpoliticalSystemName.setBounds(new Rectangle(38, 59, 270, 19));
        this.add(contpoliticalSystemName, new KDLayout.Constraints(38, 59, 270, 19, 0));
        contregulationType.setBounds(new Rectangle(39, 129, 270, 19));
        this.add(contregulationType, new KDLayout.Constraints(39, 129, 270, 19, 0));
        contauditTS.setBounds(new Rectangle(39, 165, 270, 19));
        this.add(contauditTS, new KDLayout.Constraints(39, 165, 270, 19, 0));
        conteffectDate.setBounds(new Rectangle(39, 201, 270, 19));
        this.add(conteffectDate, new KDLayout.Constraints(39, 201, 270, 19, 0));
        contexpireDate.setBounds(new Rectangle(39, 237, 270, 19));
        this.add(contexpireDate, new KDLayout.Constraints(39, 237, 270, 19, 0));
        chkisReview.setBounds(new Rectangle(362, 171, 196, 19));
        this.add(chkisReview, new KDLayout.Constraints(362, 171, 196, 19, 0));
        contmeetNum.setBounds(new Rectangle(39, 93, 270, 19));
        this.add(contmeetNum, new KDLayout.Constraints(39, 93, 270, 19, 0));
        contdataSource.setBounds(new Rectangle(362, 129, 270, 19));
        this.add(contdataSource, new KDLayout.Constraints(362, 129, 270, 19, 0));
        contoperType.setBounds(new Rectangle(362, 93, 270, 19));
        this.add(contoperType, new KDLayout.Constraints(362, 93, 270, 19, 0));
        contadminOrgUnit.setBounds(new Rectangle(666, 21, 270, 19));
        this.add(contadminOrgUnit, new KDLayout.Constraints(666, 21, 270, 19, 0));
        chkisUpload.setBounds(new Rectangle(666, 129, 270, 19));
        this.add(chkisUpload, new KDLayout.Constraints(666, 129, 270, 19, 0));
        contstate.setBounds(new Rectangle(666, 93, 270, 19));
        this.add(contstate, new KDLayout.Constraints(666, 93, 270, 19, 0));
        //contCreator
        contCreator.setBoundEditor(prmtCreator);
        //contCreateTime
        contCreateTime.setBoundEditor(kDDateCreateTime);
        //contLastUpdateUser
        contLastUpdateUser.setBoundEditor(prmtLastUpdateUser);
        //contLastUpdateTime
        contLastUpdateTime.setBoundEditor(kDDateLastUpdateTime);
        //contNumber
        contNumber.setBoundEditor(txtNumber);
        //contBizDate
        contBizDate.setBoundEditor(pkBizDate);
        //contDescription
        contDescription.setBoundEditor(txtDescription);
        //contAuditor
        contAuditor.setBoundEditor(prmtAuditor);
        //contcompany
        contcompany.setBoundEditor(prmtcompany);
        //contcompanyNumber
        contcompanyNumber.setBoundEditor(txtcompanyNumber);
        //contpoliticalSystemName
        contpoliticalSystemName.setBoundEditor(txtpoliticalSystemName);
        //contregulationType
        contregulationType.setBoundEditor(prmtregulationType);
        //contauditTS
        contauditTS.setBoundEditor(pkauditTS);
        //conteffectDate
        conteffectDate.setBoundEditor(pkeffectDate);
        //contexpireDate
        contexpireDate.setBoundEditor(pkexpireDate);
        //contmeetNum
        contmeetNum.setBoundEditor(prmtmeetNum);
        //contdataSource
        contdataSource.setBoundEditor(prmtdataSource);
        //contoperType
        contoperType.setBoundEditor(prmtoperType);
        //contadminOrgUnit
        contadminOrgUnit.setBoundEditor(prmtadminOrgUnit);
        //contstate
        contstate.setBoundEditor(state);

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {
        this.menuBar.add(menuFile);
        this.menuBar.add(menuEdit);
        this.menuBar.add(MenuService);
        this.menuBar.add(menuView);
        this.menuBar.add(menuBiz);
        this.menuBar.add(menuTable1);
        this.menuBar.add(menuTool);
        this.menuBar.add(menuWorkflow);
        this.menuBar.add(menuHelp);
        //menuFile
        menuFile.add(menuItemAddNew);
        menuFile.add(kDSeparator1);
        menuFile.add(menuItemCloudFeed);
        menuFile.add(menuItemSave);
        menuFile.add(menuItemCloudScreen);
        menuFile.add(menuItemSubmit);
        menuFile.add(menuItemCloudShare);
        menuFile.add(menuSubmitOption);
        menuFile.add(kdSeparatorFWFile1);
        menuFile.add(rMenuItemSubmit);
        menuFile.add(rMenuItemSubmitAndAddNew);
        menuFile.add(rMenuItemSubmitAndPrint);
        menuFile.add(separatorFile1);
        menuFile.add(MenuItemAttachment);
        menuFile.add(kDSeparator2);
        menuFile.add(menuItemPageSetup);
        menuFile.add(menuItemPrint);
        menuFile.add(menuItemPrintPreview);
        menuFile.add(kDSeparator6);
        menuFile.add(menuItemSendMail);
        menuFile.add(kDSeparator3);
        menuFile.add(menuItemExitCurrent);
        //menuSubmitOption
        menuSubmitOption.add(chkMenuItemSubmitAndAddNew);
        menuSubmitOption.add(chkMenuItemSubmitAndPrint);
        //menuEdit
        menuEdit.add(menuItemCopy);
        menuEdit.add(menuItemEdit);
        menuEdit.add(menuItemRemove);
        menuEdit.add(kDSeparator4);
        menuEdit.add(menuItemReset);
        menuEdit.add(separator1);
        menuEdit.add(menuItemCreateFrom);
        menuEdit.add(menuItemCreateTo);
        menuEdit.add(menuItemCopyFrom);
        menuEdit.add(separatorEdit1);
        menuEdit.add(menuItemEnterToNextRow);
        menuEdit.add(separator2);
        //MenuService
        MenuService.add(MenuItemKnowStore);
        MenuService.add(MenuItemAnwser);
        MenuService.add(SepratorService);
        MenuService.add(MenuItemRemoteAssist);
        //menuView
        menuView.add(menuItemFirst);
        menuView.add(menuItemPre);
        menuView.add(menuItemNext);
        menuView.add(menuItemLast);
        menuView.add(separator3);
        menuView.add(menuItemTraceUp);
        menuView.add(menuItemTraceDown);
        menuView.add(kDSeparator7);
        menuView.add(menuItemLocate);
        //menuBiz
        menuBiz.add(menuItemCancelCancel);
        menuBiz.add(menuItemCancel);
        menuBiz.add(MenuItemVoucher);
        menuBiz.add(menuItemDelVoucher);
        menuBiz.add(MenuItemPCVoucher);
        menuBiz.add(menuItemDelPCVoucher);
        //menuTable1
        menuTable1.add(menuItemAddLine);
        menuTable1.add(menuItemCopyLine);
        menuTable1.add(menuItemInsertLine);
        menuTable1.add(menuItemRemoveLine);
        //menuTool
        menuTool.add(menuItemSendMessage);
        menuTool.add(menuItemMsgFormat);
        menuTool.add(menuItemCalculator);
        menuTool.add(menuItemToolBarCustom);
        //menuWorkflow
        menuWorkflow.add(menuItemStartWorkFlow);
        menuWorkflow.add(separatorWF1);
        menuWorkflow.add(menuItemViewSubmitProccess);
        menuWorkflow.add(menuItemViewDoProccess);
        menuWorkflow.add(MenuItemWFG);
        menuWorkflow.add(menuItemWorkFlowList);
        menuWorkflow.add(separatorWF2);
        menuWorkflow.add(menuItemMultiapprove);
        menuWorkflow.add(menuItemNextPerson);
        menuWorkflow.add(menuItemAuditResult);
        menuWorkflow.add(kDSeparator5);
        menuWorkflow.add(kDMenuItemSendMessage);
        //menuHelp
        menuHelp.add(menuItemHelp);
        menuHelp.add(kDSeparator12);
        menuHelp.add(menuItemRegPro);
        menuHelp.add(menuItemPersonalSite);
        menuHelp.add(helpseparatorDiv);
        menuHelp.add(menuitemProductval);
        menuHelp.add(kDSeparatorProduct);
        menuHelp.add(menuItemAbout);

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {
        this.toolBar.add(btnAddNew);
        this.toolBar.add(btnCloud);
        this.toolBar.add(btnEdit);
        this.toolBar.add(btnXunTong);
        this.toolBar.add(btnSave);
        this.toolBar.add(kDSeparatorCloud);
        this.toolBar.add(btnReset);
        this.toolBar.add(btnSubmit);
        this.toolBar.add(btnCopy);
        this.toolBar.add(btnRemove);
        this.toolBar.add(btnCancelCancel);
        this.toolBar.add(btnCancel);
        this.toolBar.add(btnAttachment);
        this.toolBar.add(separatorFW1);
        this.toolBar.add(btnPageSetup);
        this.toolBar.add(btnPrint);
        this.toolBar.add(btnPrintPreview);
        this.toolBar.add(separatorFW2);
        this.toolBar.add(btnFirst);
        this.toolBar.add(btnPre);
        this.toolBar.add(btnNext);
        this.toolBar.add(btnLast);
        this.toolBar.add(separatorFW3);
        this.toolBar.add(btnTraceUp);
        this.toolBar.add(btnTraceDown);
        this.toolBar.add(btnWorkFlowG);
        this.toolBar.add(btnSignature);
        this.toolBar.add(btnViewSignature);
        this.toolBar.add(separatorFW4);
        this.toolBar.add(btnNumberSign);
        this.toolBar.add(separatorFW7);
        this.toolBar.add(btnCreateFrom);
        this.toolBar.add(btnCopyFrom);
        this.toolBar.add(btnCreateTo);
        this.toolBar.add(separatorFW5);
        this.toolBar.add(separatorFW8);
        this.toolBar.add(btnAddLine);
        this.toolBar.add(btnCopyLine);
        this.toolBar.add(btnInsertLine);
        this.toolBar.add(btnRemoveLine);
        this.toolBar.add(separatorFW6);
        this.toolBar.add(separatorFW9);
        this.toolBar.add(btnVoucher);
        this.toolBar.add(btnDelVoucher);
        this.toolBar.add(btnPCVoucher);
        this.toolBar.add(btnDelPCVoucher);
        this.toolBar.add(btnAuditResult);
        this.toolBar.add(btnMultiapprove);
        this.toolBar.add(btnWFViewdoProccess);
        this.toolBar.add(btnWFViewSubmitProccess);
        this.toolBar.add(btnNextPerson);
        this.toolBar.add(genSubject);


    }

	//Regiester control's property binding.
	private void registerBindings(){
		dataBinder.registerBinding("entrys.id", com.kingdee.bos.util.BOSUuid.class, this.kdtEntrys, "id.text");
		dataBinder.registerBinding("entrys", com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryInfo.class, this.kdtEntrys, "userObject");
		dataBinder.registerBinding("entrys.catalog", java.lang.Object.class, this.kdtEntrys, "catalog.text");
		dataBinder.registerBinding("entrys.catalogNum", String.class, this.kdtEntrys, "catalogNum.text");
		dataBinder.registerBinding("entrys.rate", java.lang.Object.class, this.kdtEntrys, "rate.text");
		dataBinder.registerBinding("entrys.voteMode", java.lang.Object.class, this.kdtEntrys, "voteMode.text");
		dataBinder.registerBinding("entrys.catalogMain", java.lang.Object.class, this.kdtEntrys, "catalogMain.text");
		dataBinder.registerBinding("entrys.catalogMainNumber", String.class, this.kdtEntrys, "catalogMainNumber.text");
		dataBinder.registerBinding("isReview", boolean.class, this.chkisReview, "selected");
		dataBinder.registerBinding("isUpload", boolean.class, this.chkisUpload, "selected");
		dataBinder.registerBinding("creator", com.kingdee.eas.base.permission.UserInfo.class, this.prmtCreator, "data");
		dataBinder.registerBinding("createTime", java.sql.Timestamp.class, this.kDDateCreateTime, "value");
		dataBinder.registerBinding("lastUpdateUser", com.kingdee.eas.base.permission.UserInfo.class, this.prmtLastUpdateUser, "data");
		dataBinder.registerBinding("lastUpdateTime", java.sql.Timestamp.class, this.kDDateLastUpdateTime, "value");
		dataBinder.registerBinding("number", String.class, this.txtNumber, "text");
		dataBinder.registerBinding("bizDate", java.util.Date.class, this.pkBizDate, "value");
		dataBinder.registerBinding("description", String.class, this.txtDescription, "text");
		dataBinder.registerBinding("auditor", com.kingdee.eas.base.permission.UserInfo.class, this.prmtAuditor, "data");
		dataBinder.registerBinding("company", com.kingdee.eas.basedata.org.CompanyOrgUnitInfo.class, this.prmtcompany, "data");
		dataBinder.registerBinding("companyNumber", String.class, this.txtcompanyNumber, "text");
		dataBinder.registerBinding("politicalSystemName", String.class, this.txtpoliticalSystemName, "text");
		dataBinder.registerBinding("regulationType", com.kingdee.eas.custom.tiog.basedata.RegulationTypeInfo.class, this.prmtregulationType, "data");
		dataBinder.registerBinding("auditTS", java.util.Date.class, this.pkauditTS, "value");
		dataBinder.registerBinding("effectDate", java.util.Date.class, this.pkeffectDate, "value");
		dataBinder.registerBinding("expireDate", java.util.Date.class, this.pkexpireDate, "value");
		dataBinder.registerBinding("meetNum", com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo.class, this.prmtmeetNum, "data");
		dataBinder.registerBinding("dataSource", com.kingdee.eas.custom.tiog.basedata.DataSourceInfo.class, this.prmtdataSource, "data");
		dataBinder.registerBinding("operType", com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo.class, this.prmtoperType, "data");
		dataBinder.registerBinding("adminOrgUnit", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.prmtadminOrgUnit, "data");
		dataBinder.registerBinding("state", com.kingdee.eas.custom.tiog.bizbill.BillState.class, this.state, "selectedItem");		
	}
	//Regiester UI State
	private void registerUIState(){		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.tiog.bizbill.app.PoliticalSystemEditUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }


    /**
     * output onShow method
     */
    public void onShow() throws Exception
    {
        super.onShow();
        this.prmtcompany.requestFocusInWindow();
    }

	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
        this.editData = (com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo)ov;
    }
    protected void removeByPK(IObjectPK pk) throws Exception {
    	IObjectValue editData = this.editData;
    	super.removeByPK(pk);
    	recycleNumberByOrg(editData,"Admin",editData.getString("number"));
    }
    
    protected void recycleNumberByOrg(IObjectValue editData,String orgType,String number) {
        if (!StringUtils.isEmpty(number))
        {
            try {
            	String companyID = null;            
            	com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID =com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}				
				if (!StringUtils.isEmpty(companyID) && iCodingRuleManager.isExist(editData, companyID) && iCodingRuleManager.isUseIntermitNumber(editData, companyID)) {
					iCodingRuleManager.recycleNumber(editData,companyID,number);					
				}
            }
            catch (Exception e)
            {
                handUIException(e);
            }
        }
    }
    protected void setAutoNumberByOrg(String orgType) {
    	if (editData == null) return;
		if (editData.getNumber() == null) {
            try {
            	String companyID = null;
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID = com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}
				com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
		        if (iCodingRuleManager.isExist(editData, companyID)) {
		            if (iCodingRuleManager.isAddView(editData, companyID)) {
		            	editData.setNumber(iCodingRuleManager.getNumber(editData,companyID));
		            }
	                txtNumber.setEnabled(false);
		        }
            }
            catch (Exception e) {
                handUIException(e);
                this.oldData = editData;
                com.kingdee.eas.util.SysUtil.abort();
            } 
        } 
        else {
            if (editData.getNumber().trim().length() > 0) {
                txtNumber.setText(editData.getNumber());
            }
        }
    }
			protected com.kingdee.eas.basedata.org.OrgType getMainBizOrgType() {
			return com.kingdee.eas.basedata.org.OrgType.getEnum("Admin");
		}

	protected KDBizPromptBox getMainBizOrg() {
		return prmtadminOrgUnit;
}


    /**
     * output loadFields method
     */
    public void loadFields()
    {
        		setAutoNumberByOrg("Admin");
        dataBinder.loadFields();
    }
		protected void setOrgF7(KDBizPromptBox f7,com.kingdee.eas.basedata.org.OrgType orgType) throws Exception
		{
			com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer oufip = new com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer(orgType);
			oufip.getModel().setIsCUFilter(true);
			f7.setFilterInfoProducer(oufip);
		}

    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );
		getValidateHelper().registerBindProperty("entrys.id", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.catalog", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.catalogNum", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.rate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.voteMode", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.catalogMain", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.catalogMainNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("isReview", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("isUpload", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("creator", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("createTime", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("lastUpdateUser", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("lastUpdateTime", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("bizDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("auditor", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("company", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("companyNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("politicalSystemName", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("regulationType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("auditTS", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("effectDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("expireDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("meetNum", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("dataSource", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("operType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("adminOrgUnit", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("state", ValidateHelper.ON_SAVE);    		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
        if (STATUS_ADDNEW.equals(this.oprtState)) {
        } else if (STATUS_EDIT.equals(this.oprtState)) {
        } else if (STATUS_VIEW.equals(this.oprtState)) {
        } else if (STATUS_FINDVIEW.equals(this.oprtState)) {
        }
    }


    /**
     * output kdtEntrys_Changed(int rowIndex,int colIndex) method
     */
    public void kdtEntrys_Changed(int rowIndex,int colIndex) throws Exception
    {
            if ("catalogMain".equalsIgnoreCase(kdtEntrys.getColumn(colIndex).getKey())) {
kdtEntrys.getCell(rowIndex,"catalogMainNumber").setValue(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)kdtEntrys.getCell(rowIndex,"catalogMain").getValue(),"number")));

}

    if ("catalog".equalsIgnoreCase(kdtEntrys.getColumn(colIndex).getKey())) {
kdtEntrys.getCell(rowIndex,"catalogNum").setValue(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)kdtEntrys.getCell(rowIndex,"catalog").getValue(),"number")));

}


    }
    /**
     * output getSelectors method
     */
    public SelectorItemCollection getSelectors()
    {
        SelectorItemCollection sic = new SelectorItemCollection();
		String selectorAll = System.getProperty("selector.all");
		if(StringUtils.isEmpty(selectorAll)){
			selectorAll = "true";
		}
    	sic.add(new SelectorItemInfo("entrys.id"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.*"));
		}
		else{
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.catalog.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.catalog.id"));
			sic.add(new SelectorItemInfo("entrys.catalog.name"));
        	sic.add(new SelectorItemInfo("entrys.catalog.number"));
		}
    	sic.add(new SelectorItemInfo("entrys.catalogNum"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.rate.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.rate.id"));
			sic.add(new SelectorItemInfo("entrys.rate.name"));
        	sic.add(new SelectorItemInfo("entrys.rate.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.voteMode.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.voteMode.id"));
			sic.add(new SelectorItemInfo("entrys.voteMode.name"));
        	sic.add(new SelectorItemInfo("entrys.voteMode.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.catalogMain.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.catalogMain.id"));
			sic.add(new SelectorItemInfo("entrys.catalogMain.name"));
        	sic.add(new SelectorItemInfo("entrys.catalogMain.number"));
		}
    	sic.add(new SelectorItemInfo("entrys.catalogMainNumber"));
        sic.add(new SelectorItemInfo("isReview"));
        sic.add(new SelectorItemInfo("isUpload"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("creator.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("creator.id"));
        	sic.add(new SelectorItemInfo("creator.number"));
        	sic.add(new SelectorItemInfo("creator.name"));
		}
        sic.add(new SelectorItemInfo("createTime"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("lastUpdateUser.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("lastUpdateUser.id"));
        	sic.add(new SelectorItemInfo("lastUpdateUser.number"));
        	sic.add(new SelectorItemInfo("lastUpdateUser.name"));
		}
        sic.add(new SelectorItemInfo("lastUpdateTime"));
        sic.add(new SelectorItemInfo("number"));
        sic.add(new SelectorItemInfo("bizDate"));
        sic.add(new SelectorItemInfo("description"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("auditor.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("auditor.id"));
        	sic.add(new SelectorItemInfo("auditor.number"));
        	sic.add(new SelectorItemInfo("auditor.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("company.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("company.id"));
        	sic.add(new SelectorItemInfo("company.number"));
        	sic.add(new SelectorItemInfo("company.name"));
		}
        sic.add(new SelectorItemInfo("companyNumber"));
        sic.add(new SelectorItemInfo("politicalSystemName"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("regulationType.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("regulationType.id"));
        	sic.add(new SelectorItemInfo("regulationType.number"));
        	sic.add(new SelectorItemInfo("regulationType.name"));
		}
        sic.add(new SelectorItemInfo("auditTS"));
        sic.add(new SelectorItemInfo("effectDate"));
        sic.add(new SelectorItemInfo("expireDate"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("meetNum.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("meetNum.id"));
        	sic.add(new SelectorItemInfo("meetNum.number"));
        	sic.add(new SelectorItemInfo("meetNum.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("dataSource.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("dataSource.id"));
        	sic.add(new SelectorItemInfo("dataSource.number"));
        	sic.add(new SelectorItemInfo("dataSource.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("operType.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("operType.id"));
        	sic.add(new SelectorItemInfo("operType.number"));
        	sic.add(new SelectorItemInfo("operType.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("adminOrgUnit.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("adminOrgUnit.id"));
        	sic.add(new SelectorItemInfo("adminOrgUnit.number"));
        	sic.add(new SelectorItemInfo("adminOrgUnit.name"));
		}
        sic.add(new SelectorItemInfo("state"));
        return sic;
    }        
    	

    /**
     * output actionSubmit_actionPerformed method
     */
    public void actionSubmit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionSubmit_actionPerformed(e);
    }
    	

    /**
     * output actionPrint_actionPerformed method
     */
    public void actionPrint_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
    	if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.print(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionPrintPreview_actionPerformed method
     */
    public void actionPrintPreview_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
        if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.printPreview(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionGenSubject_actionPerformed method
     */
    public void actionGenSubject_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionAudit_actionPerformed method
     */
    public void actionAudit_actionPerformed(ActionEvent e) throws Exception
    {
        com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemFactory.getRemoteInstance().audit(editData);
    }
    	

    /**
     * output actionUnAudit_actionPerformed method
     */
    public void actionUnAudit_actionPerformed(ActionEvent e) throws Exception
    {
        com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemFactory.getRemoteInstance().unAudit(editData);
    }
	public RequestContext prepareActionSubmit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionSubmit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionSubmit() {
    	return false;
    }
	public RequestContext prepareActionPrint(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrint(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrint() {
    	return false;
    }
	public RequestContext prepareActionPrintPreview(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrintPreview(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrintPreview() {
    	return false;
    }
	public RequestContext prepareActionGenSubject(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionGenSubject() {
    	return false;
    }
	public RequestContext prepareActionAudit(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionAudit() {
    	return false;
    }
	public RequestContext prepareActionUnAudit(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionUnAudit() {
    	return false;
    }

    /**
     * output ActionGenSubject class
     */     
    protected class ActionGenSubject extends ItemAction {     
    
        public ActionGenSubject()
        {
            this(null);
        }

        public ActionGenSubject(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionGenSubject.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionGenSubject.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionGenSubject.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractPoliticalSystemEditUI.this, "ActionGenSubject", "actionGenSubject_actionPerformed", e);
        }
    }

    /**
     * output ActionAudit class
     */     
    protected class ActionAudit extends ItemAction {     
    
        public ActionAudit()
        {
            this(null);
        }

        public ActionAudit(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            _tempStr = resHelper.getString("ActionAudit.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAudit.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAudit.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractPoliticalSystemEditUI.this, "ActionAudit", "actionAudit_actionPerformed", e);
        }
    }

    /**
     * output ActionUnAudit class
     */     
    protected class ActionUnAudit extends ItemAction {     
    
        public ActionUnAudit()
        {
            this(null);
        }

        public ActionUnAudit(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            _tempStr = resHelper.getString("ActionUnAudit.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionUnAudit.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionUnAudit.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractPoliticalSystemEditUI.this, "ActionUnAudit", "actionUnAudit_actionPerformed", e);
        }
    }

    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.tiog.bizbill.client", "PoliticalSystemEditUI");
    }
    /**
     * output isBindWorkFlow method
     */
    public boolean isBindWorkFlow()
    {
        return true;
    }

    /**
     * output getEditUIName method
     */
    protected String getEditUIName()
    {
        return com.kingdee.eas.custom.tiog.bizbill.client.PoliticalSystemEditUI.class.getName();
    }

    /**
     * output getBizInterface method
     */
    protected com.kingdee.eas.framework.ICoreBase getBizInterface() throws Exception
    {
        return com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemFactory.getRemoteInstance();
    }

    /**
     * output createNewData method
     */
    protected IObjectValue createNewData()
    {
        com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo objectValue = new com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo();
				if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin")) != null)
			objectValue.put("adminOrgUnit",com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin")));
 
        objectValue.setCreator((com.kingdee.eas.base.permission.UserInfo)(com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentUser()));		
        return objectValue;
    }


    	protected String getTDFileName() {
    	return "/bim/custom/tiog/bizbill/PoliticalSystem";
	}
    protected IMetaDataPK getTDQueryPK() {
    	return new MetaDataPK("com.kingdee.eas.custom.tiog.bizbill.app.PoliticalSystemQuery");
	}
    

    /**
     * output getDetailTable method
     */
    protected KDTable getDetailTable() {
        return kdtEntrys;
	}
    /**
     * output applyDefaultValue method
     */
    protected void applyDefaultValue(IObjectValue vo) {        
		vo.put("state","1");
        
    }        
	protected void setFieldsNull(com.kingdee.bos.dao.AbstractObjectValue arg0) {
		super.setFieldsNull(arg0);
		arg0.put("number",null);
	}

}