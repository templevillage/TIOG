/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.bizbill.client;

import org.apache.log4j.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.BorderFactory;
import javax.swing.event.*;
import javax.swing.KeyStroke;

import com.kingdee.bos.ctrl.swing.*;
import com.kingdee.bos.ctrl.kdf.table.*;
import com.kingdee.bos.ctrl.kdf.data.event.*;
import com.kingdee.bos.dao.*;
import com.kingdee.bos.dao.query.*;
import com.kingdee.bos.metadata.*;
import com.kingdee.bos.metadata.entity.*;
import com.kingdee.bos.ui.face.*;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.enums.EnumUtils;
import com.kingdee.bos.ui.face.UIRuleUtil;
import com.kingdee.bos.ctrl.swing.event.*;
import com.kingdee.bos.ctrl.kdf.table.event.*;
import com.kingdee.bos.ctrl.extendcontrols.*;
import com.kingdee.bos.ctrl.kdf.util.render.*;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.uistatemanage.ActionStateConst;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.appframework.uip.UINavigator;


/**
 * output class name
 */
public abstract class AbstractCategoryListEditUI extends com.kingdee.eas.framework.client.CoreBillEditUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractCategoryListEditUI.class);
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contCreator;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contCreateTime;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contLastUpdateUser;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contLastUpdateTime;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contBizDate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contDescription;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contAuditor;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtEntrys;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtEntrys_detailPanel = null;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtDEntrys;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtDEntrys_detailPanel = null;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contadminOrgUnit;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcompany;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcompanyNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcategoryListName;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contlistVersion;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer conteffectiveTS;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer continvalidTS;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contremark;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDLabel1;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton supportAtt;
    protected com.kingdee.bos.ctrl.swing.KDSeparator kDSeparator8;
    protected com.kingdee.bos.ctrl.swing.KDCheckBox chkisUpload;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contstate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contsource;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contoperType;
    protected com.kingdee.bos.ctrl.swing.KDLabel kDLabel3;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtCreator;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker kDDateCreateTime;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtLastUpdateUser;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker kDDateLastUpdateTime;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtNumber;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkBizDate;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtDescription;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtAuditor;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtadminOrgUnit;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtcompany;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtcompanyNumber;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtcategoryListName;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtlistVersion;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkeffectiveTS;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkinvalidTS;
    protected com.kingdee.bos.ctrl.swing.KDScrollPane scrollPaneremark;
    protected com.kingdee.bos.ctrl.swing.KDTextArea txtremark;
    protected com.kingdee.bos.ctrl.swing.KDComboBox state;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtsource;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtoperType;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton genSubject;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton audit;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton unAudit;
    protected com.kingdee.bos.ctrl.swing.KDMenu bizMenu;
    protected com.kingdee.bos.ctrl.swing.KDMenuItem mniActionAudit;
    protected com.kingdee.bos.ctrl.swing.KDMenuItem mniActionUnAudit;
    protected com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo editData = null;
    protected ActionAudit actionAudit = null;
    protected ActionUnAudit actionUnAudit = null;
    protected ActionGenSubject actionGenSubject = null;
    /**
     * output class constructor
     */
    public AbstractCategoryListEditUI() throws Exception
    {
        super();
        this.defaultObjectName = "editData";
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractCategoryListEditUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        //actionSubmit
        String _tempStr = null;
        actionSubmit.setEnabled(true);
        actionSubmit.setDaemonRun(false);

        actionSubmit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl S"));
        _tempStr = resHelper.getString("ActionSubmit.SHORT_DESCRIPTION");
        actionSubmit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.LONG_DESCRIPTION");
        actionSubmit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.NAME");
        actionSubmit.putValue(ItemAction.NAME, _tempStr);
        this.actionSubmit.setExtendProperty("canForewarn", "true");
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.ForewarnService());
        //actionPrint
        actionPrint.setEnabled(true);
        actionPrint.setDaemonRun(false);

        actionPrint.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl P"));
        _tempStr = resHelper.getString("ActionPrint.SHORT_DESCRIPTION");
        actionPrint.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.LONG_DESCRIPTION");
        actionPrint.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.NAME");
        actionPrint.putValue(ItemAction.NAME, _tempStr);
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrintPreview
        actionPrintPreview.setEnabled(true);
        actionPrintPreview.setDaemonRun(false);

        actionPrintPreview.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift ctrl P"));
        _tempStr = resHelper.getString("ActionPrintPreview.SHORT_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.LONG_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.NAME");
        actionPrintPreview.putValue(ItemAction.NAME, _tempStr);
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionAudit
        this.actionAudit = new ActionAudit(this);
        getActionManager().registerAction("actionAudit", actionAudit);
        this.actionAudit.setBindWorkFlow(true);
        this.actionAudit.setExtendProperty("canForewarn", "true");
        this.actionAudit.setExtendProperty("userDefined", "true");
        this.actionAudit.setExtendProperty("isObjectUpdateLock", "false");
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.ForewarnService());
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.WorkFlowService());
        //actionUnAudit
        this.actionUnAudit = new ActionUnAudit(this);
        getActionManager().registerAction("actionUnAudit", actionUnAudit);
        this.actionUnAudit.setBindWorkFlow(true);
        this.actionUnAudit.setExtendProperty("canForewarn", "true");
        this.actionUnAudit.setExtendProperty("userDefined", "true");
        this.actionUnAudit.setExtendProperty("isObjectUpdateLock", "false");
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.ForewarnService());
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.WorkFlowService());
        //actionGenSubject
        this.actionGenSubject = new ActionGenSubject(this);
        getActionManager().registerAction("actionGenSubject", actionGenSubject);
         this.actionGenSubject.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        this.contCreator = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contCreateTime = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contLastUpdateUser = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contLastUpdateTime = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contBizDate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contDescription = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contAuditor = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kdtEntrys = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.kdtDEntrys = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.contadminOrgUnit = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contcompany = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contcompanyNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contcategoryListName = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contlistVersion = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.conteffectiveTS = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.continvalidTS = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contremark = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDLabel1 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.supportAtt = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDSeparator8 = new com.kingdee.bos.ctrl.swing.KDSeparator();
        this.chkisUpload = new com.kingdee.bos.ctrl.swing.KDCheckBox();
        this.contstate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contsource = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contoperType = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDLabel3 = new com.kingdee.bos.ctrl.swing.KDLabel();
        this.prmtCreator = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.kDDateCreateTime = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.prmtLastUpdateUser = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.kDDateLastUpdateTime = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.txtNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.pkBizDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.txtDescription = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtAuditor = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtadminOrgUnit = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtcompany = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.txtcompanyNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtcategoryListName = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtlistVersion = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.pkeffectiveTS = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.pkinvalidTS = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.scrollPaneremark = new com.kingdee.bos.ctrl.swing.KDScrollPane();
        this.txtremark = new com.kingdee.bos.ctrl.swing.KDTextArea();
        this.state = new com.kingdee.bos.ctrl.swing.KDComboBox();
        this.prmtsource = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtoperType = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.genSubject = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.audit = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.unAudit = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.bizMenu = new com.kingdee.bos.ctrl.swing.KDMenu();
        this.mniActionAudit = new com.kingdee.bos.ctrl.swing.KDMenuItem();
        this.mniActionUnAudit = new com.kingdee.bos.ctrl.swing.KDMenuItem();
        this.contCreator.setName("contCreator");
        this.contCreateTime.setName("contCreateTime");
        this.contLastUpdateUser.setName("contLastUpdateUser");
        this.contLastUpdateTime.setName("contLastUpdateTime");
        this.contNumber.setName("contNumber");
        this.contBizDate.setName("contBizDate");
        this.contDescription.setName("contDescription");
        this.contAuditor.setName("contAuditor");
        this.kdtEntrys.setName("kdtEntrys");
        this.kdtDEntrys.setName("kdtDEntrys");
        this.contadminOrgUnit.setName("contadminOrgUnit");
        this.contcompany.setName("contcompany");
        this.contcompanyNumber.setName("contcompanyNumber");
        this.contcategoryListName.setName("contcategoryListName");
        this.contlistVersion.setName("contlistVersion");
        this.conteffectiveTS.setName("conteffectiveTS");
        this.continvalidTS.setName("continvalidTS");
        this.contremark.setName("contremark");
        this.kDLabel1.setName("kDLabel1");
        this.supportAtt.setName("supportAtt");
        this.kDSeparator8.setName("kDSeparator8");
        this.chkisUpload.setName("chkisUpload");
        this.contstate.setName("contstate");
        this.contsource.setName("contsource");
        this.contoperType.setName("contoperType");
        this.kDLabel3.setName("kDLabel3");
        this.prmtCreator.setName("prmtCreator");
        this.kDDateCreateTime.setName("kDDateCreateTime");
        this.prmtLastUpdateUser.setName("prmtLastUpdateUser");
        this.kDDateLastUpdateTime.setName("kDDateLastUpdateTime");
        this.txtNumber.setName("txtNumber");
        this.pkBizDate.setName("pkBizDate");
        this.txtDescription.setName("txtDescription");
        this.prmtAuditor.setName("prmtAuditor");
        this.prmtadminOrgUnit.setName("prmtadminOrgUnit");
        this.prmtcompany.setName("prmtcompany");
        this.txtcompanyNumber.setName("txtcompanyNumber");
        this.txtcategoryListName.setName("txtcategoryListName");
        this.txtlistVersion.setName("txtlistVersion");
        this.pkeffectiveTS.setName("pkeffectiveTS");
        this.pkinvalidTS.setName("pkinvalidTS");
        this.scrollPaneremark.setName("scrollPaneremark");
        this.txtremark.setName("txtremark");
        this.state.setName("state");
        this.prmtsource.setName("prmtsource");
        this.prmtoperType.setName("prmtoperType");
        this.genSubject.setName("genSubject");
        this.audit.setName("audit");
        this.unAudit.setName("unAudit");
        this.bizMenu.setName("bizMenu");
        this.mniActionAudit.setName("mniActionAudit");
        this.mniActionUnAudit.setName("mniActionUnAudit");
        // CoreUI		
        this.btnTraceUp.setVisible(false);		
        this.btnTraceDown.setVisible(false);		
        this.btnCreateTo.setVisible(true);		
        this.btnAddLine.setVisible(false);		
        this.btnCopyLine.setVisible(false);		
        this.btnInsertLine.setVisible(false);		
        this.btnRemoveLine.setVisible(false);		
        this.btnAuditResult.setVisible(false);		
        this.separator1.setVisible(false);		
        this.menuItemCreateTo.setVisible(true);		
        this.separator3.setVisible(false);		
        this.menuItemTraceUp.setVisible(false);		
        this.menuItemTraceDown.setVisible(false);		
        this.menuTable1.setVisible(false);		
        this.menuItemAddLine.setVisible(false);		
        this.menuItemCopyLine.setVisible(false);		
        this.menuItemInsertLine.setVisible(false);		
        this.menuItemRemoveLine.setVisible(false);		
        this.menuItemViewSubmitProccess.setVisible(false);		
        this.menuItemViewDoProccess.setVisible(false);		
        this.menuItemAuditResult.setVisible(false);
        // contCreator		
        this.contCreator.setBoundLabelText(resHelper.getString("contCreator.boundLabelText"));		
        this.contCreator.setBoundLabelLength(100);		
        this.contCreator.setBoundLabelUnderline(true);		
        this.contCreator.setEnabled(false);
        // contCreateTime		
        this.contCreateTime.setBoundLabelText(resHelper.getString("contCreateTime.boundLabelText"));		
        this.contCreateTime.setBoundLabelLength(100);		
        this.contCreateTime.setBoundLabelUnderline(true);		
        this.contCreateTime.setEnabled(false);
        // contLastUpdateUser		
        this.contLastUpdateUser.setBoundLabelText(resHelper.getString("contLastUpdateUser.boundLabelText"));		
        this.contLastUpdateUser.setBoundLabelLength(100);		
        this.contLastUpdateUser.setBoundLabelUnderline(true);		
        this.contLastUpdateUser.setEnabled(false);		
        this.contLastUpdateUser.setVisible(false);
        // contLastUpdateTime		
        this.contLastUpdateTime.setBoundLabelText(resHelper.getString("contLastUpdateTime.boundLabelText"));		
        this.contLastUpdateTime.setBoundLabelLength(100);		
        this.contLastUpdateTime.setBoundLabelUnderline(true);		
        this.contLastUpdateTime.setEnabled(false);		
        this.contLastUpdateTime.setVisible(false);
        // contNumber		
        this.contNumber.setBoundLabelText(resHelper.getString("contNumber.boundLabelText"));		
        this.contNumber.setBoundLabelLength(100);		
        this.contNumber.setBoundLabelUnderline(true);
        // contBizDate		
        this.contBizDate.setBoundLabelText(resHelper.getString("contBizDate.boundLabelText"));		
        this.contBizDate.setBoundLabelLength(100);		
        this.contBizDate.setBoundLabelUnderline(true);		
        this.contBizDate.setBoundLabelAlignment(7);		
        this.contBizDate.setVisible(true);
        // contDescription		
        this.contDescription.setBoundLabelText(resHelper.getString("contDescription.boundLabelText"));		
        this.contDescription.setBoundLabelLength(100);		
        this.contDescription.setBoundLabelUnderline(true);
        // contAuditor		
        this.contAuditor.setBoundLabelText(resHelper.getString("contAuditor.boundLabelText"));		
        this.contAuditor.setBoundLabelLength(100);		
        this.contAuditor.setBoundLabelUnderline(true);
        // kdtEntrys
		String kdtEntrysStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol3\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol4\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol5\"><c:Protection locked=\"true\" hidden=\"true\" /></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"id\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"isSelected\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"1\" /><t:Column t:key=\"catalogMain\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"2\" /><t:Column t:key=\"catalogMainNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"3\" t:styleID=\"sCol3\" /><t:Column t:key=\"catalog\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"4\" t:styleID=\"sCol4\" /><t:Column t:key=\"catalogNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"5\" t:styleID=\"sCol5\" /><t:Column t:key=\"legalFlag\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"6\" /><t:Column t:key=\"dataSource\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"7\" /><t:Column t:key=\"operType\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"8\" /><t:Column t:key=\"remark\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"9\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{id}</t:Cell><t:Cell>$Resource{isSelected}</t:Cell><t:Cell>$Resource{catalogMain}</t:Cell><t:Cell>$Resource{catalogMainNumber}</t:Cell><t:Cell>$Resource{catalog}</t:Cell><t:Cell>$Resource{catalogNumber}</t:Cell><t:Cell>$Resource{legalFlag}</t:Cell><t:Cell>$Resource{dataSource}</t:Cell><t:Cell>$Resource{operType}</t:Cell><t:Cell>$Resource{remark}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtEntrys.setFormatXml(resHelper.translateString("kdtEntrys",kdtEntrysStrXML));
        kdtEntrys.addKDTEditListener(new KDTEditAdapter() {
		public void editStopped(KDTEditEvent e) {
			try {
				kdtEntrys_Changed(e.getRowIndex(),e.getColIndex());
			}
			catch (Exception exc) {
				handUIException(exc);
			}
		}
	});

        this.kdtEntrys.addKDTSelectListener(new com.kingdee.bos.ctrl.kdf.table.event.KDTSelectListener() {
            public void tableSelectChanged(com.kingdee.bos.ctrl.kdf.table.event.KDTSelectEvent e) {
                try {
                    kdtEntrys_tableSelectChanged(e);
                } catch (Exception exc) {
                    handUIException(exc);
                } finally {
                }
            }
        });

                this.kdtEntrys.putBindContents("editData",new String[] {"id","","catalogMain","catalogMainNumber","catalog","catalogNumber","legalFlag","dataSource","operType","remark"});


        this.kdtEntrys.checkParsed();
        final KDBizPromptBox kdtEntrys_catalogMain_PromptBox = new KDBizPromptBox();
        kdtEntrys_catalogMain_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.CatalogMainQuery");
        kdtEntrys_catalogMain_PromptBox.setVisible(true);
        kdtEntrys_catalogMain_PromptBox.setEditable(true);
        kdtEntrys_catalogMain_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_catalogMain_PromptBox.setEditFormat("$number$");
        kdtEntrys_catalogMain_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_catalogMain_CellEditor = new KDTDefaultCellEditor(kdtEntrys_catalogMain_PromptBox);
        this.kdtEntrys.getColumn("catalogMain").setEditor(kdtEntrys_catalogMain_CellEditor);
        ObjectValueRender kdtEntrys_catalogMain_OVR = new ObjectValueRender();
        kdtEntrys_catalogMain_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("catalogMain").setRenderer(kdtEntrys_catalogMain_OVR);
        			EntityViewInfo evikdtEntrys_catalogMain_PromptBox = new EntityViewInfo ();
		evikdtEntrys_catalogMain_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtEntrys_catalogMain_PromptBox.setEntityViewInfo(evikdtEntrys_catalogMain_PromptBox);
					
        			kdtEntrys_catalogMain_PromptBox.addSelectorListener(new SelectorListener() {
			com.kingdee.eas.custom.tiog.basedata.client.CatalogMainListUI kdtEntrys_catalogMain_PromptBox_F7ListUI = null;
			public void willShow(SelectorEvent e) {
				if (kdtEntrys_catalogMain_PromptBox_F7ListUI == null) {
					try {
						kdtEntrys_catalogMain_PromptBox_F7ListUI = new com.kingdee.eas.custom.tiog.basedata.client.CatalogMainListUI();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					HashMap ctx = new HashMap();
					ctx.put("bizUIOwner",javax.swing.SwingUtilities.getWindowAncestor(kdtEntrys_catalogMain_PromptBox_F7ListUI));
					kdtEntrys_catalogMain_PromptBox_F7ListUI.setF7Use(true,ctx);
					kdtEntrys_catalogMain_PromptBox.setSelector(kdtEntrys_catalogMain_PromptBox_F7ListUI);
				}
			}
		});
					
        KDTextField kdtEntrys_catalogMainNumber_TextField = new KDTextField();
        kdtEntrys_catalogMainNumber_TextField.setName("kdtEntrys_catalogMainNumber_TextField");
        kdtEntrys_catalogMainNumber_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtEntrys_catalogMainNumber_CellEditor = new KDTDefaultCellEditor(kdtEntrys_catalogMainNumber_TextField);
        this.kdtEntrys.getColumn("catalogMainNumber").setEditor(kdtEntrys_catalogMainNumber_CellEditor);
        final KDBizPromptBox kdtEntrys_catalog_PromptBox = new KDBizPromptBox();
        kdtEntrys_catalog_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.CatalogQuery");
        kdtEntrys_catalog_PromptBox.setVisible(true);
        kdtEntrys_catalog_PromptBox.setEditable(true);
        kdtEntrys_catalog_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_catalog_PromptBox.setEditFormat("$number$");
        kdtEntrys_catalog_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_catalog_CellEditor = new KDTDefaultCellEditor(kdtEntrys_catalog_PromptBox);
        this.kdtEntrys.getColumn("catalog").setEditor(kdtEntrys_catalog_CellEditor);
        ObjectValueRender kdtEntrys_catalog_OVR = new ObjectValueRender();
        kdtEntrys_catalog_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("catalog").setRenderer(kdtEntrys_catalog_OVR);
        			EntityViewInfo evikdtEntrys_catalog_PromptBox = new EntityViewInfo ();
		evikdtEntrys_catalog_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtEntrys_catalog_PromptBox.setEntityViewInfo(evikdtEntrys_catalog_PromptBox);
					
        			kdtEntrys_catalog_PromptBox.addSelectorListener(new SelectorListener() {
			com.kingdee.eas.custom.tiog.basedata.client.CatalogListUI kdtEntrys_catalog_PromptBox_F7ListUI = null;
			public void willShow(SelectorEvent e) {
				if (kdtEntrys_catalog_PromptBox_F7ListUI == null) {
					try {
						kdtEntrys_catalog_PromptBox_F7ListUI = new com.kingdee.eas.custom.tiog.basedata.client.CatalogListUI();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					HashMap ctx = new HashMap();
					ctx.put("bizUIOwner",javax.swing.SwingUtilities.getWindowAncestor(kdtEntrys_catalog_PromptBox_F7ListUI));
					kdtEntrys_catalog_PromptBox_F7ListUI.setF7Use(true,ctx);
					kdtEntrys_catalog_PromptBox.setSelector(kdtEntrys_catalog_PromptBox_F7ListUI);
				}
			}
		});
					
        KDTextField kdtEntrys_catalogNumber_TextField = new KDTextField();
        kdtEntrys_catalogNumber_TextField.setName("kdtEntrys_catalogNumber_TextField");
        kdtEntrys_catalogNumber_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtEntrys_catalogNumber_CellEditor = new KDTDefaultCellEditor(kdtEntrys_catalogNumber_TextField);
        this.kdtEntrys.getColumn("catalogNumber").setEditor(kdtEntrys_catalogNumber_CellEditor);
        KDCheckBox kdtEntrys_legalFlag_CheckBox = new KDCheckBox();
        kdtEntrys_legalFlag_CheckBox.setName("kdtEntrys_legalFlag_CheckBox");
        KDTDefaultCellEditor kdtEntrys_legalFlag_CellEditor = new KDTDefaultCellEditor(kdtEntrys_legalFlag_CheckBox);
        this.kdtEntrys.getColumn("legalFlag").setEditor(kdtEntrys_legalFlag_CellEditor);
        final KDBizPromptBox kdtEntrys_dataSource_PromptBox = new KDBizPromptBox();
        kdtEntrys_dataSource_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.DataSourceQuery");
        kdtEntrys_dataSource_PromptBox.setVisible(true);
        kdtEntrys_dataSource_PromptBox.setEditable(true);
        kdtEntrys_dataSource_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_dataSource_PromptBox.setEditFormat("$number$");
        kdtEntrys_dataSource_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_dataSource_CellEditor = new KDTDefaultCellEditor(kdtEntrys_dataSource_PromptBox);
        this.kdtEntrys.getColumn("dataSource").setEditor(kdtEntrys_dataSource_CellEditor);
        ObjectValueRender kdtEntrys_dataSource_OVR = new ObjectValueRender();
        kdtEntrys_dataSource_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("dataSource").setRenderer(kdtEntrys_dataSource_OVR);
        			EntityViewInfo evikdtEntrys_dataSource_PromptBox = new EntityViewInfo ();
		evikdtEntrys_dataSource_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtEntrys_dataSource_PromptBox.setEntityViewInfo(evikdtEntrys_dataSource_PromptBox);
					
        final KDBizPromptBox kdtEntrys_operType_PromptBox = new KDBizPromptBox();
        kdtEntrys_operType_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.OperateFlagQuery");
        kdtEntrys_operType_PromptBox.setVisible(true);
        kdtEntrys_operType_PromptBox.setEditable(true);
        kdtEntrys_operType_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_operType_PromptBox.setEditFormat("$number$");
        kdtEntrys_operType_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_operType_CellEditor = new KDTDefaultCellEditor(kdtEntrys_operType_PromptBox);
        this.kdtEntrys.getColumn("operType").setEditor(kdtEntrys_operType_CellEditor);
        ObjectValueRender kdtEntrys_operType_OVR = new ObjectValueRender();
        kdtEntrys_operType_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("operType").setRenderer(kdtEntrys_operType_OVR);
        			EntityViewInfo evikdtEntrys_operType_PromptBox = new EntityViewInfo ();
		evikdtEntrys_operType_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtEntrys_operType_PromptBox.setEntityViewInfo(evikdtEntrys_operType_PromptBox);
					
        KDTextArea kdtEntrys_remark_TextArea = new KDTextArea();
        kdtEntrys_remark_TextArea.setName("kdtEntrys_remark_TextArea");
        kdtEntrys_remark_TextArea.setMaxLength(2000);
        KDTDefaultCellEditor kdtEntrys_remark_CellEditor = new KDTDefaultCellEditor(kdtEntrys_remark_TextArea);
        this.kdtEntrys.getColumn("remark").setEditor(kdtEntrys_remark_CellEditor);
        // kdtDEntrys
		String kdtDEntrysStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol3\"><c:Protection locked=\"true\" /></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"id\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"isSelected\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"1\" /><t:Column t:key=\"meetingType\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"2\" /><t:Column t:key=\"meetingTypeNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"3\" t:styleID=\"sCol3\" /><t:Column t:key=\"meetingDes\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"4\" /><t:Column t:key=\"remark\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"5\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{id}</t:Cell><t:Cell>$Resource{isSelected}</t:Cell><t:Cell>$Resource{meetingType}</t:Cell><t:Cell>$Resource{meetingTypeNumber}</t:Cell><t:Cell>$Resource{meetingDes}</t:Cell><t:Cell>$Resource{remark}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtDEntrys.setFormatXml(resHelper.translateString("kdtDEntrys",kdtDEntrysStrXML));
        kdtDEntrys.addKDTEditListener(new KDTEditAdapter() {
		public void editStopped(KDTEditEvent e) {
			try {
				kdtDEntrys_Changed(e.getRowIndex(),e.getColIndex());
			}
			catch (Exception exc) {
				handUIException(exc);
			}
		}
	});

        this.kdtDEntrys.addKDTEditListener(new com.kingdee.bos.ctrl.kdf.table.event.KDTEditAdapter() {
            public void editValueChanged(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) {
                try {
                    kdtDEntrys_editValueChanged(e);
                } catch(Exception exc) {
                    handUIException(exc);
                }
            }
        });

                this.kdtDEntrys.putBindContents("editData",new String[] {"DEntrys.id","","DEntrys.meetingType","DEntrys.meetingTypeNumber","DEntrys.meetingDes","DEntrys.remark"});


        this.kdtDEntrys.checkParsed();
        final KDBizPromptBox kdtDEntrys_meetingType_PromptBox = new KDBizPromptBox();
        kdtDEntrys_meetingType_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.MeetingTypeQuery");
        kdtDEntrys_meetingType_PromptBox.setVisible(true);
        kdtDEntrys_meetingType_PromptBox.setEditable(true);
        kdtDEntrys_meetingType_PromptBox.setDisplayFormat("$number$");
        kdtDEntrys_meetingType_PromptBox.setEditFormat("$number$");
        kdtDEntrys_meetingType_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtDEntrys_meetingType_CellEditor = new KDTDefaultCellEditor(kdtDEntrys_meetingType_PromptBox);
        this.kdtDEntrys.getColumn("meetingType").setEditor(kdtDEntrys_meetingType_CellEditor);
        ObjectValueRender kdtDEntrys_meetingType_OVR = new ObjectValueRender();
        kdtDEntrys_meetingType_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtDEntrys.getColumn("meetingType").setRenderer(kdtDEntrys_meetingType_OVR);
        			EntityViewInfo evikdtDEntrys_meetingType_PromptBox = new EntityViewInfo ();
		evikdtDEntrys_meetingType_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtDEntrys_meetingType_PromptBox.setEntityViewInfo(evikdtDEntrys_meetingType_PromptBox);
					
        KDTextField kdtDEntrys_meetingTypeNumber_TextField = new KDTextField();
        kdtDEntrys_meetingTypeNumber_TextField.setName("kdtDEntrys_meetingTypeNumber_TextField");
        kdtDEntrys_meetingTypeNumber_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtDEntrys_meetingTypeNumber_CellEditor = new KDTDefaultCellEditor(kdtDEntrys_meetingTypeNumber_TextField);
        this.kdtDEntrys.getColumn("meetingTypeNumber").setEditor(kdtDEntrys_meetingTypeNumber_CellEditor);
        KDTextField kdtDEntrys_meetingDes_TextField = new KDTextField();
        kdtDEntrys_meetingDes_TextField.setName("kdtDEntrys_meetingDes_TextField");
        kdtDEntrys_meetingDes_TextField.setMaxLength(255);
        KDTDefaultCellEditor kdtDEntrys_meetingDes_CellEditor = new KDTDefaultCellEditor(kdtDEntrys_meetingDes_TextField);
        this.kdtDEntrys.getColumn("meetingDes").setEditor(kdtDEntrys_meetingDes_CellEditor);
        KDTextArea kdtDEntrys_remark_TextArea = new KDTextArea();
        kdtDEntrys_remark_TextArea.setName("kdtDEntrys_remark_TextArea");
        kdtDEntrys_remark_TextArea.setMaxLength(2000);
        KDTDefaultCellEditor kdtDEntrys_remark_CellEditor = new KDTDefaultCellEditor(kdtDEntrys_remark_TextArea);
        this.kdtDEntrys.getColumn("remark").setEditor(kdtDEntrys_remark_CellEditor);
        // contadminOrgUnit		
        this.contadminOrgUnit.setBoundLabelText(resHelper.getString("contadminOrgUnit.boundLabelText"));		
        this.contadminOrgUnit.setBoundLabelLength(100);		
        this.contadminOrgUnit.setBoundLabelUnderline(true);		
        this.contadminOrgUnit.setVisible(true);
        // contcompany		
        this.contcompany.setBoundLabelText(resHelper.getString("contcompany.boundLabelText"));		
        this.contcompany.setBoundLabelLength(100);		
        this.contcompany.setBoundLabelUnderline(true);		
        this.contcompany.setVisible(true);
        // contcompanyNumber		
        this.contcompanyNumber.setBoundLabelText(resHelper.getString("contcompanyNumber.boundLabelText"));		
        this.contcompanyNumber.setBoundLabelLength(100);		
        this.contcompanyNumber.setBoundLabelUnderline(true);		
        this.contcompanyNumber.setVisible(true);
        // contcategoryListName		
        this.contcategoryListName.setBoundLabelText(resHelper.getString("contcategoryListName.boundLabelText"));		
        this.contcategoryListName.setBoundLabelLength(100);		
        this.contcategoryListName.setBoundLabelUnderline(true);		
        this.contcategoryListName.setVisible(true);
        // contlistVersion		
        this.contlistVersion.setBoundLabelText(resHelper.getString("contlistVersion.boundLabelText"));		
        this.contlistVersion.setBoundLabelLength(100);		
        this.contlistVersion.setBoundLabelUnderline(true);		
        this.contlistVersion.setVisible(true);
        // conteffectiveTS		
        this.conteffectiveTS.setBoundLabelText(resHelper.getString("conteffectiveTS.boundLabelText"));		
        this.conteffectiveTS.setBoundLabelLength(100);		
        this.conteffectiveTS.setBoundLabelUnderline(true);		
        this.conteffectiveTS.setVisible(true);
        // continvalidTS		
        this.continvalidTS.setBoundLabelText(resHelper.getString("continvalidTS.boundLabelText"));		
        this.continvalidTS.setBoundLabelLength(100);		
        this.continvalidTS.setBoundLabelUnderline(true);		
        this.continvalidTS.setVisible(true);
        // contremark		
        this.contremark.setBoundLabelText(resHelper.getString("contremark.boundLabelText"));		
        this.contremark.setBoundLabelLength(100);		
        this.contremark.setBoundLabelUnderline(true);		
        this.contremark.setVisible(true);
        // kDLabel1		
        this.kDLabel1.setText(resHelper.getString("kDLabel1.text"));
        // supportAtt		
        this.supportAtt.setText(resHelper.getString("supportAtt.text"));
        // kDSeparator8
        // chkisUpload		
        this.chkisUpload.setText(resHelper.getString("chkisUpload.text"));		
        this.chkisUpload.setVisible(false);		
        this.chkisUpload.setHorizontalAlignment(2);		
        this.chkisUpload.setEnabled(false);
        // contstate		
        this.contstate.setBoundLabelText(resHelper.getString("contstate.boundLabelText"));		
        this.contstate.setBoundLabelLength(100);		
        this.contstate.setBoundLabelUnderline(true);		
        this.contstate.setVisible(false);
        // contsource		
        this.contsource.setBoundLabelText(resHelper.getString("contsource.boundLabelText"));		
        this.contsource.setBoundLabelLength(100);		
        this.contsource.setBoundLabelUnderline(true);		
        this.contsource.setVisible(true);
        // contoperType		
        this.contoperType.setBoundLabelText(resHelper.getString("contoperType.boundLabelText"));		
        this.contoperType.setBoundLabelLength(100);		
        this.contoperType.setBoundLabelUnderline(true);		
        this.contoperType.setVisible(true);
        // kDLabel3		
        this.kDLabel3.setText(resHelper.getString("kDLabel3.text"));
        // prmtCreator		
        this.prmtCreator.setEnabled(false);
        // kDDateCreateTime		
        this.kDDateCreateTime.setTimeEnabled(true);		
        this.kDDateCreateTime.setEnabled(false);
        // prmtLastUpdateUser		
        this.prmtLastUpdateUser.setEnabled(false);
        // kDDateLastUpdateTime		
        this.kDDateLastUpdateTime.setTimeEnabled(true);		
        this.kDDateLastUpdateTime.setEnabled(false);
        // txtNumber		
        this.txtNumber.setMaxLength(80);
        // pkBizDate		
        this.pkBizDate.setVisible(true);		
        this.pkBizDate.setEnabled(true);
        // txtDescription		
        this.txtDescription.setMaxLength(80);
        // prmtAuditor		
        this.prmtAuditor.setEnabled(false);
        // prmtadminOrgUnit		
        this.prmtadminOrgUnit.setQueryInfo("com.kingdee.eas.basedata.org.app.AdminItemQuery");		
        this.prmtadminOrgUnit.setVisible(true);		
        this.prmtadminOrgUnit.setEditable(true);		
        this.prmtadminOrgUnit.setDisplayFormat("$name$");		
        this.prmtadminOrgUnit.setEditFormat("$number$");		
        this.prmtadminOrgUnit.setCommitFormat("$number$");		
        this.prmtadminOrgUnit.setRequired(true);
        		setOrgF7(prmtadminOrgUnit,com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"));
					
        prmtadminOrgUnit.addDataChangeListener(new DataChangeListener() {
		public void dataChanged(DataChangeEvent e) {
			try {
				prmtadminOrgUnit_Changed();
			}
			catch (Exception exc) {
				handUIException(exc);
			}
		}
	});

        // prmtcompany		
        this.prmtcompany.setQueryInfo("com.kingdee.eas.basedata.org.app.CompanyOrgUnitQuery");		
        this.prmtcompany.setVisible(true);		
        this.prmtcompany.setEditable(true);		
        this.prmtcompany.setDisplayFormat("$name$");		
        this.prmtcompany.setEditFormat("$number$");		
        this.prmtcompany.setCommitFormat("$number$");		
        this.prmtcompany.setRequired(false);
        // txtcompanyNumber		
        this.txtcompanyNumber.setVisible(true);		
        this.txtcompanyNumber.setHorizontalAlignment(2);		
        this.txtcompanyNumber.setMaxLength(80);		
        this.txtcompanyNumber.setRequired(false);
        // txtcategoryListName		
        this.txtcategoryListName.setVisible(true);		
        this.txtcategoryListName.setHorizontalAlignment(2);		
        this.txtcategoryListName.setMaxLength(255);		
        this.txtcategoryListName.setRequired(false);
        // txtlistVersion		
        this.txtlistVersion.setVisible(true);		
        this.txtlistVersion.setHorizontalAlignment(2);		
        this.txtlistVersion.setMaxLength(100);		
        this.txtlistVersion.setRequired(false);
        // pkeffectiveTS		
        this.pkeffectiveTS.setVisible(true);		
        this.pkeffectiveTS.setRequired(false);		
        this.pkeffectiveTS.setMilliSecondEnable(true);		
        this.pkeffectiveTS.setTimeEnabled(true);
        // pkinvalidTS		
        this.pkinvalidTS.setVisible(true);		
        this.pkinvalidTS.setRequired(false);
        // scrollPaneremark
        // txtremark		
        this.txtremark.setVisible(true);		
        this.txtremark.setRequired(false);		
        this.txtremark.setMaxLength(2550);
        // state		
        this.state.setVisible(false);		
        this.state.addItems(EnumUtils.getEnumList("com.kingdee.eas.custom.tiog.bizbill.BillState").toArray());		
        this.state.setRequired(false);		
        this.state.setEnabled(false);
        // prmtsource		
        this.prmtsource.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.DataSourceQuery");		
        this.prmtsource.setVisible(true);		
        this.prmtsource.setEditable(true);		
        this.prmtsource.setDisplayFormat("$name$");		
        this.prmtsource.setEditFormat("$number$");		
        this.prmtsource.setCommitFormat("$number$");		
        this.prmtsource.setRequired(false);
        		EntityViewInfo eviprmtsource = new EntityViewInfo ();
		eviprmtsource.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtsource.setEntityViewInfo(eviprmtsource);
					
        // prmtoperType		
        this.prmtoperType.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.OperateFlagQuery");		
        this.prmtoperType.setVisible(true);		
        this.prmtoperType.setEditable(true);		
        this.prmtoperType.setDisplayFormat("$name$");		
        this.prmtoperType.setEditFormat("$number$");		
        this.prmtoperType.setCommitFormat("$number$");		
        this.prmtoperType.setRequired(false);
        		EntityViewInfo eviprmtoperType = new EntityViewInfo ();
		eviprmtoperType.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtoperType.setEntityViewInfo(eviprmtoperType);
					
        // genSubject
        this.genSubject.setAction((IItemAction)ActionProxyFactory.getProxy(actionGenSubject, new Class[] { IItemAction.class }, getServiceContext()));		
        this.genSubject.setText(resHelper.getString("genSubject.text"));		
        this.genSubject.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("image_item_pen"));
        this.genSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                beforeActionPerformed(e);
                try {
                    genSubject_actionPerformed(e);
                } catch (Exception exc) {
                    handUIException(exc);
                } finally {
                    afterActionPerformed(e);
                }
            }
        });
        // audit
        this.audit.setAction((IItemAction)ActionProxyFactory.getProxy(actionAudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.audit.setText(resHelper.getString("audit.text"));
        // unAudit
        this.unAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionUnAudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.unAudit.setText(resHelper.getString("unAudit.text"));
        // bizMenu		
        this.bizMenu.setText(resHelper.getString("bizMenu.text"));
        // mniActionAudit
        this.mniActionAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionAudit, new Class[] { IItemAction.class }, getServiceContext()));
        // mniActionUnAudit
        this.mniActionUnAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionUnAudit, new Class[] { IItemAction.class }, getServiceContext()));
        this.setFocusTraversalPolicy(new com.kingdee.bos.ui.UIFocusTraversalPolicy(new java.awt.Component[] {prmtcompany,txtcompanyNumber,txtcategoryListName,txtlistVersion,pkeffectiveTS,pkinvalidTS,txtremark,chkisUpload,state,prmtsource,prmtoperType}));
        this.setFocusCycleRoot(true);
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(0, 0, 1013, 989));
        this.setLayout(new KDLayout());
        this.putClientProperty("OriginalBounds", new Rectangle(0, 0, 1013, 989));
        contCreator.setBounds(new Rectangle(416, 919, 270, 19));
        this.add(contCreator, new KDLayout.Constraints(416, 919, 270, 19, 0));
        contCreateTime.setBounds(new Rectangle(706, 919, 270, 19));
        this.add(contCreateTime, new KDLayout.Constraints(706, 919, 270, 19, 0));
        contLastUpdateUser.setBounds(new Rectangle(416, 950, 270, 19));
        this.add(contLastUpdateUser, new KDLayout.Constraints(416, 950, 270, 19, 0));
        contLastUpdateTime.setBounds(new Rectangle(706, 950, 270, 19));
        this.add(contLastUpdateTime, new KDLayout.Constraints(706, 950, 270, 19, 0));
        contNumber.setBounds(new Rectangle(29, 16, 270, 19));
        this.add(contNumber, new KDLayout.Constraints(29, 16, 270, 19, 0));
        contBizDate.setBounds(new Rectangle(697, 56, 270, 19));
        this.add(contBizDate, new KDLayout.Constraints(697, 56, 270, 19, 0));
        contDescription.setBounds(new Rectangle(697, 96, 270, 19));
        this.add(contDescription, new KDLayout.Constraints(697, 96, 270, 19, 0));
        contAuditor.setBounds(new Rectangle(24, 923, 270, 19));
        this.add(contAuditor, new KDLayout.Constraints(24, 923, 270, 19, 0));
        kdtEntrys.setBounds(new Rectangle(29, 279, 961, 242));
        kdtEntrys_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtEntrys,new com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryInfo(),null,false);
        this.add(kdtEntrys_detailPanel, new KDLayout.Constraints(29, 279, 961, 242, 0));
        kdtDEntrys.setBounds(new Rectangle(25, 558, 963, 242));
        kdtDEntrys_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtDEntrys,new com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryDEntryInfo(),null,false);
        this.add(kdtDEntrys_detailPanel, new KDLayout.Constraints(25, 558, 963, 242, 0));
        contadminOrgUnit.setBounds(new Rectangle(697, 16, 270, 19));
        this.add(contadminOrgUnit, new KDLayout.Constraints(697, 16, 270, 19, 0));
        contcompany.setBounds(new Rectangle(356, 16, 270, 19));
        this.add(contcompany, new KDLayout.Constraints(356, 16, 270, 19, 0));
        contcompanyNumber.setBounds(new Rectangle(356, 56, 270, 19));
        this.add(contcompanyNumber, new KDLayout.Constraints(356, 56, 270, 19, 0));
        contcategoryListName.setBounds(new Rectangle(29, 55, 270, 19));
        this.add(contcategoryListName, new KDLayout.Constraints(29, 55, 270, 19, 0));
        contlistVersion.setBounds(new Rectangle(29, 94, 270, 19));
        this.add(contlistVersion, new KDLayout.Constraints(29, 94, 270, 19, 0));
        conteffectiveTS.setBounds(new Rectangle(29, 133, 270, 19));
        this.add(conteffectiveTS, new KDLayout.Constraints(29, 133, 270, 19, 0));
        continvalidTS.setBounds(new Rectangle(29, 175, 270, 19));
        this.add(continvalidTS, new KDLayout.Constraints(29, 175, 270, 19, 0));
        contremark.setBounds(new Rectangle(29, 208, 600, 20));
        this.add(contremark, new KDLayout.Constraints(29, 208, 600, 20, 0));
        kDLabel1.setBounds(new Rectangle(29, 254, 100, 19));
        this.add(kDLabel1, new KDLayout.Constraints(29, 254, 100, 19, 0));
        supportAtt.setBounds(new Rectangle(20, 838, 158, 19));
        this.add(supportAtt, new KDLayout.Constraints(20, 838, 158, 19, 0));
        kDSeparator8.setBounds(new Rectangle(21, 867, 966, 8));
        this.add(kDSeparator8, new KDLayout.Constraints(21, 867, 966, 8, 0));
        chkisUpload.setBounds(new Rectangle(701, 176, 115, 19));
        this.add(chkisUpload, new KDLayout.Constraints(701, 176, 115, 19, 0));
        contstate.setBounds(new Rectangle(697, 136, 270, 19));
        this.add(contstate, new KDLayout.Constraints(697, 136, 270, 19, 0));
        contsource.setBounds(new Rectangle(356, 96, 270, 19));
        this.add(contsource, new KDLayout.Constraints(356, 96, 270, 19, 0));
        contoperType.setBounds(new Rectangle(356, 136, 270, 19));
        this.add(contoperType, new KDLayout.Constraints(356, 136, 270, 19, 0));
        kDLabel3.setBounds(new Rectangle(25, 529, 100, 19));
        this.add(kDLabel3, new KDLayout.Constraints(25, 529, 100, 19, 0));
        //contCreator
        contCreator.setBoundEditor(prmtCreator);
        //contCreateTime
        contCreateTime.setBoundEditor(kDDateCreateTime);
        //contLastUpdateUser
        contLastUpdateUser.setBoundEditor(prmtLastUpdateUser);
        //contLastUpdateTime
        contLastUpdateTime.setBoundEditor(kDDateLastUpdateTime);
        //contNumber
        contNumber.setBoundEditor(txtNumber);
        //contBizDate
        contBizDate.setBoundEditor(pkBizDate);
        //contDescription
        contDescription.setBoundEditor(txtDescription);
        //contAuditor
        contAuditor.setBoundEditor(prmtAuditor);
        //contadminOrgUnit
        contadminOrgUnit.setBoundEditor(prmtadminOrgUnit);
        //contcompany
        contcompany.setBoundEditor(prmtcompany);
        //contcompanyNumber
        contcompanyNumber.setBoundEditor(txtcompanyNumber);
        //contcategoryListName
        contcategoryListName.setBoundEditor(txtcategoryListName);
        //contlistVersion
        contlistVersion.setBoundEditor(txtlistVersion);
        //conteffectiveTS
        conteffectiveTS.setBoundEditor(pkeffectiveTS);
        //continvalidTS
        continvalidTS.setBoundEditor(pkinvalidTS);
        //contremark
        contremark.setBoundEditor(scrollPaneremark);
        //scrollPaneremark
        scrollPaneremark.getViewport().add(txtremark, null);
        //contstate
        contstate.setBoundEditor(state);
        //contsource
        contsource.setBoundEditor(prmtsource);
        //contoperType
        contoperType.setBoundEditor(prmtoperType);

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {
        this.menuBar.add(menuFile);
        this.menuBar.add(bizMenu);
        this.menuBar.add(menuEdit);
        this.menuBar.add(MenuService);
        this.menuBar.add(menuView);
        this.menuBar.add(menuBiz);
        this.menuBar.add(menuTable1);
        this.menuBar.add(menuTool);
        this.menuBar.add(menuWorkflow);
        this.menuBar.add(menuHelp);
        //menuFile
        menuFile.add(menuItemAddNew);
        menuFile.add(kDSeparator1);
        menuFile.add(menuItemCloudFeed);
        menuFile.add(menuItemSave);
        menuFile.add(menuItemCloudScreen);
        menuFile.add(menuItemSubmit);
        menuFile.add(menuItemCloudShare);
        menuFile.add(menuSubmitOption);
        menuFile.add(kdSeparatorFWFile1);
        menuFile.add(rMenuItemSubmit);
        menuFile.add(rMenuItemSubmitAndAddNew);
        menuFile.add(rMenuItemSubmitAndPrint);
        menuFile.add(separatorFile1);
        menuFile.add(MenuItemAttachment);
        menuFile.add(kDSeparator2);
        menuFile.add(menuItemPageSetup);
        menuFile.add(menuItemPrint);
        menuFile.add(menuItemPrintPreview);
        menuFile.add(kDSeparator6);
        menuFile.add(menuItemSendMail);
        menuFile.add(kDSeparator3);
        menuFile.add(menuItemExitCurrent);
        //menuSubmitOption
        menuSubmitOption.add(chkMenuItemSubmitAndAddNew);
        menuSubmitOption.add(chkMenuItemSubmitAndPrint);
        //menuEdit
        menuEdit.add(menuItemCopy);
        menuEdit.add(menuItemEdit);
        menuEdit.add(menuItemRemove);
        menuEdit.add(kDSeparator4);
        menuEdit.add(menuItemReset);
        menuEdit.add(separator1);
        menuEdit.add(menuItemCreateFrom);
        menuEdit.add(menuItemCreateTo);
        menuEdit.add(menuItemCopyFrom);
        menuEdit.add(separatorEdit1);
        menuEdit.add(menuItemEnterToNextRow);
        menuEdit.add(separator2);
        //MenuService
        MenuService.add(MenuItemKnowStore);
        MenuService.add(MenuItemAnwser);
        MenuService.add(SepratorService);
        MenuService.add(MenuItemRemoteAssist);
        //menuView
        menuView.add(menuItemFirst);
        menuView.add(menuItemPre);
        menuView.add(menuItemNext);
        menuView.add(menuItemLast);
        menuView.add(separator3);
        menuView.add(menuItemTraceUp);
        menuView.add(menuItemTraceDown);
        menuView.add(kDSeparator7);
        menuView.add(menuItemLocate);
        //menuBiz
        menuBiz.add(menuItemCancelCancel);
        menuBiz.add(menuItemCancel);
        menuBiz.add(MenuItemVoucher);
        menuBiz.add(menuItemDelVoucher);
        menuBiz.add(MenuItemPCVoucher);
        menuBiz.add(menuItemDelPCVoucher);
        //menuTable1
        menuTable1.add(menuItemAddLine);
        menuTable1.add(menuItemCopyLine);
        menuTable1.add(menuItemInsertLine);
        menuTable1.add(menuItemRemoveLine);
        //menuTool
        menuTool.add(menuItemSendMessage);
        menuTool.add(menuItemMsgFormat);
        menuTool.add(menuItemCalculator);
        menuTool.add(menuItemToolBarCustom);
        //menuWorkflow
        menuWorkflow.add(menuItemStartWorkFlow);
        menuWorkflow.add(separatorWF1);
        menuWorkflow.add(menuItemViewSubmitProccess);
        menuWorkflow.add(menuItemViewDoProccess);
        menuWorkflow.add(MenuItemWFG);
        menuWorkflow.add(menuItemWorkFlowList);
        menuWorkflow.add(separatorWF2);
        menuWorkflow.add(menuItemMultiapprove);
        menuWorkflow.add(menuItemNextPerson);
        menuWorkflow.add(menuItemAuditResult);
        menuWorkflow.add(kDSeparator5);
        menuWorkflow.add(kDMenuItemSendMessage);
        //menuHelp
        menuHelp.add(menuItemHelp);
        menuHelp.add(kDSeparator12);
        menuHelp.add(menuItemRegPro);
        menuHelp.add(menuItemPersonalSite);
        menuHelp.add(helpseparatorDiv);
        menuHelp.add(menuitemProductval);
        menuHelp.add(kDSeparatorProduct);
        menuHelp.add(menuItemAbout);

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {
        this.toolBar.add(btnAddNew);
        this.toolBar.add(btnCloud);
        this.toolBar.add(btnEdit);
        this.toolBar.add(btnXunTong);
        this.toolBar.add(btnSave);
        this.toolBar.add(kDSeparatorCloud);
        this.toolBar.add(btnReset);
        this.toolBar.add(btnSubmit);
        this.toolBar.add(btnCopy);
        this.toolBar.add(btnRemove);
        this.toolBar.add(btnCancelCancel);
        this.toolBar.add(btnCancel);
        this.toolBar.add(btnAttachment);
        this.toolBar.add(separatorFW1);
        this.toolBar.add(btnPageSetup);
        this.toolBar.add(btnPrint);
        this.toolBar.add(btnPrintPreview);
        this.toolBar.add(separatorFW2);
        this.toolBar.add(btnFirst);
        this.toolBar.add(btnPre);
        this.toolBar.add(btnNext);
        this.toolBar.add(btnLast);
        this.toolBar.add(separatorFW3);
        this.toolBar.add(btnTraceUp);
        this.toolBar.add(btnTraceDown);
        this.toolBar.add(btnWorkFlowG);
        this.toolBar.add(btnSignature);
        this.toolBar.add(btnViewSignature);
        this.toolBar.add(separatorFW4);
        this.toolBar.add(btnNumberSign);
        this.toolBar.add(separatorFW7);
        this.toolBar.add(btnCreateFrom);
        this.toolBar.add(btnCopyFrom);
        this.toolBar.add(btnCreateTo);
        this.toolBar.add(separatorFW5);
        this.toolBar.add(separatorFW8);
        this.toolBar.add(btnAddLine);
        this.toolBar.add(btnCopyLine);
        this.toolBar.add(btnInsertLine);
        this.toolBar.add(btnRemoveLine);
        this.toolBar.add(separatorFW6);
        this.toolBar.add(separatorFW9);
        this.toolBar.add(btnVoucher);
        this.toolBar.add(btnDelVoucher);
        this.toolBar.add(btnPCVoucher);
        this.toolBar.add(btnDelPCVoucher);
        this.toolBar.add(btnAuditResult);
        this.toolBar.add(btnMultiapprove);
        this.toolBar.add(btnWFViewdoProccess);
        this.toolBar.add(btnWFViewSubmitProccess);
        this.toolBar.add(btnNextPerson);
        this.toolBar.add(genSubject);
        this.toolBar.add(audit);
        this.toolBar.add(unAudit);


    }

	//Regiester control's property binding.
	private void registerBindings(){
		dataBinder.registerBinding("entrys.id", com.kingdee.bos.util.BOSUuid.class, this.kdtEntrys, "id.text");
		dataBinder.registerBinding("entrys", com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryInfo.class, this.kdtEntrys, "userObject");
		dataBinder.registerBinding("entrys.catalog", java.lang.Object.class, this.kdtEntrys, "catalog.text");
		dataBinder.registerBinding("entrys.catalogNumber", String.class, this.kdtEntrys, "catalogNumber.text");
		dataBinder.registerBinding("entrys.legalFlag", boolean.class, this.kdtEntrys, "legalFlag.text");
		dataBinder.registerBinding("entrys.dataSource", java.lang.Object.class, this.kdtEntrys, "dataSource.text");
		dataBinder.registerBinding("entrys.operType", java.lang.Object.class, this.kdtEntrys, "operType.text");
		dataBinder.registerBinding("entrys.remark", String.class, this.kdtEntrys, "remark.text");
		dataBinder.registerBinding("entrys.catalogMain", java.lang.Object.class, this.kdtEntrys, "catalogMain.text");
		dataBinder.registerBinding("entrys.catalogMainNumber", String.class, this.kdtEntrys, "catalogMainNumber.text");
		dataBinder.registerBinding("entrys.DEntrys", com.kingdee.eas.custom.tiog.bizbill.CategoryListEntryDEntryInfo.class, this.kdtDEntrys, "userObject");
		dataBinder.registerBinding("entrys.DEntrys.id", com.kingdee.bos.util.BOSUuid.class, this.kdtDEntrys, "id.text");
		dataBinder.registerBinding("entrys.DEntrys.meetingType", java.lang.Object.class, this.kdtDEntrys, "meetingType.text");
		dataBinder.registerBinding("entrys.DEntrys.meetingTypeNumber", String.class, this.kdtDEntrys, "meetingTypeNumber.text");
		dataBinder.registerBinding("entrys.DEntrys.meetingDes", String.class, this.kdtDEntrys, "meetingDes.text");
		dataBinder.registerBinding("entrys.DEntrys.remark", String.class, this.kdtDEntrys, "remark.text");
		dataBinder.registerBinding("isUpload", boolean.class, this.chkisUpload, "selected");
		dataBinder.registerBinding("creator", com.kingdee.eas.base.permission.UserInfo.class, this.prmtCreator, "data");
		dataBinder.registerBinding("createTime", java.sql.Timestamp.class, this.kDDateCreateTime, "value");
		dataBinder.registerBinding("lastUpdateUser", com.kingdee.eas.base.permission.UserInfo.class, this.prmtLastUpdateUser, "data");
		dataBinder.registerBinding("lastUpdateTime", java.sql.Timestamp.class, this.kDDateLastUpdateTime, "value");
		dataBinder.registerBinding("number", String.class, this.txtNumber, "text");
		dataBinder.registerBinding("bizDate", java.util.Date.class, this.pkBizDate, "value");
		dataBinder.registerBinding("description", String.class, this.txtDescription, "text");
		dataBinder.registerBinding("auditor", com.kingdee.eas.base.permission.UserInfo.class, this.prmtAuditor, "data");
		dataBinder.registerBinding("adminOrgUnit", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.prmtadminOrgUnit, "data");
		dataBinder.registerBinding("company", com.kingdee.eas.basedata.org.CompanyOrgUnitInfo.class, this.prmtcompany, "data");
		dataBinder.registerBinding("companyNumber", String.class, this.txtcompanyNumber, "text");
		dataBinder.registerBinding("categoryListName", String.class, this.txtcategoryListName, "text");
		dataBinder.registerBinding("listVersion", String.class, this.txtlistVersion, "text");
		dataBinder.registerBinding("effectiveTS", java.util.Date.class, this.pkeffectiveTS, "value");
		dataBinder.registerBinding("invalidTS", java.util.Date.class, this.pkinvalidTS, "value");
		dataBinder.registerBinding("remark", String.class, this.txtremark, "text");
		dataBinder.registerBinding("state", com.kingdee.eas.custom.tiog.bizbill.BillState.class, this.state, "selectedItem");
		dataBinder.registerBinding("source", com.kingdee.eas.custom.tiog.basedata.DataSourceInfo.class, this.prmtsource, "data");
		dataBinder.registerBinding("operType", com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo.class, this.prmtoperType, "data");		
	}
	//Regiester UI State
	private void registerUIState(){		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.tiog.bizbill.app.CategoryListEditUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }


    /**
     * output onShow method
     */
    public void onShow() throws Exception
    {
        super.onShow();
        this.prmtcompany.requestFocusInWindow();
    }

	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
        this.editData = (com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo)ov;
    }
    protected void removeByPK(IObjectPK pk) throws Exception {
    	IObjectValue editData = this.editData;
    	super.removeByPK(pk);
    	recycleNumberByOrg(editData,"Admin",editData.getString("number"));
    }
    
    protected void recycleNumberByOrg(IObjectValue editData,String orgType,String number) {
        if (!StringUtils.isEmpty(number))
        {
            try {
            	String companyID = null;            
            	com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID =com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}				
				if (!StringUtils.isEmpty(companyID) && iCodingRuleManager.isExist(editData, companyID) && iCodingRuleManager.isUseIntermitNumber(editData, companyID)) {
					iCodingRuleManager.recycleNumber(editData,companyID,number);					
				}
            }
            catch (Exception e)
            {
                handUIException(e);
            }
        }
    }
    protected void setAutoNumberByOrg(String orgType) {
    	if (editData == null) return;
		if (editData.getNumber() == null) {
            try {
            	String companyID = null;
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID = com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}
				com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
		        if (iCodingRuleManager.isExist(editData, companyID)) {
		            if (iCodingRuleManager.isAddView(editData, companyID)) {
		            	editData.setNumber(iCodingRuleManager.getNumber(editData,companyID));
		            }
	                txtNumber.setEnabled(false);
		        }
            }
            catch (Exception e) {
                handUIException(e);
                this.oldData = editData;
                com.kingdee.eas.util.SysUtil.abort();
            } 
        } 
        else {
            if (editData.getNumber().trim().length() > 0) {
                txtNumber.setText(editData.getNumber());
            }
        }
    }
			protected com.kingdee.eas.basedata.org.OrgType getMainBizOrgType() {
			return com.kingdee.eas.basedata.org.OrgType.getEnum("Admin");
		}

	protected KDBizPromptBox getMainBizOrg() {
		return prmtadminOrgUnit;
}


    /**
     * output loadFields method
     */
    public void loadFields()
    {
        		setAutoNumberByOrg("Admin");
        dataBinder.loadFields();
    }
		protected void setOrgF7(KDBizPromptBox f7,com.kingdee.eas.basedata.org.OrgType orgType) throws Exception
		{
			com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer oufip = new com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer(orgType);
			oufip.getModel().setIsCUFilter(true);
			f7.setFilterInfoProducer(oufip);
		}

    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );
		getValidateHelper().registerBindProperty("entrys.id", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.catalog", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.catalogNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.legalFlag", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.dataSource", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.operType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.remark", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.catalogMain", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.catalogMainNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.DEntrys", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.DEntrys.id", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.DEntrys.meetingType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.DEntrys.meetingTypeNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.DEntrys.meetingDes", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.DEntrys.remark", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("isUpload", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("creator", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("createTime", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("lastUpdateUser", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("lastUpdateTime", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("bizDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("auditor", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("adminOrgUnit", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("company", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("companyNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("categoryListName", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("listVersion", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("effectiveTS", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("invalidTS", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("remark", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("state", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("source", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("operType", ValidateHelper.ON_SAVE);    		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
        if (STATUS_ADDNEW.equals(this.oprtState)) {
        } else if (STATUS_EDIT.equals(this.oprtState)) {
        } else if (STATUS_VIEW.equals(this.oprtState)) {
        } else if (STATUS_FINDVIEW.equals(this.oprtState)) {
        }
    }

    /**
     * output kdtEntrys_tableSelectChanged method
     */
    protected void kdtEntrys_tableSelectChanged(com.kingdee.bos.ctrl.kdf.table.event.KDTSelectEvent e) throws Exception
    {
    }

    /**
     * output kdtDEntrys_editValueChanged method
     */
    protected void kdtDEntrys_editValueChanged(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) throws Exception
    {
    }

    /**
     * output genSubject_actionPerformed method
     */
    protected void genSubject_actionPerformed(java.awt.event.ActionEvent e) throws Exception
    {
    }


    /**
     * output kdtEntrys_Changed(int rowIndex,int colIndex) method
     */
    public void kdtEntrys_Changed(int rowIndex,int colIndex) throws Exception
    {
            if ("catalogMain".equalsIgnoreCase(kdtEntrys.getColumn(colIndex).getKey())) {
kdtEntrys.getCell(rowIndex,"catalogMainNumber").setValue(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)kdtEntrys.getCell(rowIndex,"catalogMain").getValue(),"number")));

}

    if ("catalog".equalsIgnoreCase(kdtEntrys.getColumn(colIndex).getKey())) {
kdtEntrys.getCell(rowIndex,"catalogNumber").setValue(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)kdtEntrys.getCell(rowIndex,"catalog").getValue(),"number")));

}


    }

    /**
     * output kdtDEntrys_Changed(int rowIndex,int colIndex) method
     */
    public void kdtDEntrys_Changed(int rowIndex,int colIndex) throws Exception
    {
            if ("meetingType".equalsIgnoreCase(kdtDEntrys.getColumn(colIndex).getKey())) {
kdtDEntrys.getCell(rowIndex,"meetingTypeNumber").setValue(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)kdtDEntrys.getCell(rowIndex,"meetingType").getValue(),"number")));

}


    }

    /**
     * output prmtadminOrgUnit_Changed() method
     */
    public void prmtadminOrgUnit_Changed() throws Exception
    {
        System.out.println("prmtadminOrgUnit_Changed() Function is executed!");
            txtcompanyNumber.setText(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)prmtadminOrgUnit.getData(),"orgCode")));


    }
    /**
     * output getSelectors method
     */
    public SelectorItemCollection getSelectors()
    {
        SelectorItemCollection sic = new SelectorItemCollection();
		String selectorAll = System.getProperty("selector.all");
		if(StringUtils.isEmpty(selectorAll)){
			selectorAll = "true";
		}
    	sic.add(new SelectorItemInfo("entrys.id"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.*"));
		}
		else{
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.catalog.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.catalog.id"));
			sic.add(new SelectorItemInfo("entrys.catalog.name"));
        	sic.add(new SelectorItemInfo("entrys.catalog.number"));
		}
    	sic.add(new SelectorItemInfo("entrys.catalogNumber"));
    	sic.add(new SelectorItemInfo("entrys.legalFlag"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.dataSource.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.dataSource.id"));
			sic.add(new SelectorItemInfo("entrys.dataSource.name"));
        	sic.add(new SelectorItemInfo("entrys.dataSource.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.operType.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.operType.id"));
			sic.add(new SelectorItemInfo("entrys.operType.name"));
        	sic.add(new SelectorItemInfo("entrys.operType.number"));
		}
    	sic.add(new SelectorItemInfo("entrys.remark"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.catalogMain.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.catalogMain.id"));
			sic.add(new SelectorItemInfo("entrys.catalogMain.name"));
        	sic.add(new SelectorItemInfo("entrys.catalogMain.number"));
		}
    	sic.add(new SelectorItemInfo("entrys.catalogMainNumber"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.DEntrys.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.DEntrys.id"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.DEntrys.meetingType.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.DEntrys.meetingType.id"));
			sic.add(new SelectorItemInfo("entrys.DEntrys.meetingType.name"));
        	sic.add(new SelectorItemInfo("entrys.DEntrys.meetingType.number"));
		}
    	sic.add(new SelectorItemInfo("entrys.DEntrys.meetingTypeNumber"));
    	sic.add(new SelectorItemInfo("entrys.DEntrys.meetingDes"));
    	sic.add(new SelectorItemInfo("entrys.DEntrys.remark"));
        sic.add(new SelectorItemInfo("isUpload"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("creator.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("creator.id"));
        	sic.add(new SelectorItemInfo("creator.number"));
        	sic.add(new SelectorItemInfo("creator.name"));
		}
        sic.add(new SelectorItemInfo("createTime"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("lastUpdateUser.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("lastUpdateUser.id"));
        	sic.add(new SelectorItemInfo("lastUpdateUser.number"));
        	sic.add(new SelectorItemInfo("lastUpdateUser.name"));
		}
        sic.add(new SelectorItemInfo("lastUpdateTime"));
        sic.add(new SelectorItemInfo("number"));
        sic.add(new SelectorItemInfo("bizDate"));
        sic.add(new SelectorItemInfo("description"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("auditor.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("auditor.id"));
        	sic.add(new SelectorItemInfo("auditor.number"));
        	sic.add(new SelectorItemInfo("auditor.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("adminOrgUnit.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("adminOrgUnit.id"));
        	sic.add(new SelectorItemInfo("adminOrgUnit.number"));
        	sic.add(new SelectorItemInfo("adminOrgUnit.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("company.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("company.id"));
        	sic.add(new SelectorItemInfo("company.number"));
        	sic.add(new SelectorItemInfo("company.name"));
		}
        sic.add(new SelectorItemInfo("companyNumber"));
        sic.add(new SelectorItemInfo("categoryListName"));
        sic.add(new SelectorItemInfo("listVersion"));
        sic.add(new SelectorItemInfo("effectiveTS"));
        sic.add(new SelectorItemInfo("invalidTS"));
        sic.add(new SelectorItemInfo("remark"));
        sic.add(new SelectorItemInfo("state"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("source.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("source.id"));
        	sic.add(new SelectorItemInfo("source.number"));
        	sic.add(new SelectorItemInfo("source.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("operType.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("operType.id"));
        	sic.add(new SelectorItemInfo("operType.number"));
        	sic.add(new SelectorItemInfo("operType.name"));
		}
        return sic;
    }        
    	

    /**
     * output actionSubmit_actionPerformed method
     */
    public void actionSubmit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionSubmit_actionPerformed(e);
    }
    	

    /**
     * output actionPrint_actionPerformed method
     */
    public void actionPrint_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionPrint_actionPerformed(e);
    }
    	

    /**
     * output actionPrintPreview_actionPerformed method
     */
    public void actionPrintPreview_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionPrintPreview_actionPerformed(e);
    }
    	

    /**
     * output actionAudit_actionPerformed method
     */
    public void actionAudit_actionPerformed(ActionEvent e) throws Exception
    {
;

        com.kingdee.eas.custom.tiog.bizbill.CategoryListFactory.getRemoteInstance().audit(editData);
    }
    	

    /**
     * output actionUnAudit_actionPerformed method
     */
    public void actionUnAudit_actionPerformed(ActionEvent e) throws Exception
    {
;

        com.kingdee.eas.custom.tiog.bizbill.CategoryListFactory.getRemoteInstance().unAudit(editData);
    }
    	

    /**
     * output actionGenSubject_actionPerformed method
     */
    public void actionGenSubject_actionPerformed(ActionEvent e) throws Exception
    {
    }
	public RequestContext prepareActionSubmit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionSubmit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionSubmit() {
    	return false;
    }
	public RequestContext prepareActionPrint(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrint(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrint() {
    	return false;
    }
	public RequestContext prepareActionPrintPreview(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrintPreview(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrintPreview() {
    	return false;
    }
	public RequestContext prepareActionAudit(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionAudit() {
    	return false;
    }
	public RequestContext prepareActionUnAudit(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionUnAudit() {
    	return false;
    }
	public RequestContext prepareActionGenSubject(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionGenSubject() {
    	return false;
    }

    /**
     * output ActionAudit class
     */     
    protected class ActionAudit extends ItemAction {     
    
        public ActionAudit()
        {
            this(null);
        }

        public ActionAudit(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            _tempStr = resHelper.getString("ActionAudit.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAudit.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAudit.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractCategoryListEditUI.this, "ActionAudit", "actionAudit_actionPerformed", e);
        }
    }

    /**
     * output ActionUnAudit class
     */     
    protected class ActionUnAudit extends ItemAction {     
    
        public ActionUnAudit()
        {
            this(null);
        }

        public ActionUnAudit(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            _tempStr = resHelper.getString("ActionUnAudit.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionUnAudit.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionUnAudit.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractCategoryListEditUI.this, "ActionUnAudit", "actionUnAudit_actionPerformed", e);
        }
    }

    /**
     * output ActionGenSubject class
     */     
    protected class ActionGenSubject extends ItemAction {     
    
        public ActionGenSubject()
        {
            this(null);
        }

        public ActionGenSubject(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionGenSubject.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionGenSubject.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionGenSubject.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractCategoryListEditUI.this, "ActionGenSubject", "actionGenSubject_actionPerformed", e);
        }
    }

    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.tiog.bizbill.client", "CategoryListEditUI");
    }
    /**
     * output isBindWorkFlow method
     */
    public boolean isBindWorkFlow()
    {
        return true;
    }

    /**
     * output getEditUIName method
     */
    protected String getEditUIName()
    {
        return com.kingdee.eas.custom.tiog.bizbill.client.CategoryListEditUI.class.getName();
    }

    /**
     * output getBizInterface method
     */
    protected com.kingdee.eas.framework.ICoreBase getBizInterface() throws Exception
    {
        return com.kingdee.eas.custom.tiog.bizbill.CategoryListFactory.getRemoteInstance();
    }

    /**
     * output createNewData method
     */
    protected IObjectValue createNewData()
    {
        com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo objectValue = new com.kingdee.eas.custom.tiog.bizbill.CategoryListInfo();
				if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin")) != null)
			objectValue.put("adminOrgUnit",com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin")));
 
        objectValue.setCreator((com.kingdee.eas.base.permission.UserInfo)(com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentUser()));		
        return objectValue;
    }


    	protected String getTDFileName() {
    	return "/bim/custom/tiog/bizbill/CategoryList";
	}
    protected IMetaDataPK getTDQueryPK() {
    	return new MetaDataPK("com.kingdee.eas.custom.tiog.bizbill.app.CategoryListQuery");
	}
    

    /**
     * output getDetailTable method
     */
    protected KDTable getDetailTable() {
        return kdtEntrys;
	}
    /**
     * output applyDefaultValue method
     */
    protected void applyDefaultValue(IObjectValue vo) {        
		vo.put("state","1");
        
    }        
	protected void setFieldsNull(com.kingdee.bos.dao.AbstractObjectValue arg0) {
		super.setFieldsNull(arg0);
		arg0.put("number",null);
	}

}