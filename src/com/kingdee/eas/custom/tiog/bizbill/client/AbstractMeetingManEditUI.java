/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.bizbill.client;

import org.apache.log4j.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.BorderFactory;
import javax.swing.event.*;
import javax.swing.KeyStroke;

import com.kingdee.bos.ctrl.swing.*;
import com.kingdee.bos.ctrl.kdf.table.*;
import com.kingdee.bos.ctrl.kdf.data.event.*;
import com.kingdee.bos.dao.*;
import com.kingdee.bos.dao.query.*;
import com.kingdee.bos.metadata.*;
import com.kingdee.bos.metadata.entity.*;
import com.kingdee.bos.ui.face.*;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.enums.EnumUtils;
import com.kingdee.bos.ui.face.UIRuleUtil;
import com.kingdee.bos.ctrl.swing.event.*;
import com.kingdee.bos.ctrl.kdf.table.event.*;
import com.kingdee.bos.ctrl.extendcontrols.*;
import com.kingdee.bos.ctrl.kdf.util.render.*;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.uistatemanage.ActionStateConst;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.appframework.uip.UINavigator;


/**
 * output class name
 */
public abstract class AbstractMeetingManEditUI extends com.kingdee.eas.framework.client.CoreBillEditUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractMeetingManEditUI.class);
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contCreator;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contCreateTime;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contLastUpdateUser;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contLastUpdateTime;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contBizDate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contDescription;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contAuditor;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contadminOrgUnit;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contremark;
    protected com.kingdee.bos.ctrl.swing.KDTabbedPane kDTabbedPane2;
    protected com.kingdee.bos.ctrl.swing.KDTabbedPane kDTabbedPane3;
    protected com.kingdee.bos.ctrl.swing.KDTabbedPane kDTabbedPane4;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcompany;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contmeetingName;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contmeetingType;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcompanyNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contmeetingTypeName;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contmeetingForm;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contmeetingTS;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contmoderator;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contreleaseTS;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contdataSource;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contoperType;
    protected com.kingdee.bos.ctrl.swing.KDTabbedPane kDTabbedPane1;
    protected com.kingdee.bos.ctrl.swing.KDSeparator kDSeparator8;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kDWorkButton1;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton kDWorkButton2;
    protected com.kingdee.bos.ctrl.swing.KDSeparator kDSeparator9;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contstate;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtCreator;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker kDDateCreateTime;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtLastUpdateUser;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker kDDateLastUpdateTime;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtNumber;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkBizDate;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtDescription;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtAuditor;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtadminOrgUnit;
    protected com.kingdee.bos.ctrl.swing.KDScrollPane scrollPaneremark;
    protected com.kingdee.bos.ctrl.swing.KDTextArea txtremark;
    protected com.kingdee.bos.ctrl.swing.KDPanel kDPanel2;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtMeetingManSubjectAttendanceEntry;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtMeetingManSubjectAttendanceEntry_detailPanel = null;
    protected com.kingdee.bos.ctrl.swing.KDPanel kDPanel3;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtMeetingManSubjectDeliberationEntry;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtMeetingManSubjectDeliberationEntry_detailPanel = null;
    protected com.kingdee.bos.ctrl.swing.KDPanel kDPanel4;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtMeetingManAttendeeEntry;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtMeetingManAttendeeEntry_detailPanel = null;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtcompany;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtmeetingName;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtmeetingType;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtcompanyNumber;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtmeetingTypeName;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtmeetingForm;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkmeetingTS;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtmoderator;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkreleaseTS;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtdataSource;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtoperType;
    protected com.kingdee.bos.ctrl.swing.KDPanel kDPanel1;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtSubjectEntry;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtSubjectEntry_detailPanel = null;
    protected com.kingdee.bos.ctrl.swing.KDComboBox state;
    protected com.kingdee.bos.ctrl.swing.KDMenu bizMenu;
    protected com.kingdee.bos.ctrl.swing.KDMenuItem mniActionAudit;
    protected com.kingdee.bos.ctrl.swing.KDMenuItem mniActionUnAudit;
    protected com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo editData = null;
    protected ActionAudit actionAudit = null;
    protected ActionUnAudit actionUnAudit = null;
    /**
     * output class constructor
     */
    public AbstractMeetingManEditUI() throws Exception
    {
        super();
        this.defaultObjectName = "editData";
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractMeetingManEditUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        //actionSubmit
        String _tempStr = null;
        actionSubmit.setEnabled(true);
        actionSubmit.setDaemonRun(false);

        actionSubmit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl S"));
        _tempStr = resHelper.getString("ActionSubmit.SHORT_DESCRIPTION");
        actionSubmit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.LONG_DESCRIPTION");
        actionSubmit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.NAME");
        actionSubmit.putValue(ItemAction.NAME, _tempStr);
        this.actionSubmit.setBindWorkFlow(true);
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrint
        actionPrint.setEnabled(true);
        actionPrint.setDaemonRun(false);

        actionPrint.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl P"));
        _tempStr = resHelper.getString("ActionPrint.SHORT_DESCRIPTION");
        actionPrint.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.LONG_DESCRIPTION");
        actionPrint.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.NAME");
        actionPrint.putValue(ItemAction.NAME, _tempStr);
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrintPreview
        actionPrintPreview.setEnabled(true);
        actionPrintPreview.setDaemonRun(false);

        actionPrintPreview.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift ctrl P"));
        _tempStr = resHelper.getString("ActionPrintPreview.SHORT_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.LONG_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.NAME");
        actionPrintPreview.putValue(ItemAction.NAME, _tempStr);
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionAudit
        this.actionAudit = new ActionAudit(this);
        getActionManager().registerAction("actionAudit", actionAudit);
        this.actionAudit.setBindWorkFlow(true);
        this.actionAudit.setExtendProperty("canForewarn", "true");
        this.actionAudit.setExtendProperty("userDefined", "true");
        this.actionAudit.setExtendProperty("isObjectUpdateLock", "false");
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.ForewarnService());
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.WorkFlowService());
        //actionUnAudit
        this.actionUnAudit = new ActionUnAudit(this);
        getActionManager().registerAction("actionUnAudit", actionUnAudit);
        this.actionUnAudit.setBindWorkFlow(true);
        this.actionUnAudit.setExtendProperty("canForewarn", "true");
        this.actionUnAudit.setExtendProperty("userDefined", "true");
        this.actionUnAudit.setExtendProperty("isObjectUpdateLock", "false");
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.ForewarnService());
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.WorkFlowService());
        this.contCreator = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contCreateTime = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contLastUpdateUser = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contLastUpdateTime = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contBizDate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contDescription = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contAuditor = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contadminOrgUnit = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contremark = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDTabbedPane2 = new com.kingdee.bos.ctrl.swing.KDTabbedPane();
        this.kDTabbedPane3 = new com.kingdee.bos.ctrl.swing.KDTabbedPane();
        this.kDTabbedPane4 = new com.kingdee.bos.ctrl.swing.KDTabbedPane();
        this.contcompany = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contmeetingName = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contmeetingType = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contcompanyNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contmeetingTypeName = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contmeetingForm = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contmeetingTS = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contmoderator = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contreleaseTS = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contdataSource = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contoperType = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDTabbedPane1 = new com.kingdee.bos.ctrl.swing.KDTabbedPane();
        this.kDSeparator8 = new com.kingdee.bos.ctrl.swing.KDSeparator();
        this.kDWorkButton1 = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDWorkButton2 = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.kDSeparator9 = new com.kingdee.bos.ctrl.swing.KDSeparator();
        this.contstate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.prmtCreator = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.kDDateCreateTime = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.prmtLastUpdateUser = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.kDDateLastUpdateTime = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.txtNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.pkBizDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.txtDescription = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtAuditor = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtadminOrgUnit = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.scrollPaneremark = new com.kingdee.bos.ctrl.swing.KDScrollPane();
        this.txtremark = new com.kingdee.bos.ctrl.swing.KDTextArea();
        this.kDPanel2 = new com.kingdee.bos.ctrl.swing.KDPanel();
        this.kdtMeetingManSubjectAttendanceEntry = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.kDPanel3 = new com.kingdee.bos.ctrl.swing.KDPanel();
        this.kdtMeetingManSubjectDeliberationEntry = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.kDPanel4 = new com.kingdee.bos.ctrl.swing.KDPanel();
        this.kdtMeetingManAttendeeEntry = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.prmtcompany = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.txtmeetingName = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtmeetingType = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.txtcompanyNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtmeetingTypeName = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtmeetingForm = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.pkmeetingTS = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.prmtmoderator = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.pkreleaseTS = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.prmtdataSource = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtoperType = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.kDPanel1 = new com.kingdee.bos.ctrl.swing.KDPanel();
        this.kdtSubjectEntry = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.state = new com.kingdee.bos.ctrl.swing.KDComboBox();
        this.bizMenu = new com.kingdee.bos.ctrl.swing.KDMenu();
        this.mniActionAudit = new com.kingdee.bos.ctrl.swing.KDMenuItem();
        this.mniActionUnAudit = new com.kingdee.bos.ctrl.swing.KDMenuItem();
        this.contCreator.setName("contCreator");
        this.contCreateTime.setName("contCreateTime");
        this.contLastUpdateUser.setName("contLastUpdateUser");
        this.contLastUpdateTime.setName("contLastUpdateTime");
        this.contNumber.setName("contNumber");
        this.contBizDate.setName("contBizDate");
        this.contDescription.setName("contDescription");
        this.contAuditor.setName("contAuditor");
        this.contadminOrgUnit.setName("contadminOrgUnit");
        this.contremark.setName("contremark");
        this.kDTabbedPane2.setName("kDTabbedPane2");
        this.kDTabbedPane3.setName("kDTabbedPane3");
        this.kDTabbedPane4.setName("kDTabbedPane4");
        this.contcompany.setName("contcompany");
        this.contmeetingName.setName("contmeetingName");
        this.contmeetingType.setName("contmeetingType");
        this.contcompanyNumber.setName("contcompanyNumber");
        this.contmeetingTypeName.setName("contmeetingTypeName");
        this.contmeetingForm.setName("contmeetingForm");
        this.contmeetingTS.setName("contmeetingTS");
        this.contmoderator.setName("contmoderator");
        this.contreleaseTS.setName("contreleaseTS");
        this.contdataSource.setName("contdataSource");
        this.contoperType.setName("contoperType");
        this.kDTabbedPane1.setName("kDTabbedPane1");
        this.kDSeparator8.setName("kDSeparator8");
        this.kDWorkButton1.setName("kDWorkButton1");
        this.kDWorkButton2.setName("kDWorkButton2");
        this.kDSeparator9.setName("kDSeparator9");
        this.contstate.setName("contstate");
        this.prmtCreator.setName("prmtCreator");
        this.kDDateCreateTime.setName("kDDateCreateTime");
        this.prmtLastUpdateUser.setName("prmtLastUpdateUser");
        this.kDDateLastUpdateTime.setName("kDDateLastUpdateTime");
        this.txtNumber.setName("txtNumber");
        this.pkBizDate.setName("pkBizDate");
        this.txtDescription.setName("txtDescription");
        this.prmtAuditor.setName("prmtAuditor");
        this.prmtadminOrgUnit.setName("prmtadminOrgUnit");
        this.scrollPaneremark.setName("scrollPaneremark");
        this.txtremark.setName("txtremark");
        this.kDPanel2.setName("kDPanel2");
        this.kdtMeetingManSubjectAttendanceEntry.setName("kdtMeetingManSubjectAttendanceEntry");
        this.kDPanel3.setName("kDPanel3");
        this.kdtMeetingManSubjectDeliberationEntry.setName("kdtMeetingManSubjectDeliberationEntry");
        this.kDPanel4.setName("kDPanel4");
        this.kdtMeetingManAttendeeEntry.setName("kdtMeetingManAttendeeEntry");
        this.prmtcompany.setName("prmtcompany");
        this.txtmeetingName.setName("txtmeetingName");
        this.prmtmeetingType.setName("prmtmeetingType");
        this.txtcompanyNumber.setName("txtcompanyNumber");
        this.txtmeetingTypeName.setName("txtmeetingTypeName");
        this.prmtmeetingForm.setName("prmtmeetingForm");
        this.pkmeetingTS.setName("pkmeetingTS");
        this.prmtmoderator.setName("prmtmoderator");
        this.pkreleaseTS.setName("pkreleaseTS");
        this.prmtdataSource.setName("prmtdataSource");
        this.prmtoperType.setName("prmtoperType");
        this.kDPanel1.setName("kDPanel1");
        this.kdtSubjectEntry.setName("kdtSubjectEntry");
        this.state.setName("state");
        this.bizMenu.setName("bizMenu");
        this.mniActionAudit.setName("mniActionAudit");
        this.mniActionUnAudit.setName("mniActionUnAudit");
        // CoreUI		
        this.btnTraceUp.setVisible(false);		
        this.btnTraceDown.setVisible(false);		
        this.btnCreateTo.setVisible(true);		
        this.btnAddLine.setVisible(false);		
        this.btnCopyLine.setVisible(false);		
        this.btnInsertLine.setVisible(false);		
        this.btnRemoveLine.setVisible(false);		
        this.btnAuditResult.setVisible(false);		
        this.separator1.setVisible(false);		
        this.menuItemCreateTo.setVisible(true);		
        this.separator3.setVisible(false);		
        this.menuItemTraceUp.setVisible(false);		
        this.menuItemTraceDown.setVisible(false);		
        this.menuTable1.setVisible(false);		
        this.menuItemAddLine.setVisible(false);		
        this.menuItemCopyLine.setVisible(false);		
        this.menuItemInsertLine.setVisible(false);		
        this.menuItemRemoveLine.setVisible(false);		
        this.menuItemViewSubmitProccess.setVisible(false);		
        this.menuItemViewDoProccess.setVisible(false);		
        this.menuItemAuditResult.setVisible(false);
        // contCreator		
        this.contCreator.setBoundLabelText(resHelper.getString("contCreator.boundLabelText"));		
        this.contCreator.setBoundLabelLength(100);		
        this.contCreator.setBoundLabelUnderline(true);		
        this.contCreator.setEnabled(false);
        // contCreateTime		
        this.contCreateTime.setBoundLabelText(resHelper.getString("contCreateTime.boundLabelText"));		
        this.contCreateTime.setBoundLabelLength(100);		
        this.contCreateTime.setBoundLabelUnderline(true);		
        this.contCreateTime.setEnabled(false);
        // contLastUpdateUser		
        this.contLastUpdateUser.setBoundLabelText(resHelper.getString("contLastUpdateUser.boundLabelText"));		
        this.contLastUpdateUser.setBoundLabelLength(100);		
        this.contLastUpdateUser.setBoundLabelUnderline(true);		
        this.contLastUpdateUser.setEnabled(false);		
        this.contLastUpdateUser.setVisible(false);
        // contLastUpdateTime		
        this.contLastUpdateTime.setBoundLabelText(resHelper.getString("contLastUpdateTime.boundLabelText"));		
        this.contLastUpdateTime.setBoundLabelLength(100);		
        this.contLastUpdateTime.setBoundLabelUnderline(true);		
        this.contLastUpdateTime.setEnabled(false);		
        this.contLastUpdateTime.setVisible(false);
        // contNumber		
        this.contNumber.setBoundLabelText(resHelper.getString("contNumber.boundLabelText"));		
        this.contNumber.setBoundLabelLength(100);		
        this.contNumber.setBoundLabelUnderline(true);
        // contBizDate		
        this.contBizDate.setBoundLabelText(resHelper.getString("contBizDate.boundLabelText"));		
        this.contBizDate.setBoundLabelLength(100);		
        this.contBizDate.setBoundLabelUnderline(true);		
        this.contBizDate.setBoundLabelAlignment(7);		
        this.contBizDate.setVisible(true);
        // contDescription		
        this.contDescription.setBoundLabelText(resHelper.getString("contDescription.boundLabelText"));		
        this.contDescription.setBoundLabelLength(100);		
        this.contDescription.setBoundLabelUnderline(true);
        // contAuditor		
        this.contAuditor.setBoundLabelText(resHelper.getString("contAuditor.boundLabelText"));		
        this.contAuditor.setBoundLabelLength(100);		
        this.contAuditor.setBoundLabelUnderline(true);
        // contadminOrgUnit		
        this.contadminOrgUnit.setBoundLabelText(resHelper.getString("contadminOrgUnit.boundLabelText"));		
        this.contadminOrgUnit.setBoundLabelLength(100);		
        this.contadminOrgUnit.setBoundLabelUnderline(true);		
        this.contadminOrgUnit.setVisible(true);
        // contremark		
        this.contremark.setBoundLabelText(resHelper.getString("contremark.boundLabelText"));		
        this.contremark.setBoundLabelLength(100);		
        this.contremark.setBoundLabelUnderline(true);		
        this.contremark.setVisible(true);
        // kDTabbedPane2
        // kDTabbedPane3
        // kDTabbedPane4
        // contcompany		
        this.contcompany.setBoundLabelText(resHelper.getString("contcompany.boundLabelText"));		
        this.contcompany.setBoundLabelLength(100);		
        this.contcompany.setBoundLabelUnderline(true);		
        this.contcompany.setVisible(true);
        // contmeetingName		
        this.contmeetingName.setBoundLabelText(resHelper.getString("contmeetingName.boundLabelText"));		
        this.contmeetingName.setBoundLabelLength(100);		
        this.contmeetingName.setBoundLabelUnderline(true);		
        this.contmeetingName.setVisible(true);
        // contmeetingType		
        this.contmeetingType.setBoundLabelText(resHelper.getString("contmeetingType.boundLabelText"));		
        this.contmeetingType.setBoundLabelLength(100);		
        this.contmeetingType.setBoundLabelUnderline(true);		
        this.contmeetingType.setVisible(true);
        // contcompanyNumber		
        this.contcompanyNumber.setBoundLabelText(resHelper.getString("contcompanyNumber.boundLabelText"));		
        this.contcompanyNumber.setBoundLabelLength(100);		
        this.contcompanyNumber.setBoundLabelUnderline(true);		
        this.contcompanyNumber.setVisible(true);
        // contmeetingTypeName		
        this.contmeetingTypeName.setBoundLabelText(resHelper.getString("contmeetingTypeName.boundLabelText"));		
        this.contmeetingTypeName.setBoundLabelLength(100);		
        this.contmeetingTypeName.setBoundLabelUnderline(true);		
        this.contmeetingTypeName.setVisible(true);
        // contmeetingForm		
        this.contmeetingForm.setBoundLabelText(resHelper.getString("contmeetingForm.boundLabelText"));		
        this.contmeetingForm.setBoundLabelLength(100);		
        this.contmeetingForm.setBoundLabelUnderline(true);		
        this.contmeetingForm.setVisible(true);
        // contmeetingTS		
        this.contmeetingTS.setBoundLabelText(resHelper.getString("contmeetingTS.boundLabelText"));		
        this.contmeetingTS.setBoundLabelLength(100);		
        this.contmeetingTS.setBoundLabelUnderline(true);		
        this.contmeetingTS.setVisible(true);
        // contmoderator		
        this.contmoderator.setBoundLabelText(resHelper.getString("contmoderator.boundLabelText"));		
        this.contmoderator.setBoundLabelLength(100);		
        this.contmoderator.setBoundLabelUnderline(true);		
        this.contmoderator.setVisible(true);
        // contreleaseTS		
        this.contreleaseTS.setBoundLabelText(resHelper.getString("contreleaseTS.boundLabelText"));		
        this.contreleaseTS.setBoundLabelLength(100);		
        this.contreleaseTS.setBoundLabelUnderline(true);		
        this.contreleaseTS.setVisible(true);
        // contdataSource		
        this.contdataSource.setBoundLabelText(resHelper.getString("contdataSource.boundLabelText"));		
        this.contdataSource.setBoundLabelLength(100);		
        this.contdataSource.setBoundLabelUnderline(true);		
        this.contdataSource.setVisible(true);
        // contoperType		
        this.contoperType.setBoundLabelText(resHelper.getString("contoperType.boundLabelText"));		
        this.contoperType.setBoundLabelLength(100);		
        this.contoperType.setBoundLabelUnderline(true);		
        this.contoperType.setVisible(true);
        // kDTabbedPane1
        // kDSeparator8
        // kDWorkButton1		
        this.kDWorkButton1.setText(resHelper.getString("kDWorkButton1.text"));
        // kDWorkButton2		
        this.kDWorkButton2.setText(resHelper.getString("kDWorkButton2.text"));
        // kDSeparator9
        // contstate		
        this.contstate.setBoundLabelText(resHelper.getString("contstate.boundLabelText"));		
        this.contstate.setBoundLabelLength(100);		
        this.contstate.setBoundLabelUnderline(true);		
        this.contstate.setVisible(true);
        // prmtCreator		
        this.prmtCreator.setEnabled(false);
        // kDDateCreateTime		
        this.kDDateCreateTime.setTimeEnabled(true);		
        this.kDDateCreateTime.setEnabled(false);
        // prmtLastUpdateUser		
        this.prmtLastUpdateUser.setEnabled(false);
        // kDDateLastUpdateTime		
        this.kDDateLastUpdateTime.setTimeEnabled(true);		
        this.kDDateLastUpdateTime.setEnabled(false);
        // txtNumber		
        this.txtNumber.setMaxLength(80);
        // pkBizDate		
        this.pkBizDate.setVisible(true);		
        this.pkBizDate.setEnabled(true);
        // txtDescription		
        this.txtDescription.setMaxLength(80);
        // prmtAuditor		
        this.prmtAuditor.setEnabled(false);
        // prmtadminOrgUnit		
        this.prmtadminOrgUnit.setQueryInfo("com.kingdee.eas.basedata.org.app.AdminItemQuery");		
        this.prmtadminOrgUnit.setVisible(true);		
        this.prmtadminOrgUnit.setEditable(true);		
        this.prmtadminOrgUnit.setDisplayFormat("$name$");		
        this.prmtadminOrgUnit.setEditFormat("$number$");		
        this.prmtadminOrgUnit.setCommitFormat("$number$");		
        this.prmtadminOrgUnit.setRequired(true);
        		setOrgF7(prmtadminOrgUnit,com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"));
					
        prmtadminOrgUnit.addDataChangeListener(new DataChangeListener() {
		public void dataChanged(DataChangeEvent e) {
			try {
				prmtadminOrgUnit_Changed();
			}
			catch (Exception exc) {
				handUIException(exc);
			}
		}
	});

        // scrollPaneremark
        // txtremark		
        this.txtremark.setVisible(true);		
        this.txtremark.setRequired(false);		
        this.txtremark.setMaxLength(2000);
        // kDPanel2
        // kdtMeetingManSubjectAttendanceEntry
		String kdtMeetingManSubjectAttendanceEntryStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:NumberFormat>&amp;int</c:NumberFormat></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"seq\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"remark\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"attendance\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"attendancePos\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{seq}</t:Cell><t:Cell>$Resource{remark}</t:Cell><t:Cell>$Resource{attendance}</t:Cell><t:Cell>$Resource{attendancePos}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtMeetingManSubjectAttendanceEntry.setFormatXml(resHelper.translateString("kdtMeetingManSubjectAttendanceEntry",kdtMeetingManSubjectAttendanceEntryStrXML));

                this.kdtMeetingManSubjectAttendanceEntry.putBindContents("editData",new String[] {"MeetingManSubjectAttendanceEntry.seq","MeetingManSubjectAttendanceEntry.remark","MeetingManSubjectAttendanceEntry.attendance","MeetingManSubjectAttendanceEntry.attendancePos"});


        this.kdtMeetingManSubjectAttendanceEntry.checkParsed();
        KDFormattedTextField kdtMeetingManSubjectAttendanceEntry_seq_TextField = new KDFormattedTextField();
        kdtMeetingManSubjectAttendanceEntry_seq_TextField.setName("kdtMeetingManSubjectAttendanceEntry_seq_TextField");
        kdtMeetingManSubjectAttendanceEntry_seq_TextField.setVisible(true);
        kdtMeetingManSubjectAttendanceEntry_seq_TextField.setEditable(true);
        kdtMeetingManSubjectAttendanceEntry_seq_TextField.setHorizontalAlignment(2);
        kdtMeetingManSubjectAttendanceEntry_seq_TextField.setDataType(0);
        KDTDefaultCellEditor kdtMeetingManSubjectAttendanceEntry_seq_CellEditor = new KDTDefaultCellEditor(kdtMeetingManSubjectAttendanceEntry_seq_TextField);
        this.kdtMeetingManSubjectAttendanceEntry.getColumn("seq").setEditor(kdtMeetingManSubjectAttendanceEntry_seq_CellEditor);
        KDTextArea kdtMeetingManSubjectAttendanceEntry_remark_TextArea = new KDTextArea();
        kdtMeetingManSubjectAttendanceEntry_remark_TextArea.setName("kdtMeetingManSubjectAttendanceEntry_remark_TextArea");
        kdtMeetingManSubjectAttendanceEntry_remark_TextArea.setMaxLength(2000);
        KDTDefaultCellEditor kdtMeetingManSubjectAttendanceEntry_remark_CellEditor = new KDTDefaultCellEditor(kdtMeetingManSubjectAttendanceEntry_remark_TextArea);
        this.kdtMeetingManSubjectAttendanceEntry.getColumn("remark").setEditor(kdtMeetingManSubjectAttendanceEntry_remark_CellEditor);
        final KDBizPromptBox kdtMeetingManSubjectAttendanceEntry_attendance_PromptBox = new KDBizPromptBox();
        kdtMeetingManSubjectAttendanceEntry_attendance_PromptBox.setQueryInfo("com.kingdee.eas.basedata.person.app.PersonQuery");
        kdtMeetingManSubjectAttendanceEntry_attendance_PromptBox.setVisible(true);
        kdtMeetingManSubjectAttendanceEntry_attendance_PromptBox.setEditable(true);
        kdtMeetingManSubjectAttendanceEntry_attendance_PromptBox.setDisplayFormat("$number$");
        kdtMeetingManSubjectAttendanceEntry_attendance_PromptBox.setEditFormat("$number$");
        kdtMeetingManSubjectAttendanceEntry_attendance_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtMeetingManSubjectAttendanceEntry_attendance_CellEditor = new KDTDefaultCellEditor(kdtMeetingManSubjectAttendanceEntry_attendance_PromptBox);
        this.kdtMeetingManSubjectAttendanceEntry.getColumn("attendance").setEditor(kdtMeetingManSubjectAttendanceEntry_attendance_CellEditor);
        ObjectValueRender kdtMeetingManSubjectAttendanceEntry_attendance_OVR = new ObjectValueRender();
        kdtMeetingManSubjectAttendanceEntry_attendance_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtMeetingManSubjectAttendanceEntry.getColumn("attendance").setRenderer(kdtMeetingManSubjectAttendanceEntry_attendance_OVR);
        final KDBizPromptBox kdtMeetingManSubjectAttendanceEntry_attendancePos_PromptBox = new KDBizPromptBox();
        kdtMeetingManSubjectAttendanceEntry_attendancePos_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.PositionQuery");
        kdtMeetingManSubjectAttendanceEntry_attendancePos_PromptBox.setVisible(true);
        kdtMeetingManSubjectAttendanceEntry_attendancePos_PromptBox.setEditable(true);
        kdtMeetingManSubjectAttendanceEntry_attendancePos_PromptBox.setDisplayFormat("$number$");
        kdtMeetingManSubjectAttendanceEntry_attendancePos_PromptBox.setEditFormat("$number$");
        kdtMeetingManSubjectAttendanceEntry_attendancePos_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtMeetingManSubjectAttendanceEntry_attendancePos_CellEditor = new KDTDefaultCellEditor(kdtMeetingManSubjectAttendanceEntry_attendancePos_PromptBox);
        this.kdtMeetingManSubjectAttendanceEntry.getColumn("attendancePos").setEditor(kdtMeetingManSubjectAttendanceEntry_attendancePos_CellEditor);
        ObjectValueRender kdtMeetingManSubjectAttendanceEntry_attendancePos_OVR = new ObjectValueRender();
        kdtMeetingManSubjectAttendanceEntry_attendancePos_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtMeetingManSubjectAttendanceEntry.getColumn("attendancePos").setRenderer(kdtMeetingManSubjectAttendanceEntry_attendancePos_OVR);
        // kDPanel3
        // kdtMeetingManSubjectDeliberationEntry
		String kdtMeetingManSubjectDeliberationEntryStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:NumberFormat>&amp;int</c:NumberFormat></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"seq\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"remark\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"deliberator\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"deliberationResult\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{seq}</t:Cell><t:Cell>$Resource{remark}</t:Cell><t:Cell>$Resource{deliberator}</t:Cell><t:Cell>$Resource{deliberationResult}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtMeetingManSubjectDeliberationEntry.setFormatXml(resHelper.translateString("kdtMeetingManSubjectDeliberationEntry",kdtMeetingManSubjectDeliberationEntryStrXML));

                this.kdtMeetingManSubjectDeliberationEntry.putBindContents("editData",new String[] {"MeetingManSubjectDeliberationEntry.seq","MeetingManSubjectDeliberationEntry.remark","MeetingManSubjectDeliberationEntry.deliberator","MeetingManSubjectDeliberationEntry.deliberationResult"});


        this.kdtMeetingManSubjectDeliberationEntry.checkParsed();
        KDFormattedTextField kdtMeetingManSubjectDeliberationEntry_seq_TextField = new KDFormattedTextField();
        kdtMeetingManSubjectDeliberationEntry_seq_TextField.setName("kdtMeetingManSubjectDeliberationEntry_seq_TextField");
        kdtMeetingManSubjectDeliberationEntry_seq_TextField.setVisible(true);
        kdtMeetingManSubjectDeliberationEntry_seq_TextField.setEditable(true);
        kdtMeetingManSubjectDeliberationEntry_seq_TextField.setHorizontalAlignment(2);
        kdtMeetingManSubjectDeliberationEntry_seq_TextField.setDataType(0);
        KDTDefaultCellEditor kdtMeetingManSubjectDeliberationEntry_seq_CellEditor = new KDTDefaultCellEditor(kdtMeetingManSubjectDeliberationEntry_seq_TextField);
        this.kdtMeetingManSubjectDeliberationEntry.getColumn("seq").setEditor(kdtMeetingManSubjectDeliberationEntry_seq_CellEditor);
        KDTextArea kdtMeetingManSubjectDeliberationEntry_remark_TextArea = new KDTextArea();
        kdtMeetingManSubjectDeliberationEntry_remark_TextArea.setName("kdtMeetingManSubjectDeliberationEntry_remark_TextArea");
        kdtMeetingManSubjectDeliberationEntry_remark_TextArea.setMaxLength(2000);
        KDTDefaultCellEditor kdtMeetingManSubjectDeliberationEntry_remark_CellEditor = new KDTDefaultCellEditor(kdtMeetingManSubjectDeliberationEntry_remark_TextArea);
        this.kdtMeetingManSubjectDeliberationEntry.getColumn("remark").setEditor(kdtMeetingManSubjectDeliberationEntry_remark_CellEditor);
        final KDBizPromptBox kdtMeetingManSubjectDeliberationEntry_deliberator_PromptBox = new KDBizPromptBox();
        kdtMeetingManSubjectDeliberationEntry_deliberator_PromptBox.setQueryInfo("com.kingdee.eas.basedata.person.app.PersonQuery");
        kdtMeetingManSubjectDeliberationEntry_deliberator_PromptBox.setVisible(true);
        kdtMeetingManSubjectDeliberationEntry_deliberator_PromptBox.setEditable(true);
        kdtMeetingManSubjectDeliberationEntry_deliberator_PromptBox.setDisplayFormat("$number$");
        kdtMeetingManSubjectDeliberationEntry_deliberator_PromptBox.setEditFormat("$number$");
        kdtMeetingManSubjectDeliberationEntry_deliberator_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtMeetingManSubjectDeliberationEntry_deliberator_CellEditor = new KDTDefaultCellEditor(kdtMeetingManSubjectDeliberationEntry_deliberator_PromptBox);
        this.kdtMeetingManSubjectDeliberationEntry.getColumn("deliberator").setEditor(kdtMeetingManSubjectDeliberationEntry_deliberator_CellEditor);
        ObjectValueRender kdtMeetingManSubjectDeliberationEntry_deliberator_OVR = new ObjectValueRender();
        kdtMeetingManSubjectDeliberationEntry_deliberator_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtMeetingManSubjectDeliberationEntry.getColumn("deliberator").setRenderer(kdtMeetingManSubjectDeliberationEntry_deliberator_OVR);
        KDTextArea kdtMeetingManSubjectDeliberationEntry_deliberationResult_TextArea = new KDTextArea();
        kdtMeetingManSubjectDeliberationEntry_deliberationResult_TextArea.setName("kdtMeetingManSubjectDeliberationEntry_deliberationResult_TextArea");
        kdtMeetingManSubjectDeliberationEntry_deliberationResult_TextArea.setMaxLength(2000);
        KDTDefaultCellEditor kdtMeetingManSubjectDeliberationEntry_deliberationResult_CellEditor = new KDTDefaultCellEditor(kdtMeetingManSubjectDeliberationEntry_deliberationResult_TextArea);
        this.kdtMeetingManSubjectDeliberationEntry.getColumn("deliberationResult").setEditor(kdtMeetingManSubjectDeliberationEntry_deliberationResult_CellEditor);
        // kDPanel4
        // kdtMeetingManAttendeeEntry
		String kdtMeetingManAttendeeEntryStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:NumberFormat>&amp;int</c:NumberFormat></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"seq\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"attendee\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"1\" /><t:Column t:key=\"reason\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"2\" /><t:Column t:key=\"remark\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"3\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{seq}</t:Cell><t:Cell>$Resource{attendee}</t:Cell><t:Cell>$Resource{reason}</t:Cell><t:Cell>$Resource{remark}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtMeetingManAttendeeEntry.setFormatXml(resHelper.translateString("kdtMeetingManAttendeeEntry",kdtMeetingManAttendeeEntryStrXML));

                this.kdtMeetingManAttendeeEntry.putBindContents("editData",new String[] {"seq","attendee","reason","remark"});


        this.kdtMeetingManAttendeeEntry.checkParsed();
        KDFormattedTextField kdtMeetingManAttendeeEntry_seq_TextField = new KDFormattedTextField();
        kdtMeetingManAttendeeEntry_seq_TextField.setName("kdtMeetingManAttendeeEntry_seq_TextField");
        kdtMeetingManAttendeeEntry_seq_TextField.setVisible(true);
        kdtMeetingManAttendeeEntry_seq_TextField.setEditable(true);
        kdtMeetingManAttendeeEntry_seq_TextField.setHorizontalAlignment(2);
        kdtMeetingManAttendeeEntry_seq_TextField.setDataType(0);
        KDTDefaultCellEditor kdtMeetingManAttendeeEntry_seq_CellEditor = new KDTDefaultCellEditor(kdtMeetingManAttendeeEntry_seq_TextField);
        this.kdtMeetingManAttendeeEntry.getColumn("seq").setEditor(kdtMeetingManAttendeeEntry_seq_CellEditor);
        final KDBizPromptBox kdtMeetingManAttendeeEntry_attendee_PromptBox = new KDBizPromptBox();
        kdtMeetingManAttendeeEntry_attendee_PromptBox.setQueryInfo("com.kingdee.eas.basedata.person.app.PersonQuery");
        kdtMeetingManAttendeeEntry_attendee_PromptBox.setVisible(true);
        kdtMeetingManAttendeeEntry_attendee_PromptBox.setEditable(true);
        kdtMeetingManAttendeeEntry_attendee_PromptBox.setDisplayFormat("$number$");
        kdtMeetingManAttendeeEntry_attendee_PromptBox.setEditFormat("$number$");
        kdtMeetingManAttendeeEntry_attendee_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtMeetingManAttendeeEntry_attendee_CellEditor = new KDTDefaultCellEditor(kdtMeetingManAttendeeEntry_attendee_PromptBox);
        this.kdtMeetingManAttendeeEntry.getColumn("attendee").setEditor(kdtMeetingManAttendeeEntry_attendee_CellEditor);
        ObjectValueRender kdtMeetingManAttendeeEntry_attendee_OVR = new ObjectValueRender();
        kdtMeetingManAttendeeEntry_attendee_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtMeetingManAttendeeEntry.getColumn("attendee").setRenderer(kdtMeetingManAttendeeEntry_attendee_OVR);
        KDTextArea kdtMeetingManAttendeeEntry_reason_TextArea = new KDTextArea();
        kdtMeetingManAttendeeEntry_reason_TextArea.setName("kdtMeetingManAttendeeEntry_reason_TextArea");
        kdtMeetingManAttendeeEntry_reason_TextArea.setMaxLength(2000);
        KDTDefaultCellEditor kdtMeetingManAttendeeEntry_reason_CellEditor = new KDTDefaultCellEditor(kdtMeetingManAttendeeEntry_reason_TextArea);
        this.kdtMeetingManAttendeeEntry.getColumn("reason").setEditor(kdtMeetingManAttendeeEntry_reason_CellEditor);
        KDTextArea kdtMeetingManAttendeeEntry_remark_TextArea = new KDTextArea();
        kdtMeetingManAttendeeEntry_remark_TextArea.setName("kdtMeetingManAttendeeEntry_remark_TextArea");
        kdtMeetingManAttendeeEntry_remark_TextArea.setMaxLength(2000);
        KDTDefaultCellEditor kdtMeetingManAttendeeEntry_remark_CellEditor = new KDTDefaultCellEditor(kdtMeetingManAttendeeEntry_remark_TextArea);
        this.kdtMeetingManAttendeeEntry.getColumn("remark").setEditor(kdtMeetingManAttendeeEntry_remark_CellEditor);
        // prmtcompany		
        this.prmtcompany.setQueryInfo("com.kingdee.eas.basedata.org.app.CompanyOrgUnitQuery");		
        this.prmtcompany.setVisible(true);		
        this.prmtcompany.setEditable(true);		
        this.prmtcompany.setDisplayFormat("$name$");		
        this.prmtcompany.setEditFormat("$number$");		
        this.prmtcompany.setCommitFormat("$number$");		
        this.prmtcompany.setRequired(false);
        // txtmeetingName		
        this.txtmeetingName.setVisible(true);		
        this.txtmeetingName.setHorizontalAlignment(2);		
        this.txtmeetingName.setMaxLength(100);		
        this.txtmeetingName.setRequired(false);
        // prmtmeetingType		
        this.prmtmeetingType.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.MeetingTypeQuery");		
        this.prmtmeetingType.setVisible(true);		
        this.prmtmeetingType.setEditable(true);		
        this.prmtmeetingType.setDisplayFormat("$name$");		
        this.prmtmeetingType.setEditFormat("$number$");		
        this.prmtmeetingType.setCommitFormat("$number$");		
        this.prmtmeetingType.setRequired(false);
        		EntityViewInfo eviprmtmeetingType = new EntityViewInfo ();
		eviprmtmeetingType.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtmeetingType.setEntityViewInfo(eviprmtmeetingType);
					
        prmtmeetingType.addDataChangeListener(new DataChangeListener() {
		public void dataChanged(DataChangeEvent e) {
			try {
				prmtmeetingType_Changed();
			}
			catch (Exception exc) {
				handUIException(exc);
			}
		}
	});

        // txtcompanyNumber		
        this.txtcompanyNumber.setVisible(true);		
        this.txtcompanyNumber.setHorizontalAlignment(2);		
        this.txtcompanyNumber.setMaxLength(80);		
        this.txtcompanyNumber.setRequired(false);
        // txtmeetingTypeName		
        this.txtmeetingTypeName.setVisible(true);		
        this.txtmeetingTypeName.setHorizontalAlignment(2);		
        this.txtmeetingTypeName.setMaxLength(80);		
        this.txtmeetingTypeName.setRequired(false);
        // prmtmeetingForm		
        this.prmtmeetingForm.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.MeetingFormQuery");		
        this.prmtmeetingForm.setVisible(true);		
        this.prmtmeetingForm.setEditable(true);		
        this.prmtmeetingForm.setDisplayFormat("$name$");		
        this.prmtmeetingForm.setEditFormat("$number$");		
        this.prmtmeetingForm.setCommitFormat("$number$");		
        this.prmtmeetingForm.setRequired(false);
        		EntityViewInfo eviprmtmeetingForm = new EntityViewInfo ();
		eviprmtmeetingForm.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtmeetingForm.setEntityViewInfo(eviprmtmeetingForm);
					
        // pkmeetingTS		
        this.pkmeetingTS.setVisible(true);		
        this.pkmeetingTS.setRequired(false);
        // prmtmoderator		
        this.prmtmoderator.setQueryInfo("com.kingdee.eas.basedata.person.app.PersonFilterQuery");		
        this.prmtmoderator.setVisible(true);		
        this.prmtmoderator.setEditable(true);		
        this.prmtmoderator.setDisplayFormat("$name$");		
        this.prmtmoderator.setEditFormat("$number$");		
        this.prmtmoderator.setCommitFormat("$number$");		
        this.prmtmoderator.setRequired(false);
        // pkreleaseTS		
        this.pkreleaseTS.setVisible(true);		
        this.pkreleaseTS.setRequired(false);
        // prmtdataSource		
        this.prmtdataSource.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.DataSourceQuery");		
        this.prmtdataSource.setVisible(true);		
        this.prmtdataSource.setEditable(true);		
        this.prmtdataSource.setDisplayFormat("$name$");		
        this.prmtdataSource.setEditFormat("$number$");		
        this.prmtdataSource.setCommitFormat("$number$");		
        this.prmtdataSource.setRequired(false);
        		EntityViewInfo eviprmtdataSource = new EntityViewInfo ();
		eviprmtdataSource.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtdataSource.setEntityViewInfo(eviprmtdataSource);
					
        // prmtoperType		
        this.prmtoperType.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.OperateFlagQuery");		
        this.prmtoperType.setVisible(true);		
        this.prmtoperType.setEditable(true);		
        this.prmtoperType.setDisplayFormat("$name$");		
        this.prmtoperType.setEditFormat("$number$");		
        this.prmtoperType.setCommitFormat("$number$");		
        this.prmtoperType.setRequired(false);
        		EntityViewInfo eviprmtoperType = new EntityViewInfo ();
		eviprmtoperType.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtoperType.setEntityViewInfo(eviprmtoperType);
					
        // kDPanel1
        // kdtSubjectEntry
		String kdtSubjectEntryStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:NumberFormat>&amp;int</c:NumberFormat></c:Style><c:Style id=\"sCol2\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol4\"><c:Protection locked=\"true\" /></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"seq\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"company\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"1\" /><t:Column t:key=\"companyNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"2\" t:styleID=\"sCol2\" /><t:Column t:key=\"subject\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"3\" /><t:Column t:key=\"subjectNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"4\" t:styleID=\"sCol4\" /><t:Column t:key=\"remark\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"5\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{seq}</t:Cell><t:Cell>$Resource{company}</t:Cell><t:Cell>$Resource{companyNumber}</t:Cell><t:Cell>$Resource{subject}</t:Cell><t:Cell>$Resource{subjectNumber}</t:Cell><t:Cell>$Resource{remark}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtSubjectEntry.setFormatXml(resHelper.translateString("kdtSubjectEntry",kdtSubjectEntryStrXML));
        kdtSubjectEntry.addKDTEditListener(new KDTEditAdapter() {
		public void editStopped(KDTEditEvent e) {
			try {
				kdtSubjectEntry_Changed(e.getRowIndex(),e.getColIndex());
			}
			catch (Exception exc) {
				handUIException(exc);
			}
		}
	});


                this.kdtSubjectEntry.putBindContents("editData",new String[] {"seq","company","companyNumber","subject","subjectNumber","remark"});


        this.kdtSubjectEntry.checkParsed();
        KDFormattedTextField kdtSubjectEntry_seq_TextField = new KDFormattedTextField();
        kdtSubjectEntry_seq_TextField.setName("kdtSubjectEntry_seq_TextField");
        kdtSubjectEntry_seq_TextField.setVisible(true);
        kdtSubjectEntry_seq_TextField.setEditable(true);
        kdtSubjectEntry_seq_TextField.setHorizontalAlignment(2);
        kdtSubjectEntry_seq_TextField.setDataType(0);
        KDTDefaultCellEditor kdtSubjectEntry_seq_CellEditor = new KDTDefaultCellEditor(kdtSubjectEntry_seq_TextField);
        this.kdtSubjectEntry.getColumn("seq").setEditor(kdtSubjectEntry_seq_CellEditor);
        final KDBizPromptBox kdtSubjectEntry_company_PromptBox = new KDBizPromptBox();
        kdtSubjectEntry_company_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.CompanyOrgUnitQuery4AsstAcct");
        kdtSubjectEntry_company_PromptBox.setVisible(true);
        kdtSubjectEntry_company_PromptBox.setEditable(true);
        kdtSubjectEntry_company_PromptBox.setDisplayFormat("$number$");
        kdtSubjectEntry_company_PromptBox.setEditFormat("$number$");
        kdtSubjectEntry_company_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtSubjectEntry_company_CellEditor = new KDTDefaultCellEditor(kdtSubjectEntry_company_PromptBox);
        this.kdtSubjectEntry.getColumn("company").setEditor(kdtSubjectEntry_company_CellEditor);
        ObjectValueRender kdtSubjectEntry_company_OVR = new ObjectValueRender();
        kdtSubjectEntry_company_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtSubjectEntry.getColumn("company").setRenderer(kdtSubjectEntry_company_OVR);
        KDTextField kdtSubjectEntry_companyNumber_TextField = new KDTextField();
        kdtSubjectEntry_companyNumber_TextField.setName("kdtSubjectEntry_companyNumber_TextField");
        kdtSubjectEntry_companyNumber_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtSubjectEntry_companyNumber_CellEditor = new KDTDefaultCellEditor(kdtSubjectEntry_companyNumber_TextField);
        this.kdtSubjectEntry.getColumn("companyNumber").setEditor(kdtSubjectEntry_companyNumber_CellEditor);
        final KDBizPromptBox kdtSubjectEntry_subject_PromptBox = new KDBizPromptBox();
        kdtSubjectEntry_subject_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.bizbill.app.SubjectQuery");
        kdtSubjectEntry_subject_PromptBox.setVisible(true);
        kdtSubjectEntry_subject_PromptBox.setEditable(true);
        kdtSubjectEntry_subject_PromptBox.setDisplayFormat("$number$");
        kdtSubjectEntry_subject_PromptBox.setEditFormat("$number$");
        kdtSubjectEntry_subject_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtSubjectEntry_subject_CellEditor = new KDTDefaultCellEditor(kdtSubjectEntry_subject_PromptBox);
        this.kdtSubjectEntry.getColumn("subject").setEditor(kdtSubjectEntry_subject_CellEditor);
        ObjectValueRender kdtSubjectEntry_subject_OVR = new ObjectValueRender();
        kdtSubjectEntry_subject_OVR.setFormat(new BizDataFormat("$subjectName$"));
        this.kdtSubjectEntry.getColumn("subject").setRenderer(kdtSubjectEntry_subject_OVR);
        			EntityViewInfo evikdtSubjectEntry_subject_PromptBox = new EntityViewInfo ();
		evikdtSubjectEntry_subject_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtSubjectEntry_subject_PromptBox.setEntityViewInfo(evikdtSubjectEntry_subject_PromptBox);
					
        KDTextField kdtSubjectEntry_subjectNumber_TextField = new KDTextField();
        kdtSubjectEntry_subjectNumber_TextField.setName("kdtSubjectEntry_subjectNumber_TextField");
        kdtSubjectEntry_subjectNumber_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtSubjectEntry_subjectNumber_CellEditor = new KDTDefaultCellEditor(kdtSubjectEntry_subjectNumber_TextField);
        this.kdtSubjectEntry.getColumn("subjectNumber").setEditor(kdtSubjectEntry_subjectNumber_CellEditor);
        KDTextArea kdtSubjectEntry_remark_TextArea = new KDTextArea();
        kdtSubjectEntry_remark_TextArea.setName("kdtSubjectEntry_remark_TextArea");
        kdtSubjectEntry_remark_TextArea.setMaxLength(2000);
        KDTDefaultCellEditor kdtSubjectEntry_remark_CellEditor = new KDTDefaultCellEditor(kdtSubjectEntry_remark_TextArea);
        this.kdtSubjectEntry.getColumn("remark").setEditor(kdtSubjectEntry_remark_CellEditor);
        // state		
        this.state.setVisible(true);		
        this.state.addItems(EnumUtils.getEnumList("com.kingdee.eas.custom.tiog.bizbill.BillState").toArray());		
        this.state.setRequired(false);
        // bizMenu		
        this.bizMenu.setText(resHelper.getString("bizMenu.text"));
        // mniActionAudit
        this.mniActionAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionAudit, new Class[] { IItemAction.class }, getServiceContext()));
        // mniActionUnAudit
        this.mniActionUnAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionUnAudit, new Class[] { IItemAction.class }, getServiceContext()));
        this.setFocusTraversalPolicy(new com.kingdee.bos.ui.UIFocusTraversalPolicy(new java.awt.Component[] {txtremark,kdtMeetingManSubjectDeliberationEntry,kdtMeetingManSubjectAttendanceEntry,prmtcompany,txtcompanyNumber,txtmeetingName,prmtmeetingType,kDDateLastUpdateTime,prmtLastUpdateUser,kDDateCreateTime,prmtCreator,prmtAuditor,txtDescription,pkBizDate,txtNumber,kdtMeetingManAttendeeEntry,kdtSubjectEntry,prmtadminOrgUnit,txtmeetingTypeName,prmtmeetingForm,pkmeetingTS,prmtmoderator,pkreleaseTS,prmtdataSource,prmtoperType,state}));
        this.setFocusCycleRoot(true);
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(0, 0, 1013, 989));
        this.setLayout(new KDLayout());
        this.putClientProperty("OriginalBounds", new Rectangle(0, 0, 1013, 989));
        contCreator.setBounds(new Rectangle(337, 926, 270, 19));
        this.add(contCreator, new KDLayout.Constraints(337, 926, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contCreateTime.setBounds(new Rectangle(668, 922, 270, 19));
        this.add(contCreateTime, new KDLayout.Constraints(668, 922, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contLastUpdateUser.setBounds(new Rectangle(337, 953, 270, 19));
        this.add(contLastUpdateUser, new KDLayout.Constraints(337, 953, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contLastUpdateTime.setBounds(new Rectangle(668, 953, 270, 19));
        this.add(contLastUpdateTime, new KDLayout.Constraints(668, 953, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contNumber.setBounds(new Rectangle(34, 12, 270, 19));
        this.add(contNumber, new KDLayout.Constraints(34, 12, 270, 19, 0));
        contBizDate.setBounds(new Rectangle(660, 41, 270, 19));
        this.add(contBizDate, new KDLayout.Constraints(660, 41, 270, 19, 0));
        contDescription.setBounds(new Rectangle(660, 70, 270, 19));
        this.add(contDescription, new KDLayout.Constraints(660, 70, 270, 19, 0));
        contAuditor.setBounds(new Rectangle(33, 953, 270, 19));
        this.add(contAuditor, new KDLayout.Constraints(33, 953, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contadminOrgUnit.setBounds(new Rectangle(660, 12, 270, 19));
        this.add(contadminOrgUnit, new KDLayout.Constraints(660, 12, 270, 19, 0));
        contremark.setBounds(new Rectangle(34, 197, 586, 19));
        this.add(contremark, new KDLayout.Constraints(34, 197, 586, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        kDTabbedPane2.setBounds(new Rectangle(482, 616, 470, 182));
        this.add(kDTabbedPane2, new KDLayout.Constraints(482, 616, 470, 182, 0));
        kDTabbedPane3.setBounds(new Rectangle(35, 616, 444, 182));
        this.add(kDTabbedPane3, new KDLayout.Constraints(35, 616, 444, 182, 0));
        kDTabbedPane4.setBounds(new Rectangle(34, 238, 917, 187));
        this.add(kDTabbedPane4, new KDLayout.Constraints(34, 238, 917, 187, 0));
        contcompany.setBounds(new Rectangle(346, 12, 270, 19));
        this.add(contcompany, new KDLayout.Constraints(346, 12, 270, 19, 0));
        contmeetingName.setBounds(new Rectangle(34, 41, 270, 19));
        this.add(contmeetingName, new KDLayout.Constraints(34, 41, 270, 19, 0));
        contmeetingType.setBounds(new Rectangle(34, 70, 270, 19));
        this.add(contmeetingType, new KDLayout.Constraints(34, 70, 270, 19, 0));
        contcompanyNumber.setBounds(new Rectangle(346, 41, 270, 19));
        this.add(contcompanyNumber, new KDLayout.Constraints(346, 41, 270, 19, 0));
        contmeetingTypeName.setBounds(new Rectangle(34, 99, 270, 19));
        this.add(contmeetingTypeName, new KDLayout.Constraints(34, 99, 270, 19, 0));
        contmeetingForm.setBounds(new Rectangle(34, 128, 270, 19));
        this.add(contmeetingForm, new KDLayout.Constraints(34, 128, 270, 19, 0));
        contmeetingTS.setBounds(new Rectangle(346, 70, 270, 19));
        this.add(contmeetingTS, new KDLayout.Constraints(346, 70, 270, 19, 0));
        contmoderator.setBounds(new Rectangle(34, 161, 270, 19));
        this.add(contmoderator, new KDLayout.Constraints(34, 161, 270, 19, 0));
        contreleaseTS.setBounds(new Rectangle(346, 99, 270, 19));
        this.add(contreleaseTS, new KDLayout.Constraints(346, 99, 270, 19, 0));
        contdataSource.setBounds(new Rectangle(346, 128, 270, 19));
        this.add(contdataSource, new KDLayout.Constraints(346, 128, 270, 19, 0));
        contoperType.setBounds(new Rectangle(346, 161, 270, 19));
        this.add(contoperType, new KDLayout.Constraints(346, 161, 270, 19, 0));
        kDTabbedPane1.setBounds(new Rectangle(35, 453, 917, 162));
        this.add(kDTabbedPane1, new KDLayout.Constraints(35, 453, 917, 162, 0));
        kDSeparator8.setBounds(new Rectangle(35, 896, 815, 10));
        this.add(kDSeparator8, new KDLayout.Constraints(35, 896, 815, 10, 0));
        kDWorkButton1.setBounds(new Rectangle(34, 877, 158, 19));
        this.add(kDWorkButton1, new KDLayout.Constraints(34, 877, 158, 19, 0));
        kDWorkButton2.setBounds(new Rectangle(33, 828, 158, 19));
        this.add(kDWorkButton2, new KDLayout.Constraints(33, 828, 158, 19, 0));
        kDSeparator9.setBounds(new Rectangle(35, 845, 815, 10));
        this.add(kDSeparator9, new KDLayout.Constraints(35, 845, 815, 10, 0));
        contstate.setBounds(new Rectangle(0, 0, 270, 19));
        this.add(contstate, new KDLayout.Constraints(0, 0, 270, 19, 0));
        //contCreator
        contCreator.setBoundEditor(prmtCreator);
        //contCreateTime
        contCreateTime.setBoundEditor(kDDateCreateTime);
        //contLastUpdateUser
        contLastUpdateUser.setBoundEditor(prmtLastUpdateUser);
        //contLastUpdateTime
        contLastUpdateTime.setBoundEditor(kDDateLastUpdateTime);
        //contNumber
        contNumber.setBoundEditor(txtNumber);
        //contBizDate
        contBizDate.setBoundEditor(pkBizDate);
        //contDescription
        contDescription.setBoundEditor(txtDescription);
        //contAuditor
        contAuditor.setBoundEditor(prmtAuditor);
        //contadminOrgUnit
        contadminOrgUnit.setBoundEditor(prmtadminOrgUnit);
        //contremark
        contremark.setBoundEditor(scrollPaneremark);
        //scrollPaneremark
        scrollPaneremark.getViewport().add(txtremark, null);
        //kDTabbedPane2
        kDTabbedPane2.add(kDPanel2, resHelper.getString("kDPanel2.constraints"));
        //kDPanel2
        kDPanel2.setLayout(new KDLayout());
        kDPanel2.putClientProperty("OriginalBounds", new Rectangle(0, 0, 469, 149));        kdtMeetingManSubjectAttendanceEntry.setBounds(new Rectangle(2, 1, 419, 144));
        kdtMeetingManSubjectAttendanceEntry_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtMeetingManSubjectAttendanceEntry,new com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo(),null,false);
        kDPanel2.add(kdtMeetingManSubjectAttendanceEntry_detailPanel, new KDLayout.Constraints(2, 1, 419, 144, 0));
        //kDTabbedPane3
        kDTabbedPane3.add(kDPanel3, resHelper.getString("kDPanel3.constraints"));
        //kDPanel3
        kDPanel3.setLayout(new KDLayout());
        kDPanel3.putClientProperty("OriginalBounds", new Rectangle(0, 0, 443, 149));        kdtMeetingManSubjectDeliberationEntry.setBounds(new Rectangle(0, 2, 428, 143));
        kdtMeetingManSubjectDeliberationEntry_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtMeetingManSubjectDeliberationEntry,new com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo(),null,false);
        kDPanel3.add(kdtMeetingManSubjectDeliberationEntry_detailPanel, new KDLayout.Constraints(0, 2, 428, 143, 0));
        //kDTabbedPane4
        kDTabbedPane4.add(kDPanel4, resHelper.getString("kDPanel4.constraints"));
        //kDPanel4
        kDPanel4.setLayout(new KDLayout());
        kDPanel4.putClientProperty("OriginalBounds", new Rectangle(0, 0, 916, 154));        kdtMeetingManAttendeeEntry.setBounds(new Rectangle(-2, 1, 897, 148));
        kdtMeetingManAttendeeEntry_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtMeetingManAttendeeEntry,new com.kingdee.eas.custom.tiog.bizbill.MeetingManMeetingManAttendeeEntryInfo(),null,false);
        kDPanel4.add(kdtMeetingManAttendeeEntry_detailPanel, new KDLayout.Constraints(-2, 1, 897, 148, 0));
        //contcompany
        contcompany.setBoundEditor(prmtcompany);
        //contmeetingName
        contmeetingName.setBoundEditor(txtmeetingName);
        //contmeetingType
        contmeetingType.setBoundEditor(prmtmeetingType);
        //contcompanyNumber
        contcompanyNumber.setBoundEditor(txtcompanyNumber);
        //contmeetingTypeName
        contmeetingTypeName.setBoundEditor(txtmeetingTypeName);
        //contmeetingForm
        contmeetingForm.setBoundEditor(prmtmeetingForm);
        //contmeetingTS
        contmeetingTS.setBoundEditor(pkmeetingTS);
        //contmoderator
        contmoderator.setBoundEditor(prmtmoderator);
        //contreleaseTS
        contreleaseTS.setBoundEditor(pkreleaseTS);
        //contdataSource
        contdataSource.setBoundEditor(prmtdataSource);
        //contoperType
        contoperType.setBoundEditor(prmtoperType);
        //kDTabbedPane1
        kDTabbedPane1.add(kDPanel1, resHelper.getString("kDPanel1.constraints"));
        //kDPanel1
        kDPanel1.setLayout(new KDLayout());
        kDPanel1.putClientProperty("OriginalBounds", new Rectangle(0, 0, 916, 129));        kdtSubjectEntry.setBounds(new Rectangle(-2, -2, 895, 164));
        kdtSubjectEntry_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtSubjectEntry,new com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo(),null,false);
        kDPanel1.add(kdtSubjectEntry_detailPanel, new KDLayout.Constraints(-2, -2, 895, 164, 0));
        //contstate
        contstate.setBoundEditor(state);

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {
        this.menuBar.add(menuFile);
        this.menuBar.add(bizMenu);
        this.menuBar.add(menuEdit);
        this.menuBar.add(MenuService);
        this.menuBar.add(menuView);
        this.menuBar.add(menuBiz);
        this.menuBar.add(menuTable1);
        this.menuBar.add(menuTool);
        this.menuBar.add(menuWorkflow);
        this.menuBar.add(menuHelp);
        //menuFile
        menuFile.add(menuItemAddNew);
        menuFile.add(kDSeparator1);
        menuFile.add(menuItemCloudFeed);
        menuFile.add(menuItemSave);
        menuFile.add(menuItemCloudScreen);
        menuFile.add(menuItemSubmit);
        menuFile.add(menuItemCloudShare);
        menuFile.add(menuSubmitOption);
        menuFile.add(kdSeparatorFWFile1);
        menuFile.add(rMenuItemSubmit);
        menuFile.add(rMenuItemSubmitAndAddNew);
        menuFile.add(rMenuItemSubmitAndPrint);
        menuFile.add(separatorFile1);
        menuFile.add(MenuItemAttachment);
        menuFile.add(kDSeparator2);
        menuFile.add(menuItemPageSetup);
        menuFile.add(menuItemPrint);
        menuFile.add(menuItemPrintPreview);
        menuFile.add(kDSeparator6);
        menuFile.add(menuItemSendMail);
        menuFile.add(kDSeparator3);
        menuFile.add(menuItemExitCurrent);
        //menuSubmitOption
        menuSubmitOption.add(chkMenuItemSubmitAndAddNew);
        menuSubmitOption.add(chkMenuItemSubmitAndPrint);
        //menuEdit
        menuEdit.add(menuItemCopy);
        menuEdit.add(menuItemEdit);
        menuEdit.add(menuItemRemove);
        menuEdit.add(kDSeparator4);
        menuEdit.add(menuItemReset);
        menuEdit.add(separator1);
        menuEdit.add(menuItemCreateFrom);
        menuEdit.add(menuItemCreateTo);
        menuEdit.add(menuItemCopyFrom);
        menuEdit.add(separatorEdit1);
        menuEdit.add(menuItemEnterToNextRow);
        menuEdit.add(separator2);
        //MenuService
        MenuService.add(MenuItemKnowStore);
        MenuService.add(MenuItemAnwser);
        MenuService.add(SepratorService);
        MenuService.add(MenuItemRemoteAssist);
        //menuView
        menuView.add(menuItemFirst);
        menuView.add(menuItemPre);
        menuView.add(menuItemNext);
        menuView.add(menuItemLast);
        menuView.add(separator3);
        menuView.add(menuItemTraceUp);
        menuView.add(menuItemTraceDown);
        menuView.add(kDSeparator7);
        menuView.add(menuItemLocate);
        //menuBiz
        menuBiz.add(menuItemCancelCancel);
        menuBiz.add(menuItemCancel);
        menuBiz.add(MenuItemVoucher);
        menuBiz.add(menuItemDelVoucher);
        menuBiz.add(MenuItemPCVoucher);
        menuBiz.add(menuItemDelPCVoucher);
        //menuTable1
        menuTable1.add(menuItemAddLine);
        menuTable1.add(menuItemCopyLine);
        menuTable1.add(menuItemInsertLine);
        menuTable1.add(menuItemRemoveLine);
        //menuTool
        menuTool.add(menuItemSendMessage);
        menuTool.add(menuItemMsgFormat);
        menuTool.add(menuItemCalculator);
        menuTool.add(menuItemToolBarCustom);
        //menuWorkflow
        menuWorkflow.add(menuItemStartWorkFlow);
        menuWorkflow.add(separatorWF1);
        menuWorkflow.add(menuItemViewSubmitProccess);
        menuWorkflow.add(menuItemViewDoProccess);
        menuWorkflow.add(MenuItemWFG);
        menuWorkflow.add(menuItemWorkFlowList);
        menuWorkflow.add(separatorWF2);
        menuWorkflow.add(menuItemMultiapprove);
        menuWorkflow.add(menuItemNextPerson);
        menuWorkflow.add(menuItemAuditResult);
        menuWorkflow.add(kDSeparator5);
        menuWorkflow.add(kDMenuItemSendMessage);
        //menuHelp
        menuHelp.add(menuItemHelp);
        menuHelp.add(kDSeparator12);
        menuHelp.add(menuItemRegPro);
        menuHelp.add(menuItemPersonalSite);
        menuHelp.add(helpseparatorDiv);
        menuHelp.add(menuitemProductval);
        menuHelp.add(kDSeparatorProduct);
        menuHelp.add(menuItemAbout);

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {
        this.toolBar.add(btnAddNew);
        this.toolBar.add(btnCloud);
        this.toolBar.add(btnEdit);
        this.toolBar.add(btnXunTong);
        this.toolBar.add(btnSave);
        this.toolBar.add(kDSeparatorCloud);
        this.toolBar.add(btnReset);
        this.toolBar.add(btnSubmit);
        this.toolBar.add(btnCopy);
        this.toolBar.add(btnRemove);
        this.toolBar.add(btnCancelCancel);
        this.toolBar.add(btnCancel);
        this.toolBar.add(btnAttachment);
        this.toolBar.add(separatorFW1);
        this.toolBar.add(btnPageSetup);
        this.toolBar.add(btnPrint);
        this.toolBar.add(btnPrintPreview);
        this.toolBar.add(separatorFW2);
        this.toolBar.add(btnFirst);
        this.toolBar.add(btnPre);
        this.toolBar.add(btnNext);
        this.toolBar.add(btnLast);
        this.toolBar.add(separatorFW3);
        this.toolBar.add(btnTraceUp);
        this.toolBar.add(btnTraceDown);
        this.toolBar.add(btnWorkFlowG);
        this.toolBar.add(btnSignature);
        this.toolBar.add(btnViewSignature);
        this.toolBar.add(separatorFW4);
        this.toolBar.add(btnNumberSign);
        this.toolBar.add(separatorFW7);
        this.toolBar.add(btnCreateFrom);
        this.toolBar.add(btnCopyFrom);
        this.toolBar.add(btnCreateTo);
        this.toolBar.add(separatorFW5);
        this.toolBar.add(separatorFW8);
        this.toolBar.add(btnAddLine);
        this.toolBar.add(btnCopyLine);
        this.toolBar.add(btnInsertLine);
        this.toolBar.add(btnRemoveLine);
        this.toolBar.add(separatorFW6);
        this.toolBar.add(separatorFW9);
        this.toolBar.add(btnVoucher);
        this.toolBar.add(btnDelVoucher);
        this.toolBar.add(btnPCVoucher);
        this.toolBar.add(btnDelPCVoucher);
        this.toolBar.add(btnAuditResult);
        this.toolBar.add(btnMultiapprove);
        this.toolBar.add(btnWFViewdoProccess);
        this.toolBar.add(btnWFViewSubmitProccess);
        this.toolBar.add(btnNextPerson);


    }

	//Regiester control's property binding.
	private void registerBindings(){
		dataBinder.registerBinding("creator", com.kingdee.eas.base.permission.UserInfo.class, this.prmtCreator, "data");
		dataBinder.registerBinding("createTime", java.sql.Timestamp.class, this.kDDateCreateTime, "value");
		dataBinder.registerBinding("lastUpdateUser", com.kingdee.eas.base.permission.UserInfo.class, this.prmtLastUpdateUser, "data");
		dataBinder.registerBinding("lastUpdateTime", java.sql.Timestamp.class, this.kDDateLastUpdateTime, "value");
		dataBinder.registerBinding("number", String.class, this.txtNumber, "text");
		dataBinder.registerBinding("bizDate", java.util.Date.class, this.pkBizDate, "value");
		dataBinder.registerBinding("description", String.class, this.txtDescription, "text");
		dataBinder.registerBinding("auditor", com.kingdee.eas.base.permission.UserInfo.class, this.prmtAuditor, "data");
		dataBinder.registerBinding("adminOrgUnit", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.prmtadminOrgUnit, "data");
		dataBinder.registerBinding("remark", String.class, this.txtremark, "text");
		dataBinder.registerBinding("SubjectEntry.MeetingManSubjectAttendanceEntry.seq", int.class, this.kdtMeetingManSubjectAttendanceEntry, "seq.text");
		dataBinder.registerBinding("SubjectEntry.MeetingManSubjectAttendanceEntry", com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo.class, this.kdtMeetingManSubjectAttendanceEntry, "userObject");
		dataBinder.registerBinding("SubjectEntry.MeetingManSubjectAttendanceEntry.remark", String.class, this.kdtMeetingManSubjectAttendanceEntry, "remark.text");
		dataBinder.registerBinding("SubjectEntry.MeetingManSubjectAttendanceEntry.attendance", java.lang.Object.class, this.kdtMeetingManSubjectAttendanceEntry, "attendance.text");
		dataBinder.registerBinding("SubjectEntry.MeetingManSubjectAttendanceEntry.attendancePos", java.lang.Object.class, this.kdtMeetingManSubjectAttendanceEntry, "attendancePos.text");
		dataBinder.registerBinding("SubjectEntry.MeetingManSubjectDeliberationEntry.seq", int.class, this.kdtMeetingManSubjectDeliberationEntry, "seq.text");
		dataBinder.registerBinding("SubjectEntry.MeetingManSubjectDeliberationEntry", com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo.class, this.kdtMeetingManSubjectDeliberationEntry, "userObject");
		dataBinder.registerBinding("SubjectEntry.MeetingManSubjectDeliberationEntry.remark", String.class, this.kdtMeetingManSubjectDeliberationEntry, "remark.text");
		dataBinder.registerBinding("SubjectEntry.MeetingManSubjectDeliberationEntry.deliberator", java.lang.Object.class, this.kdtMeetingManSubjectDeliberationEntry, "deliberator.text");
		dataBinder.registerBinding("SubjectEntry.MeetingManSubjectDeliberationEntry.deliberationResult", String.class, this.kdtMeetingManSubjectDeliberationEntry, "deliberationResult.text");
		dataBinder.registerBinding("MeetingManAttendeeEntry.seq", int.class, this.kdtMeetingManAttendeeEntry, "seq.text");
		dataBinder.registerBinding("MeetingManAttendeeEntry", com.kingdee.eas.custom.tiog.bizbill.MeetingManMeetingManAttendeeEntryInfo.class, this.kdtMeetingManAttendeeEntry, "userObject");
		dataBinder.registerBinding("MeetingManAttendeeEntry.remark", String.class, this.kdtMeetingManAttendeeEntry, "remark.text");
		dataBinder.registerBinding("MeetingManAttendeeEntry.attendee", java.lang.Object.class, this.kdtMeetingManAttendeeEntry, "attendee.text");
		dataBinder.registerBinding("MeetingManAttendeeEntry.reason", String.class, this.kdtMeetingManAttendeeEntry, "reason.text");
		dataBinder.registerBinding("company", com.kingdee.eas.basedata.org.CompanyOrgUnitInfo.class, this.prmtcompany, "data");
		dataBinder.registerBinding("meetingName", String.class, this.txtmeetingName, "text");
		dataBinder.registerBinding("meetingType", com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo.class, this.prmtmeetingType, "data");
		dataBinder.registerBinding("companyNumber", String.class, this.txtcompanyNumber, "text");
		dataBinder.registerBinding("meetingTypeName", String.class, this.txtmeetingTypeName, "text");
		dataBinder.registerBinding("meetingForm", com.kingdee.eas.custom.tiog.basedata.MeetingFormInfo.class, this.prmtmeetingForm, "data");
		dataBinder.registerBinding("meetingTS", java.util.Date.class, this.pkmeetingTS, "value");
		dataBinder.registerBinding("moderator", com.kingdee.eas.basedata.person.PersonInfo.class, this.prmtmoderator, "data");
		dataBinder.registerBinding("releaseTS", java.util.Date.class, this.pkreleaseTS, "value");
		dataBinder.registerBinding("dataSource", com.kingdee.eas.custom.tiog.basedata.DataSourceInfo.class, this.prmtdataSource, "data");
		dataBinder.registerBinding("operType", com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo.class, this.prmtoperType, "data");
		dataBinder.registerBinding("SubjectEntry.seq", int.class, this.kdtSubjectEntry, "seq.text");
		dataBinder.registerBinding("SubjectEntry", com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo.class, this.kdtSubjectEntry, "userObject");
		dataBinder.registerBinding("SubjectEntry.remark", String.class, this.kdtSubjectEntry, "remark.text");
		dataBinder.registerBinding("SubjectEntry.company", java.lang.Object.class, this.kdtSubjectEntry, "company.text");
		dataBinder.registerBinding("SubjectEntry.companyNumber", String.class, this.kdtSubjectEntry, "companyNumber.text");
		dataBinder.registerBinding("SubjectEntry.subject", java.lang.Object.class, this.kdtSubjectEntry, "subject.text");
		dataBinder.registerBinding("SubjectEntry.subjectNumber", String.class, this.kdtSubjectEntry, "subjectNumber.text");
		dataBinder.registerBinding("state", com.kingdee.eas.custom.tiog.bizbill.BillState.class, this.state, "selectedItem");		
	}
	//Regiester UI State
	private void registerUIState(){		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.tiog.bizbill.app.MeetingManEditUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }


    /**
     * output onShow method
     */
    public void onShow() throws Exception
    {
        super.onShow();
        this.txtremark.requestFocusInWindow();
    }

	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
        this.editData = (com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo)ov;
    }
    protected void removeByPK(IObjectPK pk) throws Exception {
    	IObjectValue editData = this.editData;
    	super.removeByPK(pk);
    	recycleNumberByOrg(editData,"Admin",editData.getString("number"));
    }
    
    protected void recycleNumberByOrg(IObjectValue editData,String orgType,String number) {
        if (!StringUtils.isEmpty(number))
        {
            try {
            	String companyID = null;            
            	com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID =com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}				
				if (!StringUtils.isEmpty(companyID) && iCodingRuleManager.isExist(editData, companyID) && iCodingRuleManager.isUseIntermitNumber(editData, companyID)) {
					iCodingRuleManager.recycleNumber(editData,companyID,number);					
				}
            }
            catch (Exception e)
            {
                handUIException(e);
            }
        }
    }
    protected void setAutoNumberByOrg(String orgType) {
    	if (editData == null) return;
		if (editData.getNumber() == null) {
            try {
            	String companyID = null;
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID = com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}
				com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
		        if (iCodingRuleManager.isExist(editData, companyID)) {
		            if (iCodingRuleManager.isAddView(editData, companyID)) {
		            	editData.setNumber(iCodingRuleManager.getNumber(editData,companyID));
		            }
	                txtNumber.setEnabled(false);
		        }
            }
            catch (Exception e) {
                handUIException(e);
                this.oldData = editData;
                com.kingdee.eas.util.SysUtil.abort();
            } 
        } 
        else {
            if (editData.getNumber().trim().length() > 0) {
                txtNumber.setText(editData.getNumber());
            }
        }
    }
			protected com.kingdee.eas.basedata.org.OrgType getMainBizOrgType() {
			return com.kingdee.eas.basedata.org.OrgType.getEnum("Admin");
		}

	protected KDBizPromptBox getMainBizOrg() {
		return prmtadminOrgUnit;
}


    /**
     * output loadFields method
     */
    public void loadFields()
    {
        		setAutoNumberByOrg("Admin");
        dataBinder.loadFields();
    }
		protected void setOrgF7(KDBizPromptBox f7,com.kingdee.eas.basedata.org.OrgType orgType) throws Exception
		{
			com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer oufip = new com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer(orgType);
			oufip.getModel().setIsCUFilter(true);
			f7.setFilterInfoProducer(oufip);
		}

    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );
		getValidateHelper().registerBindProperty("creator", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("createTime", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("lastUpdateUser", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("lastUpdateTime", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("bizDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("auditor", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("adminOrgUnit", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("remark", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.MeetingManSubjectAttendanceEntry.seq", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.MeetingManSubjectAttendanceEntry", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.MeetingManSubjectAttendanceEntry.remark", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.MeetingManSubjectAttendanceEntry.attendance", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.MeetingManSubjectAttendanceEntry.attendancePos", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.MeetingManSubjectDeliberationEntry.seq", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.MeetingManSubjectDeliberationEntry", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.MeetingManSubjectDeliberationEntry.remark", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.MeetingManSubjectDeliberationEntry.deliberator", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.MeetingManSubjectDeliberationEntry.deliberationResult", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("MeetingManAttendeeEntry.seq", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("MeetingManAttendeeEntry", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("MeetingManAttendeeEntry.remark", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("MeetingManAttendeeEntry.attendee", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("MeetingManAttendeeEntry.reason", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("company", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("meetingName", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("meetingType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("companyNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("meetingTypeName", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("meetingForm", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("meetingTS", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("moderator", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("releaseTS", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("dataSource", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("operType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.seq", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.remark", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.company", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.companyNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.subject", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("SubjectEntry.subjectNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("state", ValidateHelper.ON_SAVE);    		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
        if (STATUS_ADDNEW.equals(this.oprtState)) {
        } else if (STATUS_EDIT.equals(this.oprtState)) {
        } else if (STATUS_VIEW.equals(this.oprtState)) {
        } else if (STATUS_FINDVIEW.equals(this.oprtState)) {
        }
    }


    /**
     * output prmtadminOrgUnit_Changed() method
     */
    public void prmtadminOrgUnit_Changed() throws Exception
    {
        System.out.println("prmtadminOrgUnit_Changed() Function is executed!");
            txtcompanyNumber.setText(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)prmtadminOrgUnit.getData(),"orgCode")));


    }

    /**
     * output prmtmeetingType_Changed() method
     */
    public void prmtmeetingType_Changed() throws Exception
    {
        System.out.println("prmtmeetingType_Changed() Function is executed!");
            txtmeetingTypeName.setText(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)prmtmeetingType.getData(),"number")));


    }

    /**
     * output kdtSubjectEntry_Changed(int rowIndex,int colIndex) method
     */
    public void kdtSubjectEntry_Changed(int rowIndex,int colIndex) throws Exception
    {
            if ("company".equalsIgnoreCase(kdtSubjectEntry.getColumn(colIndex).getKey())) {
kdtSubjectEntry.getCell(rowIndex,"companyNumber").setValue(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)kdtSubjectEntry.getCell(rowIndex,"company").getValue(),"number")));

}

    if ("subject".equalsIgnoreCase(kdtSubjectEntry.getColumn(colIndex).getKey())) {
kdtSubjectEntry.getCell(rowIndex,"subjectNumber").setValue(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)kdtSubjectEntry.getCell(rowIndex,"subject").getValue(),"number")));

}


    }
    /**
     * output getSelectors method
     */
    public SelectorItemCollection getSelectors()
    {
        SelectorItemCollection sic = new SelectorItemCollection();
		String selectorAll = System.getProperty("selector.all");
		if(StringUtils.isEmpty(selectorAll)){
			selectorAll = "true";
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("creator.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("creator.id"));
        	sic.add(new SelectorItemInfo("creator.number"));
        	sic.add(new SelectorItemInfo("creator.name"));
		}
        sic.add(new SelectorItemInfo("createTime"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("lastUpdateUser.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("lastUpdateUser.id"));
        	sic.add(new SelectorItemInfo("lastUpdateUser.number"));
        	sic.add(new SelectorItemInfo("lastUpdateUser.name"));
		}
        sic.add(new SelectorItemInfo("lastUpdateTime"));
        sic.add(new SelectorItemInfo("number"));
        sic.add(new SelectorItemInfo("bizDate"));
        sic.add(new SelectorItemInfo("description"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("auditor.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("auditor.id"));
        	sic.add(new SelectorItemInfo("auditor.number"));
        	sic.add(new SelectorItemInfo("auditor.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("adminOrgUnit.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("adminOrgUnit.id"));
        	sic.add(new SelectorItemInfo("adminOrgUnit.number"));
        	sic.add(new SelectorItemInfo("adminOrgUnit.name"));
		}
        sic.add(new SelectorItemInfo("remark"));
    	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.seq"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.id"));
		}
    	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.remark"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.attendance.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.attendance.id"));
			sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.attendance.name"));
        	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.attendance.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.attendancePos.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.attendancePos.id"));
			sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.attendancePos.name"));
        	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectAttendanceEntry.attendancePos.number"));
		}
    	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectDeliberationEntry.seq"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectDeliberationEntry.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectDeliberationEntry.id"));
		}
    	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectDeliberationEntry.remark"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectDeliberationEntry.deliberator.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectDeliberationEntry.deliberator.id"));
			sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectDeliberationEntry.deliberator.name"));
        	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectDeliberationEntry.deliberator.number"));
		}
    	sic.add(new SelectorItemInfo("SubjectEntry.MeetingManSubjectDeliberationEntry.deliberationResult"));
    	sic.add(new SelectorItemInfo("MeetingManAttendeeEntry.seq"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("MeetingManAttendeeEntry.*"));
		}
		else{
		}
    	sic.add(new SelectorItemInfo("MeetingManAttendeeEntry.remark"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("MeetingManAttendeeEntry.attendee.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("MeetingManAttendeeEntry.attendee.id"));
			sic.add(new SelectorItemInfo("MeetingManAttendeeEntry.attendee.name"));
        	sic.add(new SelectorItemInfo("MeetingManAttendeeEntry.attendee.number"));
		}
    	sic.add(new SelectorItemInfo("MeetingManAttendeeEntry.reason"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("company.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("company.id"));
        	sic.add(new SelectorItemInfo("company.number"));
        	sic.add(new SelectorItemInfo("company.name"));
		}
        sic.add(new SelectorItemInfo("meetingName"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("meetingType.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("meetingType.id"));
        	sic.add(new SelectorItemInfo("meetingType.number"));
        	sic.add(new SelectorItemInfo("meetingType.name"));
		}
        sic.add(new SelectorItemInfo("companyNumber"));
        sic.add(new SelectorItemInfo("meetingTypeName"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("meetingForm.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("meetingForm.id"));
        	sic.add(new SelectorItemInfo("meetingForm.number"));
        	sic.add(new SelectorItemInfo("meetingForm.name"));
		}
        sic.add(new SelectorItemInfo("meetingTS"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("moderator.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("moderator.id"));
        	sic.add(new SelectorItemInfo("moderator.number"));
        	sic.add(new SelectorItemInfo("moderator.name"));
		}
        sic.add(new SelectorItemInfo("releaseTS"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("dataSource.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("dataSource.id"));
        	sic.add(new SelectorItemInfo("dataSource.number"));
        	sic.add(new SelectorItemInfo("dataSource.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("operType.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("operType.id"));
        	sic.add(new SelectorItemInfo("operType.number"));
        	sic.add(new SelectorItemInfo("operType.name"));
		}
    	sic.add(new SelectorItemInfo("SubjectEntry.seq"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("SubjectEntry.*"));
		}
		else{
		}
    	sic.add(new SelectorItemInfo("SubjectEntry.remark"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("SubjectEntry.company.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("SubjectEntry.company.id"));
			sic.add(new SelectorItemInfo("SubjectEntry.company.name"));
        	sic.add(new SelectorItemInfo("SubjectEntry.company.number"));
		}
    	sic.add(new SelectorItemInfo("SubjectEntry.companyNumber"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("SubjectEntry.subject.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("SubjectEntry.subject.id"));
			sic.add(new SelectorItemInfo("SubjectEntry.subject.subjectName"));
        	sic.add(new SelectorItemInfo("SubjectEntry.subject.number"));
		}
    	sic.add(new SelectorItemInfo("SubjectEntry.subjectNumber"));
        sic.add(new SelectorItemInfo("state"));
        return sic;
    }        
    	

    /**
     * output actionSubmit_actionPerformed method
     */
    public void actionSubmit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionSubmit_actionPerformed(e);
    }
    	

    /**
     * output actionPrint_actionPerformed method
     */
    public void actionPrint_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
    	if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.print(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionPrintPreview_actionPerformed method
     */
    public void actionPrintPreview_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
        if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.printPreview(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionAudit_actionPerformed method
     */
    public void actionAudit_actionPerformed(ActionEvent e) throws Exception
    {
actionAudit.setVisible(true);

        com.kingdee.eas.custom.tiog.bizbill.MeetingManFactory.getRemoteInstance().audit(editData);
    }
    	

    /**
     * output actionUnAudit_actionPerformed method
     */
    public void actionUnAudit_actionPerformed(ActionEvent e) throws Exception
    {
actionUnAudit.setVisible(true);

        com.kingdee.eas.custom.tiog.bizbill.MeetingManFactory.getRemoteInstance().unAudit(editData);
    }
	public RequestContext prepareActionSubmit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionSubmit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionSubmit() {
    	return false;
    }
	public RequestContext prepareActionPrint(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrint(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrint() {
    	return false;
    }
	public RequestContext prepareActionPrintPreview(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrintPreview(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrintPreview() {
    	return false;
    }
	public RequestContext prepareActionAudit(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionAudit() {
    	return false;
    }
	public RequestContext prepareActionUnAudit(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionUnAudit() {
    	return false;
    }

    /**
     * output ActionAudit class
     */     
    protected class ActionAudit extends ItemAction {     
    
        public ActionAudit()
        {
            this(null);
        }

        public ActionAudit(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            _tempStr = resHelper.getString("ActionAudit.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAudit.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAudit.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractMeetingManEditUI.this, "ActionAudit", "actionAudit_actionPerformed", e);
        }
    }

    /**
     * output ActionUnAudit class
     */     
    protected class ActionUnAudit extends ItemAction {     
    
        public ActionUnAudit()
        {
            this(null);
        }

        public ActionUnAudit(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            _tempStr = resHelper.getString("ActionUnAudit.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionUnAudit.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionUnAudit.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractMeetingManEditUI.this, "ActionUnAudit", "actionUnAudit_actionPerformed", e);
        }
    }

    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.tiog.bizbill.client", "MeetingManEditUI");
    }
    /**
     * output isBindWorkFlow method
     */
    public boolean isBindWorkFlow()
    {
        return true;
    }

    /**
     * output getEditUIName method
     */
    protected String getEditUIName()
    {
        return com.kingdee.eas.custom.tiog.bizbill.client.MeetingManEditUI.class.getName();
    }

    /**
     * output getBizInterface method
     */
    protected com.kingdee.eas.framework.ICoreBase getBizInterface() throws Exception
    {
        return com.kingdee.eas.custom.tiog.bizbill.MeetingManFactory.getRemoteInstance();
    }

    /**
     * output createNewData method
     */
    protected IObjectValue createNewData()
    {
        com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo objectValue = new com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo();
				if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin")) != null)
			objectValue.put("adminOrgUnit",com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin")));
 
        objectValue.setCreator((com.kingdee.eas.base.permission.UserInfo)(com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentUser()));		
        return objectValue;
    }


    	protected String getTDFileName() {
    	return "/bim/custom/tiog/bizbill/MeetingMan";
	}
    protected IMetaDataPK getTDQueryPK() {
    	return new MetaDataPK("com.kingdee.eas.custom.tiog.bizbill.app.MeetingManQuery");
	}
    

    /**
     * output getDetailTable method
     */
    protected KDTable getDetailTable() {
        return kdtMeetingManSubjectAttendanceEntry;
	}
    /**
     * output applyDefaultValue method
     */
    protected void applyDefaultValue(IObjectValue vo) {        
		vo.put("state","1");
        
    }        
	protected void setFieldsNull(com.kingdee.bos.dao.AbstractObjectValue arg0) {
		super.setFieldsNull(arg0);
		arg0.put("number",null);
	}

}