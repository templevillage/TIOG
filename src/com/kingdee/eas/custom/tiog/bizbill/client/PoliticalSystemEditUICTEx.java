package com.kingdee.eas.custom.tiog.bizbill.client;

import java.awt.event.ActionEvent;

import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryCollection;
import com.kingdee.eas.custom.tiog.utils.MsgBoxUtil;
import com.kingdee.eas.custom.tiog.utils.SubjectBillUtil;
import com.kingdee.eas.custom.tiog.utils.UIWindowUtil;

/**
 * 
 * @title PoliticalSystemEditUICTEx
 * @description 决策制度编辑界面
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-30
 */
public class PoliticalSystemEditUICTEx extends PoliticalSystemEditUI {

    public PoliticalSystemEditUICTEx() throws Exception {
        super();
    }

    @Override
    public void onLoad() throws Exception {
        super.onLoad();
        this.genSubject.setEnabled(true);
        this.genSubject.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgLight_green"));
    }

    /**
     * （非 Javadoc）
     * 
     * @title actionGenSubject_actionPerformed
     * @description 下推议题
     * @param e
     * @throws Exception
     * @see com.kingdee.eas.custom.tiog.bizbill.client.AbstractPoliticalSystemEditUI#actionGenSubject_actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionGenSubject_actionPerformed(ActionEvent e) throws Exception {

        String billState = this.editData.getState().getValue();
        if (!"5".equals(billState)) {// 是否已审核单据
            MsgBoxUtil.showWarning("该单据未审核，不能生成会议！");
            return;
        }

        BOSUuid id = this.editData.getId();
        String number = this.editData.getNumber();
        BOSUuid meetTypeId = (this.editData.getMeetNum() != null) ? this.editData.getMeetNum().getId() : null; // 会议类型
        BOSUuid companyId = (this.editData.getCompany() != null) ? this.editData.getCompany().getId() : null; // 企业
        PoliticalSystemEntryCollection entrys = this.editData.getEntrys();// 分录

        if (meetTypeId == null) {
            throw new EASBizException(EASBizException.CHECKBLANK, new Object[] { "会议类型" });
        }

        if (companyId == null) {
            throw new EASBizException(EASBizException.CHECKBLANK, new Object[] { "企业" });
        }

        IObjectPK pk = SubjectBillUtil.addNewSubjectBill(meetTypeId, companyId, entrys);

        // 弹出已生成会议决策单据界面
        String uiName = "com.kingdee.eas.custom.tiog.bizbill.client.SubjectEditUI";
        UIWindowUtil.showAndEdit(uiName, pk);
    }

}
