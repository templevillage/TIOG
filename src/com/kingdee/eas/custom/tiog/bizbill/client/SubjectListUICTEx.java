package com.kingdee.eas.custom.tiog.bizbill.client;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.springframework.util.CollectionUtils;

import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.basedata.org.CompanyOrgUnitFactory;
import com.kingdee.eas.basedata.org.ICompanyOrgUnit;
import com.kingdee.eas.custom.tiog.basedata.IMeetingType;
import com.kingdee.eas.custom.tiog.basedata.MeetingTypeFactory;
import com.kingdee.eas.custom.tiog.bizbill.IMeetingMan;
import com.kingdee.eas.custom.tiog.bizbill.ISubject;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManFactory;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.SubjectFactory;
import com.kingdee.eas.custom.tiog.bizbill.SubjectInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo;
import com.kingdee.eas.custom.tiog.utils.DataConvertUtil;
import com.kingdee.eas.custom.tiog.utils.MsgBoxUtil;
import com.kingdee.eas.custom.tiog.utils.UIWindowUtil;
import com.kingdee.util.StringUtils;

/**
 * 
 * @title SubjectListUICTEx
 * @description 审议议题列表界面
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-26
 */
public class SubjectListUICTEx extends SubjectListUI {

    public SubjectListUICTEx() throws Exception {
        super();
    }

    @Override
    protected String getEditUIModal() {
        return "com.kingdee.eas.base.uiframe.client.UINewTabFactory";
    }

    @Override
    public void onLoad() throws Exception {
        super.onLoad();
        this.genMeeting.setEnabled(true);
        this.genMeeting.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgLight_green"));
    }

    /**
     * （非 Javadoc）
     * 
     * @title actionGenMeeting_actionPerformed
     * @description 议题生成会议（支持多选同类型会议的议题生成会议）
     * @param e
     * @throws Exception
     * @see com.kingdee.eas.custom.tiog.bizbill.client.AbstractSubjectListUI#actionGenMeeting_actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionGenMeeting_actionPerformed(ActionEvent e) throws Exception {
        // 1. 获取选择的列表id
        ArrayList<String> ids = this.getSelectedIdValuesArrayList();
        if (CollectionUtils.isEmpty(ids)) {
            MsgBoxUtil.showWarning("请先选择待生成会议的议题！");
            return;
        }

        ICompanyOrgUnit iCompanyOrgUnit = CompanyOrgUnitFactory.getRemoteInstance(); // 企业
        IMeetingType iMeetingType = MeetingTypeFactory.getRemoteInstance(); // 会议类型
        IMeetingMan iMeetingMan = MeetingManFactory.getRemoteInstance(); // 会议决策

        MeetingManInfo meetingManInfo = new MeetingManInfo();
        MeetingManSubjectEntryCollection meetingManSubjectEntrys = meetingManInfo.getSubjectEntry();// 议题集合

        ISubject iSubject = SubjectFactory.getRemoteInstance();
        Set<BOSUuid> meetingTypeSet = new HashSet<BOSUuid>(); // 会议类型Set
        BOSUuid meetingTypeId = null;
        for (String id : ids) {
            MeetingManSubjectEntryInfo meetingManSubjectEntryInfo = new MeetingManSubjectEntryInfo();

            SubjectInfo subjectInfo = iSubject.getSubjectInfo(new ObjectUuidPK(id));
            String billNum = subjectInfo.getNumber(); // 单据编码
            String state = subjectInfo.getState().getValue(); // 单据状态
            String subjectNum = subjectInfo.getSubjectNum();// 议题编码
            String subjectName = subjectInfo.getSubjectName();// 议题名称
            meetingTypeId = (subjectInfo.getMeetingType() != null) ? subjectInfo.getMeetingType().getId()
                    : null;// 会议类型
            BOSUuid companyId = subjectInfo.getCompany().getId();// 企业
            // 2. 检验： 是否已审核单据, 单据必录校验，是否为同一种会议类型
            // 2.1 是否已审核单据
            if (!"5".equals(state)) {
                MsgBoxUtil.showWarning(new StringBuffer().append("单据编码：").append(billNum).append("单据未审核！")
                        .toString());
                return;
            }

            // 2.2 必录校验
            if (!checkBlank(billNum, subjectNum, subjectName, meetingTypeId, companyId)) {
                return;
            }
            meetingTypeSet.add(meetingTypeId);

            // 2.3 多选单据是否为同一会议类型
            if (meetingTypeSet.size() > 1) {
                MsgBoxUtil.showWarning("请选择同一会议类型的议题生成会议！");
                return;
            }

            meetingManSubjectEntryInfo.setCompany(iCompanyOrgUnit.getCompanyOrgUnitInfo(new ObjectUuidPK(
                    companyId)));// 企业
            meetingManSubjectEntryInfo.setCompanyNumber(DataConvertUtil.findOrgCode(companyId.toString())); // 组织结构代码
            meetingManSubjectEntryInfo.setSubject(subjectInfo);// 议题
            meetingManSubjectEntryInfo.setSubjectNumber(subjectInfo.getNumber()); // 议题编码
            meetingManSubjectEntrys.add(meetingManSubjectEntryInfo);

        }

        // 3. 生成一张会议决策单据
        meetingManInfo.setMeetingType(iMeetingType.getMeetingTypeInfo(new ObjectUuidPK(meetingTypeId)));// 会议类型
        IObjectPK pk = iMeetingMan.addnew(meetingManInfo);

        // 4. 弹出已生成会议决策单据界面
        String uiName = "com.kingdee.eas.custom.tiog.bizbill.client.MeetingManEditUI";
        UIWindowUtil.showAndEdit(uiName, pk);
    }

    /**
     * 
     * @title checkBlank
     * @description 检验必录字段
     * @param billNum 单据编码
     * @param subjectNum 议题编码
     * @param subjectName 议题名称
     * @param meetingTypeId 会议类型
     * @param companyId 企业
     * @return
     */
    private boolean checkBlank(String billNum, String subjectNum, String subjectName, BOSUuid meetingTypeId,
            BOSUuid companyId) {

        boolean flag = false;
        if (StringUtils.isEmpty(subjectNum)) {
            MsgBoxUtil.fieldBlankWarning(billNum, "议题编码");
            return flag;
        }

        if (StringUtils.isEmpty(subjectName)) {
            MsgBoxUtil.fieldBlankWarning(billNum, "议题名称");
            return flag;
        }

        if (meetingTypeId == null) {
            MsgBoxUtil.fieldBlankWarning(billNum, "会议类型");
            return flag;
        }

        if (companyId == null) {
            MsgBoxUtil.fieldBlankWarning(billNum, "企业");
            return flag;
        }

        return true;
    }

}
