package com.kingdee.eas.custom.tiog.bizbill.client;

import java.awt.event.ActionEvent;

import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.tiog.utils.MeetingManBillUtil;
import com.kingdee.eas.custom.tiog.utils.MsgBoxUtil;
import com.kingdee.eas.custom.tiog.utils.UIWindowUtil;
import com.kingdee.util.StringUtils;

/**
 * 
 * @title SubjectEditUICTEx
 * @description 审议议题表单界面
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-26
 */
public class SubjectEditUICTEx extends SubjectEditUI {

    public SubjectEditUICTEx() throws Exception {
        super();
    }

    @Override
    public void onLoad() throws Exception {
        super.onLoad();
        this.genMeeting.setEnabled(true);
        this.genMeeting.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgLight_green"));
    }

    /**
     * （非 Javadoc）
     * 
     * @title actionGenMeeting_actionPerformed
     * @description 议题生成会议
     * @param e
     * @throws Exception
     * @see com.kingdee.eas.custom.tiog.bizbill.client.AbstractSubjectEditUI#actionGenMeeting_actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionGenMeeting_actionPerformed(ActionEvent e) throws Exception {
        // 检验单据状态
        String billState = this.editData.getState().getValue();

        if (!"5".equals(billState)) {// 是否已审核单据
            MsgBoxUtil.showWarning("该单据未审核，不能生成会议！");
            return;
        }

        // 1. 获取表单中的议题编码 名称 企业id 企业编码 会议类型
        BOSUuid id = this.editData.getId();
        String number = this.editData.getNumber();// 单据编码
        String subjectNum = this.editData.getSubjectNum();// 议题编码
        String subjectName = this.editData.getSubjectName();// 议题名称
        BOSUuid companyId = (this.editData.getCompany() != null) ? this.editData.getCompany().getId() : null;// 企业
        String companyName = (this.editData.getCompany() != null) ? this.editData.getCompany().getName() : "";// 企业名称
        BOSUuid meetTypeId = (this.editData.getMeetingType() != null) ? this.editData.getMeetingType()
                .getId() : null;// 会议类型
        String meetTypeName = (this.editData.getMeetingType() != null) ? this.editData.getMeetingType()
                .getName() : "";// 会议类型名称

        checkBlank(subjectNum, subjectName, meetTypeId, companyId);
        // 2. 生成一张会议决策单据
        IObjectPK pk = MeetingManBillUtil.addNewMeetingManBill(id, companyId, meetTypeId);

        // 3. 弹出已生成会议决策单据界面
        String uiName = "com.kingdee.eas.custom.tiog.bizbill.client.MeetingManEditUI";
        UIWindowUtil.showAndEdit(uiName, pk);

    }

    /**
     * 
     * @title checkBlank
     * @description 必录字段校验
     * @param subjectNum 议题编码
     * @param subjectName 议题名称
     * @param meetType 会议类型
     * @param companyId
     * @throws EASBizException
     */
    private void checkBlank(String subjectNum, String subjectName, BOSUuid meetType, BOSUuid companyId)
            throws EASBizException {
        if (StringUtils.isEmpty(subjectName)) {
            throw new EASBizException(EASBizException.CHECKBLANK, new Object[] { "议题名称" });
        }

        if (StringUtils.isEmpty(subjectNum)) {
            throw new EASBizException(EASBizException.CHECKBLANK, new Object[] { "议题编码" });
        }

        if (meetType == null) {
            throw new EASBizException(EASBizException.CHECKBLANK, new Object[] { "会议类型" });
        }
        if (companyId == null) {
            throw new EASBizException(EASBizException.CHECKBLANK, new Object[] { "企业" });
        }
    }

}
