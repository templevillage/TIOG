package com.kingdee.eas.custom.tiog.bizbill.client;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.springframework.util.CollectionUtils;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.basedata.org.CompanyOrgUnitFactory;
import com.kingdee.eas.basedata.org.ICompanyOrgUnit;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.tiog.basedata.CatalogMainFactory;
import com.kingdee.eas.custom.tiog.basedata.ICatalogMain;
import com.kingdee.eas.custom.tiog.basedata.IMeetingType;
import com.kingdee.eas.custom.tiog.basedata.MeetingTypeFactory;
import com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem;
import com.kingdee.eas.custom.tiog.bizbill.ISubject;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemFactory;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectFactory;
import com.kingdee.eas.custom.tiog.bizbill.SubjectInfo;
import com.kingdee.eas.custom.tiog.utils.DataConvertUtil;
import com.kingdee.eas.custom.tiog.utils.MsgBoxUtil;
import com.kingdee.eas.custom.tiog.utils.UIWindowUtil;

/**
 * 
 * @title PoliticalSystemListUICTEx
 * @description 决策制度列表界面
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-30
 */
public class PoliticalSystemListUICTEx extends PoliticalSystemListUI {

    public PoliticalSystemListUICTEx() throws Exception {
        super();
    }

    /**
     * （非 Javadoc）
     * 
     * @title getEditUIModal
     * @description 以新页签形式打开编辑界面
     * @return
     * @see com.kingdee.eas.framework.client.CoreBillListUI#getEditUIModal()
     */
    @Override
    protected String getEditUIModal() {
        return "com.kingdee.eas.base.uiframe.client.UINewTabFactory";
    }

    @Override
    public void onLoad() throws Exception {
        super.onLoad();
        this.genSubject.setEnabled(true);
        this.genSubject.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgLight_green"));
    }

    /**
     * （非 Javadoc）
     * 
     * @title actionGenSubject_actionPerformed
     * @description 下推议题
     * @param e
     * @throws Exception
     * @see com.kingdee.eas.custom.tiog.bizbill.client.AbstractPoliticalSystemListUI#actionGenSubject_actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionGenSubject_actionPerformed(ActionEvent e) throws Exception {
        // 1.获取多选列表
        ArrayList<String> ids = this.getSelectedIdValuesArrayList();
        if (CollectionUtils.isEmpty(ids)) {
            MsgBoxUtil.showWarning("请先选择待生成议题的决策制度单据！");
            return;
        }

        ISubject iSubject = SubjectFactory.getRemoteInstance(); // 议题
        IPoliticalSystem iPoliticalSystem = PoliticalSystemFactory.getRemoteInstance(); // 制度
        ICompanyOrgUnit iCompanyOrgUnit = CompanyOrgUnitFactory.getRemoteInstance();// 企业
        IMeetingType iMeetingType = MeetingTypeFactory.getRemoteInstance(); // 会议类型

        SubjectInfo subjectInfo = new SubjectInfo();// 议题
        SubjectCatalogEntryCollection subjectCatalogEntry = subjectInfo.getCatalogEntry(); // 议题事项
        Set<BOSUuid> meetingTypeSet = new HashSet<BOSUuid>(); // 会议类型Set
        BOSUuid companyId = null;
        BOSUuid meetTypeId = null;
        for (String id : ids) {
            PoliticalSystemInfo info = iPoliticalSystem.getPoliticalSystemInfo(new ObjectUuidPK(id));
            String billNum = info.getNumber();
            meetTypeId = (info.getMeetNum() != null) ? info.getMeetNum().getId() : null; // 会议类型
            companyId = (info.getCompany() != null) ? info.getCompany().getId() : null;// 企业
            String state = info.getState().getValue(); // 单据状态

            // 2. 检验： 是否已审核单据, 单据必录校验，是否为同一种会议类型
            if (!check(meetingTypeSet, companyId, meetTypeId, billNum, state)) {
                return;
            }

            assemableSubjectCatalogEntry(subjectCatalogEntry, info);
        }

        subjectInfo.setMeetingType(iMeetingType.getMeetingTypeInfo(new ObjectUuidPK(meetTypeId)));
        subjectInfo.setCompany(iCompanyOrgUnit.getCompanyOrgUnitInfo(new ObjectUuidPK(companyId)));
        subjectInfo.setCompanyNumber(DataConvertUtil.findOrgCode(companyId.toString()));

        // 3. 生成一张会议决策单据
        IObjectPK pk = iSubject.addnew(subjectInfo);

        // 4. 弹出已生成议题单据界面
        String uiName = "com.kingdee.eas.custom.tiog.bizbill.client.SubjectEditUI";
        UIWindowUtil.showAndEdit(uiName, pk);
    }

    /**
     * 
     * @title check
     * @description 校验
     * @param meetingTypeSet
     * @param companyId 企业
     * @param meetTypeId 会议类型
     * @param billNum 单据编码
     * @param state 单据状态
     * @return boolean 通过校验返回true，否则返回false
     */
    private boolean check(Set<BOSUuid> meetingTypeSet, BOSUuid companyId, BOSUuid meetTypeId, String billNum,
            String state) {

        // 2.1 是否已审核单据
        if (!"5".equals(state)) {
            MsgBoxUtil.showWarning(new StringBuffer().append("单据编码：").append(billNum).append("单据未审核！")
                    .toString());
            return false;
        }

        // 2.2 必录校验
        if (meetTypeId == null) {
            MsgBoxUtil.fieldBlankWarning(billNum, "会议类型");
            return false;
        }

        if (companyId == null) {
            MsgBoxUtil.fieldBlankWarning(billNum, "企业");
            return false;
        }

        meetingTypeSet.add(meetTypeId);
        // 2.3 多选单据是否为同一会议类型
        if (meetingTypeSet.size() > 1) {
            MsgBoxUtil.showWarning("请选择同一会议类型的议题生成会议！");
            return false;
        }

        return true;

    }

    /**
     * 
     * @title getSubjectCatalogEntry
     * @description 组装审议议题的议题事项分录
     * @param iCatalogMain 事项实例
     * @param subjectCatalogEntry
     * @param info 决策制度
     * @throws BOSException
     * @throws EASBizException
     */
    private void assemableSubjectCatalogEntry(SubjectCatalogEntryCollection subjectCatalogEntry,
            PoliticalSystemInfo info) throws BOSException, EASBizException {

        ICatalogMain iCatalogMain = CatalogMainFactory.getRemoteInstance(); // 事项
        PoliticalSystemEntryCollection entrys = info.getEntrys();
        for (int i = 0; i < entrys.size(); i++) {
            SubjectCatalogEntryInfo subjectEntryInfo = new SubjectCatalogEntryInfo();
            PoliticalSystemEntryInfo entryInfo = entrys.get(i);

            BOSUuid catalogMainId = entryInfo.getCatalogMain().getId();
            String catalogMainNumber = entryInfo.getCatalogMainNumber();
            subjectEntryInfo.setCatalogMain(iCatalogMain.getCatalogMainInfo(new ObjectUuidPK(catalogMainId)));
            subjectEntryInfo.setCatalogMainNumber(catalogMainNumber);
            subjectCatalogEntry.add(subjectEntryInfo);
        }
    }

}
