/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.bizbill.client;

import org.apache.log4j.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.BorderFactory;
import javax.swing.event.*;
import javax.swing.KeyStroke;

import com.kingdee.bos.ctrl.swing.*;
import com.kingdee.bos.ctrl.kdf.table.*;
import com.kingdee.bos.ctrl.kdf.data.event.*;
import com.kingdee.bos.dao.*;
import com.kingdee.bos.dao.query.*;
import com.kingdee.bos.metadata.*;
import com.kingdee.bos.metadata.entity.*;
import com.kingdee.bos.ui.face.*;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.enums.EnumUtils;
import com.kingdee.bos.ui.face.UIRuleUtil;
import com.kingdee.bos.ctrl.swing.event.*;
import com.kingdee.bos.ctrl.kdf.table.event.*;
import com.kingdee.bos.ctrl.extendcontrols.*;
import com.kingdee.bos.ctrl.kdf.util.render.*;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.uistatemanage.ActionStateConst;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.appframework.uip.UINavigator;


/**
 * output class name
 */
public abstract class AbstractSubjectEditUI extends com.kingdee.eas.framework.client.CoreBillEditUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractSubjectEditUI.class);
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contCreator;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contCreateTime;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contLastUpdateUser;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contLastUpdateTime;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contBizDate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contDescription;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contAuditor;
    protected com.kingdee.bos.ctrl.swing.KDTabbedPane paneBIZLayerControl17;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contadminOrgUnit;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcompany;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcompanyNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contsubjectName;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer conttaskSource;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contspecial;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contpassFlag;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contapprovalFlag;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contsubjectResult;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contoperType;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contremark;
    protected com.kingdee.bos.ctrl.swing.KDTabbedPane kDTabbedPane1;
    protected com.kingdee.bos.ctrl.swing.KDCheckBox chkadoptFlag;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contsubjectNum;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contstate;
    protected com.kingdee.bos.ctrl.swing.KDCheckBox chkisUpload;
    protected com.kingdee.bos.ctrl.swing.KDTabbedPane kDTabbedPane2;
    protected com.kingdee.bos.ctrl.swing.KDCheckBox chksuperviseFlag;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contmeetingType;
    protected com.kingdee.bos.ctrl.swing.KDTabbedPane kDTabbedPane3;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtCreator;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker kDDateCreateTime;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtLastUpdateUser;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker kDDateLastUpdateTime;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtNumber;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkBizDate;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtDescription;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtAuditor;
    protected com.kingdee.bos.ctrl.swing.KDPanel catalogTab;
    protected com.kingdee.bos.ctrl.swing.KDPanel otherTab;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtCatalogEntry;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtCatalogEntry_detailPanel = null;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtOtherEntrys;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtOtherEntrys_detailPanel = null;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtadminOrgUnit;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtcompany;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtcompanyNumber;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtsubjectName;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmttaskSource;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtspecial;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtpassFlag;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtapprovalFlag;
    protected com.kingdee.bos.ctrl.swing.KDScrollPane scrollPanesubjectResult;
    protected com.kingdee.bos.ctrl.swing.KDTextArea txtsubjectResult;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtoperType;
    protected com.kingdee.bos.ctrl.swing.KDScrollPane scrollPaneremark;
    protected com.kingdee.bos.ctrl.swing.KDTextArea txtremark;
    protected com.kingdee.bos.ctrl.swing.KDPanel kDPanel2;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtDeliberationEntry;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtDeliberationEntry_detailPanel = null;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtsubjectNum;
    protected com.kingdee.bos.ctrl.swing.KDComboBox state;
    protected com.kingdee.bos.ctrl.swing.KDPanel kDPanel1;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtAttendanceEntry;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtAttendanceEntry_detailPanel = null;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtmeetingType;
    protected com.kingdee.bos.ctrl.swing.KDPanel kDPanel3;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtRefSubjectEntry;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtRefSubjectEntry_detailPanel = null;
    protected com.kingdee.bos.ctrl.swing.KDWorkButton genMeeting;
    protected com.kingdee.eas.custom.tiog.bizbill.SubjectInfo editData = null;
    protected ActionGenMeeting actionGenMeeting = null;
    protected ActionAudit actionAudit = null;
    protected ActionUnAudit actionUnAudit = null;
    /**
     * output class constructor
     */
    public AbstractSubjectEditUI() throws Exception
    {
        super();
        this.defaultObjectName = "editData";
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractSubjectEditUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        //actionSubmit
        String _tempStr = null;
        actionSubmit.setEnabled(true);
        actionSubmit.setDaemonRun(false);

        actionSubmit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl S"));
        _tempStr = resHelper.getString("ActionSubmit.SHORT_DESCRIPTION");
        actionSubmit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.LONG_DESCRIPTION");
        actionSubmit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.NAME");
        actionSubmit.putValue(ItemAction.NAME, _tempStr);
        this.actionSubmit.setBindWorkFlow(true);
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrint
        actionPrint.setEnabled(true);
        actionPrint.setDaemonRun(false);

        actionPrint.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl P"));
        _tempStr = resHelper.getString("ActionPrint.SHORT_DESCRIPTION");
        actionPrint.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.LONG_DESCRIPTION");
        actionPrint.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.NAME");
        actionPrint.putValue(ItemAction.NAME, _tempStr);
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrintPreview
        actionPrintPreview.setEnabled(true);
        actionPrintPreview.setDaemonRun(false);

        actionPrintPreview.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift ctrl P"));
        _tempStr = resHelper.getString("ActionPrintPreview.SHORT_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.LONG_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.NAME");
        actionPrintPreview.putValue(ItemAction.NAME, _tempStr);
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionGenMeeting
        this.actionGenMeeting = new ActionGenMeeting(this);
        getActionManager().registerAction("actionGenMeeting", actionGenMeeting);
         this.actionGenMeeting.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionAudit
        this.actionAudit = new ActionAudit(this);
        getActionManager().registerAction("actionAudit", actionAudit);
        this.actionAudit.setBindWorkFlow(true);
        this.actionAudit.setExtendProperty("canForewarn", "true");
        this.actionAudit.setExtendProperty("userDefined", "true");
        this.actionAudit.setExtendProperty("isObjectUpdateLock", "false");
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.ForewarnService());
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.WorkFlowService());
        //actionUnAudit
        this.actionUnAudit = new ActionUnAudit(this);
        getActionManager().registerAction("actionUnAudit", actionUnAudit);
        this.actionUnAudit.setBindWorkFlow(true);
        this.actionUnAudit.setExtendProperty("canForewarn", "true");
        this.actionUnAudit.setExtendProperty("userDefined", "true");
        this.actionUnAudit.setExtendProperty("isObjectUpdateLock", "false");
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.ForewarnService());
         this.actionUnAudit.addService(new com.kingdee.eas.framework.client.service.WorkFlowService());
        this.contCreator = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contCreateTime = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contLastUpdateUser = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contLastUpdateTime = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contBizDate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contDescription = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contAuditor = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.paneBIZLayerControl17 = new com.kingdee.bos.ctrl.swing.KDTabbedPane();
        this.contadminOrgUnit = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contcompany = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contcompanyNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contsubjectName = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.conttaskSource = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contspecial = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contpassFlag = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contapprovalFlag = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contsubjectResult = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contoperType = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contremark = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDTabbedPane1 = new com.kingdee.bos.ctrl.swing.KDTabbedPane();
        this.chkadoptFlag = new com.kingdee.bos.ctrl.swing.KDCheckBox();
        this.contsubjectNum = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contstate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.chkisUpload = new com.kingdee.bos.ctrl.swing.KDCheckBox();
        this.kDTabbedPane2 = new com.kingdee.bos.ctrl.swing.KDTabbedPane();
        this.chksuperviseFlag = new com.kingdee.bos.ctrl.swing.KDCheckBox();
        this.contmeetingType = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDTabbedPane3 = new com.kingdee.bos.ctrl.swing.KDTabbedPane();
        this.prmtCreator = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.kDDateCreateTime = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.prmtLastUpdateUser = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.kDDateLastUpdateTime = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.txtNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.pkBizDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.txtDescription = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtAuditor = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.catalogTab = new com.kingdee.bos.ctrl.swing.KDPanel();
        this.otherTab = new com.kingdee.bos.ctrl.swing.KDPanel();
        this.kdtCatalogEntry = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.kdtOtherEntrys = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.prmtadminOrgUnit = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtcompany = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.txtcompanyNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtsubjectName = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmttaskSource = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtspecial = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtpassFlag = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtapprovalFlag = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.scrollPanesubjectResult = new com.kingdee.bos.ctrl.swing.KDScrollPane();
        this.txtsubjectResult = new com.kingdee.bos.ctrl.swing.KDTextArea();
        this.prmtoperType = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.scrollPaneremark = new com.kingdee.bos.ctrl.swing.KDScrollPane();
        this.txtremark = new com.kingdee.bos.ctrl.swing.KDTextArea();
        this.kDPanel2 = new com.kingdee.bos.ctrl.swing.KDPanel();
        this.kdtDeliberationEntry = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.txtsubjectNum = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.state = new com.kingdee.bos.ctrl.swing.KDComboBox();
        this.kDPanel1 = new com.kingdee.bos.ctrl.swing.KDPanel();
        this.kdtAttendanceEntry = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.prmtmeetingType = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.kDPanel3 = new com.kingdee.bos.ctrl.swing.KDPanel();
        this.kdtRefSubjectEntry = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.genMeeting = new com.kingdee.bos.ctrl.swing.KDWorkButton();
        this.contCreator.setName("contCreator");
        this.contCreateTime.setName("contCreateTime");
        this.contLastUpdateUser.setName("contLastUpdateUser");
        this.contLastUpdateTime.setName("contLastUpdateTime");
        this.contNumber.setName("contNumber");
        this.contBizDate.setName("contBizDate");
        this.contDescription.setName("contDescription");
        this.contAuditor.setName("contAuditor");
        this.paneBIZLayerControl17.setName("paneBIZLayerControl17");
        this.contadminOrgUnit.setName("contadminOrgUnit");
        this.contcompany.setName("contcompany");
        this.contcompanyNumber.setName("contcompanyNumber");
        this.contsubjectName.setName("contsubjectName");
        this.conttaskSource.setName("conttaskSource");
        this.contspecial.setName("contspecial");
        this.contpassFlag.setName("contpassFlag");
        this.contapprovalFlag.setName("contapprovalFlag");
        this.contsubjectResult.setName("contsubjectResult");
        this.contoperType.setName("contoperType");
        this.contremark.setName("contremark");
        this.kDTabbedPane1.setName("kDTabbedPane1");
        this.chkadoptFlag.setName("chkadoptFlag");
        this.contsubjectNum.setName("contsubjectNum");
        this.contstate.setName("contstate");
        this.chkisUpload.setName("chkisUpload");
        this.kDTabbedPane2.setName("kDTabbedPane2");
        this.chksuperviseFlag.setName("chksuperviseFlag");
        this.contmeetingType.setName("contmeetingType");
        this.kDTabbedPane3.setName("kDTabbedPane3");
        this.prmtCreator.setName("prmtCreator");
        this.kDDateCreateTime.setName("kDDateCreateTime");
        this.prmtLastUpdateUser.setName("prmtLastUpdateUser");
        this.kDDateLastUpdateTime.setName("kDDateLastUpdateTime");
        this.txtNumber.setName("txtNumber");
        this.pkBizDate.setName("pkBizDate");
        this.txtDescription.setName("txtDescription");
        this.prmtAuditor.setName("prmtAuditor");
        this.catalogTab.setName("catalogTab");
        this.otherTab.setName("otherTab");
        this.kdtCatalogEntry.setName("kdtCatalogEntry");
        this.kdtOtherEntrys.setName("kdtOtherEntrys");
        this.prmtadminOrgUnit.setName("prmtadminOrgUnit");
        this.prmtcompany.setName("prmtcompany");
        this.txtcompanyNumber.setName("txtcompanyNumber");
        this.txtsubjectName.setName("txtsubjectName");
        this.prmttaskSource.setName("prmttaskSource");
        this.prmtspecial.setName("prmtspecial");
        this.prmtpassFlag.setName("prmtpassFlag");
        this.prmtapprovalFlag.setName("prmtapprovalFlag");
        this.scrollPanesubjectResult.setName("scrollPanesubjectResult");
        this.txtsubjectResult.setName("txtsubjectResult");
        this.prmtoperType.setName("prmtoperType");
        this.scrollPaneremark.setName("scrollPaneremark");
        this.txtremark.setName("txtremark");
        this.kDPanel2.setName("kDPanel2");
        this.kdtDeliberationEntry.setName("kdtDeliberationEntry");
        this.txtsubjectNum.setName("txtsubjectNum");
        this.state.setName("state");
        this.kDPanel1.setName("kDPanel1");
        this.kdtAttendanceEntry.setName("kdtAttendanceEntry");
        this.prmtmeetingType.setName("prmtmeetingType");
        this.kDPanel3.setName("kDPanel3");
        this.kdtRefSubjectEntry.setName("kdtRefSubjectEntry");
        this.genMeeting.setName("genMeeting");
        // CoreUI		
        this.btnTraceUp.setVisible(false);		
        this.btnTraceDown.setVisible(false);		
        this.btnCreateTo.setVisible(true);		
        this.btnAddLine.setVisible(false);		
        this.btnCopyLine.setVisible(false);		
        this.btnInsertLine.setVisible(false);		
        this.btnRemoveLine.setVisible(false);		
        this.btnAuditResult.setVisible(false);		
        this.separator1.setVisible(false);		
        this.menuItemCreateTo.setVisible(true);		
        this.separator3.setVisible(false);		
        this.menuItemTraceUp.setVisible(false);		
        this.menuItemTraceDown.setVisible(false);		
        this.menuTable1.setVisible(false);		
        this.menuItemAddLine.setVisible(false);		
        this.menuItemCopyLine.setVisible(false);		
        this.menuItemInsertLine.setVisible(false);		
        this.menuItemRemoveLine.setVisible(false);		
        this.menuItemViewSubmitProccess.setVisible(false);		
        this.menuItemViewDoProccess.setVisible(false);		
        this.menuItemAuditResult.setVisible(false);
        // contCreator		
        this.contCreator.setBoundLabelText(resHelper.getString("contCreator.boundLabelText"));		
        this.contCreator.setBoundLabelLength(100);		
        this.contCreator.setBoundLabelUnderline(true);		
        this.contCreator.setEnabled(false);		
        this.contCreator.setBoundLabelAlignment(7);		
        this.contCreator.setVisible(true);		
        this.contCreator.setForeground(new java.awt.Color(0,0,0));
        // contCreateTime		
        this.contCreateTime.setBoundLabelText(resHelper.getString("contCreateTime.boundLabelText"));		
        this.contCreateTime.setBoundLabelLength(100);		
        this.contCreateTime.setBoundLabelUnderline(true);		
        this.contCreateTime.setEnabled(false);		
        this.contCreateTime.setBoundLabelAlignment(7);		
        this.contCreateTime.setVisible(true);		
        this.contCreateTime.setForeground(new java.awt.Color(0,0,0));
        // contLastUpdateUser		
        this.contLastUpdateUser.setBoundLabelText(resHelper.getString("contLastUpdateUser.boundLabelText"));		
        this.contLastUpdateUser.setBoundLabelLength(100);		
        this.contLastUpdateUser.setBoundLabelUnderline(true);		
        this.contLastUpdateUser.setEnabled(false);		
        this.contLastUpdateUser.setVisible(false);		
        this.contLastUpdateUser.setBoundLabelAlignment(7);		
        this.contLastUpdateUser.setForeground(new java.awt.Color(0,0,0));
        // contLastUpdateTime		
        this.contLastUpdateTime.setBoundLabelText(resHelper.getString("contLastUpdateTime.boundLabelText"));		
        this.contLastUpdateTime.setBoundLabelLength(100);		
        this.contLastUpdateTime.setBoundLabelUnderline(true);		
        this.contLastUpdateTime.setEnabled(false);		
        this.contLastUpdateTime.setVisible(false);		
        this.contLastUpdateTime.setBoundLabelAlignment(7);		
        this.contLastUpdateTime.setForeground(new java.awt.Color(0,0,0));
        // contNumber		
        this.contNumber.setBoundLabelText(resHelper.getString("contNumber.boundLabelText"));		
        this.contNumber.setBoundLabelLength(100);		
        this.contNumber.setBoundLabelUnderline(true);		
        this.contNumber.setBoundLabelAlignment(7);		
        this.contNumber.setVisible(true);		
        this.contNumber.setForeground(new java.awt.Color(0,0,0));
        // contBizDate		
        this.contBizDate.setBoundLabelText(resHelper.getString("contBizDate.boundLabelText"));		
        this.contBizDate.setBoundLabelLength(100);		
        this.contBizDate.setBoundLabelUnderline(true);		
        this.contBizDate.setBoundLabelAlignment(7);		
        this.contBizDate.setVisible(true);		
        this.contBizDate.setForeground(new java.awt.Color(0,0,0));
        // contDescription		
        this.contDescription.setBoundLabelText(resHelper.getString("contDescription.boundLabelText"));		
        this.contDescription.setBoundLabelLength(100);		
        this.contDescription.setBoundLabelUnderline(true);		
        this.contDescription.setBoundLabelAlignment(7);		
        this.contDescription.setVisible(true);		
        this.contDescription.setForeground(new java.awt.Color(0,0,0));
        // contAuditor		
        this.contAuditor.setBoundLabelText(resHelper.getString("contAuditor.boundLabelText"));		
        this.contAuditor.setBoundLabelLength(100);		
        this.contAuditor.setBoundLabelUnderline(true);		
        this.contAuditor.setBoundLabelAlignment(7);		
        this.contAuditor.setVisible(true);		
        this.contAuditor.setForeground(new java.awt.Color(0,0,0));
        // paneBIZLayerControl17		
        this.paneBIZLayerControl17.setVisible(true);
        // contadminOrgUnit		
        this.contadminOrgUnit.setBoundLabelText(resHelper.getString("contadminOrgUnit.boundLabelText"));		
        this.contadminOrgUnit.setBoundLabelLength(100);		
        this.contadminOrgUnit.setBoundLabelUnderline(true);		
        this.contadminOrgUnit.setVisible(true);
        // contcompany		
        this.contcompany.setBoundLabelText(resHelper.getString("contcompany.boundLabelText"));		
        this.contcompany.setBoundLabelLength(100);		
        this.contcompany.setBoundLabelUnderline(true);		
        this.contcompany.setVisible(true);
        // contcompanyNumber		
        this.contcompanyNumber.setBoundLabelText(resHelper.getString("contcompanyNumber.boundLabelText"));		
        this.contcompanyNumber.setBoundLabelLength(100);		
        this.contcompanyNumber.setBoundLabelUnderline(true);		
        this.contcompanyNumber.setVisible(true);
        // contsubjectName		
        this.contsubjectName.setBoundLabelText(resHelper.getString("contsubjectName.boundLabelText"));		
        this.contsubjectName.setBoundLabelLength(100);		
        this.contsubjectName.setBoundLabelUnderline(true);		
        this.contsubjectName.setVisible(true);
        // conttaskSource		
        this.conttaskSource.setBoundLabelText(resHelper.getString("conttaskSource.boundLabelText"));		
        this.conttaskSource.setBoundLabelLength(100);		
        this.conttaskSource.setBoundLabelUnderline(true);		
        this.conttaskSource.setVisible(true);
        // contspecial		
        this.contspecial.setBoundLabelText(resHelper.getString("contspecial.boundLabelText"));		
        this.contspecial.setBoundLabelLength(100);		
        this.contspecial.setBoundLabelUnderline(true);		
        this.contspecial.setVisible(true);
        // contpassFlag		
        this.contpassFlag.setBoundLabelText(resHelper.getString("contpassFlag.boundLabelText"));		
        this.contpassFlag.setBoundLabelLength(100);		
        this.contpassFlag.setBoundLabelUnderline(true);		
        this.contpassFlag.setVisible(true);
        // contapprovalFlag		
        this.contapprovalFlag.setBoundLabelText(resHelper.getString("contapprovalFlag.boundLabelText"));		
        this.contapprovalFlag.setBoundLabelLength(100);		
        this.contapprovalFlag.setBoundLabelUnderline(true);		
        this.contapprovalFlag.setVisible(true);
        // contsubjectResult		
        this.contsubjectResult.setBoundLabelText(resHelper.getString("contsubjectResult.boundLabelText"));		
        this.contsubjectResult.setBoundLabelLength(100);		
        this.contsubjectResult.setBoundLabelUnderline(true);		
        this.contsubjectResult.setVisible(true);
        // contoperType		
        this.contoperType.setBoundLabelText(resHelper.getString("contoperType.boundLabelText"));		
        this.contoperType.setBoundLabelLength(100);		
        this.contoperType.setBoundLabelUnderline(true);		
        this.contoperType.setVisible(true);
        // contremark		
        this.contremark.setBoundLabelText(resHelper.getString("contremark.boundLabelText"));		
        this.contremark.setBoundLabelLength(100);		
        this.contremark.setBoundLabelUnderline(true);		
        this.contremark.setVisible(true);
        // kDTabbedPane1		
        this.kDTabbedPane1.setVisible(false);		
        this.kDTabbedPane1.setEnabled(false);
        // chkadoptFlag		
        this.chkadoptFlag.setText(resHelper.getString("chkadoptFlag.text"));		
        this.chkadoptFlag.setHorizontalAlignment(2);
        // contsubjectNum		
        this.contsubjectNum.setBoundLabelText(resHelper.getString("contsubjectNum.boundLabelText"));		
        this.contsubjectNum.setBoundLabelLength(100);		
        this.contsubjectNum.setBoundLabelUnderline(true);		
        this.contsubjectNum.setVisible(true);
        // contstate		
        this.contstate.setBoundLabelText(resHelper.getString("contstate.boundLabelText"));		
        this.contstate.setBoundLabelLength(100);		
        this.contstate.setBoundLabelUnderline(true);		
        this.contstate.setVisible(false);
        // chkisUpload		
        this.chkisUpload.setText(resHelper.getString("chkisUpload.text"));		
        this.chkisUpload.setVisible(false);		
        this.chkisUpload.setHorizontalAlignment(2);		
        this.chkisUpload.setEnabled(false);
        // kDTabbedPane2		
        this.kDTabbedPane2.setVisible(false);		
        this.kDTabbedPane2.setEnabled(false);
        // chksuperviseFlag		
        this.chksuperviseFlag.setText(resHelper.getString("chksuperviseFlag.text"));		
        this.chksuperviseFlag.setHorizontalAlignment(2);
        // contmeetingType		
        this.contmeetingType.setBoundLabelText(resHelper.getString("contmeetingType.boundLabelText"));		
        this.contmeetingType.setBoundLabelLength(100);		
        this.contmeetingType.setBoundLabelUnderline(true);		
        this.contmeetingType.setVisible(true);
        // kDTabbedPane3		
        this.kDTabbedPane3.setVisible(true);
        // prmtCreator		
        this.prmtCreator.setEnabled(false);		
        this.prmtCreator.setRequired(false);		
        this.prmtCreator.setForeground(new java.awt.Color(0,0,0));
        // kDDateCreateTime		
        this.kDDateCreateTime.setTimeEnabled(true);		
        this.kDDateCreateTime.setEnabled(false);		
        this.kDDateCreateTime.setRequired(false);		
        this.kDDateCreateTime.setForeground(new java.awt.Color(0,0,0));
        // prmtLastUpdateUser		
        this.prmtLastUpdateUser.setEnabled(false);		
        this.prmtLastUpdateUser.setVisible(false);		
        this.prmtLastUpdateUser.setRequired(false);		
        this.prmtLastUpdateUser.setForeground(new java.awt.Color(0,0,0));
        // kDDateLastUpdateTime		
        this.kDDateLastUpdateTime.setTimeEnabled(true);		
        this.kDDateLastUpdateTime.setEnabled(false);		
        this.kDDateLastUpdateTime.setVisible(false);		
        this.kDDateLastUpdateTime.setRequired(false);		
        this.kDDateLastUpdateTime.setForeground(new java.awt.Color(0,0,0));
        // txtNumber		
        this.txtNumber.setMaxLength(80);		
        this.txtNumber.setEnabled(true);		
        this.txtNumber.setHorizontalAlignment(2);		
        this.txtNumber.setRequired(false);		
        this.txtNumber.setForeground(new java.awt.Color(0,0,0));
        // pkBizDate		
        this.pkBizDate.setEnabled(true);		
        this.pkBizDate.setRequired(false);		
        this.pkBizDate.setForeground(new java.awt.Color(0,0,0));
        // txtDescription		
        this.txtDescription.setMaxLength(80);		
        this.txtDescription.setEnabled(true);		
        this.txtDescription.setHorizontalAlignment(2);		
        this.txtDescription.setRequired(false);		
        this.txtDescription.setForeground(new java.awt.Color(0,0,0));
        // prmtAuditor		
        this.prmtAuditor.setEnabled(false);		
        this.prmtAuditor.setRequired(false);		
        this.prmtAuditor.setForeground(new java.awt.Color(0,0,0));
        // catalogTab		
        this.catalogTab.setVisible(true);
        // otherTab		
        this.otherTab.setVisible(true);
        // kdtCatalogEntry
		String kdtCatalogEntryStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:NumberFormat>&amp;int</c:NumberFormat></c:Style><c:Style id=\"sCol2\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol3\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol4\"><c:Protection hidden=\"true\" /></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"seq\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"catalogMain\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"1\" /><t:Column t:key=\"catalogMainNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"2\" t:styleID=\"sCol2\" /><t:Column t:key=\"meetingType\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"3\" t:styleID=\"sCol3\" /><t:Column t:key=\"catalog\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"4\" t:styleID=\"sCol4\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{seq}</t:Cell><t:Cell>$Resource{catalogMain}</t:Cell><t:Cell>$Resource{catalogMainNumber}</t:Cell><t:Cell>$Resource{meetingType}</t:Cell><t:Cell>$Resource{catalog}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtCatalogEntry.setFormatXml(resHelper.translateString("kdtCatalogEntry",kdtCatalogEntryStrXML));
        kdtCatalogEntry.addKDTEditListener(new KDTEditAdapter() {
		public void editStopped(KDTEditEvent e) {
			try {
				kdtCatalogEntry_Changed(e.getRowIndex(),e.getColIndex());
			}
			catch (Exception exc) {
				handUIException(exc);
			}
		}
	});


                this.kdtCatalogEntry.putBindContents("editData",new String[] {"seq","catalogMain","catalogMainNumber","meetingType","catalog"});


        this.kdtCatalogEntry.checkParsed();
        KDFormattedTextField kdtCatalogEntry_seq_TextField = new KDFormattedTextField();
        kdtCatalogEntry_seq_TextField.setName("kdtCatalogEntry_seq_TextField");
        kdtCatalogEntry_seq_TextField.setVisible(true);
        kdtCatalogEntry_seq_TextField.setEditable(true);
        kdtCatalogEntry_seq_TextField.setHorizontalAlignment(2);
        kdtCatalogEntry_seq_TextField.setDataType(0);
        KDTDefaultCellEditor kdtCatalogEntry_seq_CellEditor = new KDTDefaultCellEditor(kdtCatalogEntry_seq_TextField);
        this.kdtCatalogEntry.getColumn("seq").setEditor(kdtCatalogEntry_seq_CellEditor);
        final KDBizPromptBox kdtCatalogEntry_catalogMain_PromptBox = new KDBizPromptBox();
        kdtCatalogEntry_catalogMain_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.CatalogMainQuery");
        kdtCatalogEntry_catalogMain_PromptBox.setVisible(true);
        kdtCatalogEntry_catalogMain_PromptBox.setEditable(true);
        kdtCatalogEntry_catalogMain_PromptBox.setDisplayFormat("$number$");
        kdtCatalogEntry_catalogMain_PromptBox.setEditFormat("$number$");
        kdtCatalogEntry_catalogMain_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtCatalogEntry_catalogMain_CellEditor = new KDTDefaultCellEditor(kdtCatalogEntry_catalogMain_PromptBox);
        this.kdtCatalogEntry.getColumn("catalogMain").setEditor(kdtCatalogEntry_catalogMain_CellEditor);
        ObjectValueRender kdtCatalogEntry_catalogMain_OVR = new ObjectValueRender();
        kdtCatalogEntry_catalogMain_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtCatalogEntry.getColumn("catalogMain").setRenderer(kdtCatalogEntry_catalogMain_OVR);
        			EntityViewInfo evikdtCatalogEntry_catalogMain_PromptBox = new EntityViewInfo ();
		evikdtCatalogEntry_catalogMain_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtCatalogEntry_catalogMain_PromptBox.setEntityViewInfo(evikdtCatalogEntry_catalogMain_PromptBox);
					
        			kdtCatalogEntry_catalogMain_PromptBox.addSelectorListener(new SelectorListener() {
			com.kingdee.eas.custom.tiog.basedata.client.CatalogMainListUI kdtCatalogEntry_catalogMain_PromptBox_F7ListUI = null;
			public void willShow(SelectorEvent e) {
				if (kdtCatalogEntry_catalogMain_PromptBox_F7ListUI == null) {
					try {
						kdtCatalogEntry_catalogMain_PromptBox_F7ListUI = new com.kingdee.eas.custom.tiog.basedata.client.CatalogMainListUI();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					HashMap ctx = new HashMap();
					ctx.put("bizUIOwner",javax.swing.SwingUtilities.getWindowAncestor(kdtCatalogEntry_catalogMain_PromptBox_F7ListUI));
					kdtCatalogEntry_catalogMain_PromptBox_F7ListUI.setF7Use(true,ctx);
					kdtCatalogEntry_catalogMain_PromptBox.setSelector(kdtCatalogEntry_catalogMain_PromptBox_F7ListUI);
				}
			}
		});
					
        KDTextField kdtCatalogEntry_catalogMainNumber_TextField = new KDTextField();
        kdtCatalogEntry_catalogMainNumber_TextField.setName("kdtCatalogEntry_catalogMainNumber_TextField");
        kdtCatalogEntry_catalogMainNumber_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtCatalogEntry_catalogMainNumber_CellEditor = new KDTDefaultCellEditor(kdtCatalogEntry_catalogMainNumber_TextField);
        this.kdtCatalogEntry.getColumn("catalogMainNumber").setEditor(kdtCatalogEntry_catalogMainNumber_CellEditor);
        final KDBizPromptBox kdtCatalogEntry_meetingType_PromptBox = new KDBizPromptBox();
        kdtCatalogEntry_meetingType_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.MeetingTypeQuery");
        kdtCatalogEntry_meetingType_PromptBox.setVisible(true);
        kdtCatalogEntry_meetingType_PromptBox.setEditable(true);
        kdtCatalogEntry_meetingType_PromptBox.setDisplayFormat("$number$");
        kdtCatalogEntry_meetingType_PromptBox.setEditFormat("$number$");
        kdtCatalogEntry_meetingType_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtCatalogEntry_meetingType_CellEditor = new KDTDefaultCellEditor(kdtCatalogEntry_meetingType_PromptBox);
        this.kdtCatalogEntry.getColumn("meetingType").setEditor(kdtCatalogEntry_meetingType_CellEditor);
        ObjectValueRender kdtCatalogEntry_meetingType_OVR = new ObjectValueRender();
        kdtCatalogEntry_meetingType_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtCatalogEntry.getColumn("meetingType").setRenderer(kdtCatalogEntry_meetingType_OVR);
        			EntityViewInfo evikdtCatalogEntry_meetingType_PromptBox = new EntityViewInfo ();
		evikdtCatalogEntry_meetingType_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtCatalogEntry_meetingType_PromptBox.setEntityViewInfo(evikdtCatalogEntry_meetingType_PromptBox);
					
        final KDBizPromptBox kdtCatalogEntry_catalog_PromptBox = new KDBizPromptBox();
        kdtCatalogEntry_catalog_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.CatalogQuery");
        kdtCatalogEntry_catalog_PromptBox.setVisible(true);
        kdtCatalogEntry_catalog_PromptBox.setEditable(true);
        kdtCatalogEntry_catalog_PromptBox.setDisplayFormat("$number$");
        kdtCatalogEntry_catalog_PromptBox.setEditFormat("$number$");
        kdtCatalogEntry_catalog_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtCatalogEntry_catalog_CellEditor = new KDTDefaultCellEditor(kdtCatalogEntry_catalog_PromptBox);
        this.kdtCatalogEntry.getColumn("catalog").setEditor(kdtCatalogEntry_catalog_CellEditor);
        ObjectValueRender kdtCatalogEntry_catalog_OVR = new ObjectValueRender();
        kdtCatalogEntry_catalog_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtCatalogEntry.getColumn("catalog").setRenderer(kdtCatalogEntry_catalog_OVR);
        			EntityViewInfo evikdtCatalogEntry_catalog_PromptBox = new EntityViewInfo ();
		evikdtCatalogEntry_catalog_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtCatalogEntry_catalog_PromptBox.setEntityViewInfo(evikdtCatalogEntry_catalog_PromptBox);
					
        			kdtCatalogEntry_catalog_PromptBox.addSelectorListener(new SelectorListener() {
			com.kingdee.eas.custom.tiog.basedata.client.CatalogListUI kdtCatalogEntry_catalog_PromptBox_F7ListUI = null;
			public void willShow(SelectorEvent e) {
				if (kdtCatalogEntry_catalog_PromptBox_F7ListUI == null) {
					try {
						kdtCatalogEntry_catalog_PromptBox_F7ListUI = new com.kingdee.eas.custom.tiog.basedata.client.CatalogListUI();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					HashMap ctx = new HashMap();
					ctx.put("bizUIOwner",javax.swing.SwingUtilities.getWindowAncestor(kdtCatalogEntry_catalog_PromptBox_F7ListUI));
					kdtCatalogEntry_catalog_PromptBox_F7ListUI.setF7Use(true,ctx);
					kdtCatalogEntry_catalog_PromptBox.setSelector(kdtCatalogEntry_catalog_PromptBox_F7ListUI);
				}
			}
		});
					
        // kdtOtherEntrys
		String kdtOtherEntrysStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:Protection hidden=\"true\" /></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"id\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{id}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtOtherEntrys.setFormatXml(resHelper.translateString("kdtOtherEntrys",kdtOtherEntrysStrXML));

                this.kdtOtherEntrys.putBindContents("editData",new String[] {"id"});


        this.kdtOtherEntrys.checkParsed();
        // prmtadminOrgUnit		
        this.prmtadminOrgUnit.setQueryInfo("com.kingdee.eas.basedata.org.app.AdminItemQuery");		
        this.prmtadminOrgUnit.setEditable(true);		
        this.prmtadminOrgUnit.setDisplayFormat("$name$");		
        this.prmtadminOrgUnit.setEditFormat("$number$");		
        this.prmtadminOrgUnit.setCommitFormat("$number$");		
        this.prmtadminOrgUnit.setRequired(true);
        		setOrgF7(prmtadminOrgUnit,com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"));
					
        prmtadminOrgUnit.addDataChangeListener(new DataChangeListener() {
		public void dataChanged(DataChangeEvent e) {
			try {
				prmtadminOrgUnit_Changed();
			}
			catch (Exception exc) {
				handUIException(exc);
			}
		}
	});

        // prmtcompany		
        this.prmtcompany.setQueryInfo("com.kingdee.eas.basedata.org.app.CompanyOrgUnitQuery");		
        this.prmtcompany.setEditable(true);		
        this.prmtcompany.setDisplayFormat("$name$");		
        this.prmtcompany.setEditFormat("$number$");		
        this.prmtcompany.setCommitFormat("$number$");		
        this.prmtcompany.setRequired(false);
        // txtcompanyNumber		
        this.txtcompanyNumber.setHorizontalAlignment(2);		
        this.txtcompanyNumber.setMaxLength(80);		
        this.txtcompanyNumber.setRequired(false);
        // txtsubjectName		
        this.txtsubjectName.setHorizontalAlignment(2);		
        this.txtsubjectName.setMaxLength(255);		
        this.txtsubjectName.setRequired(false);
        // prmttaskSource		
        this.prmttaskSource.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.TaskSourceQuery");		
        this.prmttaskSource.setEditable(true);		
        this.prmttaskSource.setDisplayFormat("$name$");		
        this.prmttaskSource.setEditFormat("$number$");		
        this.prmttaskSource.setCommitFormat("$number$");		
        this.prmttaskSource.setRequired(false);
        		EntityViewInfo eviprmttaskSource = new EntityViewInfo ();
		eviprmttaskSource.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmttaskSource.setEntityViewInfo(eviprmttaskSource);
					
        // prmtspecial		
        this.prmtspecial.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.SpecialQuery");		
        this.prmtspecial.setEditable(true);		
        this.prmtspecial.setDisplayFormat("$name$");		
        this.prmtspecial.setEditFormat("$number$");		
        this.prmtspecial.setCommitFormat("$number$");		
        this.prmtspecial.setRequired(false);
        		EntityViewInfo eviprmtspecial = new EntityViewInfo ();
		eviprmtspecial.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtspecial.setEntityViewInfo(eviprmtspecial);
					
        // prmtpassFlag		
        this.prmtpassFlag.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.PassStatusQuery");		
        this.prmtpassFlag.setEditable(true);		
        this.prmtpassFlag.setDisplayFormat("$name$");		
        this.prmtpassFlag.setEditFormat("$number$");		
        this.prmtpassFlag.setCommitFormat("$number$");		
        this.prmtpassFlag.setRequired(false);
        		EntityViewInfo eviprmtpassFlag = new EntityViewInfo ();
		eviprmtpassFlag.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtpassFlag.setEntityViewInfo(eviprmtpassFlag);
					
        // prmtapprovalFlag		
        this.prmtapprovalFlag.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.UpReportStatusQuery");		
        this.prmtapprovalFlag.setEditable(true);		
        this.prmtapprovalFlag.setDisplayFormat("$name$");		
        this.prmtapprovalFlag.setEditFormat("$number$");		
        this.prmtapprovalFlag.setCommitFormat("$number$");		
        this.prmtapprovalFlag.setRequired(false);
        		EntityViewInfo eviprmtapprovalFlag = new EntityViewInfo ();
		eviprmtapprovalFlag.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtapprovalFlag.setEntityViewInfo(eviprmtapprovalFlag);
					
        // scrollPanesubjectResult
        // txtsubjectResult		
        this.txtsubjectResult.setRequired(false);		
        this.txtsubjectResult.setMaxLength(2550);
        // prmtoperType		
        this.prmtoperType.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.OperateFlagQuery");		
        this.prmtoperType.setEditable(true);		
        this.prmtoperType.setDisplayFormat("$name$");		
        this.prmtoperType.setEditFormat("$number$");		
        this.prmtoperType.setCommitFormat("$number$");		
        this.prmtoperType.setRequired(false);
        		EntityViewInfo eviprmtoperType = new EntityViewInfo ();
		eviprmtoperType.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtoperType.setEntityViewInfo(eviprmtoperType);
					
        // scrollPaneremark
        // txtremark		
        this.txtremark.setRequired(false);		
        this.txtremark.setMaxLength(2550);
        // kDPanel2		
        this.kDPanel2.setVisible(true);
        // kdtDeliberationEntry
		String kdtDeliberationEntryStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:NumberFormat>&amp;int</c:NumberFormat></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"seq\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"deliberationPerson\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"deliberationResult\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{seq}</t:Cell><t:Cell>$Resource{deliberationPerson}</t:Cell><t:Cell>$Resource{deliberationResult}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtDeliberationEntry.setFormatXml(resHelper.translateString("kdtDeliberationEntry",kdtDeliberationEntryStrXML));

                this.kdtDeliberationEntry.putBindContents("editData",new String[] {"seq","deliberationPerson","deliberationResult"});


        this.kdtDeliberationEntry.checkParsed();
        KDFormattedTextField kdtDeliberationEntry_seq_TextField = new KDFormattedTextField();
        kdtDeliberationEntry_seq_TextField.setName("kdtDeliberationEntry_seq_TextField");
        kdtDeliberationEntry_seq_TextField.setVisible(true);
        kdtDeliberationEntry_seq_TextField.setEditable(true);
        kdtDeliberationEntry_seq_TextField.setHorizontalAlignment(2);
        kdtDeliberationEntry_seq_TextField.setDataType(0);
        KDTDefaultCellEditor kdtDeliberationEntry_seq_CellEditor = new KDTDefaultCellEditor(kdtDeliberationEntry_seq_TextField);
        this.kdtDeliberationEntry.getColumn("seq").setEditor(kdtDeliberationEntry_seq_CellEditor);
        final KDBizPromptBox kdtDeliberationEntry_deliberationPerson_PromptBox = new KDBizPromptBox();
        kdtDeliberationEntry_deliberationPerson_PromptBox.setQueryInfo("com.kingdee.eas.basedata.person.app.PersonQuery");
        kdtDeliberationEntry_deliberationPerson_PromptBox.setVisible(true);
        kdtDeliberationEntry_deliberationPerson_PromptBox.setEditable(true);
        kdtDeliberationEntry_deliberationPerson_PromptBox.setDisplayFormat("$number$");
        kdtDeliberationEntry_deliberationPerson_PromptBox.setEditFormat("$number$");
        kdtDeliberationEntry_deliberationPerson_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtDeliberationEntry_deliberationPerson_CellEditor = new KDTDefaultCellEditor(kdtDeliberationEntry_deliberationPerson_PromptBox);
        this.kdtDeliberationEntry.getColumn("deliberationPerson").setEditor(kdtDeliberationEntry_deliberationPerson_CellEditor);
        ObjectValueRender kdtDeliberationEntry_deliberationPerson_OVR = new ObjectValueRender();
        kdtDeliberationEntry_deliberationPerson_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtDeliberationEntry.getColumn("deliberationPerson").setRenderer(kdtDeliberationEntry_deliberationPerson_OVR);
        KDTextArea kdtDeliberationEntry_deliberationResult_TextArea = new KDTextArea();
        kdtDeliberationEntry_deliberationResult_TextArea.setName("kdtDeliberationEntry_deliberationResult_TextArea");
        kdtDeliberationEntry_deliberationResult_TextArea.setMaxLength(2000);
        KDTDefaultCellEditor kdtDeliberationEntry_deliberationResult_CellEditor = new KDTDefaultCellEditor(kdtDeliberationEntry_deliberationResult_TextArea);
        this.kdtDeliberationEntry.getColumn("deliberationResult").setEditor(kdtDeliberationEntry_deliberationResult_CellEditor);
        // txtsubjectNum		
        this.txtsubjectNum.setHorizontalAlignment(2);		
        this.txtsubjectNum.setMaxLength(255);		
        this.txtsubjectNum.setRequired(false);
        // state		
        this.state.setVisible(false);		
        this.state.addItems(EnumUtils.getEnumList("com.kingdee.eas.custom.tiog.bizbill.BillState").toArray());		
        this.state.setRequired(false);		
        this.state.setEnabled(false);
        // kDPanel1		
        this.kDPanel1.setVisible(true);
        // kdtAttendanceEntry
		String kdtAttendanceEntryStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:NumberFormat>&amp;int</c:NumberFormat></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"seq\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"attendancePerson\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"position\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{seq}</t:Cell><t:Cell>$Resource{attendancePerson}</t:Cell><t:Cell>$Resource{position}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtAttendanceEntry.setFormatXml(resHelper.translateString("kdtAttendanceEntry",kdtAttendanceEntryStrXML));

                this.kdtAttendanceEntry.putBindContents("editData",new String[] {"seq","attendancePerson","position"});


        this.kdtAttendanceEntry.checkParsed();
        KDFormattedTextField kdtAttendanceEntry_seq_TextField = new KDFormattedTextField();
        kdtAttendanceEntry_seq_TextField.setName("kdtAttendanceEntry_seq_TextField");
        kdtAttendanceEntry_seq_TextField.setVisible(true);
        kdtAttendanceEntry_seq_TextField.setEditable(true);
        kdtAttendanceEntry_seq_TextField.setHorizontalAlignment(2);
        kdtAttendanceEntry_seq_TextField.setDataType(0);
        KDTDefaultCellEditor kdtAttendanceEntry_seq_CellEditor = new KDTDefaultCellEditor(kdtAttendanceEntry_seq_TextField);
        this.kdtAttendanceEntry.getColumn("seq").setEditor(kdtAttendanceEntry_seq_CellEditor);
        final KDBizPromptBox kdtAttendanceEntry_attendancePerson_PromptBox = new KDBizPromptBox();
        kdtAttendanceEntry_attendancePerson_PromptBox.setQueryInfo("com.kingdee.eas.basedata.person.app.PersonQuery");
        kdtAttendanceEntry_attendancePerson_PromptBox.setVisible(true);
        kdtAttendanceEntry_attendancePerson_PromptBox.setEditable(true);
        kdtAttendanceEntry_attendancePerson_PromptBox.setDisplayFormat("$number$");
        kdtAttendanceEntry_attendancePerson_PromptBox.setEditFormat("$number$");
        kdtAttendanceEntry_attendancePerson_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtAttendanceEntry_attendancePerson_CellEditor = new KDTDefaultCellEditor(kdtAttendanceEntry_attendancePerson_PromptBox);
        this.kdtAttendanceEntry.getColumn("attendancePerson").setEditor(kdtAttendanceEntry_attendancePerson_CellEditor);
        ObjectValueRender kdtAttendanceEntry_attendancePerson_OVR = new ObjectValueRender();
        kdtAttendanceEntry_attendancePerson_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtAttendanceEntry.getColumn("attendancePerson").setRenderer(kdtAttendanceEntry_attendancePerson_OVR);
        final KDBizPromptBox kdtAttendanceEntry_position_PromptBox = new KDBizPromptBox();
        kdtAttendanceEntry_position_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.PositionQuery");
        kdtAttendanceEntry_position_PromptBox.setVisible(true);
        kdtAttendanceEntry_position_PromptBox.setEditable(true);
        kdtAttendanceEntry_position_PromptBox.setDisplayFormat("$number$");
        kdtAttendanceEntry_position_PromptBox.setEditFormat("$number$");
        kdtAttendanceEntry_position_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtAttendanceEntry_position_CellEditor = new KDTDefaultCellEditor(kdtAttendanceEntry_position_PromptBox);
        this.kdtAttendanceEntry.getColumn("position").setEditor(kdtAttendanceEntry_position_CellEditor);
        ObjectValueRender kdtAttendanceEntry_position_OVR = new ObjectValueRender();
        kdtAttendanceEntry_position_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtAttendanceEntry.getColumn("position").setRenderer(kdtAttendanceEntry_position_OVR);
        // prmtmeetingType		
        this.prmtmeetingType.setQueryInfo("com.kingdee.eas.custom.tiog.basedata.app.MeetingTypeQuery");		
        this.prmtmeetingType.setVisible(true);		
        this.prmtmeetingType.setEditable(true);		
        this.prmtmeetingType.setDisplayFormat("$name$");		
        this.prmtmeetingType.setEditFormat("$number$");		
        this.prmtmeetingType.setCommitFormat("$number$");		
        this.prmtmeetingType.setRequired(false);
        		EntityViewInfo eviprmtmeetingType = new EntityViewInfo ();
		eviprmtmeetingType.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		prmtmeetingType.setEntityViewInfo(eviprmtmeetingType);
					
        // kDPanel3		
        this.kDPanel3.setVisible(true);
        // kdtRefSubjectEntry
		String kdtRefSubjectEntryStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:NumberFormat>&amp;int</c:NumberFormat></c:Style><c:Style id=\"sCol3\"><c:Protection locked=\"true\" /></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"seq\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"company\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"refSubject\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"refSubjectNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol3\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{seq}</t:Cell><t:Cell>$Resource{company}</t:Cell><t:Cell>$Resource{refSubject}</t:Cell><t:Cell>$Resource{refSubjectNumber}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtRefSubjectEntry.setFormatXml(resHelper.translateString("kdtRefSubjectEntry",kdtRefSubjectEntryStrXML));
        kdtRefSubjectEntry.addKDTEditListener(new KDTEditAdapter() {
		public void editStopped(KDTEditEvent e) {
			try {
				kdtRefSubjectEntry_Changed(e.getRowIndex(),e.getColIndex());
			}
			catch (Exception exc) {
				handUIException(exc);
			}
		}
	});


                this.kdtRefSubjectEntry.putBindContents("editData",new String[] {"seq","company","refSubject","refSubjectNumber"});


        this.kdtRefSubjectEntry.checkParsed();
        KDFormattedTextField kdtRefSubjectEntry_seq_TextField = new KDFormattedTextField();
        kdtRefSubjectEntry_seq_TextField.setName("kdtRefSubjectEntry_seq_TextField");
        kdtRefSubjectEntry_seq_TextField.setVisible(true);
        kdtRefSubjectEntry_seq_TextField.setEditable(true);
        kdtRefSubjectEntry_seq_TextField.setHorizontalAlignment(2);
        kdtRefSubjectEntry_seq_TextField.setDataType(0);
        KDTDefaultCellEditor kdtRefSubjectEntry_seq_CellEditor = new KDTDefaultCellEditor(kdtRefSubjectEntry_seq_TextField);
        this.kdtRefSubjectEntry.getColumn("seq").setEditor(kdtRefSubjectEntry_seq_CellEditor);
        final KDBizPromptBox kdtRefSubjectEntry_company_PromptBox = new KDBizPromptBox();
        kdtRefSubjectEntry_company_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.CompanyOrgUnitQuery4AsstAcct");
        kdtRefSubjectEntry_company_PromptBox.setVisible(true);
        kdtRefSubjectEntry_company_PromptBox.setEditable(true);
        kdtRefSubjectEntry_company_PromptBox.setDisplayFormat("$number$");
        kdtRefSubjectEntry_company_PromptBox.setEditFormat("$number$");
        kdtRefSubjectEntry_company_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtRefSubjectEntry_company_CellEditor = new KDTDefaultCellEditor(kdtRefSubjectEntry_company_PromptBox);
        this.kdtRefSubjectEntry.getColumn("company").setEditor(kdtRefSubjectEntry_company_CellEditor);
        ObjectValueRender kdtRefSubjectEntry_company_OVR = new ObjectValueRender();
        kdtRefSubjectEntry_company_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtRefSubjectEntry.getColumn("company").setRenderer(kdtRefSubjectEntry_company_OVR);
        final KDBizPromptBox kdtRefSubjectEntry_refSubject_PromptBox = new KDBizPromptBox();
        kdtRefSubjectEntry_refSubject_PromptBox.setQueryInfo("com.kingdee.eas.custom.tiog.bizbill.app.SubjectQuery");
        kdtRefSubjectEntry_refSubject_PromptBox.setVisible(true);
        kdtRefSubjectEntry_refSubject_PromptBox.setEditable(true);
        kdtRefSubjectEntry_refSubject_PromptBox.setDisplayFormat("$number$");
        kdtRefSubjectEntry_refSubject_PromptBox.setEditFormat("$number$");
        kdtRefSubjectEntry_refSubject_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtRefSubjectEntry_refSubject_CellEditor = new KDTDefaultCellEditor(kdtRefSubjectEntry_refSubject_PromptBox);
        this.kdtRefSubjectEntry.getColumn("refSubject").setEditor(kdtRefSubjectEntry_refSubject_CellEditor);
        ObjectValueRender kdtRefSubjectEntry_refSubject_OVR = new ObjectValueRender();
        kdtRefSubjectEntry_refSubject_OVR.setFormat(new BizDataFormat("$subjectName$"));
        this.kdtRefSubjectEntry.getColumn("refSubject").setRenderer(kdtRefSubjectEntry_refSubject_OVR);
        			EntityViewInfo evikdtRefSubjectEntry_refSubject_PromptBox = new EntityViewInfo ();
		evikdtRefSubjectEntry_refSubject_PromptBox.setFilter(com.kingdee.eas.framework.FrameWorkUtils.getF7FilterInfoByAuthorizedOrg(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin"),"adminOrgUnit.id"));
		kdtRefSubjectEntry_refSubject_PromptBox.setEntityViewInfo(evikdtRefSubjectEntry_refSubject_PromptBox);
					
        KDTextField kdtRefSubjectEntry_refSubjectNumber_TextField = new KDTextField();
        kdtRefSubjectEntry_refSubjectNumber_TextField.setName("kdtRefSubjectEntry_refSubjectNumber_TextField");
        kdtRefSubjectEntry_refSubjectNumber_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtRefSubjectEntry_refSubjectNumber_CellEditor = new KDTDefaultCellEditor(kdtRefSubjectEntry_refSubjectNumber_TextField);
        this.kdtRefSubjectEntry.getColumn("refSubjectNumber").setEditor(kdtRefSubjectEntry_refSubjectNumber_CellEditor);
        // genMeeting
        this.genMeeting.setAction((IItemAction)ActionProxyFactory.getProxy(actionGenMeeting, new Class[] { IItemAction.class }, getServiceContext()));		
        this.genMeeting.setText(resHelper.getString("genMeeting.text"));
        this.setFocusTraversalPolicy(new com.kingdee.bos.ui.UIFocusTraversalPolicy(new java.awt.Component[] {kDDateLastUpdateTime,prmtLastUpdateUser,kDDateCreateTime,prmtCreator,prmtAuditor,txtDescription,pkBizDate,kdtOtherEntrys,txtNumber,prmtcompany,txtcompanyNumber,txtsubjectName,prmttaskSource,prmtspecial,prmtpassFlag,prmtapprovalFlag,chkadoptFlag,txtsubjectResult,chksuperviseFlag,prmtoperType,txtremark,txtsubjectNum,prmtadminOrgUnit,kdtDeliberationEntry,kdtAttendanceEntry,kdtRefSubjectEntry,kdtCatalogEntry,state,chkisUpload,prmtmeetingType}));
        this.setFocusCycleRoot(true);
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(0, 0, 1013, 989));
        this.setLayout(new KDLayout());
        this.putClientProperty("OriginalBounds", new Rectangle(0, 0, 1013, 989));
        contCreator.setBounds(new Rectangle(21, 844, 270, 19));
        this.add(contCreator, new KDLayout.Constraints(21, 844, 270, 19, 0));
        contCreateTime.setBounds(new Rectangle(353, 841, 270, 19));
        this.add(contCreateTime, new KDLayout.Constraints(353, 841, 270, 19, 0));
        contLastUpdateUser.setBounds(new Rectangle(23, 876, 270, 19));
        this.add(contLastUpdateUser, new KDLayout.Constraints(23, 876, 270, 19, 0));
        contLastUpdateTime.setBounds(new Rectangle(354, 876, 270, 19));
        this.add(contLastUpdateTime, new KDLayout.Constraints(354, 876, 270, 19, 0));
        contNumber.setBounds(new Rectangle(36, 14, 270, 19));
        this.add(contNumber, new KDLayout.Constraints(36, 14, 270, 19, 0));
        contBizDate.setBounds(new Rectangle(341, 14, 270, 19));
        this.add(contBizDate, new KDLayout.Constraints(341, 14, 270, 19, 0));
        contDescription.setBounds(new Rectangle(692, 42, 270, 19));
        this.add(contDescription, new KDLayout.Constraints(692, 42, 270, 19, 0));
        contAuditor.setBounds(new Rectangle(685, 876, 270, 19));
        this.add(contAuditor, new KDLayout.Constraints(685, 876, 270, 19, 0));
        paneBIZLayerControl17.setBounds(new Rectangle(33, 302, 793, 301));
        this.add(paneBIZLayerControl17, new KDLayout.Constraints(33, 302, 793, 301, 0));
        contadminOrgUnit.setBounds(new Rectangle(692, 14, 270, 19));
        this.add(contadminOrgUnit, new KDLayout.Constraints(692, 14, 270, 19, 0));
        contcompany.setBounds(new Rectangle(341, 42, 270, 19));
        this.add(contcompany, new KDLayout.Constraints(341, 42, 270, 19, 0));
        contcompanyNumber.setBounds(new Rectangle(343, 74, 270, 19));
        this.add(contcompanyNumber, new KDLayout.Constraints(343, 74, 270, 19, 0));
        contsubjectName.setBounds(new Rectangle(38, 74, 270, 19));
        this.add(contsubjectName, new KDLayout.Constraints(38, 74, 270, 19, 0));
        conttaskSource.setBounds(new Rectangle(34, 130, 270, 19));
        this.add(conttaskSource, new KDLayout.Constraints(34, 130, 270, 19, 0));
        contspecial.setBounds(new Rectangle(34, 102, 270, 19));
        this.add(contspecial, new KDLayout.Constraints(34, 102, 270, 19, 0));
        contpassFlag.setBounds(new Rectangle(339, 130, 270, 19));
        this.add(contpassFlag, new KDLayout.Constraints(339, 130, 270, 19, 0));
        contapprovalFlag.setBounds(new Rectangle(34, 163, 270, 19));
        this.add(contapprovalFlag, new KDLayout.Constraints(34, 163, 270, 19, 0));
        contsubjectResult.setBounds(new Rectangle(36, 201, 603, 41));
        this.add(contsubjectResult, new KDLayout.Constraints(36, 201, 603, 41, 0));
        contoperType.setBounds(new Rectangle(339, 102, 270, 19));
        this.add(contoperType, new KDLayout.Constraints(339, 102, 270, 19, 0));
        contremark.setBounds(new Rectangle(36, 250, 603, 41));
        this.add(contremark, new KDLayout.Constraints(36, 250, 603, 41, 0));
        kDTabbedPane1.setBounds(new Rectangle(19, 917, 346, 195));
        this.add(kDTabbedPane1, new KDLayout.Constraints(19, 917, 346, 195, 0));
        chkadoptFlag.setBounds(new Rectangle(597, 164, 146, 19));
        this.add(chkadoptFlag, new KDLayout.Constraints(597, 164, 146, 19, 0));
        contsubjectNum.setBounds(new Rectangle(36, 42, 270, 19));
        this.add(contsubjectNum, new KDLayout.Constraints(36, 42, 270, 19, 0));
        contstate.setBounds(new Rectangle(692, 74, 270, 19));
        this.add(contstate, new KDLayout.Constraints(692, 74, 270, 19, 0));
        chkisUpload.setBounds(new Rectangle(789, 233, 117, 19));
        this.add(chkisUpload, new KDLayout.Constraints(789, 233, 117, 19, 0));
        kDTabbedPane2.setBounds(new Rectangle(607, 902, 343, 209));
        this.add(kDTabbedPane2, new KDLayout.Constraints(607, 902, 343, 209, 0));
        chksuperviseFlag.setBounds(new Rectangle(339, 163, 141, 19));
        this.add(chksuperviseFlag, new KDLayout.Constraints(339, 163, 141, 19, 0));
        contmeetingType.setBounds(new Rectangle(692, 126, 270, 19));
        this.add(contmeetingType, new KDLayout.Constraints(692, 126, 270, 19, 0));
        kDTabbedPane3.setBounds(new Rectangle(33, 626, 789, 189));
        this.add(kDTabbedPane3, new KDLayout.Constraints(33, 626, 789, 189, 0));
        //contCreator
        contCreator.setBoundEditor(prmtCreator);
        //contCreateTime
        contCreateTime.setBoundEditor(kDDateCreateTime);
        //contLastUpdateUser
        contLastUpdateUser.setBoundEditor(prmtLastUpdateUser);
        //contLastUpdateTime
        contLastUpdateTime.setBoundEditor(kDDateLastUpdateTime);
        //contNumber
        contNumber.setBoundEditor(txtNumber);
        //contBizDate
        contBizDate.setBoundEditor(pkBizDate);
        //contDescription
        contDescription.setBoundEditor(txtDescription);
        //contAuditor
        contAuditor.setBoundEditor(prmtAuditor);
        //paneBIZLayerControl17
        paneBIZLayerControl17.add(catalogTab, resHelper.getString("catalogTab.constraints"));
        paneBIZLayerControl17.add(otherTab, resHelper.getString("otherTab.constraints"));
        //catalogTab
        catalogTab.setLayout(null);        kdtCatalogEntry.setBounds(new Rectangle(3, 2, 782, 264));
        kdtCatalogEntry_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtCatalogEntry,new com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryInfo(),null,false);
        catalogTab.add(kdtCatalogEntry_detailPanel, null);
        //otherTab
        otherTab.setLayout(null);        kdtOtherEntrys.setBounds(new Rectangle(1, 4, 783, 268));
        kdtOtherEntrys_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtOtherEntrys,new com.kingdee.eas.custom.tiog.bizbill.SubjectOtherEntryInfo(),null,false);
        otherTab.add(kdtOtherEntrys_detailPanel, null);
        //contadminOrgUnit
        contadminOrgUnit.setBoundEditor(prmtadminOrgUnit);
        //contcompany
        contcompany.setBoundEditor(prmtcompany);
        //contcompanyNumber
        contcompanyNumber.setBoundEditor(txtcompanyNumber);
        //contsubjectName
        contsubjectName.setBoundEditor(txtsubjectName);
        //conttaskSource
        conttaskSource.setBoundEditor(prmttaskSource);
        //contspecial
        contspecial.setBoundEditor(prmtspecial);
        //contpassFlag
        contpassFlag.setBoundEditor(prmtpassFlag);
        //contapprovalFlag
        contapprovalFlag.setBoundEditor(prmtapprovalFlag);
        //contsubjectResult
        contsubjectResult.setBoundEditor(scrollPanesubjectResult);
        //scrollPanesubjectResult
        scrollPanesubjectResult.getViewport().add(txtsubjectResult, null);
        //contoperType
        contoperType.setBoundEditor(prmtoperType);
        //contremark
        contremark.setBoundEditor(scrollPaneremark);
        //scrollPaneremark
        scrollPaneremark.getViewport().add(txtremark, null);
        //kDTabbedPane1
        kDTabbedPane1.add(kDPanel2, resHelper.getString("kDPanel2.constraints"));
        //kDPanel2
        kDPanel2.setLayout(null);        kdtDeliberationEntry.setBounds(new Rectangle(11, 8, 952, 155));
        kdtDeliberationEntry_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtDeliberationEntry,new com.kingdee.eas.custom.tiog.bizbill.SubjectDeliberationEntryInfo(),null,false);
        kDPanel2.add(kdtDeliberationEntry_detailPanel, null);
        //contsubjectNum
        contsubjectNum.setBoundEditor(txtsubjectNum);
        //contstate
        contstate.setBoundEditor(state);
        //kDTabbedPane2
        kDTabbedPane2.add(kDPanel1, resHelper.getString("kDPanel1.constraints"));
        //kDPanel1
        kDPanel1.setLayout(null);        kdtAttendanceEntry.setBounds(new Rectangle(10, 10, 954, 163));
        kdtAttendanceEntry_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtAttendanceEntry,new com.kingdee.eas.custom.tiog.bizbill.SubjectAttendanceEntryInfo(),null,false);
        kDPanel1.add(kdtAttendanceEntry_detailPanel, null);
        //contmeetingType
        contmeetingType.setBoundEditor(prmtmeetingType);
        //kDTabbedPane3
        kDTabbedPane3.add(kDPanel3, resHelper.getString("kDPanel3.constraints"));
        //kDPanel3
        kDPanel3.setLayout(null);        kdtRefSubjectEntry.setBounds(new Rectangle(-1, 1, 780, 154));
        kdtRefSubjectEntry_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtRefSubjectEntry,new com.kingdee.eas.custom.tiog.bizbill.SubjectRefSubjectEntryInfo(),null,false);
        kDPanel3.add(kdtRefSubjectEntry_detailPanel, null);

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {
        this.menuBar.add(menuFile);
        this.menuBar.add(menuEdit);
        this.menuBar.add(MenuService);
        this.menuBar.add(menuView);
        this.menuBar.add(menuBiz);
        this.menuBar.add(menuTable1);
        this.menuBar.add(menuTool);
        this.menuBar.add(menuWorkflow);
        this.menuBar.add(menuHelp);
        //menuFile
        menuFile.add(menuItemAddNew);
        menuFile.add(kDSeparator1);
        menuFile.add(menuItemCloudFeed);
        menuFile.add(menuItemSave);
        menuFile.add(menuItemCloudScreen);
        menuFile.add(menuItemSubmit);
        menuFile.add(menuItemCloudShare);
        menuFile.add(menuSubmitOption);
        menuFile.add(kdSeparatorFWFile1);
        menuFile.add(rMenuItemSubmit);
        menuFile.add(rMenuItemSubmitAndAddNew);
        menuFile.add(rMenuItemSubmitAndPrint);
        menuFile.add(separatorFile1);
        menuFile.add(MenuItemAttachment);
        menuFile.add(kDSeparator2);
        menuFile.add(menuItemPageSetup);
        menuFile.add(menuItemPrint);
        menuFile.add(menuItemPrintPreview);
        menuFile.add(kDSeparator6);
        menuFile.add(menuItemSendMail);
        menuFile.add(kDSeparator3);
        menuFile.add(menuItemExitCurrent);
        //menuSubmitOption
        menuSubmitOption.add(chkMenuItemSubmitAndAddNew);
        menuSubmitOption.add(chkMenuItemSubmitAndPrint);
        //menuEdit
        menuEdit.add(menuItemCopy);
        menuEdit.add(menuItemEdit);
        menuEdit.add(menuItemRemove);
        menuEdit.add(kDSeparator4);
        menuEdit.add(menuItemReset);
        menuEdit.add(separator1);
        menuEdit.add(menuItemCreateFrom);
        menuEdit.add(menuItemCreateTo);
        menuEdit.add(menuItemCopyFrom);
        menuEdit.add(separatorEdit1);
        menuEdit.add(menuItemEnterToNextRow);
        menuEdit.add(separator2);
        //MenuService
        MenuService.add(MenuItemKnowStore);
        MenuService.add(MenuItemAnwser);
        MenuService.add(SepratorService);
        MenuService.add(MenuItemRemoteAssist);
        //menuView
        menuView.add(menuItemFirst);
        menuView.add(menuItemPre);
        menuView.add(menuItemNext);
        menuView.add(menuItemLast);
        menuView.add(separator3);
        menuView.add(menuItemTraceUp);
        menuView.add(menuItemTraceDown);
        menuView.add(kDSeparator7);
        menuView.add(menuItemLocate);
        //menuBiz
        menuBiz.add(menuItemCancelCancel);
        menuBiz.add(menuItemCancel);
        menuBiz.add(MenuItemVoucher);
        menuBiz.add(menuItemDelVoucher);
        menuBiz.add(MenuItemPCVoucher);
        menuBiz.add(menuItemDelPCVoucher);
        //menuTable1
        menuTable1.add(menuItemAddLine);
        menuTable1.add(menuItemCopyLine);
        menuTable1.add(menuItemInsertLine);
        menuTable1.add(menuItemRemoveLine);
        //menuTool
        menuTool.add(menuItemSendMessage);
        menuTool.add(menuItemMsgFormat);
        menuTool.add(menuItemCalculator);
        menuTool.add(menuItemToolBarCustom);
        //menuWorkflow
        menuWorkflow.add(menuItemStartWorkFlow);
        menuWorkflow.add(separatorWF1);
        menuWorkflow.add(menuItemViewSubmitProccess);
        menuWorkflow.add(menuItemViewDoProccess);
        menuWorkflow.add(MenuItemWFG);
        menuWorkflow.add(menuItemWorkFlowList);
        menuWorkflow.add(separatorWF2);
        menuWorkflow.add(menuItemMultiapprove);
        menuWorkflow.add(menuItemNextPerson);
        menuWorkflow.add(menuItemAuditResult);
        menuWorkflow.add(kDSeparator5);
        menuWorkflow.add(kDMenuItemSendMessage);
        //menuHelp
        menuHelp.add(menuItemHelp);
        menuHelp.add(kDSeparator12);
        menuHelp.add(menuItemRegPro);
        menuHelp.add(menuItemPersonalSite);
        menuHelp.add(helpseparatorDiv);
        menuHelp.add(menuitemProductval);
        menuHelp.add(kDSeparatorProduct);
        menuHelp.add(menuItemAbout);

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {
        this.toolBar.add(btnAddNew);
        this.toolBar.add(btnCloud);
        this.toolBar.add(btnEdit);
        this.toolBar.add(btnXunTong);
        this.toolBar.add(btnSave);
        this.toolBar.add(kDSeparatorCloud);
        this.toolBar.add(btnReset);
        this.toolBar.add(btnSubmit);
        this.toolBar.add(btnCopy);
        this.toolBar.add(btnRemove);
        this.toolBar.add(btnCancelCancel);
        this.toolBar.add(btnCancel);
        this.toolBar.add(btnAttachment);
        this.toolBar.add(separatorFW1);
        this.toolBar.add(btnPageSetup);
        this.toolBar.add(btnPrint);
        this.toolBar.add(btnPrintPreview);
        this.toolBar.add(separatorFW2);
        this.toolBar.add(btnFirst);
        this.toolBar.add(btnPre);
        this.toolBar.add(btnNext);
        this.toolBar.add(btnLast);
        this.toolBar.add(separatorFW3);
        this.toolBar.add(btnTraceUp);
        this.toolBar.add(btnTraceDown);
        this.toolBar.add(btnWorkFlowG);
        this.toolBar.add(btnSignature);
        this.toolBar.add(btnViewSignature);
        this.toolBar.add(separatorFW4);
        this.toolBar.add(btnNumberSign);
        this.toolBar.add(separatorFW7);
        this.toolBar.add(btnCreateFrom);
        this.toolBar.add(btnCopyFrom);
        this.toolBar.add(btnCreateTo);
        this.toolBar.add(separatorFW5);
        this.toolBar.add(separatorFW8);
        this.toolBar.add(btnAddLine);
        this.toolBar.add(btnCopyLine);
        this.toolBar.add(btnInsertLine);
        this.toolBar.add(btnRemoveLine);
        this.toolBar.add(separatorFW6);
        this.toolBar.add(separatorFW9);
        this.toolBar.add(btnVoucher);
        this.toolBar.add(btnDelVoucher);
        this.toolBar.add(btnPCVoucher);
        this.toolBar.add(btnDelPCVoucher);
        this.toolBar.add(btnAuditResult);
        this.toolBar.add(btnMultiapprove);
        this.toolBar.add(btnWFViewdoProccess);
        this.toolBar.add(btnWFViewSubmitProccess);
        this.toolBar.add(btnNextPerson);
        this.toolBar.add(genMeeting);


    }

	//Regiester control's property binding.
	private void registerBindings(){
		dataBinder.registerBinding("adoptFlag", boolean.class, this.chkadoptFlag, "selected");
		dataBinder.registerBinding("isUpload", boolean.class, this.chkisUpload, "selected");
		dataBinder.registerBinding("superviseFlag", boolean.class, this.chksuperviseFlag, "selected");
		dataBinder.registerBinding("creator", com.kingdee.eas.base.permission.UserInfo.class, this.prmtCreator, "data");
		dataBinder.registerBinding("createTime", java.sql.Timestamp.class, this.kDDateCreateTime, "value");
		dataBinder.registerBinding("lastUpdateUser", com.kingdee.eas.base.permission.UserInfo.class, this.prmtLastUpdateUser, "data");
		dataBinder.registerBinding("lastUpdateTime", java.sql.Timestamp.class, this.kDDateLastUpdateTime, "value");
		dataBinder.registerBinding("number", String.class, this.txtNumber, "text");
		dataBinder.registerBinding("bizDate", java.util.Date.class, this.pkBizDate, "value");
		dataBinder.registerBinding("description", String.class, this.txtDescription, "text");
		dataBinder.registerBinding("auditor", com.kingdee.eas.base.permission.UserInfo.class, this.prmtAuditor, "data");
		dataBinder.registerBinding("CatalogEntry.seq", int.class, this.kdtCatalogEntry, "seq.text");
		dataBinder.registerBinding("CatalogEntry", com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryInfo.class, this.kdtCatalogEntry, "userObject");
		dataBinder.registerBinding("CatalogEntry.catalog", java.lang.Object.class, this.kdtCatalogEntry, "catalog.text");
		dataBinder.registerBinding("CatalogEntry.meetingType", java.lang.Object.class, this.kdtCatalogEntry, "meetingType.text");
		dataBinder.registerBinding("CatalogEntry.catalogMain", java.lang.Object.class, this.kdtCatalogEntry, "catalogMain.text");
		dataBinder.registerBinding("CatalogEntry.catalogMainNumber", String.class, this.kdtCatalogEntry, "catalogMainNumber.text");
		dataBinder.registerBinding("OtherEntrys.id", com.kingdee.bos.util.BOSUuid.class, this.kdtOtherEntrys, "id.text");
		dataBinder.registerBinding("OtherEntrys", com.kingdee.eas.custom.tiog.bizbill.SubjectOtherEntryInfo.class, this.kdtOtherEntrys, "userObject");
		dataBinder.registerBinding("adminOrgUnit", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.prmtadminOrgUnit, "data");
		dataBinder.registerBinding("company", com.kingdee.eas.basedata.org.CompanyOrgUnitInfo.class, this.prmtcompany, "data");
		dataBinder.registerBinding("companyNumber", String.class, this.txtcompanyNumber, "text");
		dataBinder.registerBinding("subjectName", String.class, this.txtsubjectName, "text");
		dataBinder.registerBinding("taskSource", com.kingdee.eas.custom.tiog.basedata.TaskSourceInfo.class, this.prmttaskSource, "data");
		dataBinder.registerBinding("special", com.kingdee.eas.custom.tiog.basedata.SpecialInfo.class, this.prmtspecial, "data");
		dataBinder.registerBinding("passFlag", com.kingdee.eas.custom.tiog.basedata.PassStatusInfo.class, this.prmtpassFlag, "data");
		dataBinder.registerBinding("approvalFlag", com.kingdee.eas.custom.tiog.basedata.UpReportStatusInfo.class, this.prmtapprovalFlag, "data");
		dataBinder.registerBinding("subjectResult", String.class, this.txtsubjectResult, "text");
		dataBinder.registerBinding("operType", com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo.class, this.prmtoperType, "data");
		dataBinder.registerBinding("remark", String.class, this.txtremark, "text");
		dataBinder.registerBinding("DeliberationEntry.seq", int.class, this.kdtDeliberationEntry, "seq.text");
		dataBinder.registerBinding("DeliberationEntry", com.kingdee.eas.custom.tiog.bizbill.SubjectDeliberationEntryInfo.class, this.kdtDeliberationEntry, "userObject");
		dataBinder.registerBinding("DeliberationEntry.deliberationPerson", java.lang.Object.class, this.kdtDeliberationEntry, "deliberationPerson.text");
		dataBinder.registerBinding("DeliberationEntry.deliberationResult", String.class, this.kdtDeliberationEntry, "deliberationResult.text");
		dataBinder.registerBinding("subjectNum", String.class, this.txtsubjectNum, "text");
		dataBinder.registerBinding("state", com.kingdee.eas.custom.tiog.bizbill.BillState.class, this.state, "selectedItem");
		dataBinder.registerBinding("AttendanceEntry.seq", int.class, this.kdtAttendanceEntry, "seq.text");
		dataBinder.registerBinding("AttendanceEntry", com.kingdee.eas.custom.tiog.bizbill.SubjectAttendanceEntryInfo.class, this.kdtAttendanceEntry, "userObject");
		dataBinder.registerBinding("AttendanceEntry.attendancePerson", java.lang.Object.class, this.kdtAttendanceEntry, "attendancePerson.text");
		dataBinder.registerBinding("AttendanceEntry.position", java.lang.Object.class, this.kdtAttendanceEntry, "position.text");
		dataBinder.registerBinding("meetingType", com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo.class, this.prmtmeetingType, "data");
		dataBinder.registerBinding("RefSubjectEntry.seq", int.class, this.kdtRefSubjectEntry, "seq.text");
		dataBinder.registerBinding("RefSubjectEntry", com.kingdee.eas.custom.tiog.bizbill.SubjectRefSubjectEntryInfo.class, this.kdtRefSubjectEntry, "userObject");
		dataBinder.registerBinding("RefSubjectEntry.company", java.lang.Object.class, this.kdtRefSubjectEntry, "company.text");
		dataBinder.registerBinding("RefSubjectEntry.refSubject", java.lang.Object.class, this.kdtRefSubjectEntry, "refSubject.text");
		dataBinder.registerBinding("RefSubjectEntry.refSubjectNumber", String.class, this.kdtRefSubjectEntry, "refSubjectNumber.text");		
	}
	//Regiester UI State
	private void registerUIState(){		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.tiog.bizbill.app.SubjectEditUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }


    /**
     * output onShow method
     */
    public void onShow() throws Exception
    {
        super.onShow();
        this.kDDateLastUpdateTime.requestFocusInWindow();
    }

	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
        this.editData = (com.kingdee.eas.custom.tiog.bizbill.SubjectInfo)ov;
    }
    protected void removeByPK(IObjectPK pk) throws Exception {
    	IObjectValue editData = this.editData;
    	super.removeByPK(pk);
    	recycleNumberByOrg(editData,"Admin",editData.getString("number"));
    }
    
    protected void recycleNumberByOrg(IObjectValue editData,String orgType,String number) {
        if (!StringUtils.isEmpty(number))
        {
            try {
            	String companyID = null;            
            	com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID =com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}				
				if (!StringUtils.isEmpty(companyID) && iCodingRuleManager.isExist(editData, companyID) && iCodingRuleManager.isUseIntermitNumber(editData, companyID)) {
					iCodingRuleManager.recycleNumber(editData,companyID,number);					
				}
            }
            catch (Exception e)
            {
                handUIException(e);
            }
        }
    }
    protected void setAutoNumberByOrg(String orgType) {
    	if (editData == null) return;
		if (editData.getNumber() == null) {
            try {
            	String companyID = null;
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID = com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}
				com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
		        if (iCodingRuleManager.isExist(editData, companyID)) {
		            if (iCodingRuleManager.isAddView(editData, companyID)) {
		            	editData.setNumber(iCodingRuleManager.getNumber(editData,companyID));
		            }
	                txtNumber.setEnabled(false);
		        }
            }
            catch (Exception e) {
                handUIException(e);
                this.oldData = editData;
                com.kingdee.eas.util.SysUtil.abort();
            } 
        } 
        else {
            if (editData.getNumber().trim().length() > 0) {
                txtNumber.setText(editData.getNumber());
            }
        }
    }
			protected com.kingdee.eas.basedata.org.OrgType getMainBizOrgType() {
			return com.kingdee.eas.basedata.org.OrgType.getEnum("Admin");
		}

	protected KDBizPromptBox getMainBizOrg() {
		return prmtadminOrgUnit;
}


    /**
     * output loadFields method
     */
    public void loadFields()
    {
        		setAutoNumberByOrg("Admin");
        dataBinder.loadFields();
    }
		protected void setOrgF7(KDBizPromptBox f7,com.kingdee.eas.basedata.org.OrgType orgType) throws Exception
		{
			com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer oufip = new com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer(orgType);
			oufip.getModel().setIsCUFilter(true);
			f7.setFilterInfoProducer(oufip);
		}

    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );
		getValidateHelper().registerBindProperty("adoptFlag", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("isUpload", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("superviseFlag", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("creator", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("createTime", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("lastUpdateUser", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("lastUpdateTime", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("bizDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("auditor", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("CatalogEntry.seq", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("CatalogEntry", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("CatalogEntry.catalog", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("CatalogEntry.meetingType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("CatalogEntry.catalogMain", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("CatalogEntry.catalogMainNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("OtherEntrys.id", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("OtherEntrys", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("adminOrgUnit", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("company", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("companyNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("subjectName", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("taskSource", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("special", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("passFlag", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("approvalFlag", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("subjectResult", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("operType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("remark", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("DeliberationEntry.seq", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("DeliberationEntry", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("DeliberationEntry.deliberationPerson", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("DeliberationEntry.deliberationResult", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("subjectNum", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("state", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("AttendanceEntry.seq", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("AttendanceEntry", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("AttendanceEntry.attendancePerson", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("AttendanceEntry.position", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("meetingType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("RefSubjectEntry.seq", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("RefSubjectEntry", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("RefSubjectEntry.company", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("RefSubjectEntry.refSubject", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("RefSubjectEntry.refSubjectNumber", ValidateHelper.ON_SAVE);    		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
        if (STATUS_ADDNEW.equals(this.oprtState)) {
        } else if (STATUS_EDIT.equals(this.oprtState)) {
        } else if (STATUS_VIEW.equals(this.oprtState)) {
        } else if (STATUS_FINDVIEW.equals(this.oprtState)) {
        }
    }


    /**
     * output kdtCatalogEntry_Changed(int rowIndex,int colIndex) method
     */
    public void kdtCatalogEntry_Changed(int rowIndex,int colIndex) throws Exception
    {
            if ("catalogMain".equalsIgnoreCase(kdtCatalogEntry.getColumn(colIndex).getKey())) {
kdtCatalogEntry.getCell(rowIndex,"catalogMainNumber").setValue(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)kdtCatalogEntry.getCell(rowIndex,"catalogMain").getValue(),"number")));

}


    }

    /**
     * output prmtadminOrgUnit_Changed() method
     */
    public void prmtadminOrgUnit_Changed() throws Exception
    {
        System.out.println("prmtadminOrgUnit_Changed() Function is executed!");
            txtcompanyNumber.setText(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)prmtadminOrgUnit.getData(),"orgCode")));


    }

    /**
     * output kdtRefSubjectEntry_Changed(int rowIndex,int colIndex) method
     */
    public void kdtRefSubjectEntry_Changed(int rowIndex,int colIndex) throws Exception
    {
            if ("refSubject".equalsIgnoreCase(kdtRefSubjectEntry.getColumn(colIndex).getKey())) {
kdtRefSubjectEntry.getCell(rowIndex,"refSubjectNumber").setValue(com.kingdee.bos.ui.face.UIRuleUtil.getString(com.kingdee.bos.ui.face.UIRuleUtil.getProperty((com.kingdee.bos.dao.IObjectValue)kdtRefSubjectEntry.getCell(rowIndex,"refSubject").getValue(),"number")));

}


    }
    /**
     * output getSelectors method
     */
    public SelectorItemCollection getSelectors()
    {
        SelectorItemCollection sic = new SelectorItemCollection();
		String selectorAll = System.getProperty("selector.all");
		if(StringUtils.isEmpty(selectorAll)){
			selectorAll = "true";
		}
        sic.add(new SelectorItemInfo("adoptFlag"));
        sic.add(new SelectorItemInfo("isUpload"));
        sic.add(new SelectorItemInfo("superviseFlag"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("creator.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("creator.id"));
        	sic.add(new SelectorItemInfo("creator.number"));
        	sic.add(new SelectorItemInfo("creator.name"));
		}
        sic.add(new SelectorItemInfo("createTime"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("lastUpdateUser.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("lastUpdateUser.id"));
        	sic.add(new SelectorItemInfo("lastUpdateUser.number"));
        	sic.add(new SelectorItemInfo("lastUpdateUser.name"));
		}
        sic.add(new SelectorItemInfo("lastUpdateTime"));
        sic.add(new SelectorItemInfo("number"));
        sic.add(new SelectorItemInfo("bizDate"));
        sic.add(new SelectorItemInfo("description"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("auditor.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("auditor.id"));
        	sic.add(new SelectorItemInfo("auditor.number"));
        	sic.add(new SelectorItemInfo("auditor.name"));
		}
    	sic.add(new SelectorItemInfo("CatalogEntry.seq"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("CatalogEntry.*"));
		}
		else{
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("CatalogEntry.catalog.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("CatalogEntry.catalog.id"));
			sic.add(new SelectorItemInfo("CatalogEntry.catalog.name"));
        	sic.add(new SelectorItemInfo("CatalogEntry.catalog.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("CatalogEntry.meetingType.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("CatalogEntry.meetingType.id"));
			sic.add(new SelectorItemInfo("CatalogEntry.meetingType.name"));
        	sic.add(new SelectorItemInfo("CatalogEntry.meetingType.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("CatalogEntry.catalogMain.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("CatalogEntry.catalogMain.id"));
			sic.add(new SelectorItemInfo("CatalogEntry.catalogMain.name"));
        	sic.add(new SelectorItemInfo("CatalogEntry.catalogMain.number"));
		}
    	sic.add(new SelectorItemInfo("CatalogEntry.catalogMainNumber"));
    	sic.add(new SelectorItemInfo("OtherEntrys.id"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("OtherEntrys.*"));
		}
		else{
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("adminOrgUnit.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("adminOrgUnit.id"));
        	sic.add(new SelectorItemInfo("adminOrgUnit.number"));
        	sic.add(new SelectorItemInfo("adminOrgUnit.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("company.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("company.id"));
        	sic.add(new SelectorItemInfo("company.number"));
        	sic.add(new SelectorItemInfo("company.name"));
		}
        sic.add(new SelectorItemInfo("companyNumber"));
        sic.add(new SelectorItemInfo("subjectName"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("taskSource.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("taskSource.id"));
        	sic.add(new SelectorItemInfo("taskSource.number"));
        	sic.add(new SelectorItemInfo("taskSource.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("special.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("special.id"));
        	sic.add(new SelectorItemInfo("special.number"));
        	sic.add(new SelectorItemInfo("special.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("passFlag.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("passFlag.id"));
        	sic.add(new SelectorItemInfo("passFlag.number"));
        	sic.add(new SelectorItemInfo("passFlag.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("approvalFlag.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("approvalFlag.id"));
        	sic.add(new SelectorItemInfo("approvalFlag.number"));
        	sic.add(new SelectorItemInfo("approvalFlag.name"));
		}
        sic.add(new SelectorItemInfo("subjectResult"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("operType.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("operType.id"));
        	sic.add(new SelectorItemInfo("operType.number"));
        	sic.add(new SelectorItemInfo("operType.name"));
		}
        sic.add(new SelectorItemInfo("remark"));
    	sic.add(new SelectorItemInfo("DeliberationEntry.seq"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("DeliberationEntry.*"));
		}
		else{
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("DeliberationEntry.deliberationPerson.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("DeliberationEntry.deliberationPerson.id"));
			sic.add(new SelectorItemInfo("DeliberationEntry.deliberationPerson.name"));
        	sic.add(new SelectorItemInfo("DeliberationEntry.deliberationPerson.number"));
		}
    	sic.add(new SelectorItemInfo("DeliberationEntry.deliberationResult"));
        sic.add(new SelectorItemInfo("subjectNum"));
        sic.add(new SelectorItemInfo("state"));
    	sic.add(new SelectorItemInfo("AttendanceEntry.seq"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("AttendanceEntry.*"));
		}
		else{
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("AttendanceEntry.attendancePerson.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("AttendanceEntry.attendancePerson.id"));
			sic.add(new SelectorItemInfo("AttendanceEntry.attendancePerson.name"));
        	sic.add(new SelectorItemInfo("AttendanceEntry.attendancePerson.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("AttendanceEntry.position.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("AttendanceEntry.position.id"));
			sic.add(new SelectorItemInfo("AttendanceEntry.position.name"));
        	sic.add(new SelectorItemInfo("AttendanceEntry.position.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("meetingType.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("meetingType.id"));
        	sic.add(new SelectorItemInfo("meetingType.number"));
        	sic.add(new SelectorItemInfo("meetingType.name"));
		}
    	sic.add(new SelectorItemInfo("RefSubjectEntry.seq"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("RefSubjectEntry.*"));
		}
		else{
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("RefSubjectEntry.company.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("RefSubjectEntry.company.id"));
			sic.add(new SelectorItemInfo("RefSubjectEntry.company.name"));
        	sic.add(new SelectorItemInfo("RefSubjectEntry.company.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("RefSubjectEntry.refSubject.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("RefSubjectEntry.refSubject.id"));
			sic.add(new SelectorItemInfo("RefSubjectEntry.refSubject.subjectName"));
        	sic.add(new SelectorItemInfo("RefSubjectEntry.refSubject.number"));
		}
    	sic.add(new SelectorItemInfo("RefSubjectEntry.refSubjectNumber"));
        return sic;
    }        
    	

    /**
     * output actionSubmit_actionPerformed method
     */
    public void actionSubmit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionSubmit_actionPerformed(e);
    }
    	

    /**
     * output actionPrint_actionPerformed method
     */
    public void actionPrint_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
    	if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.print(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionPrintPreview_actionPerformed method
     */
    public void actionPrintPreview_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
        if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.printPreview(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionGenMeeting_actionPerformed method
     */
    public void actionGenMeeting_actionPerformed(ActionEvent e) throws Exception
    {
    }
    	

    /**
     * output actionAudit_actionPerformed method
     */
    public void actionAudit_actionPerformed(ActionEvent e) throws Exception
    {
actionAudit.setVisible(true);

        com.kingdee.eas.custom.tiog.bizbill.SubjectFactory.getRemoteInstance().audit(editData);
    }
    	

    /**
     * output actionUnAudit_actionPerformed method
     */
    public void actionUnAudit_actionPerformed(ActionEvent e) throws Exception
    {
actionUnAudit.setVisible(true);

        com.kingdee.eas.custom.tiog.bizbill.SubjectFactory.getRemoteInstance().unAudit(editData);
    }
	public RequestContext prepareActionSubmit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionSubmit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionSubmit() {
    	return false;
    }
	public RequestContext prepareActionPrint(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrint(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrint() {
    	return false;
    }
	public RequestContext prepareActionPrintPreview(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrintPreview(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrintPreview() {
    	return false;
    }
	public RequestContext prepareActionGenMeeting(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionGenMeeting() {
    	return false;
    }
	public RequestContext prepareActionAudit(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionAudit() {
    	return false;
    }
	public RequestContext prepareActionUnAudit(IItemAction itemAction) throws Exception {
			RequestContext request = new RequestContext();		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionUnAudit() {
    	return false;
    }

    /**
     * output ActionGenMeeting class
     */     
    protected class ActionGenMeeting extends ItemAction {     
    
        public ActionGenMeeting()
        {
            this(null);
        }

        public ActionGenMeeting(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            this.setEnabled(false);
            _tempStr = resHelper.getString("ActionGenMeeting.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionGenMeeting.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionGenMeeting.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractSubjectEditUI.this, "ActionGenMeeting", "actionGenMeeting_actionPerformed", e);
        }
    }

    /**
     * output ActionAudit class
     */     
    protected class ActionAudit extends ItemAction {     
    
        public ActionAudit()
        {
            this(null);
        }

        public ActionAudit(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            _tempStr = resHelper.getString("ActionAudit.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAudit.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionAudit.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractSubjectEditUI.this, "ActionAudit", "actionAudit_actionPerformed", e);
        }
    }

    /**
     * output ActionUnAudit class
     */     
    protected class ActionUnAudit extends ItemAction {     
    
        public ActionUnAudit()
        {
            this(null);
        }

        public ActionUnAudit(IUIObject uiObject)
        {     
		super(uiObject);     
        
            String _tempStr = null;
            _tempStr = resHelper.getString("ActionUnAudit.SHORT_DESCRIPTION");
            this.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionUnAudit.LONG_DESCRIPTION");
            this.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
            _tempStr = resHelper.getString("ActionUnAudit.NAME");
            this.putValue(ItemAction.NAME, _tempStr);
        }

        public void actionPerformed(ActionEvent e)
        {
        	getUIContext().put("ORG.PK", getOrgPK(this));
            innerActionPerformed("eas", AbstractSubjectEditUI.this, "ActionUnAudit", "actionUnAudit_actionPerformed", e);
        }
    }

    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.tiog.bizbill.client", "SubjectEditUI");
    }
    /**
     * output isBindWorkFlow method
     */
    public boolean isBindWorkFlow()
    {
        return true;
    }

    /**
     * output getEditUIName method
     */
    protected String getEditUIName()
    {
        return com.kingdee.eas.custom.tiog.bizbill.client.SubjectEditUI.class.getName();
    }

    /**
     * output getBizInterface method
     */
    protected com.kingdee.eas.framework.ICoreBase getBizInterface() throws Exception
    {
        return com.kingdee.eas.custom.tiog.bizbill.SubjectFactory.getRemoteInstance();
    }

    /**
     * output createNewData method
     */
    protected IObjectValue createNewData()
    {
        com.kingdee.eas.custom.tiog.bizbill.SubjectInfo objectValue = new com.kingdee.eas.custom.tiog.bizbill.SubjectInfo();
				if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin")) != null)
			objectValue.put("adminOrgUnit",com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum("Admin")));
 
        objectValue.setCreator((com.kingdee.eas.base.permission.UserInfo)(com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentUser()));		
        return objectValue;
    }


    	protected String getTDFileName() {
    	return "/bim/custom/tiog/bizbill/Subject";
	}
    protected IMetaDataPK getTDQueryPK() {
    	return new MetaDataPK("com.kingdee.eas.custom.tiog.bizbill.app.SubjectQuery");
	}
    

    /**
     * output getDetailTable method
     */
    protected KDTable getDetailTable() {
        return kdtCatalogEntry;
	}
    /**
     * output applyDefaultValue method
     */
    protected void applyDefaultValue(IObjectValue vo) {        
		vo.put("state","1");
        
    }        
	protected void setFieldsNull(com.kingdee.bos.dao.AbstractObjectValue arg0) {
		super.setFieldsNull(arg0);
		arg0.put("number",null);
	}

}