package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class PoliticalSystemCollection extends AbstractObjectCollection 
{
    public PoliticalSystemCollection()
    {
        super(PoliticalSystemInfo.class);
    }
    public boolean add(PoliticalSystemInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(PoliticalSystemCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(PoliticalSystemInfo item)
    {
        return removeObject(item);
    }
    public PoliticalSystemInfo get(int index)
    {
        return(PoliticalSystemInfo)getObject(index);
    }
    public PoliticalSystemInfo get(Object key)
    {
        return(PoliticalSystemInfo)getObject(key);
    }
    public void set(int index, PoliticalSystemInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(PoliticalSystemInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(PoliticalSystemInfo item)
    {
        return super.indexOf(item);
    }
}