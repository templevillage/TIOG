package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class SubjectRefSubjectEntryCollection extends AbstractObjectCollection 
{
    public SubjectRefSubjectEntryCollection()
    {
        super(SubjectRefSubjectEntryInfo.class);
    }
    public boolean add(SubjectRefSubjectEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(SubjectRefSubjectEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(SubjectRefSubjectEntryInfo item)
    {
        return removeObject(item);
    }
    public SubjectRefSubjectEntryInfo get(int index)
    {
        return(SubjectRefSubjectEntryInfo)getObject(index);
    }
    public SubjectRefSubjectEntryInfo get(Object key)
    {
        return(SubjectRefSubjectEntryInfo)getObject(key);
    }
    public void set(int index, SubjectRefSubjectEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(SubjectRefSubjectEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(SubjectRefSubjectEntryInfo item)
    {
        return super.indexOf(item);
    }
}