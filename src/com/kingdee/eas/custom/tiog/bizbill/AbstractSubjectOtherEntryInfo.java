package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractSubjectOtherEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractSubjectOtherEntryInfo()
    {
        this("id");
    }
    protected AbstractSubjectOtherEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 其它资料分录 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.SubjectInfo item)
    {
        put("parent", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("0BCF1A64");
    }
}