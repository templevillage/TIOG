package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingSubjectEntryCollection extends AbstractObjectCollection 
{
    public MeetingSubjectEntryCollection()
    {
        super(MeetingSubjectEntryInfo.class);
    }
    public boolean add(MeetingSubjectEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingSubjectEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingSubjectEntryInfo item)
    {
        return removeObject(item);
    }
    public MeetingSubjectEntryInfo get(int index)
    {
        return(MeetingSubjectEntryInfo)getObject(index);
    }
    public MeetingSubjectEntryInfo get(Object key)
    {
        return(MeetingSubjectEntryInfo)getObject(key);
    }
    public void set(int index, MeetingSubjectEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingSubjectEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingSubjectEntryInfo item)
    {
        return super.indexOf(item);
    }
}