package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class SubjectInfo extends AbstractSubjectInfo implements Serializable 
{
    public SubjectInfo()
    {
        super();
    }
    protected SubjectInfo(String pkField)
    {
        super(pkField);
    }
}