package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingFactory
{
    private MeetingFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeeting getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeeting)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("2B222A91") ,com.kingdee.eas.custom.tiog.bizbill.IMeeting.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeeting getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeeting)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("2B222A91") ,com.kingdee.eas.custom.tiog.bizbill.IMeeting.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeeting getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeeting)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("2B222A91"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeeting getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeeting)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("2B222A91"));
    }
}