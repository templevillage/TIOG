package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class MeetingManEntryInfo extends AbstractMeetingManEntryInfo implements Serializable 
{
    public MeetingManEntryInfo()
    {
        super();
    }
    protected MeetingManEntryInfo(String pkField)
    {
        super(pkField);
    }
}