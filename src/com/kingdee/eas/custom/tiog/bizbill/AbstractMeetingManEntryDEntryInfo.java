package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractMeetingManEntryDEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractMeetingManEntryDEntryInfo()
    {
        this("id");
    }
    protected AbstractMeetingManEntryDEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��ϸ��¼ 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingManEntryInfo getParent1()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingManEntryInfo)get("parent1");
    }
    public void setParent1(com.kingdee.eas.custom.tiog.bizbill.MeetingManEntryInfo item)
    {
        put("parent1", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("8D5EB8B7");
    }
}