package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class CategoryListEntryDEntryFactory
{
    private CategoryListEntryDEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntryDEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntryDEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("9481297A") ,com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntryDEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntryDEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntryDEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("9481297A") ,com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntryDEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntryDEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntryDEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("9481297A"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntryDEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntryDEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("9481297A"));
    }
}