package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class SubjectCollection extends AbstractObjectCollection 
{
    public SubjectCollection()
    {
        super(SubjectInfo.class);
    }
    public boolean add(SubjectInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(SubjectCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(SubjectInfo item)
    {
        return removeObject(item);
    }
    public SubjectInfo get(int index)
    {
        return(SubjectInfo)getObject(index);
    }
    public SubjectInfo get(Object key)
    {
        return(SubjectInfo)getObject(key);
    }
    public void set(int index, SubjectInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(SubjectInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(SubjectInfo item)
    {
        return super.indexOf(item);
    }
}