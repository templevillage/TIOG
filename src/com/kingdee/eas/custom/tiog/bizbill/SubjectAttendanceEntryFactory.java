package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class SubjectAttendanceEntryFactory
{
    private SubjectAttendanceEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectAttendanceEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectAttendanceEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("5D4D2D27") ,com.kingdee.eas.custom.tiog.bizbill.ISubjectAttendanceEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectAttendanceEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectAttendanceEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("5D4D2D27") ,com.kingdee.eas.custom.tiog.bizbill.ISubjectAttendanceEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectAttendanceEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectAttendanceEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("5D4D2D27"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectAttendanceEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectAttendanceEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("5D4D2D27"));
    }
}