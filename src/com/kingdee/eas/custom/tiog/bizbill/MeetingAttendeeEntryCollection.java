package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingAttendeeEntryCollection extends AbstractObjectCollection 
{
    public MeetingAttendeeEntryCollection()
    {
        super(MeetingAttendeeEntryInfo.class);
    }
    public boolean add(MeetingAttendeeEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingAttendeeEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingAttendeeEntryInfo item)
    {
        return removeObject(item);
    }
    public MeetingAttendeeEntryInfo get(int index)
    {
        return(MeetingAttendeeEntryInfo)getObject(index);
    }
    public MeetingAttendeeEntryInfo get(Object key)
    {
        return(MeetingAttendeeEntryInfo)getObject(key);
    }
    public void set(int index, MeetingAttendeeEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingAttendeeEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingAttendeeEntryInfo item)
    {
        return super.indexOf(item);
    }
}