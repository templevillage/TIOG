package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class PoliticalSystemEntryCollection extends AbstractObjectCollection 
{
    public PoliticalSystemEntryCollection()
    {
        super(PoliticalSystemEntryInfo.class);
    }
    public boolean add(PoliticalSystemEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(PoliticalSystemEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(PoliticalSystemEntryInfo item)
    {
        return removeObject(item);
    }
    public PoliticalSystemEntryInfo get(int index)
    {
        return(PoliticalSystemEntryInfo)getObject(index);
    }
    public PoliticalSystemEntryInfo get(Object key)
    {
        return(PoliticalSystemEntryInfo)getObject(key);
    }
    public void set(int index, PoliticalSystemEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(PoliticalSystemEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(PoliticalSystemEntryInfo item)
    {
        return super.indexOf(item);
    }
}