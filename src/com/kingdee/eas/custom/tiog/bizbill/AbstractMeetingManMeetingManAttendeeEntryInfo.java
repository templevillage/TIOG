package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractMeetingManMeetingManAttendeeEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractMeetingManMeetingManAttendeeEntryInfo()
    {
        this("id");
    }
    protected AbstractMeetingManMeetingManAttendeeEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 参会情况 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo item)
    {
        put("parent", item);
    }
    /**
     * Object:参会情况's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: 参会情况 's 参会人 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getAttendee()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("attendee");
    }
    public void setAttendee(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("attendee", item);
    }
    /**
     * Object:参会情况's 缺席原因property 
     */
    public String getReason()
    {
        return getString("reason");
    }
    public void setReason(String item)
    {
        setString("reason", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("9C2205B0");
    }
}