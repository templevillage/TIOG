package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface ISubject extends ICoreBillBase
{
    public SubjectCollection getSubjectCollection() throws BOSException;
    public SubjectCollection getSubjectCollection(EntityViewInfo view) throws BOSException;
    public SubjectCollection getSubjectCollection(String oql) throws BOSException;
    public SubjectInfo getSubjectInfo(IObjectPK pk) throws BOSException, EASBizException;
    public SubjectInfo getSubjectInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public SubjectInfo getSubjectInfo(String oql) throws BOSException, EASBizException;
    public void audit(SubjectInfo model) throws BOSException;
    public void unAudit(SubjectInfo model) throws BOSException;
}