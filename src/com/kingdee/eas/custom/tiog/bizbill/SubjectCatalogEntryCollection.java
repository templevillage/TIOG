package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class SubjectCatalogEntryCollection extends AbstractObjectCollection 
{
    public SubjectCatalogEntryCollection()
    {
        super(SubjectCatalogEntryInfo.class);
    }
    public boolean add(SubjectCatalogEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(SubjectCatalogEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(SubjectCatalogEntryInfo item)
    {
        return removeObject(item);
    }
    public SubjectCatalogEntryInfo get(int index)
    {
        return(SubjectCatalogEntryInfo)getObject(index);
    }
    public SubjectCatalogEntryInfo get(Object key)
    {
        return(SubjectCatalogEntryInfo)getObject(key);
    }
    public void set(int index, SubjectCatalogEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(SubjectCatalogEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(SubjectCatalogEntryInfo item)
    {
        return super.indexOf(item);
    }
}