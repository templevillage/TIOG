package com.kingdee.eas.custom.tiog.bizbill.util;

import java.util.ArrayList;
import java.util.List;

import com.kingdee.bos.ctrl.kdf.table.KDTable;
import com.kingdee.eas.framework.client.ListUI;
import com.kingdee.eas.util.SysUtil;
import com.kingdee.eas.util.client.KDTableUtil;
import com.kingdee.eas.util.client.MsgBox;

public class CommonUtil {

	
	public static List<String> getSelectedIDs(ListUI ui, KDTable tblMain) {
		// TODO Auto-generated method stub
		List selectedIDs = null;
		int[] selectIds = KDTableUtil.getSelectedRows(tblMain);
		if (selectIds.length < 0) {
			MsgBox.showError(ui, "请选择单据！");
			SysUtil.abort();
		} else {
			selectedIDs = new ArrayList();
		}
		for (int i = 0; i < selectIds.length; i++) {
			String id = tblMain.getRow(selectIds[i]).getCell("id").getValue()
					.toString();
			if (null != id && !"".equals(id))
				selectedIDs.add(id);
		}

		return selectedIDs;
	}

}
