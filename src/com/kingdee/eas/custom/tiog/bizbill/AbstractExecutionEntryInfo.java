package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractExecutionEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractExecutionEntryInfo()
    {
        this("id");
    }
    protected AbstractExecutionEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 分录 's 单据头 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.ExecutionInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 分录 's 落实责任部门 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getResponseDept()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("responseDept");
    }
    public void setResponseDept(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("responseDept", item);
    }
    /**
     * Object: 分录 's 落实人 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getResponsePer()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("responsePer");
    }
    public void setResponsePer(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("responsePer", item);
    }
    /**
     * Object:分录's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("60776F24");
    }
}