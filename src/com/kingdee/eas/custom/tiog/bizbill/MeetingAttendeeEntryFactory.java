package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingAttendeeEntryFactory
{
    private MeetingAttendeeEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendeeEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendeeEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("8ABBA8C7") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendeeEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendeeEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendeeEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("8ABBA8C7") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendeeEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendeeEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendeeEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("8ABBA8C7"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendeeEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendeeEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("8ABBA8C7"));
    }
}