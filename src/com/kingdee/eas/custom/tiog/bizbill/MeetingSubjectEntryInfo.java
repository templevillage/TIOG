package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class MeetingSubjectEntryInfo extends AbstractMeetingSubjectEntryInfo implements Serializable 
{
    public MeetingSubjectEntryInfo()
    {
        super();
    }
    protected MeetingSubjectEntryInfo(String pkField)
    {
        super(pkField);
    }
}