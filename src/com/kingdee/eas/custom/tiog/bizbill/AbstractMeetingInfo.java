package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractMeetingInfo extends com.kingdee.eas.framework.CoreBillBaseInfo implements Serializable 
{
    public AbstractMeetingInfo()
    {
        this("id");
    }
    protected AbstractMeetingInfo(String pkField)
    {
        super(pkField);
        put("SubjectEntry", new com.kingdee.eas.custom.tiog.bizbill.MeetingSubjectEntryCollection());
        put("AttendeeEntry", new com.kingdee.eas.custom.tiog.bizbill.MeetingAttendeeEntryCollection());
    }
    /**
     * Object:决策会议's 是否生成凭证property 
     */
    public boolean isFivouchered()
    {
        return getBoolean("Fivouchered");
    }
    public void setFivouchered(boolean item)
    {
        setBoolean("Fivouchered", item);
    }
    /**
     * Object: 决策会议 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    /**
     * Object: 决策会议 's 参会人集合 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingAttendeeEntryCollection getAttendeeEntry()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingAttendeeEntryCollection)get("AttendeeEntry");
    }
    /**
     * Object: 决策会议 's 会议议题集合 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingSubjectEntryCollection getSubjectEntry()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingSubjectEntryCollection)get("SubjectEntry");
    }
    /**
     * Object: 决策会议 's 公司 property 
     */
    public com.kingdee.eas.basedata.org.CompanyOrgUnitInfo getCompany()
    {
        return (com.kingdee.eas.basedata.org.CompanyOrgUnitInfo)get("company");
    }
    public void setCompany(com.kingdee.eas.basedata.org.CompanyOrgUnitInfo item)
    {
        put("company", item);
    }
    /**
     * Object:决策会议's 企业编码property 
     */
    public String getCompanyNumber()
    {
        return getString("companyNumber");
    }
    public void setCompanyNumber(String item)
    {
        setString("companyNumber", item);
    }
    /**
     * Object:决策会议's 会议名称property 
     */
    public String getMeetingName()
    {
        return getString("meetingName");
    }
    public void setMeetingName(String item)
    {
        setString("meetingName", item);
    }
    /**
     * Object: 决策会议 's 会议类型 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo getMeetingType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo)get("meetingType");
    }
    public void setMeetingType(com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo item)
    {
        put("meetingType", item);
    }
    /**
     * Object:决策会议's 会议类型名称property 
     */
    public String getMeetingTypeName()
    {
        return getString("meetingTypeName");
    }
    public void setMeetingTypeName(String item)
    {
        setString("meetingTypeName", item);
    }
    /**
     * Object: 决策会议 's 会议形式 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.MeetingFormInfo getMeetingForm()
    {
        return (com.kingdee.eas.custom.tiog.basedata.MeetingFormInfo)get("meetingForm");
    }
    public void setMeetingForm(com.kingdee.eas.custom.tiog.basedata.MeetingFormInfo item)
    {
        put("meetingForm", item);
    }
    /**
     * Object:决策会议's 会议时间property 
     */
    public java.util.Date getMeetingTS()
    {
        return getDate("meetingTS");
    }
    public void setMeetingTS(java.util.Date item)
    {
        setDate("meetingTS", item);
    }
    /**
     * Object: 决策会议 's 主持人 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getModerator()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("moderator");
    }
    public void setModerator(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("moderator", item);
    }
    /**
     * Object:决策会议's 会议纪要印发时间property 
     */
    public java.util.Date getReleaseTS()
    {
        return getDate("releaseTS");
    }
    public void setReleaseTS(java.util.Date item)
    {
        setDate("releaseTS", item);
    }
    /**
     * Object: 决策会议 's 数据来源 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.DataSourceInfo getDataSource()
    {
        return (com.kingdee.eas.custom.tiog.basedata.DataSourceInfo)get("dataSource");
    }
    public void setDataSource(com.kingdee.eas.custom.tiog.basedata.DataSourceInfo item)
    {
        put("dataSource", item);
    }
    /**
     * Object:决策会议's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: 决策会议 's 操作标识 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo getOperType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo)get("operType");
    }
    public void setOperType(com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo item)
    {
        put("operType", item);
    }
    /**
     * Object:决策会议's 单据状态property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.BillState getState()
    {
        return com.kingdee.eas.custom.tiog.bizbill.BillState.getEnum(getString("state"));
    }
    public void setState(com.kingdee.eas.custom.tiog.bizbill.BillState item)
    {
		if (item != null) {
        setString("state", item.getValue());
		}
    }
    /**
     * Object:决策会议's 报送标识property 
     */
    public boolean isIsUpload()
    {
        return getBoolean("isUpload");
    }
    public void setIsUpload(boolean item)
    {
        setBoolean("isUpload", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("2B222A91");
    }
}