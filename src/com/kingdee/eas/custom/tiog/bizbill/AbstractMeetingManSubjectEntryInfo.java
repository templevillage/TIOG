package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractMeetingManSubjectEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractMeetingManSubjectEntryInfo()
    {
        this("id");
    }
    protected AbstractMeetingManSubjectEntryInfo(String pkField)
    {
        super(pkField);
        put("MeetingManSubjectDeliberationEntry", new com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectDeliberationEntryCollection());
        put("MeetingManSubjectAttendanceEntry", new com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectAttendanceEntryCollection());
    }
    /**
     * Object: 议题集合 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 议题集合 's 列席情况 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectAttendanceEntryCollection getMeetingManSubjectAttendanceEntry()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectAttendanceEntryCollection)get("MeetingManSubjectAttendanceEntry");
    }
    /**
     * Object: 议题集合 's 审议情况 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectDeliberationEntryCollection getMeetingManSubjectDeliberationEntry()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryMeetingManSubjectDeliberationEntryCollection)get("MeetingManSubjectDeliberationEntry");
    }
    /**
     * Object:议题集合's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: 议题集合 's 企业 property 
     */
    public com.kingdee.eas.basedata.org.CompanyOrgUnitInfo getCompany()
    {
        return (com.kingdee.eas.basedata.org.CompanyOrgUnitInfo)get("company");
    }
    public void setCompany(com.kingdee.eas.basedata.org.CompanyOrgUnitInfo item)
    {
        put("company", item);
    }
    /**
     * Object:议题集合's 公司编码property 
     */
    public String getCompanyNumber()
    {
        return getString("companyNumber");
    }
    public void setCompanyNumber(String item)
    {
        setString("companyNumber", item);
    }
    /**
     * Object: 议题集合 's 议题 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectInfo getSubject()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectInfo)get("subject");
    }
    public void setSubject(com.kingdee.eas.custom.tiog.bizbill.SubjectInfo item)
    {
        put("subject", item);
    }
    /**
     * Object:议题集合's 议题编码property 
     */
    public String getSubjectNumber()
    {
        return getString("subjectNumber");
    }
    public void setSubjectNumber(String item)
    {
        setString("subjectNumber", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("655BDA4F");
    }
}