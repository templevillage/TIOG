package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class CategoryListFactory
{
    private CategoryListFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryList getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryList)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("9B951826") ,com.kingdee.eas.custom.tiog.bizbill.ICategoryList.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryList getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryList)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("9B951826") ,com.kingdee.eas.custom.tiog.bizbill.ICategoryList.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryList getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryList)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("9B951826"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryList getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryList)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("9B951826"));
    }
}