package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class ExecutionEntryFactory
{
    private ExecutionEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IExecutionEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IExecutionEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("60776F24") ,com.kingdee.eas.custom.tiog.bizbill.IExecutionEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IExecutionEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IExecutionEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("60776F24") ,com.kingdee.eas.custom.tiog.bizbill.IExecutionEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IExecutionEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IExecutionEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("60776F24"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IExecutionEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IExecutionEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("60776F24"));
    }
}