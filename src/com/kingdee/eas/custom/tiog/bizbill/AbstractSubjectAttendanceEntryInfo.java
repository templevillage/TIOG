package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractSubjectAttendanceEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractSubjectAttendanceEntryInfo()
    {
        this("id");
    }
    protected AbstractSubjectAttendanceEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 列席情况 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.SubjectInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 列席情况 's 列席人员 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getAttendancePerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("attendancePerson");
    }
    public void setAttendancePerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("attendancePerson", item);
    }
    /**
     * Object: 列席情况 's 列席人职位 property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("5D4D2D27");
    }
}