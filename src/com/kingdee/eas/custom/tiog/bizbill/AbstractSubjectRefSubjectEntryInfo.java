package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractSubjectRefSubjectEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractSubjectRefSubjectEntryInfo()
    {
        this("id");
    }
    protected AbstractSubjectRefSubjectEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 关联议题 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.SubjectInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 关联议题 's 公司 property 
     */
    public com.kingdee.eas.basedata.org.CompanyOrgUnitInfo getCompany()
    {
        return (com.kingdee.eas.basedata.org.CompanyOrgUnitInfo)get("company");
    }
    public void setCompany(com.kingdee.eas.basedata.org.CompanyOrgUnitInfo item)
    {
        put("company", item);
    }
    /**
     * Object: 关联议题 's 关联议题 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectInfo getRefSubject()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectInfo)get("refSubject");
    }
    public void setRefSubject(com.kingdee.eas.custom.tiog.bizbill.SubjectInfo item)
    {
        put("refSubject", item);
    }
    /**
     * Object:关联议题's 关联议题编码property 
     */
    public String getRefSubjectNumber()
    {
        return getString("refSubjectNumber");
    }
    public void setRefSubjectNumber(String item)
    {
        setString("refSubjectNumber", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("55FC74F7");
    }
}