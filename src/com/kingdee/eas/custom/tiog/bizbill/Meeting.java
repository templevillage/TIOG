package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.framework.CoreBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.tiog.bizbill.app.*;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class Meeting extends CoreBillBase implements IMeeting
{
    public Meeting()
    {
        super();
        registerInterface(IMeeting.class, this);
    }
    public Meeting(Context ctx)
    {
        super(ctx);
        registerInterface(IMeeting.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("2B222A91");
    }
    private MeetingController getController() throws BOSException
    {
        return (MeetingController)getBizController();
    }
    /**
     *取集合-System defined method
     *@return
     */
    public MeetingCollection getMeetingCollection() throws BOSException
    {
        try {
            return getController().getMeetingCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param view 取集合
     *@return
     */
    public MeetingCollection getMeetingCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getMeetingCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param oql 取集合
     *@return
     */
    public MeetingCollection getMeetingCollection(String oql) throws BOSException
    {
        try {
            return getController().getMeetingCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@return
     */
    public MeetingInfo getMeetingInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getMeetingInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@param selector 取值
     *@return
     */
    public MeetingInfo getMeetingInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getMeetingInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param oql 取值
     *@return
     */
    public MeetingInfo getMeetingInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getMeetingInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}