/**
 * output package name
 */
package com.kingdee.eas.custom.tiog.bizbill;

import java.util.Map;
import java.util.List;
import java.util.Iterator;
import com.kingdee.util.enums.StringEnum;

/**
 * output class name
 */
public class BillState extends StringEnum
{
    public static final String SAVED_VALUE = "1";//alias=�ݴ�
    public static final String AUDITED_VALUE = "5";//alias=�����

    public static final BillState Saved = new BillState("Saved", SAVED_VALUE);
    public static final BillState Audited = new BillState("Audited", AUDITED_VALUE);

    /**
     * construct function
     * @param String billState
     */
    private BillState(String name, String billState)
    {
        super(name, billState);
    }
    
    /**
     * getEnum function
     * @param String arguments
     */
    public static BillState getEnum(String billState)
    {
        return (BillState)getEnum(BillState.class, billState);
    }

    /**
     * getEnumMap function
     */
    public static Map getEnumMap()
    {
        return getEnumMap(BillState.class);
    }

    /**
     * getEnumList function
     */
    public static List getEnumList()
    {
         return getEnumList(BillState.class);
    }
    
    /**
     * getIterator function
     */
    public static Iterator iterator()
    {
         return iterator(BillState.class);
    }
}