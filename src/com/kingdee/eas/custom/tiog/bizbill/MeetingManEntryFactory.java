package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingManEntryFactory
{
    private MeetingManEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("27C728E9") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("27C728E9") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("27C728E9"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("27C728E9"));
    }
}