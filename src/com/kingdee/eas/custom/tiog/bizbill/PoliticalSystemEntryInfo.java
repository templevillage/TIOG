package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class PoliticalSystemEntryInfo extends AbstractPoliticalSystemEntryInfo implements Serializable 
{
    public PoliticalSystemEntryInfo()
    {
        super();
    }
    protected PoliticalSystemEntryInfo(String pkField)
    {
        super(pkField);
    }
}