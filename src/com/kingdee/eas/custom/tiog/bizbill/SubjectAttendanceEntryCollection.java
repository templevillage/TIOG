package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class SubjectAttendanceEntryCollection extends AbstractObjectCollection 
{
    public SubjectAttendanceEntryCollection()
    {
        super(SubjectAttendanceEntryInfo.class);
    }
    public boolean add(SubjectAttendanceEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(SubjectAttendanceEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(SubjectAttendanceEntryInfo item)
    {
        return removeObject(item);
    }
    public SubjectAttendanceEntryInfo get(int index)
    {
        return(SubjectAttendanceEntryInfo)getObject(index);
    }
    public SubjectAttendanceEntryInfo get(Object key)
    {
        return(SubjectAttendanceEntryInfo)getObject(key);
    }
    public void set(int index, SubjectAttendanceEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(SubjectAttendanceEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(SubjectAttendanceEntryInfo item)
    {
        return super.indexOf(item);
    }
}