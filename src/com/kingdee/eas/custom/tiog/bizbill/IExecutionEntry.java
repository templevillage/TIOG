package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.eas.framework.ICoreBillEntryBase;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IExecutionEntry extends ICoreBillEntryBase
{
    public ExecutionEntryInfo getExecutionEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public ExecutionEntryInfo getExecutionEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public ExecutionEntryInfo getExecutionEntryInfo(String oql) throws BOSException, EASBizException;
    public ExecutionEntryCollection getExecutionEntryCollection() throws BOSException;
    public ExecutionEntryCollection getExecutionEntryCollection(EntityViewInfo view) throws BOSException;
    public ExecutionEntryCollection getExecutionEntryCollection(String oql) throws BOSException;
}