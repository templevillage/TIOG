package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractSubjectDeliberationEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractSubjectDeliberationEntryInfo()
    {
        this("id");
    }
    protected AbstractSubjectDeliberationEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 审议情况 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.SubjectInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 审议情况 's 审议人 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getDeliberationPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("deliberationPerson");
    }
    public void setDeliberationPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("deliberationPerson", item);
    }
    /**
     * Object:审议情况's 审议结果property 
     */
    public String getDeliberationResult()
    {
        return getString("deliberationResult");
    }
    public void setDeliberationResult(String item)
    {
        setString("deliberationResult", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("2A0409EC");
    }
}