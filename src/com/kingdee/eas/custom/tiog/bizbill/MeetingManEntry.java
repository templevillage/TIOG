package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.framework.ICoreBillEntryBase;
import com.kingdee.eas.custom.tiog.bizbill.app.*;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBillEntryBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class MeetingManEntry extends CoreBillEntryBase implements IMeetingManEntry
{
    public MeetingManEntry()
    {
        super();
        registerInterface(IMeetingManEntry.class, this);
    }
    public MeetingManEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IMeetingManEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("27C728E9");
    }
    private MeetingManEntryController getController() throws BOSException
    {
        return (MeetingManEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public MeetingManEntryInfo getMeetingManEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getMeetingManEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public MeetingManEntryInfo getMeetingManEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getMeetingManEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public MeetingManEntryInfo getMeetingManEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getMeetingManEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public MeetingManEntryCollection getMeetingManEntryCollection() throws BOSException
    {
        try {
            return getController().getMeetingManEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public MeetingManEntryCollection getMeetingManEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getMeetingManEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public MeetingManEntryCollection getMeetingManEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getMeetingManEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}