package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.framework.ICoreBillEntryBase;
import com.kingdee.eas.custom.tiog.bizbill.app.*;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBillEntryBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class ExecutionEntry extends CoreBillEntryBase implements IExecutionEntry
{
    public ExecutionEntry()
    {
        super();
        registerInterface(IExecutionEntry.class, this);
    }
    public ExecutionEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IExecutionEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("60776F24");
    }
    private ExecutionEntryController getController() throws BOSException
    {
        return (ExecutionEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public ExecutionEntryInfo getExecutionEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getExecutionEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public ExecutionEntryInfo getExecutionEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getExecutionEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public ExecutionEntryInfo getExecutionEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getExecutionEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public ExecutionEntryCollection getExecutionEntryCollection() throws BOSException
    {
        try {
            return getController().getExecutionEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public ExecutionEntryCollection getExecutionEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getExecutionEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public ExecutionEntryCollection getExecutionEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getExecutionEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}