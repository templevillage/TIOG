package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingManSubjectEntryMeetingManSubjectDeliberationEntryCollection extends AbstractObjectCollection 
{
    public MeetingManSubjectEntryMeetingManSubjectDeliberationEntryCollection()
    {
        super(MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo.class);
    }
    public boolean add(MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingManSubjectEntryMeetingManSubjectDeliberationEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo item)
    {
        return removeObject(item);
    }
    public MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo get(int index)
    {
        return(MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo)getObject(index);
    }
    public MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo get(Object key)
    {
        return(MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo)getObject(key);
    }
    public void set(int index, MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingManSubjectEntryMeetingManSubjectDeliberationEntryInfo item)
    {
        return super.indexOf(item);
    }
}