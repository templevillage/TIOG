package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.framework.ICoreBillEntryBase;
import com.kingdee.eas.custom.tiog.bizbill.app.*;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBillEntryBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class CategoryListEntry extends CoreBillEntryBase implements ICategoryListEntry
{
    public CategoryListEntry()
    {
        super();
        registerInterface(ICategoryListEntry.class, this);
    }
    public CategoryListEntry(Context ctx)
    {
        super(ctx);
        registerInterface(ICategoryListEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("92B227EC");
    }
    private CategoryListEntryController getController() throws BOSException
    {
        return (CategoryListEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public CategoryListEntryInfo getCategoryListEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getCategoryListEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public CategoryListEntryInfo getCategoryListEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getCategoryListEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public CategoryListEntryInfo getCategoryListEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getCategoryListEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public CategoryListEntryCollection getCategoryListEntryCollection() throws BOSException
    {
        try {
            return getController().getCategoryListEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public CategoryListEntryCollection getCategoryListEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getCategoryListEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public CategoryListEntryCollection getCategoryListEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getCategoryListEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}