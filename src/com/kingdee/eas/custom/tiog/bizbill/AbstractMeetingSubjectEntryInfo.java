package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractMeetingSubjectEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractMeetingSubjectEntryInfo()
    {
        this("id");
    }
    protected AbstractMeetingSubjectEntryInfo(String pkField)
    {
        super(pkField);
        //put("deliberationEntry", new com.kingdee.eas.custom.tiog.bizbill.MeetingDeliberationEntryCollection());
        put("attendanceEntry", new com.kingdee.eas.custom.tiog.bizbill.MeetingAttendanceEntryCollection());
    }
    /**
     * Object: �������⼯�� 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.MeetingInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: �������⼯�� 's ���� property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectInfo getSubject()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectInfo)get("subject");
    }
    public void setSubject(com.kingdee.eas.custom.tiog.bizbill.SubjectInfo item)
    {
        put("subject", item);
    }
    /**
     * Object:�������⼯��'s �������property 
     */
    public String getSubjectName()
    {
        return getString("subjectName");
    }
    public void setSubjectName(String item)
    {
        setString("subjectName", item);
    }
    /**
     * Object:�������⼯��'s ��עproperty 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: �������⼯�� 's ��ϯ��� ������¼ property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingAttendanceEntryCollection getAttendanceEntry()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingAttendanceEntryCollection)get("attendanceEntry");
    }
    /**
     * Object: �������⼯�� 's ������� ������¼ property 
     */
//   
/*    
    public com.kingdee.eas.custom.tiog.bizbill.MeetingDeliberationEntryCollection getDeliberationEntry()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingDeliberationEntryCollection)get("deliberationEntry");
    }
*/    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("CDCD4BD7");
    }
}