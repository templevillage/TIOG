package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractSubjectInfo extends com.kingdee.eas.framework.CoreBillBaseInfo implements Serializable 
{
    public AbstractSubjectInfo()
    {
        this("id");
    }
    protected AbstractSubjectInfo(String pkField)
    {
        super(pkField);
        put("DeliberationEntry", new com.kingdee.eas.custom.tiog.bizbill.SubjectDeliberationEntryCollection());
        put("RefSubjectEntry", new com.kingdee.eas.custom.tiog.bizbill.SubjectRefSubjectEntryCollection());
        put("AttendanceEntry", new com.kingdee.eas.custom.tiog.bizbill.SubjectAttendanceEntryCollection());
        put("CatalogEntry", new com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryCollection());
        put("OtherEntrys", new com.kingdee.eas.custom.tiog.bizbill.SubjectOtherEntryCollection());
    }
    /**
     * Object:审议议题's 是否生成凭证property 
     */
    public boolean isFivouchered()
    {
        return getBoolean("Fivouchered");
    }
    public void setFivouchered(boolean item)
    {
        setBoolean("Fivouchered", item);
    }
    /**
     * Object: 审议议题 's 其它资料分录 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectOtherEntryCollection getOtherEntrys()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectOtherEntryCollection)get("OtherEntrys");
    }
    /**
     * Object: 审议议题 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    /**
     * Object: 审议议题 's 企业 property 
     */
    public com.kingdee.eas.basedata.org.CompanyOrgUnitInfo getCompany()
    {
        return (com.kingdee.eas.basedata.org.CompanyOrgUnitInfo)get("company");
    }
    public void setCompany(com.kingdee.eas.basedata.org.CompanyOrgUnitInfo item)
    {
        put("company", item);
    }
    /**
     * Object:审议议题's 企业编码property 
     */
    public String getCompanyNumber()
    {
        return getString("companyNumber");
    }
    public void setCompanyNumber(String item)
    {
        setString("companyNumber", item);
    }
    /**
     * Object:审议议题's 议题名称property 
     */
    public String getSubjectName()
    {
        return getString("subjectName");
    }
    public void setSubjectName(String item)
    {
        setString("subjectName", item);
    }
    /**
     * Object: 审议议题 's 任务来源 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.TaskSourceInfo getTaskSource()
    {
        return (com.kingdee.eas.custom.tiog.basedata.TaskSourceInfo)get("taskSource");
    }
    public void setTaskSource(com.kingdee.eas.custom.tiog.basedata.TaskSourceInfo item)
    {
        put("taskSource", item);
    }
    /**
     * Object: 审议议题 's 专项名称 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.SpecialInfo getSpecial()
    {
        return (com.kingdee.eas.custom.tiog.basedata.SpecialInfo)get("special");
    }
    public void setSpecial(com.kingdee.eas.custom.tiog.basedata.SpecialInfo item)
    {
        put("special", item);
    }
    /**
     * Object: 审议议题 's 是否通过 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.PassStatusInfo getPassFlag()
    {
        return (com.kingdee.eas.custom.tiog.basedata.PassStatusInfo)get("passFlag");
    }
    public void setPassFlag(com.kingdee.eas.custom.tiog.basedata.PassStatusInfo item)
    {
        put("passFlag", item);
    }
    /**
     * Object: 审议议题 's 是否需报国资委审批 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.UpReportStatusInfo getApprovalFlag()
    {
        return (com.kingdee.eas.custom.tiog.basedata.UpReportStatusInfo)get("approvalFlag");
    }
    public void setApprovalFlag(com.kingdee.eas.custom.tiog.basedata.UpReportStatusInfo item)
    {
        put("approvalFlag", item);
    }
    /**
     * Object:审议议题's 是否听取意见property 
     */
    public boolean isAdoptFlag()
    {
        return getBoolean("adoptFlag");
    }
    public void setAdoptFlag(boolean item)
    {
        setBoolean("adoptFlag", item);
    }
    /**
     * Object:审议议题's 议题决议property 
     */
    public String getSubjectResult()
    {
        return getString("subjectResult");
    }
    public void setSubjectResult(String item)
    {
        setString("subjectResult", item);
    }
    /**
     * Object:审议议题's 是否需要督办property 
     */
    public boolean isSuperviseFlag()
    {
        return getBoolean("superviseFlag");
    }
    public void setSuperviseFlag(boolean item)
    {
        setBoolean("superviseFlag", item);
    }
    /**
     * Object: 审议议题 's 操作标识 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo getOperType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo)get("operType");
    }
    public void setOperType(com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo item)
    {
        put("operType", item);
    }
    /**
     * Object:审议议题's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: 审议议题 's 议题事项 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryCollection getCatalogEntry()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryCollection)get("CatalogEntry");
    }
    /**
     * Object: 审议议题 's 关联议题 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectRefSubjectEntryCollection getRefSubjectEntry()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectRefSubjectEntryCollection)get("RefSubjectEntry");
    }
    /**
     * Object: 审议议题 's 列席情况 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectAttendanceEntryCollection getAttendanceEntry()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectAttendanceEntryCollection)get("AttendanceEntry");
    }
    /**
     * Object: 审议议题 's 审议情况 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectDeliberationEntryCollection getDeliberationEntry()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectDeliberationEntryCollection)get("DeliberationEntry");
    }
    /**
     * Object:审议议题's 议题编码property 
     */
    public String getSubjectNum()
    {
        return getString("subjectNum");
    }
    public void setSubjectNum(String item)
    {
        setString("subjectNum", item);
    }
    /**
     * Object:审议议题's 单据状态property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.BillState getState()
    {
        return com.kingdee.eas.custom.tiog.bizbill.BillState.getEnum(getString("state"));
    }
    public void setState(com.kingdee.eas.custom.tiog.bizbill.BillState item)
    {
		if (item != null) {
        setString("state", item.getValue());
		}
    }
    /**
     * Object:审议议题's 报送标识property 
     */
    public boolean isIsUpload()
    {
        return getBoolean("isUpload");
    }
    public void setIsUpload(boolean item)
    {
        setBoolean("isUpload", item);
    }
    /**
     * Object: 审议议题 's 会议类型 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo getMeetingType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo)get("meetingType");
    }
    public void setMeetingType(com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo item)
    {
        put("meetingType", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("83A63582");
    }
}