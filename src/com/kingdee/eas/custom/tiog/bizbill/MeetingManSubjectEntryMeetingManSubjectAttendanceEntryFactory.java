package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingManSubjectEntryMeetingManSubjectAttendanceEntryFactory
{
    private MeetingManSubjectEntryMeetingManSubjectAttendanceEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectAttendanceEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectAttendanceEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("08F156AB") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectAttendanceEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectAttendanceEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectAttendanceEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("08F156AB") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectAttendanceEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectAttendanceEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectAttendanceEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("08F156AB"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectAttendanceEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManSubjectEntryMeetingManSubjectAttendanceEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("08F156AB"));
    }
}