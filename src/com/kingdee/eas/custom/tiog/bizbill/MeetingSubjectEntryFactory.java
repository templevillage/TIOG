package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingSubjectEntryFactory
{
    private MeetingSubjectEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingSubjectEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingSubjectEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("CDCD4BD7") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingSubjectEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingSubjectEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingSubjectEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("CDCD4BD7") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingSubjectEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingSubjectEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingSubjectEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("CDCD4BD7"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingSubjectEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingSubjectEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("CDCD4BD7"));
    }
}