package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingManEntryDEntryFactory
{
    private MeetingManEntryDEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntryDEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntryDEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("8D5EB8B7") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntryDEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntryDEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntryDEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("8D5EB8B7") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntryDEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntryDEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntryDEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("8D5EB8B7"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntryDEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingManEntryDEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("8D5EB8B7"));
    }
}