package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingCollection extends AbstractObjectCollection 
{
    public MeetingCollection()
    {
        super(MeetingInfo.class);
    }
    public boolean add(MeetingInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingInfo item)
    {
        return removeObject(item);
    }
    public MeetingInfo get(int index)
    {
        return(MeetingInfo)getObject(index);
    }
    public MeetingInfo get(Object key)
    {
        return(MeetingInfo)getObject(key);
    }
    public void set(int index, MeetingInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingInfo item)
    {
        return super.indexOf(item);
    }
}