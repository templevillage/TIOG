package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class PoliticalSystemEntryFactory
{
    private PoliticalSystemEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystemEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystemEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("83A31FB0") ,com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystemEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystemEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystemEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("83A31FB0") ,com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystemEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystemEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystemEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("83A31FB0"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystemEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystemEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("83A31FB0"));
    }
}