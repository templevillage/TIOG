package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class PoliticalSystemFactory
{
    private PoliticalSystemFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("C09155E2") ,com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("C09155E2") ,com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("C09155E2"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IPoliticalSystem)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("C09155E2"));
    }
}