package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class SubjectDeliberationEntryInfo extends AbstractSubjectDeliberationEntryInfo implements Serializable 
{
    public SubjectDeliberationEntryInfo()
    {
        super();
    }
    protected SubjectDeliberationEntryInfo(String pkField)
    {
        super(pkField);
    }
}