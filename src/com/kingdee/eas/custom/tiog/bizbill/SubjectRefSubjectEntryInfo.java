package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class SubjectRefSubjectEntryInfo extends AbstractSubjectRefSubjectEntryInfo implements Serializable 
{
    public SubjectRefSubjectEntryInfo()
    {
        super();
    }
    protected SubjectRefSubjectEntryInfo(String pkField)
    {
        super(pkField);
    }
}