package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class MeetingManEntryDEntryCollection extends AbstractObjectCollection 
{
    public MeetingManEntryDEntryCollection()
    {
        super(MeetingManEntryDEntryInfo.class);
    }
    public boolean add(MeetingManEntryDEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(MeetingManEntryDEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(MeetingManEntryDEntryInfo item)
    {
        return removeObject(item);
    }
    public MeetingManEntryDEntryInfo get(int index)
    {
        return(MeetingManEntryDEntryInfo)getObject(index);
    }
    public MeetingManEntryDEntryInfo get(Object key)
    {
        return(MeetingManEntryDEntryInfo)getObject(key);
    }
    public void set(int index, MeetingManEntryDEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(MeetingManEntryDEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(MeetingManEntryDEntryInfo item)
    {
        return super.indexOf(item);
    }
}