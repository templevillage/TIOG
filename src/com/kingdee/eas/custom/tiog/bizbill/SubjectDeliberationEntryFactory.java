package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class SubjectDeliberationEntryFactory
{
    private SubjectDeliberationEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectDeliberationEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectDeliberationEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("2A0409EC") ,com.kingdee.eas.custom.tiog.bizbill.ISubjectDeliberationEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectDeliberationEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectDeliberationEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("2A0409EC") ,com.kingdee.eas.custom.tiog.bizbill.ISubjectDeliberationEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectDeliberationEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectDeliberationEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("2A0409EC"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubjectDeliberationEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubjectDeliberationEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("2A0409EC"));
    }
}