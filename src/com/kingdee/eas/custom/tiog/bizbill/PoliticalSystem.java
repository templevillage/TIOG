package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.framework.CoreBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.ICoreBillBase;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.tiog.bizbill.app.*;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class PoliticalSystem extends CoreBillBase implements IPoliticalSystem
{
    public PoliticalSystem()
    {
        super();
        registerInterface(IPoliticalSystem.class, this);
    }
    public PoliticalSystem(Context ctx)
    {
        super(ctx);
        registerInterface(IPoliticalSystem.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("C09155E2");
    }
    private PoliticalSystemController getController() throws BOSException
    {
        return (PoliticalSystemController)getBizController();
    }
    /**
     *取集合-System defined method
     *@return
     */
    public PoliticalSystemCollection getPoliticalSystemCollection() throws BOSException
    {
        try {
            return getController().getPoliticalSystemCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param view 取集合
     *@return
     */
    public PoliticalSystemCollection getPoliticalSystemCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getPoliticalSystemCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param oql 取集合
     *@return
     */
    public PoliticalSystemCollection getPoliticalSystemCollection(String oql) throws BOSException
    {
        try {
            return getController().getPoliticalSystemCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@return
     */
    public PoliticalSystemInfo getPoliticalSystemInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getPoliticalSystemInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@param selector 取值
     *@return
     */
    public PoliticalSystemInfo getPoliticalSystemInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getPoliticalSystemInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param oql 取值
     *@return
     */
    public PoliticalSystemInfo getPoliticalSystemInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getPoliticalSystemInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *审核-User defined method
     *@param model model
     */
    public void audit(PoliticalSystemInfo model) throws BOSException
    {
        try {
            getController().audit(getContext(), model);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *反审核-User defined method
     *@param model model
     */
    public void unAudit(PoliticalSystemInfo model) throws BOSException
    {
        try {
            getController().unAudit(getContext(), model);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}