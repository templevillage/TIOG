package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class MeetingAttendanceEntryInfo extends AbstractMeetingAttendanceEntryInfo implements Serializable 
{
    public MeetingAttendanceEntryInfo()
    {
        super();
    }
    protected MeetingAttendanceEntryInfo(String pkField)
    {
        super(pkField);
    }
}