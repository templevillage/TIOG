package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class ExecutionFactory
{
    private ExecutionFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IExecution getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IExecution)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("E27E2DEE") ,com.kingdee.eas.custom.tiog.bizbill.IExecution.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IExecution getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IExecution)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("E27E2DEE") ,com.kingdee.eas.custom.tiog.bizbill.IExecution.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IExecution getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IExecution)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("E27E2DEE"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IExecution getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IExecution)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("E27E2DEE"));
    }
}