package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class CategoryListInfo extends AbstractCategoryListInfo implements Serializable 
{
    public CategoryListInfo()
    {
        super();
    }
    protected CategoryListInfo(String pkField)
    {
        super(pkField);
    }
}