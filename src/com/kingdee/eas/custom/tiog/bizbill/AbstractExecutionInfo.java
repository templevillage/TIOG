package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractExecutionInfo extends com.kingdee.eas.framework.CoreBillBaseInfo implements Serializable 
{
    public AbstractExecutionInfo()
    {
        this("id");
    }
    protected AbstractExecutionInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.tiog.bizbill.ExecutionEntryCollection());
    }
    /**
     * Object: 组织实施 's 分录 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.ExecutionEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ExecutionEntryCollection)get("entrys");
    }
    /**
     * Object:组织实施's 是否生成凭证property 
     */
    public boolean isFivouchered()
    {
        return getBoolean("Fivouchered");
    }
    public void setFivouchered(boolean item)
    {
        setBoolean("Fivouchered", item);
    }
    /**
     * Object: 组织实施 's 行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrgUnit()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrgUnit");
    }
    public void setAdminOrgUnit(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrgUnit", item);
    }
    /**
     * Object: 组织实施 's 企业 property 
     */
    public com.kingdee.eas.basedata.org.CompanyOrgUnitInfo getCompany()
    {
        return (com.kingdee.eas.basedata.org.CompanyOrgUnitInfo)get("company");
    }
    public void setCompany(com.kingdee.eas.basedata.org.CompanyOrgUnitInfo item)
    {
        put("company", item);
    }
    /**
     * Object:组织实施's 企业编码property 
     */
    public String getCompanyNumber()
    {
        return getString("companyNumber");
    }
    public void setCompanyNumber(String item)
    {
        setString("companyNumber", item);
    }
    /**
     * Object: 组织实施 's 议题 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectInfo getSubject()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectInfo)get("subject");
    }
    public void setSubject(com.kingdee.eas.custom.tiog.bizbill.SubjectInfo item)
    {
        put("subject", item);
    }
    /**
     * Object:组织实施's 议题编码property 
     */
    public String getSubjectNumber()
    {
        return getString("subjectNumber");
    }
    public void setSubjectNumber(String item)
    {
        setString("subjectNumber", item);
    }
    /**
     * Object:组织实施's 预期成效property 
     */
    public String getEffect()
    {
        return getString("effect");
    }
    public void setEffect(String item)
    {
        setString("effect", item);
    }
    /**
     * Object: 组织实施 's 实施状态 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.ExecuteStatusInfo getExeStatus()
    {
        return (com.kingdee.eas.custom.tiog.basedata.ExecuteStatusInfo)get("exeStatus");
    }
    public void setExeStatus(com.kingdee.eas.custom.tiog.basedata.ExecuteStatusInfo item)
    {
        put("exeStatus", item);
    }
    /**
     * Object: 组织实施 's 数据来源 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.DataSourceInfo getDataSource()
    {
        return (com.kingdee.eas.custom.tiog.basedata.DataSourceInfo)get("dataSource");
    }
    public void setDataSource(com.kingdee.eas.custom.tiog.basedata.DataSourceInfo item)
    {
        put("dataSource", item);
    }
    /**
     * Object:组织实施's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: 组织实施 's 操作标识 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo getOperType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo)get("operType");
    }
    public void setOperType(com.kingdee.eas.custom.tiog.basedata.OperateFlagInfo item)
    {
        put("operType", item);
    }
    /**
     * Object:组织实施's 单据状态property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.BillState getState()
    {
        return com.kingdee.eas.custom.tiog.bizbill.BillState.getEnum(getString("state"));
    }
    public void setState(com.kingdee.eas.custom.tiog.bizbill.BillState item)
    {
		if (item != null) {
        setString("state", item.getValue());
		}
    }
    /**
     * Object:组织实施's 报送标识property 
     */
    public boolean isIsUpload()
    {
        return getBoolean("isUpload");
    }
    public void setIsUpload(boolean item)
    {
        setBoolean("isUpload", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("E27E2DEE");
    }
}