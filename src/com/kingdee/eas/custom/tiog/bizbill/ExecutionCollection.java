package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class ExecutionCollection extends AbstractObjectCollection 
{
    public ExecutionCollection()
    {
        super(ExecutionInfo.class);
    }
    public boolean add(ExecutionInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(ExecutionCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(ExecutionInfo item)
    {
        return removeObject(item);
    }
    public ExecutionInfo get(int index)
    {
        return(ExecutionInfo)getObject(index);
    }
    public ExecutionInfo get(Object key)
    {
        return(ExecutionInfo)getObject(key);
    }
    public void set(int index, ExecutionInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(ExecutionInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(ExecutionInfo item)
    {
        return super.indexOf(item);
    }
}