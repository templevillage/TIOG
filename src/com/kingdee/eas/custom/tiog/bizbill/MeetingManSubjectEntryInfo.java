package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class MeetingManSubjectEntryInfo extends AbstractMeetingManSubjectEntryInfo implements Serializable 
{
    public MeetingManSubjectEntryInfo()
    {
        super();
    }
    protected MeetingManSubjectEntryInfo(String pkField)
    {
        super(pkField);
    }
}