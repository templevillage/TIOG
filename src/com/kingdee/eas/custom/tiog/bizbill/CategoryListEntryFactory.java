package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class CategoryListEntryFactory
{
    private CategoryListEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("92B227EC") ,com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("92B227EC") ,com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("92B227EC"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ICategoryListEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("92B227EC"));
    }
}