package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingManFactory
{
    private MeetingManFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingMan getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingMan)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("7CF8AD09") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingMan.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingMan getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingMan)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("7CF8AD09") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingMan.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingMan getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingMan)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("7CF8AD09"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingMan getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingMan)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("7CF8AD09"));
    }
}