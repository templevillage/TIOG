package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class MeetingAttendanceEntryFactory
{
    private MeetingAttendanceEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendanceEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendanceEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("41549838") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendanceEntry.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendanceEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendanceEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("41549838") ,com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendanceEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendanceEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendanceEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("41549838"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendanceEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.IMeetingAttendanceEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("41549838"));
    }
}