package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractSubjectCatalogEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractSubjectCatalogEntryInfo()
    {
        this("id");
    }
    protected AbstractSubjectCatalogEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 议题事项 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.SubjectInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.SubjectInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.SubjectInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 议题事项 's 事项 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogInfo getCatalog()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogInfo)get("catalog");
    }
    public void setCatalog(com.kingdee.eas.custom.tiog.basedata.CatalogInfo item)
    {
        put("catalog", item);
    }
    /**
     * Object: 议题事项 's 会议类型 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo getMeetingType()
    {
        return (com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo)get("meetingType");
    }
    public void setMeetingType(com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo item)
    {
        put("meetingType", item);
    }
    /**
     * Object: 议题事项 's 事项 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo getCatalogMain()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo)get("catalogMain");
    }
    public void setCatalogMain(com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo item)
    {
        put("catalogMain", item);
    }
    /**
     * Object:议题事项's 事项编码property 
     */
    public String getCatalogMainNumber()
    {
        return getString("catalogMainNumber");
    }
    public void setCatalogMainNumber(String item)
    {
        setString("catalogMainNumber", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("3D79367B");
    }
}