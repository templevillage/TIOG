package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractMeetingManEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractMeetingManEntryInfo()
    {
        this("id");
    }
    protected AbstractMeetingManEntryInfo(String pkField)
    {
        super(pkField);
        put("DEntrys", new com.kingdee.eas.custom.tiog.bizbill.MeetingManEntryDEntryCollection());
    }
    /**
     * Object: 分录 's 单据头 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 分录 's 明细分录 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingManEntryDEntryCollection getDEntrys()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingManEntryDEntryCollection)get("DEntrys");
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("27C728E9");
    }
}