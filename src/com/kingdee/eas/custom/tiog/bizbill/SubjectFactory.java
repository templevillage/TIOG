package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class SubjectFactory
{
    private SubjectFactory()
    {
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubject getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubject)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("83A63582") ,com.kingdee.eas.custom.tiog.bizbill.ISubject.class);
    }
    
    public static com.kingdee.eas.custom.tiog.bizbill.ISubject getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubject)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("83A63582") ,com.kingdee.eas.custom.tiog.bizbill.ISubject.class, objectCtx);
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubject getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubject)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("83A63582"));
    }
    public static com.kingdee.eas.custom.tiog.bizbill.ISubject getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.tiog.bizbill.ISubject)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("83A63582"));
    }
}