package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;

public class MeetingAttendeeEntryInfo extends AbstractMeetingAttendeeEntryInfo implements Serializable 
{
    public MeetingAttendeeEntryInfo()
    {
        super();
    }
    protected MeetingAttendeeEntryInfo(String pkField)
    {
        super(pkField);
    }
}