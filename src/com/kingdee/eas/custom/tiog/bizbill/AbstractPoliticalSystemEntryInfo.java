package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractPoliticalSystemEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractPoliticalSystemEntryInfo()
    {
        this("id");
    }
    protected AbstractPoliticalSystemEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 分录 's 单据头 property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo getParent()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: 分录 's 事项 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogInfo getCatalog()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogInfo)get("catalog");
    }
    public void setCatalog(com.kingdee.eas.custom.tiog.basedata.CatalogInfo item)
    {
        put("catalog", item);
    }
    /**
     * Object:分录's 事项编码property 
     */
    public String getCatalogNum()
    {
        return getString("catalogNum");
    }
    public void setCatalogNum(String item)
    {
        setString("catalogNum", item);
    }
    /**
     * Object: 分录 's 人数占比 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.HandupRateInfo getRate()
    {
        return (com.kingdee.eas.custom.tiog.basedata.HandupRateInfo)get("rate");
    }
    public void setRate(com.kingdee.eas.custom.tiog.basedata.HandupRateInfo item)
    {
        put("rate", item);
    }
    /**
     * Object: 分录 's 表决方式 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.VoteModeInfo getVoteMode()
    {
        return (com.kingdee.eas.custom.tiog.basedata.VoteModeInfo)get("voteMode");
    }
    public void setVoteMode(com.kingdee.eas.custom.tiog.basedata.VoteModeInfo item)
    {
        put("voteMode", item);
    }
    /**
     * Object: 分录 's 事项 property 
     */
    public com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo getCatalogMain()
    {
        return (com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo)get("catalogMain");
    }
    public void setCatalogMain(com.kingdee.eas.custom.tiog.basedata.CatalogMainInfo item)
    {
        put("catalogMain", item);
    }
    /**
     * Object:分录's 事项编码property 
     */
    public String getCatalogMainNumber()
    {
        return getString("catalogMainNumber");
    }
    public void setCatalogMainNumber(String item)
    {
        setString("catalogMainNumber", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("83A31FB0");
    }
}