package com.kingdee.eas.custom.tiog.bizbill;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.eas.framework.ICoreBillEntryBase;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IPoliticalSystemEntry extends ICoreBillEntryBase
{
    public PoliticalSystemEntryInfo getPoliticalSystemEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public PoliticalSystemEntryInfo getPoliticalSystemEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public PoliticalSystemEntryInfo getPoliticalSystemEntryInfo(String oql) throws BOSException, EASBizException;
    public PoliticalSystemEntryCollection getPoliticalSystemEntryCollection() throws BOSException;
    public PoliticalSystemEntryCollection getPoliticalSystemEntryCollection(EntityViewInfo view) throws BOSException;
    public PoliticalSystemEntryCollection getPoliticalSystemEntryCollection(String oql) throws BOSException;
}