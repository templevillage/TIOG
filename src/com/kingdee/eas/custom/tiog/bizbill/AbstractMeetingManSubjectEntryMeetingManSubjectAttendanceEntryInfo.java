package com.kingdee.eas.custom.tiog.bizbill;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractMeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractMeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo()
    {
        this("id");
    }
    protected AbstractMeetingManSubjectEntryMeetingManSubjectAttendanceEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 列席情况 's null property 
     */
    public com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo getParent1()
    {
        return (com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo)get("parent1");
    }
    public void setParent1(com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo item)
    {
        put("parent1", item);
    }
    /**
     * Object:列席情况's 备注property 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: 列席情况 's 列席人员 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getAttendance()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("attendance");
    }
    public void setAttendance(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("attendance", item);
    }
    /**
     * Object: 列席情况 's 列席人职位 property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getAttendancePos()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("attendancePos");
    }
    public void setAttendancePos(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("attendancePos", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("08F156AB");
    }
}