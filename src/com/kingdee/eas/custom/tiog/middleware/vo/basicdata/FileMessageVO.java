package com.kingdee.eas.custom.tiog.middleware.vo.basicdata;

import com.kingdee.eas.custom.tiog.middleware.common.FileTypeEnum;

import java.util.Arrays;

/**
 *
 * Title: FilleMessageVO Description:附件
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-23       
 */
public class FileMessageVO {
    //附件名称
    private String name;
    //附件类型
    private String type;
    //附件流
    private byte[] files;
    //附件id
    private String id;
    //上传类型
    private Enum<FileTypeEnum> filetype;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getFiles() {
        return files;
    }

    public void setFiles(byte[] files) {
        this.files = files;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Enum<FileTypeEnum> getFiletype() {
        return filetype;
    }

    public void setFiletype(Enum<FileTypeEnum> filetype) {
        this.filetype = filetype;
    }

    @Override
    public String toString() {
        return "FileMessageVO{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", files=" + Arrays.toString(files) +
                ", id='" + id + '\'' +
                ", filetype=" + filetype +
                '}';
    }
}
