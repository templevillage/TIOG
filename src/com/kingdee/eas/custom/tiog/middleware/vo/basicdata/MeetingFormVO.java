package com.kingdee.eas.custom.tiog.middleware.vo.basicdata;

import javax.xml.bind.annotation.*;

/**
 *
 * Title: MeetingFormVO Description:会议形式
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-23       
 */

@XmlType(propOrder = {"form_name","form_code"})
@XmlAccessorType(XmlAccessType.FIELD)
public class MeetingFormVO {
    //会议形式名称
    @XmlElement(name = "form_name")
    private String form_name;
    //会议形式编码
    @XmlElement(name = "form_code")
    private String form_code;

    public String getForm_name() {
        return form_name;
    }

    public void setForm_name(String form_name) {
        this.form_name = form_name;
    }

    public String getForm_code() {
        return form_code;
    }

    public void setForm_code(String form_code) {
        this.form_code = form_code;
    }

    @Override
    public String toString() {
        return "MeetingFormVO{" +
                "form_name='" + form_name + '\'' +
                ", form_code='" + form_code + '\'' +
                '}';
    }
}
