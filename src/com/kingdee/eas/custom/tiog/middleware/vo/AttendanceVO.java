package com.kingdee.eas.custom.tiog.middleware.vo;

import javax.xml.bind.annotation.*;

/**
 *
 * Title: AttendanceVO Description:列席人
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"attendance_name","position"})
public class AttendanceVO {
    //列席人员名称
    @XmlElement(name = "attendance_name")
    private String attendance_name;
    //列席人职位
    @XmlElement(name = "position")
    private String position;

    public String getAttendance_name() {
        return attendance_name;
    }

    public void setAttendance_name(String attendance_name) {
        this.attendance_name = attendance_name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "AttendanceVO{" +
                "attendance_name='" + attendance_name + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
