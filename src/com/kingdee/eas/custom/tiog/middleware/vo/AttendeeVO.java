package com.kingdee.eas.custom.tiog.middleware.vo;

import javax.xml.bind.annotation.*;

/**
 *
 * Title: AttendeeVO Description:参会人
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 */
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlRootElement(name = "country")
@XmlType(propOrder = {"attendee_name","reason"})
public class AttendeeVO {
    //参会人姓名
    @XmlElement(name = "attendee_name")
    private String attendee_name;
    //缺席原因
    @XmlElement(name = "reason")
    private String reason;

    public String getAttendee_name() {
        return attendee_name;
    }

    public void setAttendee_name(String attendee_name) {
        this.attendee_name = attendee_name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "AttendeeVO{" +
                "attendee_name='" + attendee_name + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
