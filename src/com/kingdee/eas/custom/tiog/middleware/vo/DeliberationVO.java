package com.kingdee.eas.custom.tiog.middleware.vo;

import javax.xml.bind.annotation.*;

/**
 *
 * Title: DeliberationVO Description:审议结果
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"deliberation_personnel","deliberation_result"})
public class DeliberationVO {
    //审议人
    @XmlElement(name = "deliberation_personnel")
    private String deliberation_personnel;
    //审议结果
    @XmlElement(name = "deliberation_result")
    private String deliberation_result;

    public String getDeliberation_personnel() {
        return deliberation_personnel;
    }

    @Override
    public String toString() {
        return "DeliberationVO{" +
                "deliberation_personnel='" + deliberation_personnel + '\'' +
                ", deliberation_result='" + deliberation_result + '\'' +
                '}';
    }

    public void setDeliberation_personnel(String deliberation_personnel) {
        this.deliberation_personnel = deliberation_personnel;
    }

    public String getDeliberation_result() {
        return deliberation_result;
    }

    public void setDeliberation_result(String deliberation_result) {
        this.deliberation_result = deliberation_result;
    }
}
