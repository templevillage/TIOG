package com.kingdee.eas.custom.tiog.middleware.vo;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 *
 * Title: itemListVO Description:事项清单列表
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19                
 */
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlRootElement(name = "student")
@XmlType(propOrder = {"item_id","item_name","item_code","item_meeting_list","legal_flag","remark","oper_type"})
public class ItemVO {
    //事项标识
    @XmlElement(name = "item_id")
    private String item_id;
    //事项名称
    @XmlElement(name = "item_name")
    private String item_name;
    //事项编码
    @XmlElement(name = "item_code")
    private String item_code;
    //事项标识
    @XmlElementWrapper(name = "item_meeting_list")
    @XmlElement(name = "item_meeting")
    private List<ItemMeetingVO> item_meeting_list;
    //是否需经法律审核
    @XmlElement(name = "legal_flag")
    private String legal_flag;
    //备注
    @XmlElement(name = "remark")
    private String remark;
    //操作标识（add表示新增，edit表示修改，del表示删除）
    @XmlElement(name = "oper_type")
    private String oper_type;

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }

    public List<ItemMeetingVO> getItem_meeting_list() {
        return item_meeting_list;
    }

    public void setItem_meeting_list(List<ItemMeetingVO> item_meeting_list) {
        this.item_meeting_list = item_meeting_list;
    }

    public String getLegal_flag() {
        return legal_flag;
    }

    public void setLegal_flag(String legal_flag) {
        this.legal_flag = legal_flag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOper_type() {
        return oper_type;
    }

    public void setOper_type(String oper_type) {
        this.oper_type = oper_type;
    }

    @Override
    public String toString() {
        return "ItemVO{" +
                "item_id='" + item_id + '\'' +
                ", item_name='" + item_name + '\'' +
                ", item_code='" + item_code + '\'' +
                ", item_meeting_list=" + item_meeting_list +
                ", legal_flag='" + legal_flag + '\'' +
                ", remark='" + remark + '\'' +
                ", oper_type='" + oper_type + '\'' +
                '}';
    }
}
