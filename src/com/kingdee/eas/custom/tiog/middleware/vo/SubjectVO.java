package com.kingdee.eas.custom.tiog.middleware.vo;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 *
 * Title: SubjectVO Description:议题列表
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 */
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlRootElement(name = "country")
@XmlType(propOrder = {"subject_id","subject_name","subject_code","source_name","special_name","item_list","relation_list","attendance_list"
        ,"deliberation_list","pass_flag","approval_flag","adopt_flag","subject_result","supervise_flag","remark","oper_type"})
public class SubjectVO {
    //议题标识
    @XmlElement(name = "subject_id")
    private String subject_id;
    //议题名称
    @XmlElement(name = "subject_name")
    private String subject_name;
    //议题编码
    @XmlElement(name = "subject_code")
    private String subject_code;
    //任务来源
    @XmlElement(name = "source_name")
    private String source_name;
    //专项名称
    @XmlElement(name = "special_name")
    private String special_name;
    //事项编码
    @XmlElementWrapper(name = "item_list")
    @XmlElement(name = "item_code")
    private String[] item_list;
    //关联关系
    @XmlElementWrapper(name = "relation_list")
    @XmlElement(name = "relation")
    private List<RelationVO> relation_list;
    //列席人数组
    @XmlElementWrapper(name = "attendance_list")
    @XmlElement(name = "attendance")
    private List<AttendanceVO> attendance_list;
    //审议结果
    @XmlElementWrapper(name = "deliberation_list")
    @XmlElement(name = "deliberation")
    private List<DeliberationVO> deliberation_list;
    //是否通过
    @XmlElement(name = "pass_flag")
    private String pass_flag;
    //是否需报国资委审批
    @XmlElement(name = "approval_flag")
    private String approval_flag;
    //是否听取意见
    @XmlElement(name = "adopt_flag")
    private String adopt_flag;
    //议题决议
    @XmlElement(name = "subject_result")
    private String subject_result;
    //专项名称
    @XmlElement(name = "supervise_flag")
    private String supervise_flag;
    //备注
    @XmlElement(name = "remark")
    private String remark;
    //操作标识（add表示新增，edit表示修改，del表示删除）
    @XmlElement(name = "oper_type")
    private String oper_type;

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public String getSpecial_name() {
        return special_name;
    }

    public void setSpecial_name(String special_name) {
        this.special_name = special_name;
    }

    public String[] getItem_list() {
        return item_list;
    }

    public void setItem_list(String[] item_list) {
        this.item_list = item_list;
    }

    public List<RelationVO> getRelation_list() {
        return relation_list;
    }

    public void setRelation_list(List<RelationVO> relation_list) {
        this.relation_list = relation_list;
    }

    public List<AttendanceVO> getAttendance_list() {
        return attendance_list;
    }

    public void setAttendance_list(List<AttendanceVO> attendance_list) {
        this.attendance_list = attendance_list;
    }

    public List<DeliberationVO> getDeliberation_list() {
        return deliberation_list;
    }

    public void setDeliberation_list(List<DeliberationVO> deliberation_list) {
        this.deliberation_list = deliberation_list;
    }

    public String getPass_flag() {
        return pass_flag;
    }

    public void setPass_flag(String pass_flag) {
        this.pass_flag = pass_flag;
    }

    public String getApproval_flag() {
        return approval_flag;
    }

    public void setApproval_flag(String approval_flag) {
        this.approval_flag = approval_flag;
    }

    public String getAdopt_flag() {
        return adopt_flag;
    }

    public void setAdopt_flag(String adopt_flag) {
        this.adopt_flag = adopt_flag;
    }

    public String getSubject_result() {
        return subject_result;
    }

    public void setSubject_result(String subject_result) {
        this.subject_result = subject_result;
    }

    public String getSupervise_flag() {
        return supervise_flag;
    }

    public void setSupervise_flag(String supervise_flag) {
        this.supervise_flag = supervise_flag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOper_type() {
        return oper_type;
    }

    public void setOper_type(String oper_type) {
        this.oper_type = oper_type;
    }

    @Override
    public String toString() {
        return "SubjectVO{" +
                "subject_id='" + subject_id + '\'' +
                ", subject_name='" + subject_name + '\'' +
                ", subject_code='" + subject_code + '\'' +
                ", source_name='" + source_name + '\'' +
                ", special_name='" + special_name + '\'' +
                ", item_list=" + item_list +
                ", relation_list=" + relation_list +
                ", attendance_list=" + attendance_list +
                ", deliberation_list=" + deliberation_list +
                ", pass_flag='" + pass_flag + '\'' +
                ", approval_flag='" + approval_flag + '\'' +
                ", adopt_flag='" + adopt_flag + '\'' +
                ", subject_result='" + subject_result + '\'' +
                ", supervise_flag='" + supervise_flag + '\'' +
                ", remark='" + remark + '\'' +
                ", oper_type='" + oper_type + '\'' +
                '}';
    }
}
