package com.kingdee.eas.custom.tiog.middleware.vo;

import javax.xml.bind.annotation.*;

/**
 *
 * Title: ItemMeeting Description:决策会议及顺序
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19      
 */
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlRootElement(name = "student")
@XmlType(propOrder = {"type_name","type_code"})
public class ItemMeetingVO {
    //企业自定义的会议类型
    @XmlElement(name = "type_name")
    private String type_name;
    //统一会议类型编码
    @XmlElement(name = "type_code")
    private String type_code;

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getType_code() {
        return type_code;
    }

    public void setType_code(String type_code) {
        this.type_code = type_code;
    }

    @Override
    public String toString() {
        return "ItemMeeting{" +
                "type_name='" + type_name + '\'' +
                ", type_code='" + type_code + '\'' +
                '}';
    }
}
