package com.kingdee.eas.custom.tiog.middleware.vo;

import javax.xml.bind.annotation.*;

/**
 *
 * Title: RelationVO Description:关联关系
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19    
 */
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlRootElement(name = "tiol")
@XmlType(propOrder = {"rel_company_id","rel_subject_code"})
public class RelationVO {
    //关联决策企业编码
    @XmlElement(name = "rel_company_id")
    private String rel_company_id;
    //关联议题编码
    @XmlElement(name = "rel_subject_code")
    private String rel_subject_code;

    public String getRel_company_id() {
        return rel_company_id;
    }

    public void setRel_company_id(String rel_company_id) {
        this.rel_company_id = rel_company_id;
    }

    public String getRel_subject_code() {
        return rel_subject_code;
    }

    public void setRel_subject_code(String rel_subject_code) {
        this.rel_subject_code = rel_subject_code;
    }

    @Override
    public String toString() {
        return "RelationVO{" +
                "rel_company_id='" + rel_company_id + '\'' +
                ", rel_subject_code='" + rel_subject_code + '\'' +
                '}';
    }
}
