package com.kingdee.eas.custom.tiog.middleware.vo.basicdata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * Title: VoteModeVO Description:表决方式
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-23       
 */

@XmlType(propOrder = {"mode_name","mode_radix","mode_rate"})
@XmlAccessorType(XmlAccessType.FIELD)
public class VoteModeVO {
    //表决方式名称
    @XmlElement(name = "mode_name")
    private String mode_name;
    //表决计算基数
    @XmlElement(name = "mode_radix")
    private String mode_radix;
    //表决通过比例
    @XmlElement(name = "mode_rate")
    private String mode_rate;

    public String getMode_name() {
        return mode_name;
    }

    public void setMode_name(String mode_name) {
        this.mode_name = mode_name;
    }

    public String getMode_radix() {
        return mode_radix;
    }

    public void setMode_radix(String mode_radix) {
        this.mode_radix = mode_radix;
    }

    public String getMode_rate() {
        return mode_rate;
    }

    public void setMode_rate(String mode_rate) {
        this.mode_rate = mode_rate;
    }

    @Override
    public String toString() {
        return "VoteModeVO{" +
                "mode_name='" + mode_name + '\'' +
                ", mode_radix='" + mode_radix + '\'' +
                ", mode_rate='" + mode_rate + '\'' +
                '}';
    }
}
