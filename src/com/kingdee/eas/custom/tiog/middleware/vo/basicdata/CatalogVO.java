package com.kingdee.eas.custom.tiog.middleware.vo.basicdata;

import javax.xml.bind.annotation.*;

/**
 *
 * Title: CatalogList Description:事项目录
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-23       
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"catalog_name","catalog_code","catalog_level","order_number"})
public class CatalogVO {
    //private CatalogVO CatalogVO;
    //事项目录名称
    @XmlElement(name = "catalog_name")
    private String  catalog_name;
    //事项目录编码
    @XmlElement(name = "catalog_code")
    private String  catalog_code;
    //事项目录级别
    @XmlElement(name = "catalog_level")
    private String  catalog_level;
    //排序号
    @XmlElement(name = "order_number")
    private String  order_number;

   /* public com.kingdee.eas.custom.tiog.middleware.vo.basicdata.CatalogVO getCatalogVO() {
        return CatalogVO;
    }

    public void setCatalogVO(com.kingdee.eas.custom.tiog.middleware.vo.basicdata.CatalogVO catalogVO) {
        CatalogVO = catalogVO;
    }*/

    public String getCatalog_name() {
        return catalog_name;
    }

    public void setCatalog_name(String catalog_name) {
        this.catalog_name = catalog_name;
    }

    public String getCatalog_code() {
        return catalog_code;
    }

    public void setCatalog_code(String catalog_code) {
        this.catalog_code = catalog_code;
    }

    public String getCatalog_level() {
        return catalog_level;
    }

    public void setCatalog_level(String catalog_level) {
        this.catalog_level = catalog_level;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    @Override
    public String toString() {
        return "CatalogVO{" +
                /*"CatalogVO=" + CatalogVO +*/
                ", catalog_name='" + catalog_name + '\'' +
                ", catalog_code='" + catalog_code + '\'' +
                ", catalog_level='" + catalog_level + '\'' +
                ", order_number='" + order_number + '\'' +
                '}';
    }
}
