package com.kingdee.eas.custom.tiog.middleware.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * Title: DutyVO Description:落实责任信息
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"dept","name"})
public class DutyVO {
    //落实责任部门
    @XmlElement(name = "dept")
    private String dept;
    //责任人
    @XmlElement(name = "name")
    private String name;

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DutyVO{" +
                "dept='" + dept + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
