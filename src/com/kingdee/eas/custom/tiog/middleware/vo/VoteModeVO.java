package com.kingdee.eas.custom.tiog.middleware.vo;

import javax.xml.bind.annotation.*;

/**
 *
 * Title: VoteModeVO Description:表决方式
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19  
 * <!--表决方式集合-->    
 *  * 25.  <vote_mode_list>   
 *  * 26.    <vote_mode>   
 *  * 27.      <!--事项编码-->    
 *  * 28.      <item_code>默认</item_code>    
 *  * 29.      <!--人数占比-->    
 *  * 30.      <rate>1/2</rate>    
 *  * 31.      <!--表决方式-->    
 *  * 32.      <vote_mode>赞成票超过到会成员人数的1/2</vote_mode>   
 *  * 33.    </vote_mode>    
 *  * 34.    <vote_mode>   
 *  * 35.      <item_code>H06-001</item_code>    
 *  * 36.      <rate>1</rate>    
 *  * 37.      <vote_mode>赞成票超过到会成员人数的3/4</vote_mode>   
 *  * 38.    </vote_mode>    
 *  * 39.    <vote_mode>   
 *  * 40.      <item_code>P08-005</item_code>    
 *  * 41.      <rate>2/3</rate>    
 *  * 42.      <vote_mode>赞成票超过应到会成员人数的2/3</vote_mode>   
 *  * 43.    </vote_mode>   
 *  * 44.  </vote_mode_list>    
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"item_code","rate","vote_mode"})
public class VoteModeVO {
    //事项编码
    @XmlElement(name = "item_code")
    private String item_code;
    //人数占比
    @XmlElement(name = "rate")
    private String rate;
    //表决方式
    @XmlElement(name = "vote_mode")
    private String vote_mode;

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getVote_mode() {
        return vote_mode;
    }

    public void setVote_mode(String vote_mode) {
        this.vote_mode = vote_mode;
    }

    @Override
    public String toString() {
        return "VoteModeVO{" +
                "item_code='" + item_code + '\'' +
                ", rate='" + rate + '\'' +
                ", vote_mode='" + vote_mode + '\'' +
                '}';
    }
}
