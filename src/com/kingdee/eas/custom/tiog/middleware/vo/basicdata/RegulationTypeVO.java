package com.kingdee.eas.custom.tiog.middleware.vo.basicdata;

import javax.xml.bind.annotation.*;

/**
 *
 * Title: RegulationTypeVO Description:制度类型
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-20       
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"name","flag"})
public class RegulationTypeVO {
    //制度类型名称
    @XmlElement(name = "name")
    private String name;
    //是否必须上传
    @XmlElement(name = "flag")
    private String flag;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "RegulationTypeVO{" +
                "name='" + name + '\'' +
                ", flag='" + flag + '\'' +
                '}';
    }
}
