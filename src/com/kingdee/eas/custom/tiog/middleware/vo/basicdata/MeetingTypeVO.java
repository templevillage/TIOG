package com.kingdee.eas.custom.tiog.middleware.vo.basicdata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
/**
 *
 * Title: MeetingTypeVO Description:会议形式
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-20       
 */
@XmlType(propOrder = {"type_name","type_code","group_type"})
@XmlAccessorType(XmlAccessType.FIELD)
public class MeetingTypeVO {

        //会议类型名称
        @XmlElement(name = "type_name")
        private String type_name;
        //会议类型编码
        @XmlElement(name = "type_code")
        private String type_code;
        //领导班子类型
        @XmlElement(name = "group_type")
        private String group_type;

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getType_code() {
        return type_code;
    }

    public void setType_code(String type_code) {
        this.type_code = type_code;
    }

    public String getGroup_type() {
        return group_type;
    }

    public void setGroup_type(String group_type) {
        this.group_type = group_type;
    }

    @Override
        public String toString() {
        return "MeetingType{" +
                "type_name='" + type_name + '\'' +
                ", type_code='" + type_code + '\'' +
                ", group_type='" + group_type + '\'' +
                '}';
    }

}
