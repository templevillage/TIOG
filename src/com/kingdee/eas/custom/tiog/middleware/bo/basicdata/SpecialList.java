package com.kingdee.eas.custom.tiog.middleware.bo.basicdata;

import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;

import javax.xml.bind.annotation.*;
import java.util.Arrays;

/**
 *
 * Title: SourceList Description:专项名称
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-23       
 */
@XmlRootElement(name = "tiol")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"special"})
public class SpecialList {
    @XmlElementWrapper(name = "special_list")
    @XmlElement(name = "special")
    //任务来源名称
    private String[] special;

    public String[] getSpecial() {
        return special;
    }

    public void setSpecial(String[] special) {
        this.special = special;
    }

    @Override
    public String toString() {
        return "SpecialList{" +
                "special=" + Arrays.toString(special) +
                '}';
    }

    public static void main(String[] args) {
        /*SpecialList specialList = new SpecialList();
        String[] special = new String[]{"党风廉政建设","精准扶贫","援疆援藏","提速降费"};
        specialList.setSpecial(special);
        String str = JaxbUtil.converTomXml(specialList);
        System.out.println(str);*/
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<tiol>\n" +
                "    <special_list>\n" +
                "        <special>党风廉政建设</special>\n" +
                "        <special>精准扶贫</special>\n" +
                "        <special>援疆援藏</special>\n" +
                "        <special>提速降费</special>\n" +
                "    </special_list>\n" +
                "</tiol>";
        SpecialList specialList = JaxbUtil.converyToJavaBean(str,SpecialList.class);
        System.out.println(specialList);
    }
}
