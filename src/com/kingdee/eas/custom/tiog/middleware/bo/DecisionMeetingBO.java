package com.kingdee.eas.custom.tiog.middleware.bo;

import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;
import com.kingdee.eas.custom.tiog.middleware.vo.*;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Title: DecisionMeetingBO Description:决策会议
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "tiol")
@XmlType(propOrder = {"meeting_id","company_name","company_id","meeting_name","meeting_code","meeting_type_name","meeting_type_code","meeting_form","meeting_time","release_time"
        ,"moderator","remark","source","oper_type","attendee_list","subject_list"})
public class DecisionMeetingBO {
    //会议标识
    @XmlElement(name = "meeting_id")
    private String meeting_id;
    //企业名称
    @XmlElement(name = "company_name")
    private String company_name;
    //企业编码
    @XmlElement(name = "company_id")
    private String company_id;
    //会议名称
    @XmlElement(name = "meeting_name")
    private String meeting_name;
    //会议编码
    @XmlElement(name = "meeting_code")
    private String meeting_code;
    //会议类型
    @XmlElement(name = "meeting_type_name")
    private String meeting_type_name;
    //会议类型编码
    @XmlElement(name = "meeting_type_code")
    private String meeting_type_code;
    //会议形式
    @XmlElement(name = "meeting_form")
    private String meeting_form;
    //会议时间
    @XmlElement(name = "meeting_time")
    private String meeting_time;
    //会议纪要印发时间
    @XmlElement(name = "release_time")
    private String release_time;
    //主持人
    @XmlElement(name = "moderator")
    private String moderator;
    //备注
    @XmlElement(name = "remark")
    private String remark;
    //数据来源
    @XmlElement(name = "source")
    private String source;
    //操作标识（add表示新增，edit表示修改，del表示删除
    @XmlElement(name = "oper_type")
    private String oper_type;
    //参会人
    @XmlElementWrapper(name = "attendee_list")
    @XmlElement(name = "attendee")
    private List<AttendeeVO> attendee_list;
    //议题列表数组
    @XmlElementWrapper(name = "subject_list")
    @XmlElement(name = "subject")
    private List<SubjectVO> subject_list;

    public String getMeeting_id() {
        return meeting_id;
    }

    public void setMeeting_id(String meeting_id) {
        this.meeting_id = meeting_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getMeeting_name() {
        return meeting_name;
    }

    public void setMeeting_name(String meeting_name) {
        this.meeting_name = meeting_name;
    }

    public String getMeeting_code() {
        return meeting_code;
    }

    public void setMeeting_code(String meeting_code) {
        this.meeting_code = meeting_code;
    }

    public String getMeeting_type_name() {
        return meeting_type_name;
    }

    public void setMeeting_type_name(String meeting_type_name) {
        this.meeting_type_name = meeting_type_name;
    }

    public String getMeeting_type_code() {
        return meeting_type_code;
    }

    public void setMeeting_type_code(String meeting_type_code) {
        this.meeting_type_code = meeting_type_code;
    }

    public String getMeeting_form() {
        return meeting_form;
    }

    public void setMeeting_form(String meeting_form) {
        this.meeting_form = meeting_form;
    }

    public String getMeeting_time() {
        return meeting_time;
    }

    public void setMeeting_time(String meeting_time) {
        this.meeting_time = meeting_time;
    }

    public String getRelease_time() {
        return release_time;
    }

    public void setRelease_time(String release_time) {
        this.release_time = release_time;
    }

    public String getModerator() {
        return moderator;
    }

    public void setModerator(String moderator) {
        this.moderator = moderator;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOper_type() {
        return oper_type;
    }

    public void setOper_type(String oper_type) {
        this.oper_type = oper_type;
    }

    public List<AttendeeVO> getAttendee_list() {
        return attendee_list;
    }

    public void setAttendee_list(List<AttendeeVO> attendee_list) {
        this.attendee_list = attendee_list;
    }

    public List<SubjectVO> getSubject_list() {
        return subject_list;
    }

    public void setSubject_list(List<SubjectVO> subject_list) {
        this.subject_list = subject_list;
    }

    @Override
    public String toString() {
        return "DecisionMeetingBO{" +
                "meeting_id='" + meeting_id + '\'' +
                ", company_name='" + company_name + '\'' +
                ", company_id='" + company_id + '\'' +
                ", meeting_name='" + meeting_name + '\'' +
                ", meeting_code='" + meeting_code + '\'' +
                ", meeting_type_name='" + meeting_type_name + '\'' +
                ", meeting_type_code='" + meeting_type_code + '\'' +
                ", meeting_form='" + meeting_form + '\'' +
                ", meeting_time='" + meeting_time + '\'' +
                ", release_time='" + release_time + '\'' +
                ", moderator='" + moderator + '\'' +
                ", remark='" + remark + '\'' +
                ", source='" + source + '\'' +
                ", oper_type='" + oper_type + '\'' +
                ", attendee_list=" + attendee_list +
                ", subject_list=" + subject_list +
                '}';
    }


    public static void main(String[] args) {
        DecisionMeetingBO decisionMeetingBO = new DecisionMeetingBO();
        decisionMeetingBO.setMeeting_id("65832456558134956791543829657891");
        decisionMeetingBO.setCompany_name("某集团有限公司");
        decisionMeetingBO.setCompany_id("9111XXXXXX563N");
        decisionMeetingBO.setMeeting_name("第10次总经理办公会");
        decisionMeetingBO.setMeeting_code("JL20180010");
        decisionMeetingBO.setMeeting_type_name("总经理办公会");
        decisionMeetingBO.setMeeting_type_code("JL");
        decisionMeetingBO.setMeeting_form("现场会议");
        decisionMeetingBO.setMeeting_time("2018-10-09");
        decisionMeetingBO.setRelease_time("2018-10-15");
        decisionMeetingBO.setModerator("张XX");
        decisionMeetingBO.setRemark("无");
        decisionMeetingBO.setSource("系统");
        decisionMeetingBO.setOper_type("add");
        List<AttendeeVO> attendeeVOList = new ArrayList<AttendeeVO>();
        AttendeeVO attendeeVO01 = new AttendeeVO();
        attendeeVO01.setAttendee_name("李XX");
        attendeeVO01.setReason("因公出国");
        AttendeeVO attendeeVO02 = new AttendeeVO();
        attendeeVO02.setAttendee_name("周XX");
        attendeeVOList.add(attendeeVO01);
        attendeeVOList.add(attendeeVO02);
        decisionMeetingBO.setAttendee_list(attendeeVOList);
        List<SubjectVO> subjectVOArrayList = new ArrayList<SubjectVO>();
        SubjectVO subjectVO01 = new SubjectVO();
        subjectVO01.setSubject_id("9462145655813495679154382355234");
        subjectVO01.setSubject_name("关于结对帮扶工作的XX事项审议");
        subjectVO01.setSubject_code("JL20180010-001");
        subjectVO01.setSource_name("扶贫办");
        subjectVO01.setSpecial_name("精准扶贫");
        List<ItemVO> itemList01 = new ArrayList<ItemVO>();
        ItemVO itemVO01 = new ItemVO();
        itemVO01.setItem_code("D01-201901-000001");
        ItemVO itemVO02 = new ItemVO();
        itemVO02.setItem_code("D01-201901-000002");
        itemList01.add(itemVO01);
        itemList01.add(itemVO02);
        String[] strings = new String[2];
        strings[0] = "D01-201901-000001";
        strings[1] = "D01-201901-000002";
        subjectVO01.setItem_list(strings);
        List<RelationVO> relationVOList01 = new ArrayList<RelationVO>();
        RelationVO relationVO01 = new RelationVO();
        relationVO01.setRel_company_id("8111XXXXXX2465");
        relationVO01.setRel_subject_code("DZ20180008-001");
        RelationVO relationVO02 = new RelationVO();
        relationVO02.setRel_subject_code("DZ20180009-001");
        relationVOList01.add(relationVO01);
        relationVOList01.add(relationVO02);
        subjectVO01.setRelation_list(relationVOList01);
        List<AttendanceVO> attendanceVOList = new ArrayList<AttendanceVO>();
        AttendanceVO attendanceVO01 = new AttendanceVO();
        attendanceVO01.setAttendance_name("王XX");
        attendanceVO01.setPosition("总法律顾问");
        AttendanceVO attendanceVO02 = new AttendanceVO();
        attendanceVO02.setAttendance_name("赵XX");
        attendanceVO02.setPosition("总经济师");
        attendanceVOList.add(attendanceVO01);
        attendanceVOList.add(attendanceVO02);
        subjectVO01.setAttendance_list(attendanceVOList);
        List<DeliberationVO> deliberationVOList01 = new ArrayList<DeliberationVO>();
        DeliberationVO deliberationVO01 = new DeliberationVO();
        deliberationVO01.setDeliberation_personnel("张XX");
        deliberationVO01.setDeliberation_result("同意");
        DeliberationVO deliberationVO02 = new DeliberationVO();
        deliberationVO02.setDeliberation_personnel("李XX");
        deliberationVO02.setDeliberation_result("原则同意");
        deliberationVOList01.add(deliberationVO01);
        deliberationVOList01.add(deliberationVO02);
        subjectVO01.setDeliberation_list(deliberationVOList01);
        subjectVO01.setPass_flag("是");
        subjectVO01.setApproval_flag("备案");
        subjectVO01.setAdopt_flag("是");
        subjectVO01.setSubject_result("会议听取了XX事项的汇报。会议不同意××××××");
        subjectVO01.setSupervise_flag("是");
        subjectVO01.setOper_type("add");
        subjectVOArrayList.add(subjectVO01);
        decisionMeetingBO.setSubject_list(subjectVOArrayList);
        String str = JaxbUtil.converTomXml(decisionMeetingBO);
        System.out.println(str);
    }
}
