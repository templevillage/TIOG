package com.kingdee.eas.custom.tiog.middleware.bo.basicdata;


import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;
import com.kingdee.eas.custom.tiog.middleware.vo.basicdata.MeetingTypeVO;


import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Title: MeetingType Description:会议类型
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 */
@XmlRootElement(name = "tiol")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"meeting_type_list"})
public class MeetingTypeList {
    @XmlElementWrapper(name = "meeting_type_list")
    @XmlElement(name = "meeting_type")
    private List<MeetingTypeVO> meeting_type_list;

    public List<MeetingTypeVO> getMeeting_type_list() {
        return meeting_type_list;
    }

    public void setMeeting_type_list(List<MeetingTypeVO> meeting_type_list) {
        this.meeting_type_list = meeting_type_list;
    }

    @Override
    public String toString() {
        return "MeetingType{" +
                "meeting_type_list=" + meeting_type_list +
                '}';
    }







    public static void main(String[] args) {
        MeetingTypeList meetingType = new MeetingTypeList();
        List<MeetingTypeVO> meetingTypeVOList = new ArrayList<MeetingTypeVO>();
        MeetingTypeVO meetingTypeVO01 =  new MeetingTypeVO();
        meetingTypeVO01.setType_name("党委（党组）会");
        meetingTypeVO01.setType_code("DZ");
        meetingTypeVO01.setGroup_type("党委（党组）成员");
        MeetingTypeVO meetingTypeVO02 =  new MeetingTypeVO();
        meetingTypeVO02.setType_name("股东会");
        meetingTypeVO02.setType_code("GD");
        meetingTypeVOList.add(meetingTypeVO01);
        meetingTypeVOList.add(meetingTypeVO02);
        meetingType.setMeeting_type_list(meetingTypeVOList);
        //String str = JaxbUtil.converTomXml(meetingType);
        String str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>  \n" +
                "<tiol>   \n" +
                "  <meeting_type_list>   \n" +
                "    <meeting_type>   \n" +
                "      <!--会议类型名称-->    \n" +
                "      <type_name>党委（党组）会</type_name>    \n" +
                "      <!--会议类型编码-->    \n" +
                "      <type_code>DZ</type_code>    \n" +
                "      <!--领导班子类型-->    \n" +
                "      <group_type>党委（党组）成员</group_type>   \n" +
                "    </meeting_type>    \n" +
                "    <meeting_type>   \n" +
                "      <type_name>股东会</type_name>    \n" +
                "      <type_code>GD</type_code>    \n" +
                "      <group_type/>   \n" +
                "    </meeting_type>    \n" +
                "    <meeting_type>   \n" +
                "      <type_name>董事会</type_name>    \n" +
                "      <type_code>DS</type_code>    \n" +
                "      <group_type>董事会成员</group_type>   \n" +
                "    </meeting_type>    \n" +
                "    <meeting_type>   \n" +
                "      <type_name>经理层办公会</type_name>    \n" +
                "      <type_code>JL</type_code>    \n" +
                "      <group_type>经理班子成员</group_type>   \n" +
                "    </meeting_type>    \n" +
                "    <meeting_type>   \n" +
                "      <type_name>职代会</type_name>    \n" +
                "      <type_code>ZD</type_code>    \n" +
                "      <group_type/>   \n" +
                "    </meeting_type>    \n" +
                "    <meeting_type>   \n" +
                "      <type_name>其他</type_name>    \n" +
                "      <type_code>QT</type_code>    \n" +
                "      <group_type/>   \n" +
                "    </meeting_type>   \n" +
                "  </meeting_type_list>   \n" +
                "</tiol>  ";
        System.out.println(str);
        MeetingTypeList meetingType1 = JaxbUtil.converyToJavaBean(str, MeetingTypeList.class);
        System.out.println(meetingType);
    }
}
