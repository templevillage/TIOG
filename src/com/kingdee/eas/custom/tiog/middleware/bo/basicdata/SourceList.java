package com.kingdee.eas.custom.tiog.middleware.bo.basicdata;

import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;

import javax.xml.bind.annotation.*;
import java.util.Arrays;

/**
 *
 * Title: SourceList Description:任务来源
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-23       
 */
@XmlRootElement(name = "tiol")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"source"})
public class SourceList {
    @XmlElementWrapper(name = "source_list")
    @XmlElement(name = "source")
    //任务来源名称
    private String[] source;

    public String[] getSource() {
        return source;
    }

    public void setSource(String[] source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "SourceList{" +
                "source=" + Arrays.toString(source) +
                '}';
    }

    public static void main(String[] args) {
       /* SourceList sourceList = new SourceList();
        String[] source = new String[]{"习近平总书记的有关批示指示"};
        sourceList.setSource(source);
        String str = JaxbUtil.converTomXml(sourceList);
        System.out.println(str);*/
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<tiol>\n" +
                "    <source_list>\n" +
                "        <source>习近平总书记的有关批示指示</source>\n" +
                "    </source_list>\n" +
                "</tiol>";
        SourceList sourceList1 = JaxbUtil.converyToJavaBean(str,SourceList.class);
        System.out.println(sourceList1);
    }
}
