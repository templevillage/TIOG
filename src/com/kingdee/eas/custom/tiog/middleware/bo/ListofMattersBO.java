package com.kingdee.eas.custom.tiog.middleware.bo;

import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;
import com.kingdee.eas.custom.tiog.middleware.vo.ItemMeetingVO;
import com.kingdee.eas.custom.tiog.middleware.vo.ItemVO;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Title: ListofMattersBO Description:事项清单
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19    
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "tiol")
@XmlType(propOrder = {"list_id","company_name","company_id","list_name","list_version","effective_date","invalid_date","remark"
        ,"source","oper_type","item_list"})
public class ListofMattersBO {
    //事项清单标识
    @XmlElement(name = "list_id")
    private String list_id;
    //企业名称
    @XmlElement(name = "company_name")
    private String company_name;
    //企业编码
    @XmlElement(name = "company_id")
    private String company_id;
    //事项清单名称
    @XmlElement(name = "list_name")
    private String list_name;
    //版本号
    @XmlElement(name = "list_version")
    private String list_version;
    //生效日期
    @XmlElement(name = "effective_date")
    private String effective_date;
    //失效日期
    @XmlElement(name = "invalid_date")
    private String invalid_date;
    //描述
    @XmlElement(name = "remark")
    private String remark;
    //数据来源
    @XmlElement(name = "source")
    private String source;
    //操作标识（add表示新增，edit表示修改，del表示删除）
    @XmlElement(name = "oper_type")
    private String oper_type;
    //事项清单列表
    @XmlElementWrapper(name = "item_list")
    @XmlElement(name = "item")
    private List<ItemVO> item_list;

    public String getList_id() {
        return list_id;
    }

    public void setList_id(String list_id) {
        this.list_id = list_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getList_name() {
        return list_name;
    }

    public void setList_name(String list_name) {
        this.list_name = list_name;
    }

    public String getList_version() {
        return list_version;
    }

    public void setList_version(String list_version) {
        this.list_version = list_version;
    }

    public String getEffective_date() {
        return effective_date;
    }

    public void setEffective_date(String effective_date) {
        this.effective_date = effective_date;
    }

    public String getInvalid_date() {
        return invalid_date;
    }

    public void setInvalid_date(String invalid_date) {
        this.invalid_date = invalid_date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOper_type() {
        return oper_type;
    }

    public void setOper_type(String oper_type) {
        this.oper_type = oper_type;
    }

    public List<ItemVO> getItem_list() {
        return item_list;
    }

    public void setItem_list(List<ItemVO> item_list) {
        this.item_list = item_list;
    }

    @Override
    public String toString() {
        return "ListofMattersVO{" +
                "list_id='" + list_id + '\'' +
                ", company_name='" + company_name + '\'' +
                ", company_id='" + company_id + '\'' +
                ", list_name='" + list_name + '\'' +
                ", list_version='" + list_version + '\'' +
                ", effective_date='" + effective_date + '\'' +
                ", invalid_date='" + invalid_date + '\'' +
                ", remark='" + remark + '\'' +
                ", source='" + source + '\'' +
                ", oper_type='" + oper_type + '\'' +
                ", item_list=" + item_list +
                '}';
    }

    public static void main(String[] args) {
        /**
         <?xml version="1.0" encoding="utf-8"?>  
           
         <tiol>   
           <!--事项清单标识-->    
           <list_id>25317158520088613924664242764841</list_id>    
           <!--企业名称-->    
           <company_name>某集团有限公司</company_name>    
           <!--企业编码-->    
           <company_id>9111XXXXXX563N</company_id>    
           <!--事项清单名称-->    
           <list_name>2019年XX公司“三重一大”事项清单</list_name>    
           <!--版本号-->    
           <list_version>201901</list_version>    
           <!--生效日期-->    
           <effective_date>2018-10-10</effective_date>    
           <!--失效日期-->    
           <invalid_date>2018-12-31</invalid_date>    
           <!--描述-->    
           <remark>依据2018年第XXX次董事会的决议，制定本事项清单。</remark>    
           <!--数据来源-->    
           <source>系统</source>    
           <!--操作标识（add表示新增，edit表示修改，del表示删除）-->    
           <oper_type>add</oper_type>    
           <!--事项清单列表-->    
           <item_list>   
             <item>   
               <!--事项标识-->    
               <item_id>65832456558134956791543829657810</item_id>    
               <!--事项名称-->    
               <item_name>公司总部组织机构调整</item_name>    
               <!--事项编码-->    
               <item_code>D12-201901-000001</item_code>    
               <!--决策会议及顺序-->    
               <item_meeting_list>   
                 <item_meeting>   
                   <!--企业自定义的会议类型-->    
                   <type_name>党委会</type_name>    
                   <!--统一会议类型编码-->    
                   <type_code>DZ</type_code>   
                 </item_meeting>    
                 <item_meeting>   
                   <type_name>总经理办公会</type_name>    
                   <type_code>JL</type_code>   
                 </item_meeting>    
               </item_meeting_list>    
               <!--是否需经法律审核-->    
               <legal_flag>否</legal_flag>    
               <!--备注-->    
               <remark/>    
               <!--操作标识（add表示新增，edit表示修改，del表示删除）-->    
               <oper_type>add</oper_type>   
             </item>    
             <item>   
               <item_id>o40362cfolqvmx09501jyzfifemyfpcx</item_id>    
               <item_name>公司二级单位组织机构调整</item_name>    
               <item_code>D12-201901-000002</item_code>    
               <item_meeting_list>   
                 <item_meeting>   
                   <type_name>党委会</type_name>    
                   <type_code>DZ</type_code>   
                 </item_meeting>    
                 <item_meeting>   
                   <type_name>董事会</type_name>    
                   <type_code>DS</type_code>   
                 </item_meeting>   
               </item_meeting_list>    
               <legal_flag>是</legal_flag>    
               <remark/>    
               <oper_type>add</oper_type>   
             </item>   
           </item_list>   
         </tiol>  
         */
        ListofMattersBO listofMattersVO = new ListofMattersBO();
        listofMattersVO.setList_id("25317158520088613924664242764841");
        listofMattersVO.setCompany_name("某集团有限公司");
        listofMattersVO.setCompany_id("9111XXXXXX563N");
        listofMattersVO.setList_name("2019年XX公司“三重一大”事项清单");
        listofMattersVO.setList_version("201901");
        listofMattersVO.setEffective_date("2018-10-10");
        listofMattersVO.setInvalid_date("2018-12-31");
        listofMattersVO.setRemark("依据2018年第XXX次董事会的决议，制定本事项清单。");
        listofMattersVO.setSource("系统");
        listofMattersVO.setOper_type("add");
        List<ItemVO> itemVOList = new ArrayList<ItemVO>();
        //itemVO1
        ItemVO itemVO1 = new ItemVO();
        itemVO1.setItem_id("65832456558134956791543829657810");
        itemVO1.setItem_name("公司总部组织机构调整");
        itemVO1.setItem_code("D12-201901-000001");
        List<ItemMeetingVO> itemMeetingList = new ArrayList<ItemMeetingVO>();
        ItemMeetingVO itemMeeting01 = new ItemMeetingVO();
        itemMeeting01.setType_name("党委会");
        itemMeeting01.setType_code("DZ");
        ItemMeetingVO itemMeeting02 = new ItemMeetingVO();
        itemMeeting02.setType_name("董事会");
        itemMeeting02.setType_code("DS");
        itemMeetingList.add(itemMeeting01);
        itemMeetingList.add(itemMeeting02);
        itemVO1.setItem_meeting_list(itemMeetingList);
        itemVO1.setLegal_flag("是");
        itemVO1.setRemark("无");
        itemVO1.setOper_type("add");
        //itemVO2
        ItemVO itemVO2 = new ItemVO();
        itemVO2.setItem_id("o40362cfolqvmx09501jyzfifemyfpcx");
        itemVO2.setItem_name("公司二级单位组织机构调整");
        itemVO2.setItem_code("D12-201901-000002");
        List<ItemMeetingVO> itemMeetingList01 = new ArrayList<ItemMeetingVO>();
        ItemMeetingVO itemMeeting03 = new ItemMeetingVO();
        itemMeeting03.setType_name("党委会");
        itemMeeting03.setType_code("DZ");
        ItemMeetingVO itemMeeting04 = new ItemMeetingVO();
        itemMeeting04.setType_name("总经理办公会");
        itemMeeting04.setType_code("JL");
        itemMeetingList01.add(itemMeeting03);
        itemMeetingList01.add(itemMeeting04);
        itemVO2.setItem_meeting_list(itemMeetingList01);
        itemVO2.setLegal_flag("否");
        itemVO2.setRemark("无");
        itemVO2.setOper_type("add");
        itemVOList.add(itemVO1);
        itemVOList.add(itemVO2);
        listofMattersVO.setItem_list(itemVOList);
        String str = JaxbUtil.converTomXml(listofMattersVO);
        System.out.println(str);
    }


















}
