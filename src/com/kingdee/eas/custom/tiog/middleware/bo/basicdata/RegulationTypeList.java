package com.kingdee.eas.custom.tiog.middleware.bo.basicdata;

import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;
import com.kingdee.eas.custom.tiog.middleware.vo.basicdata.RegulationTypeVO;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 *
 * Title: RegulationTypeList Description:制度类型
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-20       
 */
@XmlRootElement(name = "tiol")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"regulation_type_list"})
public class RegulationTypeList {
    @XmlElementWrapper(name = "regulation_type_list")
    @XmlElement(name = "regulation_type")
    private List<RegulationTypeVO> regulation_type_list;

    public List<RegulationTypeVO> getRegulation_type_list() {
        return regulation_type_list;
    }

    public void setRegulation_type_list(List<RegulationTypeVO> regulation_type_list) {
        this.regulation_type_list = regulation_type_list;
    }

    @Override
    public String toString() {
        return "RegulationTypeList{" +
                "regulation_type_list=" + regulation_type_list +
                '}';
    }


    public static void main(String[] args) {
       /* RegulationTypeList regulationTypeList = new RegulationTypeList();
        RegulationTypeVO regulationTypeVO = new RegulationTypeVO();
        regulationTypeVO.setName("公司章程");
        regulationTypeVO.setFlag("是");
        List<RegulationTypeVO> regulationTypeVOList = new ArrayList<>();
        regulationTypeVOList.add(regulationTypeVO);
        regulationTypeList.setRegulation_type_list(regulationTypeVOList);
        String str = JaxbUtil.converTomXml(regulationTypeList);
        System.out.println(str);*/
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<tiol>\n" +
                "    <regulation_type_list>   \n" +
                "    <regulation_type>   \n" +
                "      <!--制度类型名称-->    \n" +
                "      <name>公司章程</name>    \n" +
                "      <!--是否必须上传-->    \n" +
                "      <flag>是</flag>   \n" +
                "    </regulation_type>    \n" +
                "    <regulation_type>   \n" +
                "      <name>实施办法</name>    \n" +
                "      <flag>是</flag>   \n" +
                "    </regulation_type>    \n" +
                "    <regulation_type>   \n" +
                "      <name>议事规则</name>    \n" +
                "      <flag>是</flag>   \n" +
                "    </regulation_type>    \n" +
                "    <regulation_type>   \n" +
                "      <name>授权管理办法</name>    \n" +
                "      <flag>否</flag>   \n" +
                "    </regulation_type>   \n" +
                "  </regulation_type_list>   \n" +
                "</tiol>";
        RegulationTypeList regulationTypeList = JaxbUtil.converyToJavaBean(str, RegulationTypeList.class);
        System.out.println(regulationTypeList);
    }
}
