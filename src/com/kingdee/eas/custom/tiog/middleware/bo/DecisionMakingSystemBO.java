package com.kingdee.eas.custom.tiog.middleware.bo;

import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;
import com.kingdee.eas.custom.tiog.middleware.vo.VoteModeVO;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Title: DecisionMakingSystemBO Description:决策制度
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 */
@XmlRootElement(name = "tiol")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"regulation_id","company_name","company_id","regulation_name","regulation_type_name","approval_date","effective_date"
        ,"invalid_date","audit_flag","meeting_type_code","vote_mode_list","remark","source","oper_type"})
public class DecisionMakingSystemBO {
    //制度标识
    @XmlElement(name = "regulation_id")
    private String regulation_id;
    //企业名称
    @XmlElement(name = "company_name")
    private String company_name;
    //企业编码
    @XmlElement(name = "company_id")
    private String company_id;
    //制度名称
    @XmlElement(name = "regulation_name")
    private String regulation_name;
    //制度类型名称
    @XmlElement(name = "regulation_type_name")
    private String regulation_type_name;
    //审批日期
    @XmlElement(name = "approval_date")
    private String approval_date;
    //生效日期
    @XmlElement(name = "effective_date")
    private String effective_date;
    //失效日期
    @XmlElement(name = "invalid_date")
    private String invalid_date;
    //是否经过合法审查
    @XmlElement(name = "audit_flag")
    private String audit_flag;
    //会议类型编码
    @XmlElement(name = "meeting_type_code")
    private String meeting_type_code;
    //表决方式集合
    @XmlElementWrapper(name = "vote_mode_list")
    @XmlElement(name = "vote_mode")
    private List<VoteModeVO> vote_mode_list;
    //备注
    @XmlElement(name = "remark")
    private String remark;
    //数据来源
    @XmlElement(name = "source")
    private String source;
    //操作标识（add表示新增，edit表示修改，del表示删除）
    @XmlElement(name = "oper_type")
    private String oper_type;

    public String getRegulation_id() {
        return regulation_id;
    }

    public void setRegulation_id(String regulation_id) {
        this.regulation_id = regulation_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getRegulation_name() {
        return regulation_name;
    }

    public void setRegulation_name(String regulation_name) {
        this.regulation_name = regulation_name;
    }

    public String getRegulation_type_name() {
        return regulation_type_name;
    }

    public void setRegulation_type_name(String regulation_type_name) {
        this.regulation_type_name = regulation_type_name;
    }

    public String getApproval_date() {
        return approval_date;
    }

    public void setApproval_date(String approval_date) {
        this.approval_date = approval_date;
    }

    public String getEffective_date() {
        return effective_date;
    }

    public void setEffective_date(String effective_date) {
        this.effective_date = effective_date;
    }

    public String getInvalid_date() {
        return invalid_date;
    }

    public void setInvalid_date(String invalid_date) {
        this.invalid_date = invalid_date;
    }

    public String getAudit_flag() {
        return audit_flag;
    }

    public void setAudit_flag(String audit_flag) {
        this.audit_flag = audit_flag;
    }

    public String getMeeting_type_code() {
        return meeting_type_code;
    }

    public void setMeeting_type_code(String meeting_type_code) {
        this.meeting_type_code = meeting_type_code;
    }

    public List<VoteModeVO> getVote_mode_list() {
        return vote_mode_list;
    }

    public void setVote_mode_list(List<VoteModeVO> vote_mode_list) {
        this.vote_mode_list = vote_mode_list;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOper_type() {
        return oper_type;
    }

    public void setOper_type(String oper_type) {
        this.oper_type = oper_type;
    }

    @Override
    public String toString() {
        return "DecisionMakingSystemVO{" +
                "regulation_id='" + regulation_id + '\'' +
                ", company_name='" + company_name + '\'' +
                ", company_id='" + company_id + '\'' +
                ", regulation_name='" + regulation_name + '\'' +
                ", regulation_type_name='" + regulation_type_name + '\'' +
                ", approval_date='" + approval_date + '\'' +
                ", effective_date='" + effective_date + '\'' +
                ", invalid_date='" + invalid_date + '\'' +
                ", audit_flag='" + audit_flag + '\'' +
                ", meeting_type_code='" + meeting_type_code + '\'' +
                ", vote_mode_list=" + vote_mode_list +
                ", remark='" + remark + '\'' +
                ", source='" + source + '\'' +
                ", oper_type='" + oper_type + '\'' +
                '}';
    }


    public static void main(String[] args) {
        /**
         * 4. <!--制度标识-->    
         * 5.  <regulation_id>190dd45b018747da82268974577ff1f5</regulation_id>    
         * 6.  <!--企业名称-->    
         * 7.  <company_name>某集团有限公司</company_name>    
         * 8.  <!--企业编码-->    
         * 9.  <company_id>9111XXXXXX563N</company_id>    
         * 10.  <!--制度名称-->    
         * 11.  <regulation_name>XX集团工作规则</regulation_name>    
         * 12.  <!--制度类型名称-->    
         * 13.  <regulation_type_name>议事规则</regulation_type_name>    
         * 14.  <!--审批日期-->    
         * 15.  <approval_date>2018-10-10</approval_date>    
         * 16.  <!--生效日期-->    
         * 17.  <effective_date>2018-10-10</effective_date>    
         * 18.  <!--失效日期-->    
         * 19.  <invalid_date>2018-12-31</invalid_date>    
         * 20.  <!--是否经过合法审查-->    
         * 21.  <audit_flag>是</audit_flag>    
         * 22.  <!--会议类型编码-->    
         * 23.  <meeting_type_code>JL</meeting_type_code>    
         * 24.  <!--表决方式集合-->    
         * 25.  <vote_mode_list>   
         * 26.    <vote_mode>   
         * 27.      <!--事项编码-->    
         * 28.      <item_code>默认</item_code>    
         * 29.      <!--人数占比-->    
         * 30.      <rate>1/2</rate>    
         * 31.      <!--表决方式-->    
         * 32.      <vote_mode>赞成票超过到会成员人数的1/2</vote_mode>   
         * 33.    </vote_mode>    
         * 34.    <vote_mode>   
         * 35.      <item_code>H06-001</item_code>    
         * 36.      <rate>1</rate>    
         * 37.      <vote_mode>赞成票超过到会成员人数的3/4</vote_mode>   
         * 38.    </vote_mode>    
         * 39.    <vote_mode>   
         * 40.      <item_code>P08-005</item_code>    
         * 41.      <rate>2/3</rate>    
         * 42.      <vote_mode>赞成票超过应到会成员人数的2/3</vote_mode>   
         * 43.    </vote_mode>   
         * 44.  </vote_mode_list>    
         * 45.  <!--备注-->    
         * 46.  <remark/>    
         * 47.  <!--数据来源-->    
         * 48.  <source>系统</source>    
         * 49.  <!--操作标识（add表示新增，edit表示修改，del表示删除）-->    
         * 50.  <oper_type>add</oper_type>   
         * </tiol>
         */
        DecisionMakingSystemBO decisionMakingSystemVO =  new DecisionMakingSystemBO();
        decisionMakingSystemVO.setRegulation_id("190dd45b018747da82268974577ff1f5");
        decisionMakingSystemVO.setCompany_name("某集团有限公司");
        decisionMakingSystemVO.setCompany_id("9111XXXXXX563N");
        decisionMakingSystemVO.setRegulation_name("XX集团工作规则");
        decisionMakingSystemVO.setRegulation_type_name("议事规则");
        decisionMakingSystemVO.setApproval_date("2018-10-10");
        decisionMakingSystemVO.setEffective_date("2018-10-10");
        decisionMakingSystemVO.setInvalid_date("2018-12-31");
        decisionMakingSystemVO.setAudit_flag("是");
        decisionMakingSystemVO.setMeeting_type_code("JL");
        List<VoteModeVO> voteModeVOList = new ArrayList<VoteModeVO>();
        VoteModeVO voteModeVO1 = new VoteModeVO();
        voteModeVO1.setItem_code("默认");
        voteModeVO1.setRate("1/2");
        voteModeVO1.setVote_mode("赞成票超过到会成员人数的1/2");
        VoteModeVO voteModeVO2 = new VoteModeVO();
        voteModeVO2.setItem_code("H06-001");
        voteModeVO2.setRate("1");
        voteModeVO2.setVote_mode("赞成票超过到会成员人数的3/4");
        voteModeVOList.add(voteModeVO1);
        voteModeVOList.add(voteModeVO2);
        decisionMakingSystemVO.setVote_mode_list(voteModeVOList);
        decisionMakingSystemVO.setRemark("备注");
        decisionMakingSystemVO.setSource("系统");
        decisionMakingSystemVO.setOper_type("add");
        String str = JaxbUtil.converTomXml(decisionMakingSystemVO);
        System.out.println(str);
    }
}
