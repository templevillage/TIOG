package com.kingdee.eas.custom.tiog.middleware.bo.basicdata;

import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 *
 * Title: CatalogList Description:事项目录
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-23       
 */
@XmlRootElement(name = "tiol")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"catalog_list","catalog_name","catalog_code","catalog_level","order_number"})
public class CatalogList {
    @XmlElementWrapper(name = "catalog_list")
    @XmlElement(name = "catalog")
    private List<CatalogList> catalog_list;
    //事项目录名称
    @XmlElement(name = "catalog_name")
    private String  catalog_name;
    //事项目录编码
    @XmlElement(name = "catalog_code")
    private String  catalog_code;
    //事项目录级别
    @XmlElement(name = "catalog_level")
    private String  catalog_level;
    //排序号
    @XmlElement(name = "order_number")
    private String  order_number;

    public List<CatalogList> getCatalog_list() {
        return catalog_list;
    }

    public void setCatalog_list(List<CatalogList> catalog_list) {
        this.catalog_list = catalog_list;
    }

    public String getCatalog_name() {
        return catalog_name;
    }

    public void setCatalog_name(String catalog_name) {
        this.catalog_name = catalog_name;
    }

    public String getCatalog_code() {
        return catalog_code;
    }

    public void setCatalog_code(String catalog_code) {
        this.catalog_code = catalog_code;
    }

    public String getCatalog_level() {
        return catalog_level;
    }

    public void setCatalog_level(String catalog_level) {
        this.catalog_level = catalog_level;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    @Override
    public String toString() {
        return "CatalogList{" +
                "catalog_list=" + catalog_list +
                ", catalog_name='" + catalog_name + '\'' +
                ", catalog_code='" + catalog_code + '\'' +
                ", catalog_level='" + catalog_level + '\'' +
                ", order_number='" + order_number + '\'' +
                '}';
    }

    public static void main(String[] args) {
        /*//等级1
        CatalogList catalogList1 = new CatalogList();
        catalogList1.setCatalog_name("重大决策事项");
        catalogList1.setCatalog_code("");
        catalogList1.setCatalog_level("1");
        catalogList1.setOrder_number("1");
        //等级2
        CatalogList catalogList2 = new CatalogList();
        catalogList2.setCatalog_name("贯彻执行党和国家决策部署的重大措施");
        catalogList2.setCatalog_code("");
        catalogList2.setCatalog_level("2");
        catalogList2.setOrder_number("1");
        List<CatalogList> catalogListList2 = new ArrayList<>();
        catalogListList2.add(catalogList2);
        //等级3
        CatalogList catalogList3_1 = new CatalogList();
        catalogList3_1.setCatalog_name("路线方针政策");
        catalogList3_1.setCatalog_code("D01");
        catalogList3_1.setCatalog_level("3");
        catalogList3_1.setOrder_number("1");
        CatalogList catalogList3_2 = new CatalogList();
        catalogList3_2.setCatalog_name("法律法规");
        catalogList3_2.setCatalog_code("D02");
        catalogList3_2.setCatalog_level("3");
        catalogList3_2.setOrder_number("2");
        CatalogList catalogList3_3 = new CatalogList();
        catalogList3_3.setCatalog_name("法律法规");
        catalogList3_3.setCatalog_code("D03");
        catalogList3_3.setCatalog_level("3");
        catalogList3_3.setOrder_number("3");
        List<CatalogList> catalogVOList3 = new ArrayList<>();
        catalogVOList3.add(catalogList3_1);
        catalogVOList3.add(catalogList3_2);
        catalogVOList3.add(catalogList3_3);
        catalogList1.setCatalog_list(catalogListList2);
        catalogList2.setCatalog_list(catalogVOList3);
        CatalogList catalogList = new CatalogList();
        List<CatalogList> catalogListList =  new ArrayList<>();
        catalogListList.add(catalogList1);
        catalogList.setCatalog_list(catalogListList);
        String str = JaxbUtil.converTomXml(catalogList);
        System.out.println(str);*/
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"+
                "<tiol>\n" +
                "  <catalog_list>   \n" +
                "    <catalog>   \n" +
                "      <!--事项目录名称-->    \n" +
                "      <catalog_name>重大决策事项</catalog_name>    \n" +
                "      <!--事项目录编码-->    \n" +
                "      <catalog_code/>    \n" +
                "      <!--事项目录级别-->    \n" +
                "      <catalog_level>1</catalog_level>    \n" +
                "      <!--排序号-->    \n" +
                "      <order_number>1</order_number>    \n" +
                "      <catalog_list>   \n" +
                "        <catalog>   \n" +
                "          <!--事项目录名称-->    \n" +
                "          <catalog_name>贯彻执行党和国家决策部署的重大措施</catalog_name>    \n" +
                "          <!--事项目录编码-->    \n" +
                "          <catalog_code/>    \n" +
                "          <!--事项目录级别-->    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <!--排序号-->    \n" +
                "          <order_number>1</order_number>    \n" +
                "          <catalog_list>   \n" +
                "            <catalog>   \n" +
                "              <catalog_name>路线方针政策</catalog_name>    \n" +
                "              <catalog_code>D01</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>1</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>法律法规</catalog_name>    \n" +
                "              <catalog_code>D02</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>2</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>法律法规</catalog_name>    \n" +
                "              <catalog_code>D03</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>3</order_number>   \n" +
                "            </catalog>   \n" +
                "          </catalog_list>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>运营管理重大决策</catalog_name>    \n" +
                "          <catalog_code/>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>2</order_number>    \n" +
                "          <catalog_list>   \n" +
                "            <catalog>   \n" +
                "              <catalog_name>发展战略</catalog_name>    \n" +
                "              <catalog_code>D04</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>1</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>企业破产</catalog_name>    \n" +
                "              <catalog_code>D05</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>2</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>企业改制</catalog_name>    \n" +
                "              <catalog_code>D06</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>3</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>兼并重组</catalog_name>    \n" +
                "              <catalog_code>D07</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>4</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>资产调整</catalog_name>    \n" +
                "              <catalog_code>D08</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>5</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>产权转让</catalog_name>    \n" +
                "              <catalog_code>D09</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>6</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>对外投资</catalog_name>    \n" +
                "              <catalog_code>D10</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>7</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>利益调配</catalog_name>    \n" +
                "              <catalog_code>D11</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>8</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>机构调整</catalog_name>    \n" +
                "              <catalog_code>D12</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>9</order_number>   \n" +
                "            </catalog>   \n" +
                "          </catalog_list>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>党的建设重大决策</catalog_name>    \n" +
                "          <catalog_code>D13</catalog_code>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>3</order_number>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>安全稳定的重大决策</catalog_name>    \n" +
                "          <catalog_code>D14</catalog_code>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>4</order_number>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>其他重大决策</catalog_name>    \n" +
                "          <catalog_code>D15</catalog_code>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>5</order_number>   \n" +
                "        </catalog>   \n" +
                "      </catalog_list>   \n" +
                "    </catalog>    \n" +
                "    <catalog>   \n" +
                "      <catalog_name>重要人事任免事项</catalog_name>    \n" +
                "      <catalog_code/>    \n" +
                "      <catalog_level>1</catalog_level>    \n" +
                "      <order_number>2</order_number>    \n" +
                "      <catalog_list>   \n" +
                "        <catalog>   \n" +
                "          <catalog_name>中层以上经营管理人员管理</catalog_name>    \n" +
                "          <catalog_code/>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>1</order_number>    \n" +
                "          <catalog_list>   \n" +
                "            <catalog>   \n" +
                "              <catalog_name>人员任命</catalog_name>    \n" +
                "              <catalog_code>H01</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>1</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>人员免职</catalog_name>    \n" +
                "              <catalog_code>H02</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>2</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>人员聘用</catalog_name>    \n" +
                "              <catalog_code>H03</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>3</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>人员解聘</catalog_name>    \n" +
                "              <catalog_code>H04</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>4</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>后备人选</catalog_name>    \n" +
                "              <catalog_code>H05</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>5</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>接受纪律审查监察调查情况</catalog_name>    \n" +
                "              <catalog_code>H06</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>6</order_number>   \n" +
                "            </catalog>   \n" +
                "          </catalog_list>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>下属企业和单位人员管理</catalog_name>    \n" +
                "          <catalog_code/>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>2</order_number>    \n" +
                "          <catalog_list>   \n" +
                "            <catalog>   \n" +
                "              <catalog_name>领导班子成员任命</catalog_name>    \n" +
                "              <catalog_code>H07</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>1</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>领导班子成员免职</catalog_name>    \n" +
                "              <catalog_code>H08</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>2</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>领导班子成员聘用</catalog_name>    \n" +
                "              <catalog_code>H09</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>3</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>领导班子成员解聘</catalog_name>    \n" +
                "              <catalog_code>H10</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>4</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>领导班子成员后备人选</catalog_name>    \n" +
                "              <catalog_code>H11</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>5</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>接受纪律审查监察调查情况</catalog_name>    \n" +
                "              <catalog_code>H12</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>6</order_number>   \n" +
                "            </catalog>   \n" +
                "          </catalog_list>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>控股和参股企业人员管理</catalog_name>    \n" +
                "          <catalog_code/>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>3</order_number>    \n" +
                "          <catalog_list>   \n" +
                "            <catalog>   \n" +
                "              <catalog_name>委派股东代表</catalog_name>    \n" +
                "              <catalog_code>H13</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>1</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>推荐董事会成员</catalog_name>    \n" +
                "              <catalog_code>H14</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>2</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>监事会成员推荐</catalog_name>    \n" +
                "              <catalog_code>H15</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>3</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>推荐经理</catalog_name>    \n" +
                "              <catalog_code>H16</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>4</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>推荐财务负责人</catalog_name>    \n" +
                "              <catalog_code>H17</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>5</order_number>   \n" +
                "            </catalog>   \n" +
                "          </catalog_list>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>其他重要人事任免</catalog_name>    \n" +
                "          <catalog_code>H18</catalog_code>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>4</order_number>   \n" +
                "        </catalog>   \n" +
                "      </catalog_list>   \n" +
                "    </catalog>    \n" +
                "    <catalog>   \n" +
                "      <catalog_name>重大项目安排事项</catalog_name>    \n" +
                "      <catalog_code/>    \n" +
                "      <catalog_level>1</catalog_level>    \n" +
                "      <order_number>3</order_number>    \n" +
                "      <catalog_list>   \n" +
                "        <catalog>   \n" +
                "          <catalog_name>年度投资计划</catalog_name>    \n" +
                "          <catalog_code>P01</catalog_code>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>1</order_number>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>融资项目</catalog_name>    \n" +
                "          <catalog_code>P02</catalog_code>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>2</order_number>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>担保项目</catalog_name>    \n" +
                "          <catalog_code>P03</catalog_code>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>3</order_number>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>金融衍生业务</catalog_name>    \n" +
                "          <catalog_code/>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>4</order_number>    \n" +
                "          <catalog_list>   \n" +
                "            <catalog>   \n" +
                "              <catalog_name>期权</catalog_name>    \n" +
                "              <catalog_code>P04</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>1</order_number>   \n" +
                "            </catalog>    \n" +
                "            <catalog>   \n" +
                "              <catalog_name>期货</catalog_name>    \n" +
                "              <catalog_code>P05</catalog_code>    \n" +
                "              <catalog_level>3</catalog_level>    \n" +
                "              <order_number>2</order_number>   \n" +
                "            </catalog>   \n" +
                "          </catalog_list>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>重要设备和技术引进</catalog_name>    \n" +
                "          <catalog_code>P06</catalog_code>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>5</order_number>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>采购大宗物资和购买服务</catalog_name>    \n" +
                "          <catalog_code>P07</catalog_code>    \n" +
                "          <catalog_level>2</catalog_level>    \n" +
                "          <order_number>6</order_number>   \n" +
                "        </catalog>    \n" +
                "        <catalog>   \n" +
                "          <catalog_name>重大工程建设项目</catalog_name>    \n" +
                "          <catalog_code>P08</catalog_code>    \n" +
                "          <catalog_level>2</catalog_level>\n" +
                "          <order_number>7</order_number>\n" +
                "        </catalog>\n" +
                "        <catalog>\n" +
                "          <catalog_name>其他重大项目安排</catalog_name>\n" +
                "          <catalog_code>P09</catalog_code>\n" +
                "          <catalog_level>2</catalog_level>\n" +
                "          <order_number>8</order_number>\n" +
                "        </catalog>\n" +
                "      </catalog_list>\n" +
                "    </catalog>\n" +
                "    <catalog>\n" +
                "      <catalog_name>大额度资金运作事项</catalog_name>\n" +
                "      <catalog_code/>\n" +
                "      <catalog_level>1</catalog_level>\n" +
                "      <order_number>3</order_number>\n" +
                "      <catalog_list>\n" +
                "        <catalog>\n" +
                "          <catalog_name>年度预算内大额度资金调动和使用</catalog_name>\n" +
                "          <catalog_code>F01</catalog_code>\n" +
                "          <catalog_level>2</catalog_level>\n" +
                "          <order_number>1</order_number>\n" +
                "        </catalog>\n" +
                "        <catalog>\n" +
                "          <catalog_name>超预算的资金调动和使用</catalog_name>\n" +
                "          <catalog_code>F02</catalog_code>\n" +
                "          <catalog_level>2</catalog_level>\n" +
                "          <order_number>2</order_number>\n" +
                "        </catalog>\n" +
                "        <catalog>\n" +
                "          <catalog_name>对外大额捐赠、赞助</catalog_name>\n" +
                "          <catalog_code>F03</catalog_code>\n" +
                "          <catalog_level>2</catalog_level>\n" +
                "          <order_number>3</order_number>\n" +
                "        </catalog>\n" +
                "        <catalog>\n" +
                "          <catalog_name>其他大额度资金运作</catalog_name>\n" +
                "          <catalog_code>F04</catalog_code>\n" +
                "          <catalog_level>2</catalog_level>\n" +
                "          <order_number>4</order_number>\n" +
                "        </catalog>\n" +
                "      </catalog_list>\n" +
                "    </catalog>\n" +
                "  </catalog_list>\n" +
                "</tiol>";
        CatalogList catalogList = JaxbUtil.converyToJavaBean(str.trim(),CatalogList.class);
        System.out.println(catalogList.toString());
    }
}
