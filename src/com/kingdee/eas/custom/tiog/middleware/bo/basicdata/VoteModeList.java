package com.kingdee.eas.custom.tiog.middleware.bo.basicdata;

import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;
import com.kingdee.eas.custom.tiog.middleware.vo.basicdata.VoteModeVO;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 *
 * Title: VoteModeList Description:表决方式
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-23       
 */
@XmlRootElement(name = "tiol")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"vote_mode_list"})
public class VoteModeList {
    @XmlElementWrapper(name = "vote_mode_list")
    @XmlElement(name = "vote_mode")
    private List<VoteModeVO> vote_mode_list;

    public List getVote_mode_list() {
        return vote_mode_list;
    }

    public void setVote_mode_list(List vote_mode_list) {
        this.vote_mode_list = vote_mode_list;
    }

    @Override
    public String toString() {
        return "VoteModeList{" +
                "vote_mode_list=" + vote_mode_list +
                '}';
    }

    public static void main(String[] args) {
       /* VoteModeList voteModeList = new VoteModeList();
        List<VoteModeVO> voteModeVOList = new ArrayList<>();
        VoteModeVO voteModeVO1 = new VoteModeVO();
        voteModeVO1.setMode_name("全票通过");
        voteModeVO1.setMode_radix("");
        voteModeVO1.setMode_radix("1");
        VoteModeVO voteModeVO2 = new VoteModeVO();
        voteModeVO2.setMode_name("赞成票超过应到会成员人数的4/5");
        voteModeVO2.setMode_radix("应到会人数");
        voteModeVO2.setMode_radix("4/5");
        VoteModeVO voteModeVO3 = new VoteModeVO();
        voteModeVO3.setMode_name("赞成票超过应到会成员人数的3/4");
        voteModeVO3.setMode_radix("应到会人数");
        voteModeVO3.setMode_radix("3/4");
        VoteModeVO voteModeVO4 = new VoteModeVO();
        voteModeVO4.setMode_name("赞成票超过到会成员人数的2/3");
        voteModeVO4.setMode_radix("到会人数");
        voteModeVO4.setMode_radix("2/3");
        voteModeVOList.add(voteModeVO1);
        voteModeVOList.add(voteModeVO2);
        voteModeVOList.add(voteModeVO3);
        voteModeVOList.add(voteModeVO4);
        voteModeList.setVote_mode_list(voteModeVOList);
        String str = JaxbUtil.converTomXml(voteModeList);
        System.out.println(str);*/
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<tiol>\n" +
                "    <vote_mode_list>\n" +
                "        <vote_mode>\n" +
                "            <mode_name>全票通过</mode_name>\n" +
                "            <mode_radix>1</mode_radix>\n" +
                "        </vote_mode>\n" +
                "        <vote_mode>\n" +
                "            <mode_name>赞成票超过应到会成员人数的4/5</mode_name>\n" +
                "            <mode_radix>4/5</mode_radix>\n" +
                "        </vote_mode>\n" +
                "        <vote_mode>\n" +
                "            <mode_name>赞成票超过应到会成员人数的3/4</mode_name>\n" +
                "            <mode_radix>3/4</mode_radix>\n" +
                "        </vote_mode>\n" +
                "        <vote_mode>\n" +
                "            <mode_name>赞成票超过到会成员人数的2/3</mode_name>\n" +
                "            <mode_radix>2/3</mode_radix>\n" +
                "        </vote_mode>\n" +
                "    </vote_mode_list>\n" +
                "</tiol>\n";
        VoteModeList voteModeList = JaxbUtil.converyToJavaBean(str,VoteModeList.class);
        System.out.println(voteModeList);
    }
}
