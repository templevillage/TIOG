package com.kingdee.eas.custom.tiog.middleware.bo.basicdata;

import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;
import com.kingdee.eas.custom.tiog.middleware.vo.basicdata.MeetingFormVO;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 *
 * Title: MeetingFormList Description:会议形式
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 */
@XmlRootElement(name = "tiol")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"meeting_form_list"})
public class MeetingFormList {
    //会议形式列表
    @XmlElementWrapper(name = "meeting_form_list")
    @XmlElement(name = "meeting_form")
    private List<MeetingFormVO> meeting_form_list;

    public List getMeeting_form_list() {
        return meeting_form_list;
    }

    public void setMeeting_form_list(List meeting_form_list) {
        this.meeting_form_list = meeting_form_list;
    }

    @Override
    public String toString() {
        return "MeetingFormList{" +
                "meeting_form_list=" + meeting_form_list +
                '}';
    }


    public static void main(String[] args) {
       /* MeetingFormList meetingFormList1 = new MeetingFormList();
        List<MeetingFormVO> meetingFormVOList = new ArrayList<>();
        MeetingFormVO meetingFormVO1 = new MeetingFormVO();
        meetingFormVO1.setForm_name("现场会议");
        meetingFormVO1.setForm_code("01");
        MeetingFormVO meetingFormVO2 = new MeetingFormVO();
        meetingFormVO2.setForm_name("传签会议");
        meetingFormVO2.setForm_code("02");
        MeetingFormVO meetingFormVO3 = new MeetingFormVO();
        meetingFormVO3.setForm_name("电视电话会议");
        meetingFormVO3.setForm_code("03");
        MeetingFormVO meetingFormVO4 = new MeetingFormVO();
        meetingFormVO3.setForm_name("学习会议");
        meetingFormVO3.setForm_code("04");
        meetingFormVOList.add(meetingFormVO1);
        meetingFormVOList.add(meetingFormVO2);
        meetingFormVOList.add(meetingFormVO3);
        meetingFormVOList.add(meetingFormVO4);
        meetingFormList1.setMeeting_form_list(meetingFormVOList);
        //String str = JaxbUtil.converTomXml(meetingFormList1);
        System.out.println(str);*/
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<tiol>\n" +
                "    <meeting_form_list>\n" +
                "        <meeting_form>\n" +
                "            <form_name>现场会议</form_name>\n" +
                "            <form_code>01</form_code>\n" +
                "        </meeting_form>\n" +
                "        <meeting_form>\n" +
                "            <form_name>传签会议</form_name>\n" +
                "            <form_code>02</form_code>\n" +
                "        </meeting_form>\n" +
                "        <meeting_form>\n" +
                "            <form_name>学习会议</form_name>\n" +
                "            <form_code>04</form_code>\n" +
                "        </meeting_form>\n" +
                "        <meeting_form/>\n" +
                "    </meeting_form_list>\n" +
                "</tiol>";

        MeetingFormList meetingFormList = JaxbUtil.converyToJavaBean(str, MeetingFormList.class);
        System.out.println(meetingFormList);
    }
}
