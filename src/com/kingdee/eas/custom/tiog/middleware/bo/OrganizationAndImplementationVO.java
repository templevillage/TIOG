package com.kingdee.eas.custom.tiog.middleware.bo;

import com.kingdee.eas.custom.tiog.middleware.util.JaxbUtil;
import com.kingdee.eas.custom.tiog.middleware.vo.DutyVO;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Title: OrganizationAndImplementationVO Description:组织实施
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "tiol")
@XmlType(propOrder = {"execution_id","company_name","company_id","subject_code","duty_list","effect","status","description","remark","source","oper_type"})
public class OrganizationAndImplementationVO {
    //组织实施标识
    @XmlElement(name = "execution_id")
    private String execution_id;
    //企业名称
    @XmlElement(name = "company_name")
    private String company_name;
    //企业编码
    @XmlElement(name = "company_id")
    private String company_id;
    //议题编码
    @XmlElement(name = "subject_code")
    private String subject_code;
    //落实责任信息
    @XmlElementWrapper(name = "duty_list")
    @XmlElement(name = "duty")
    private List<DutyVO> duty_list;
    //预期成效
    @XmlElement(name = "effect")
    private String effect;
    //实施状态
    @XmlElement(name = "status")
    private String status;
    //实施情况\
    @XmlElement(name = "description")
    private String description;
    //备注
    @XmlElement(name = "remark")
    private String remark;
    //数据来源
    @XmlElement(name = "source")
    private String source;
    //操作标识（add表示新增，edit表示修改，del表示删除
    @XmlElement(name = "oper_type")
    private String oper_type;

    public String getExecution_id() {
        return execution_id;
    }

    public void setExecution_id(String execution_id) {
        this.execution_id = execution_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public List getDuty_list() {
        return duty_list;
    }

    public void setDuty_list(List duty_list) {
        this.duty_list = duty_list;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOper_type() {
        return oper_type;
    }

    public void setOper_type(String oper_type) {
        this.oper_type = oper_type;
    }

    @Override
    public String toString() {
        return "OrganizationAndImplementationVO{" +
                "execution_id='" + execution_id + '\'' +
                ", company_name='" + company_name + '\'' +
                ", company_id='" + company_id + '\'' +
                ", subject_code='" + subject_code + '\'' +
                ", duty_list=" + duty_list +
                ", effect='" + effect + '\'' +
                ", status='" + status + '\'' +
                ", description='" + description + '\'' +
                ", remark='" + remark + '\'' +
                ", source='" + source + '\'' +
                ", oper_type='" + oper_type + '\'' +
                '}';
    }


    public static void main(String[] args) {
        OrganizationAndImplementationVO organizationAndImplementationVO = new OrganizationAndImplementationVO();
        organizationAndImplementationVO.setExecution_id("0kmjvat2tvrn5itjva1ihlhuzwu6uo0t");
        organizationAndImplementationVO.setCompany_name("某集团有限公司");
        organizationAndImplementationVO.setCompany_id("9111XXXXXX563N");
        organizationAndImplementationVO.setSubject_code("JL20180010-001");
        List<DutyVO> dutyVOList = new ArrayList<DutyVO>();
        DutyVO dutyVO01 = new DutyVO();
        dutyVO01.setDept("财务部");
        dutyVO01.setName("张某某");
        DutyVO dutyVO02 = new DutyVO();
        dutyVO02.setDept("规划部");
        dutyVO02.setName("王某某");
        dutyVOList.add(dutyVO01);
        dutyVOList.add(dutyVO02);
        organizationAndImplementationVO.setDuty_list(dutyVOList);
        organizationAndImplementationVO.setEffect("XX项目正式实施并投入运营后，可以产生某效益。");
        organizationAndImplementationVO.setStatus("完成");
        organizationAndImplementationVO.setDescription("该决策议题的时间进度以及具体组织实施情况");
        organizationAndImplementationVO.setSource("系统");
        organizationAndImplementationVO.setOper_type("add");
        String str = JaxbUtil.converTomXml(organizationAndImplementationVO);
        System.out.println(str);
    }
}
