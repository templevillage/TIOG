package com.kingdee.eas.custom.tiog.middleware.common;
/**
 *
 * Title: FileType Description: 附件类型
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-23 
 */
public enum FileTypeEnum {
    //正式文件
    FORMAFILE("0","正式文件",""),
    //佐证材料
    PROOFDATA("1","佐证材料",""),
    //听取意见
    LISTENOPINION("2","会议议题","听取意见"),
    //议题材料
    ISSUEDATA("3","会议议题","议题材料"),
    //会议纪要
    MEETINGSUMMARY("4","会议纪要",""),
    //会议通知
    MEETINGNOTICE("5","会议通知",""),
    //时间进度
    TIMESPEED("6","时间进度","");
    private String code;
    private String value1;
    private String value2;
    private FileTypeEnum(String code, String value1, String value2){
        this.code = code;
        this.value1 = value1;
        this.value2 = value2;
    }

    public String getCode() {
        return code;
    }
    public String getValue1() {
        return value1;
    }
    public String getValue2() {
        return value2;
    }


}
