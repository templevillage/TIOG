package com.kingdee.eas.custom.tiog.middleware.service;


import com.kingdee.bos.Context;
import com.kingdee.eas.custom.tiog.middleware.common.FileTypeEnum;
import com.kingdee.eas.custom.tiog.middleware.util.FileUtil;
import com.kingdee.eas.custom.tiog.middleware.util.ZipUtil;
import com.kingdee.eas.custom.tiog.middleware.vo.basicdata.FileMessageVO;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Title: UploadService Description:三重一大上传服务类
 *
 * @author spj Email:tjsongpeijun@kingdee.com
 * @date 2020-03-19       
 * 包含决策制度，事项清单，决策会议，组织实施
 */
public class UploadService {
    /**
     * Title: uploadDataAssembler
     * Description:上传资料组装器
     * @param xml 转换好的xml
     * @param fileList 上传的附件集合
     * @param date 上传的日期，不一定是当前日期，格式（yyyyMMdd）
     * @param type 上传的信息类型{集团总部决策制度:0005,集团总部事项清单:0004,集团总部决策会议:0006,集团总部组织实施:0012}
     * @return zip包存放位置
     */
    public static String uploadDataAssembler(String xml, List<FileMessageVO> fileList,String date,String type){
        /**
         *  1.创建上传资料文件夹，2.将xml固化到文件夹中，3，将附件固化到文件夹中，4，将文件夹压缩成zip
         */
        System.out.println(type+"上传资料组装器，组装开始.入参：xml："+xml+" fileList:"+fileList.size()+" date:"+date);
        //创建文件夹，怕有重复文件夹，利用随机数来创建文件夹
        File file = createMkdir();
        String path = file.toString();
        //创建xml文件并将xml写入文件夹中
        String xmlname = createXml(path,xml,date,type);
        //开始创建附件
        createFileMessage(fileList,path);
        //将文件夹打包成压缩包
        String zippath =  createZip(path,xmlname);
        // 删除文件夹
        FileUtil.deleteDir(file);
        System.out.println(type+"上传资料组装器,组装完成，zip包存放地址："+zippath);
        return zippath;
    }
    /**
     * Title: uploadDataAssembler
     * Description:上传资料组装器
     * @param xml 转换好的xml
     * @param fileList 上传的附件集合
     * @param date 上传的日期，不一定是当前日期，格式（yyyyMMdd）
     * @param type 上传的信息类型{集团总部决策制度:0005,集团总部事项清单:0004,集团总部决策会议:0006,集团总部组织实施:0012}
     * @param ctx 是否在服务端运行}
     * @return zip包存放位置
     */
    public static String uploadDataAssembler(String xml, List<FileMessageVO> fileList,String date,String type,Context ctx){
        /**
         * TODO 1.创建上传资料文件夹，2.将xml固化到文件夹中，3，将附件固化到文件夹中，4，将文件夹压缩成zip
         */
        System.out.println(type+"上传资料组装器，组装开始.入参：xml："+xml+" fileList:"+fileList.size()+" date:"+date);
        //创建文件夹，怕有重复文件夹，利用随机数来创建文件夹
        File file = createMkdir();
        String path = file.toString();
        //创建xml文件并将xml写入文件夹中
        String xmlname = createXml(path,xml,date,type);
        //开始创建附件
        createFileMessage(fileList,path);
        //将文件夹打包成压缩包
        String zippath =  createZip(path,xmlname);
        // 删除文件夹
        FileUtil.deleteDir(file);
        System.out.println(type+"上传资料组装器,组装完成，zip包存放地址："+zippath);
        return zippath;
    }
    /**
     * Title: createMkdir
     * Description:创建文件夹
     * @return 文件夹名称
     */
    public static File createMkdir(){
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath()+"temp"+(int)(Math.random()*100);
        File file = new File(path);
        System.out.println("创建上传资料文件夹，path："+path);
        if (!file.exists()){
            file.mkdir();
        }
        return file;
    }
    /**
     * Title: createXml
     * Description:创建xml
     * @param path :存放路径
     * @param xml :xml信息
     * @param date:上传时间
     * @param type : 接口类型
     * @return xml文件名
     */
    public static String createXml(String path,String xml,String date,String type){
        String xmlname = type+"_"+1000+"_"+date;
        System.out.println("创建xml文件，xmlname："+xmlname);
        File file1 = new File(path+"/"+xmlname+".xml");
        if (!file1.exists()){
            try {
                boolean newFile = file1.createNewFile();
                FileWriter fw = new FileWriter(file1);
                // 把数据写入到输出流
                fw.write(xml);
                fw.flush();
                // 关闭输出流
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return xmlname;
    }


    /**
     * Title: createFileMessage
     * Description:创建附件信息
     * @param path :存放路径
     * @param fileList 附件列表
     */
    public static void createFileMessage(List<FileMessageVO> fileList,String path){
        for(FileMessageVO fileMessageVO:fileList){
            String name = fileMessageVO.getName();
            String type = fileMessageVO.getType();
            String filetype = fileMessageVO.getFiletype().name();
            //获取附件文件夹名称
            String filetypepath = FileTypeEnum.valueOf(filetype).getValue1();
            //子文件夹名称
            String filetypepath1 = FileTypeEnum.valueOf(filetype).getValue2();
            //创建文件夹
            File file2 = new File(path+"/"+filetypepath+"/"+filetypepath1);
            if (!file2.exists()){
                file2.mkdir();
            }
            //将附件存到文件夹中
            FileUtil.byte2File(fileMessageVO.getFiles(),file2.getPath(),name+"."+type);
            System.out.println("创建附件，附件地址："+file2.getPath()+"附件名称:"+name);
        }
    }
    /**
     * Title: createZip
     * Description:创建zip
     * @param path :存放路径
     * @param xmlname xml名称
     * @return zip路径
     */
    public static String createZip(String path,String xmlname){
        File file = new File(Thread.currentThread().getContextClassLoader().getResource("").getPath()+"/"+xmlname+"_"+((int)(Math.random()*100))+".zip");
        FileOutputStream fos1 = null;
        try {
            fos1=new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ZipUtil.toZip(path.toString(),fos1,true);
        System.out.println("创建压缩包，压缩包存放路径："+file.toString());
        return file.toString();
    }

    public static void main(String[] args) {
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<tiol>\n" +
                "    <meeting_form_list>\n" +
                "        <meeting_form>\n" +
                "            <form_name>现场会议</form_name>\n" +
                "            <form_code>01</form_code>\n" +
                "        </meeting_form>\n" +
                "        <meeting_form>\n" +
                "            <form_name>传签会议</form_name>\n" +
                "            <form_code>02</form_code>\n" +
                "        </meeting_form>\n" +
                "        <meeting_form>\n" +
                "            <form_name>学习会议</form_name>\n" +
                "            <form_code>04</form_code>\n" +
                "        </meeting_form>\n" +
                "        <meeting_form/>\n" +
                "    </meeting_form_list>\n" +
                "</tiol>";
        List<FileMessageVO> fileMessageVOList = new ArrayList<FileMessageVO>();
        FileMessageVO fileMessageVO = new FileMessageVO();
        fileMessageVO.setId("0932849032809483024");
        fileMessageVO.setName("测试pdf");
        fileMessageVO.setType("pdf");
        fileMessageVO.setFiletype(FileTypeEnum.FORMAFILE);
        byte[] bytes = FileUtil.file2Byte("D:/aaa.pdf");
        fileMessageVO.setFiles(bytes);
        fileMessageVOList.add(fileMessageVO);
        uploadDataAssembler(str,fileMessageVOList,"20200324","0006");
    }
}
