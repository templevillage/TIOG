package com.kingdee.eas.custom.tiog.middleware.service;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.base.attachment.util.Sout;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.CatalogList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.MeetingFormList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.MeetingTypeList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.RegulationTypeList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.SourceList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.SpecialList;
import com.kingdee.eas.custom.tiog.middleware.bo.basicdata.VoteModeList;
import com.kingdee.eas.custom.tiog.service.log.LogmessageEntryFactory;
import com.kingdee.eas.custom.tiog.service.log.LogmessageEntryInfo;
import com.kingdee.eas.custom.tiog.service.log.LogmessageFactory;
import com.kingdee.eas.custom.tiog.service.log.LogmessageInfo;
import com.kingdee.eas.custom.tiog.utils.DateUtil;

/**
 * 
 * Title: LogService Description: 日志系统
 * 
 * @author 宋佩骏 Email:tjsongpeijun@kingdee.com
 * @date 2020-3-26
 */
public class LogService {

	private Context ctx;

	public LogService(Context ctx) {
		this.ctx = ctx;
	}
	public LogService() {
		
	}

	/**
	 * 
	 * <p>
	 * Title: saveDownloadLog
	 * </p>
	 * <p>
	 * Description:保存下载日志
	 * </p>
	 * 
	 * @param result
	 *            返回信息
	 * @param request
	 *            请求信息
	 * @return
	 */
	public boolean saveDownloadLog(Map<String, Object> result, String request) {
		try {
			System.out.println("保存下载日志,入参 result："+result+" request:"+request);
			LogmessageInfo logmessageInfo = new LogmessageInfo();
			BOSUuid id = BOSUuid.create("3D9E429C");
			logmessageInfo.setId(id);
			logmessageInfo.setType("下载基础资料");
			DateUtil.formatALL(new Date());
			if (result.size() == 0) {
				logmessageInfo.setRESULT("失败");
			} else {
				logmessageInfo.setRESULT("成功");
				for (Map.Entry<String, Object> entry : result.entrySet()) {
					LogmessageEntryInfo logmessageEntryInfo = new LogmessageEntryInfo();
					logmessageEntryInfo.setParent(logmessageInfo);
					logmessageEntryInfo.setREQUESTMAS(request);
					String fileName = entry.getKey();
					Object fileContent = entry.getValue();
					// 事项目录
					if ("catalog.xml".equals(fileName)) {
						fileContent = (CatalogList) fileContent;
					}
					// 会议类型
					if ("meeting_type.xml".equals(fileName)) {
						fileContent = (MeetingTypeList) fileContent;
					}
					// 制度类型
					if ("regulation_type.xml".equals(fileName)) {
						fileContent = (RegulationTypeList) fileContent;
					}
					// 会议形式
					if ("meeting_form.xml".equals(fileName)) {
						fileContent = (MeetingFormList) fileContent;
					}
					// 任务来源
					if ("source.xml".equals(fileName)) {
						fileContent = (SourceList) fileContent;
					}
					// 专项名称
					if ("special.xml".equals(fileName)) {
						fileContent = (SpecialList) fileContent;
					}
					// 表决方式
					if ("vote_mode.xml".equals(fileName)) {
						fileContent = (VoteModeList) fileContent;
					}
					logmessageEntryInfo
							.setRESPONSEMAS(fileContent != null ? fileContent
									.toString() : "返回结果为空");
					logmessageEntryInfo.setFROM(fileName);
					LogmessageEntryFactory.getLocalInstance(ctx).save(logmessageEntryInfo);
				}
			}
			if(ctx == null){
				LogmessageFactory.getRemoteInstance().save(logmessageInfo);
			}else{
			LogmessageFactory.getLocalInstance(ctx).save(logmessageInfo);
			}
		} catch (EASBizException e) {
			System.out.println("保存下载日志报错，报错日志为:" + e.getMessage());
			return false;
		} catch (BOSException e) {
			System.out.println("保存下载日志报错，报错日志为:" + e.getMessage());
			return false;
		} catch (ParseException e) {
			System.out.println("保存下载日志报错，报错日志为:" + e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * 
	 * <p>
	 * Title: saveUploadLog
	 * </p>
	 * <p>
	 * Description:保存上传日志
	 * </p>
	 * 
	 * @param result
	 *            返回信息
	 * @param request
	 *            请求信息
	 * @param type
	 *            上传类型
	 * @param number
	 *            原单编号
	 * @return
	 */
	public boolean saveUploadLog(String result, String request, String type,String number) {
		try {
			System.out.println("保存上传日志,入参 result："+result+" request:"+request+" type:"+type);
			LogmessageInfo logmessageInfo = new LogmessageInfo();
			BOSUuid id = BOSUuid.create("3D9E429C");
			logmessageInfo.setId(id);
			logmessageInfo.setType("上传资料");
			DateUtil.formatALL(new Date());
			logmessageInfo.setRESULT("成功");
			logmessageInfo.setYNUMBER(number);
			LogmessageEntryInfo logmessageEntryInfo = new LogmessageEntryInfo();
			
			logmessageEntryInfo.setREQUESTMAS(request);
			logmessageEntryInfo.setRESPONSEMAS(result);
			logmessageEntryInfo.setFROM(type);
			logmessageEntryInfo.setParent(logmessageInfo);
			LogmessageFactory.getLocalInstance(ctx).save(logmessageInfo);
			if(ctx == null){
				LogmessageEntryFactory.getRemoteInstance().save(logmessageEntryInfo);
			}else{
				LogmessageEntryFactory.getLocalInstance(ctx).save(logmessageEntryInfo);
			}
		} catch (EASBizException e) {
			System.out.println("保存上传日志报错，报错日志为:" + e.getMessage());
			return false;
		} catch (BOSException e) {
			System.out.println("保存上传日志报错，报错日志为:" + e.getMessage());
			return false;
		} catch (ParseException e) {
			System.out.println("保存上传日志报错，报错日志为:" + e.getMessage());
			return false;
		}
		return true;
	}

}
