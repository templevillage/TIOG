package com.kingdee.eas.custom.tiog.utils;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.basedata.org.CompanyOrgUnitFactory;
import com.kingdee.eas.basedata.org.ICompanyOrgUnit;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.tiog.basedata.CatalogMainFactory;
import com.kingdee.eas.custom.tiog.basedata.ICatalogMain;
import com.kingdee.eas.custom.tiog.basedata.IMeetingType;
import com.kingdee.eas.custom.tiog.basedata.MeetingTypeFactory;
import com.kingdee.eas.custom.tiog.bizbill.ISubject;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.PoliticalSystemEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.SubjectCatalogEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectFactory;
import com.kingdee.eas.custom.tiog.bizbill.SubjectInfo;

/**
 * 
 * @title SubjectBillUtil
 * @description 审议议题工具类
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-30
 */
public class SubjectBillUtil {

    /**
     * 
     * @title addNewSubjectBill
     * @description 制度生成议题
     * @param meetTypeId 会议类型
     * @param companyId 企业
     * @param entrys 制度分录
     * @return IObjectPK 议题新单据PK
     * @throws BOSException
     * @throws EASBizException
     */
    public static IObjectPK addNewSubjectBill(BOSUuid meetTypeId, BOSUuid companyId,
            PoliticalSystemEntryCollection entrys) throws BOSException, EASBizException {
        ISubject iSubject = SubjectFactory.getRemoteInstance(); // 议题
        ICompanyOrgUnit iCompanyOrgUnit = CompanyOrgUnitFactory.getRemoteInstance();// 企业
        ICatalogMain iCatalogMain = CatalogMainFactory.getRemoteInstance(); // 事项
        IMeetingType iMeetingType = MeetingTypeFactory.getRemoteInstance(); // 会议类型

        SubjectInfo subjectInfo = new SubjectInfo(); // 议题info
        SubjectCatalogEntryCollection subjectCatalogEntrys = subjectInfo.getCatalogEntry();// 议题事项分录
        for (int i = 0; i < entrys.size(); i++) {
            SubjectCatalogEntryInfo subjectCatalogInfo = new SubjectCatalogEntryInfo();

            PoliticalSystemEntryInfo entryInfo = entrys.get(i);
            BOSUuid catalogMainId = entryInfo.getCatalogMain().getId(); // 事项
            String catalogMainNumber = entryInfo.getCatalogMainNumber();// 事项编码

            subjectCatalogInfo.setCatalogMain(iCatalogMain
                    .getCatalogMainInfo(new ObjectUuidPK(catalogMainId)));
            subjectCatalogInfo.setCatalogMainNumber(catalogMainNumber);
            subjectCatalogEntrys.add(subjectCatalogInfo);
        }

        subjectInfo.setCompany(iCompanyOrgUnit.getCompanyOrgUnitInfo(new ObjectUuidPK(companyId)));
        subjectInfo.setCompanyNumber(DataConvertUtil.findOrgCode(companyId.toString())); // 组织机构代码
        subjectInfo.setMeetingType(iMeetingType.getMeetingTypeInfo(new ObjectUuidPK(meetTypeId)));

        return iSubject.addnew(subjectInfo);
    }

}
