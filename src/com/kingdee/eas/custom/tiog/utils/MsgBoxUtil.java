package com.kingdee.eas.custom.tiog.utils;

import com.kingdee.eas.rptclient.newrpt.util.MsgBox;

public class MsgBoxUtil {
    private static StringBuffer log = new StringBuffer();

    /**
     * 
     * @title fieldBlankWarning
     * @description 字段检验，弹出警告框
     * @param billNum 单据编码
     * @param fieldName 字段名称
     */
    public static void fieldBlankWarning(String billNum, String fieldName) {
        log.setLength(0);
        showWarning(log.append("单据编码：").append(billNum).append(fieldName).append("为空，请检查！").toString());

    }

    /**
     * 
     * @title showWarning
     * @description 弹出警告信息
     * @param warnMsg 警告信息
     */
    public static void showWarning(String warnMsg) {
        MsgBox.showWarning(warnMsg);
    }

    /**
     * 
     * @title showInfo
     * @description 弹出提示信息
     * @param msg 提示信息
     */
    public static void showInfo(String msg) {
        MsgBox.showInfo(msg);
    }

}
