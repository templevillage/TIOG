package com.kingdee.eas.custom.tiog.utils;

import java.util.HashMap;
import java.util.Map;

import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.ui.face.IUIFactory;
import com.kingdee.bos.ui.face.IUIWindow;
import com.kingdee.bos.ui.face.UIException;
import com.kingdee.bos.ui.face.UIFactory;

/**
 * 
 * @title UIWindowUtil
 * @description 弹窗工具类
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-26
 */
public class UIWindowUtil {

    public static void showOnlyView(String uiName, IObjectPK pk) throws UIException {
        Map ctx = new HashMap();
        ctx.put("ID", pk.toString());

        IUIFactory uiFactory = UIFactory
                .createUIFactory("com.kingdee.eas.base.uiframe.client.UINewFrameFactory");

        IUIWindow uiWindow = uiFactory
                .create(uiName, ctx, null, com.kingdee.eas.common.client.OprtState.VIEW);
        uiWindow.show();
    }

    public static void showAndEdit(String uiName, IObjectPK pk) throws UIException {
        Map ctx = new HashMap();
        ctx.put("ID", pk.toString());

        IUIFactory uiFactory = UIFactory
                .createUIFactory("com.kingdee.eas.base.uiframe.client.UINewFrameFactory");

        IUIWindow uiWindow = uiFactory
                .create(uiName, ctx, null, com.kingdee.eas.common.client.OprtState.EDIT);
        uiWindow.show();
    }

}
