package com.kingdee.eas.custom.tiog.utils;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.eas.basedata.org.AdminOrgUnitFactory;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.basedata.org.IAdminOrgUnit;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.util.StringUtils;

/**
 * 
 * @title DataConvertUtil
 * @description 数据转换工具类
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-25
 */
public class DataConvertUtil {

    /**
     * 
     * @title findOrgCode
     * @description 根据公司内码去找行政组织的组织机构代码 《服务端调用Local》
     * @param companyId 公司内码
     * @return 组织结构代码
     * @throws EASBizException
     * @throws BOSException
     */
    public static String findOrgCode(Context ctx, String companyId) throws EASBizException, BOSException {
        return findOrgCode(companyId, AdminOrgUnitFactory.getLocalInstance(ctx));
    }

    /**
     * 
     * @title findOrgCode
     * @description 根据公司内码去找行政组织的组织机构代码 《远程调用Remote》
     * @param companyId 公司内码
     * @return 组织结构代码
     * @throws EASBizException
     * @throws BOSException
     */
    public static String findOrgCode(String companyId) throws EASBizException, BOSException {
        return findOrgCode(companyId, AdminOrgUnitFactory.getRemoteInstance());
    }

    /**
     * 
     * @title findOrgCode
     * @description 根据公司内码去找行政组织的组织机构代码
     * @param companyId 公司内码
     * @param iAdminOrgUnit 接口实例
     * @return 组织结构代码
     * @throws EASBizException
     * @throws BOSException
     */
    private static String findOrgCode(String companyId, IAdminOrgUnit iAdminOrgUnit) throws EASBizException,
            BOSException {
        String orgCode = "";
        if (StringUtils.isEmpty(companyId)) {
            return orgCode;
        }

        if (iAdminOrgUnit.exists(new ObjectUuidPK(companyId))) {
            SelectorItemCollection sic = new SelectorItemCollection();
            sic.add(new SelectorItemInfo("id"));
            sic.add(new SelectorItemInfo("name"));
            sic.add(new SelectorItemInfo("number"));
            sic.add(new SelectorItemInfo("orgCode")); // 组织结构代码

            AdminOrgUnitInfo adminOrgUnitInfo = iAdminOrgUnit.getAdminOrgUnitInfo(
                    new ObjectUuidPK(companyId), sic);
            orgCode = adminOrgUnitInfo.getOrgCode();
        }
        return orgCode;
    }
}
