package com.kingdee.eas.custom.tiog.utils;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.basedata.org.CompanyOrgUnitFactory;
import com.kingdee.eas.basedata.org.CompanyOrgUnitInfo;
import com.kingdee.eas.basedata.org.ICompanyOrgUnit;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.tiog.basedata.IMeetingType;
import com.kingdee.eas.custom.tiog.basedata.MeetingTypeFactory;
import com.kingdee.eas.custom.tiog.basedata.MeetingTypeInfo;
import com.kingdee.eas.custom.tiog.bizbill.IMeetingMan;
import com.kingdee.eas.custom.tiog.bizbill.ISubject;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManFactory;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManInfo;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryCollection;
import com.kingdee.eas.custom.tiog.bizbill.MeetingManSubjectEntryInfo;
import com.kingdee.eas.custom.tiog.bizbill.SubjectFactory;
import com.kingdee.eas.custom.tiog.bizbill.SubjectInfo;

/**
 * 
 * @title MeetingManBillUtil
 * @description 会议决策单据工具类
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-26
 */
public class MeetingManBillUtil {

    /**
     * 
     * @title addMeetingManBill
     * @description 议题生成会议
     * @param id 议题单据内码
     * @param companyId 企业内码
     * @param meetTypeId 会议类型内码
     * @return uuid 新会议单据内码
     * @throws BOSException
     * @throws EASBizException
     */
    public static IObjectPK addNewMeetingManBill(BOSUuid id, BOSUuid companyId, BOSUuid meetTypeId)
            throws BOSException, EASBizException {

        IMeetingMan iMeetingMan = MeetingManFactory.getRemoteInstance();
        MeetingManInfo meetingManInfo = new MeetingManInfo();

        // 议题
        ISubject iSubject = SubjectFactory.getRemoteInstance();
        SubjectInfo subjectInfo = iSubject.getSubjectInfo(new ObjectUuidPK(id));
        // 会议类型
        IMeetingType iMeetingType = MeetingTypeFactory.getRemoteInstance();
        MeetingTypeInfo meetingTypeInfo = iMeetingType.getMeetingTypeInfo(new ObjectUuidPK(meetTypeId));
        // 企业
        ICompanyOrgUnit iCompanyOrgUnit = CompanyOrgUnitFactory.getRemoteInstance();
        CompanyOrgUnitInfo companyOrgUnitInfo = iCompanyOrgUnit.getCompanyOrgUnitInfo(new ObjectUuidPK(
                companyId));

        meetingManInfo.setMeetingType(meetingTypeInfo);// 会议类型
        meetingManInfo.setMeetingName(meetingTypeInfo.getName()); // 会议类型名称

        MeetingManSubjectEntryCollection entrys = meetingManInfo.getSubjectEntry();// 议题分录
        MeetingManSubjectEntryInfo subjectEntryInfo = new MeetingManSubjectEntryInfo();
        subjectEntryInfo.setCompany(companyOrgUnitInfo);// 企业
        subjectEntryInfo.setCompanyNumber(DataConvertUtil.findOrgCode(companyId.toString())); // 组织结构代码
        subjectEntryInfo.setSubject(subjectInfo);// 议题
        subjectEntryInfo.setSubjectNumber(subjectInfo.getNumber()); // 议题编码
        entrys.add(subjectEntryInfo);

        IObjectPK uuid = iMeetingMan.addnew(meetingManInfo);
        return uuid;
    }

    /**
     * 
     * @title findBillNum
     * @description 获取会议决策单据编码
     * @param pk 单据内码PK
     * @return 单据编码
     * @throws BOSException
     * @throws EASBizException
     */
    public static String findBillNum(IObjectPK pk) throws BOSException, EASBizException {
        SelectorItemCollection sic = new SelectorItemCollection();
        sic.add(new SelectorItemInfo("id"));
        sic.add(new SelectorItemInfo("number"));
        IMeetingMan iMeetingMan = MeetingManFactory.getRemoteInstance();
        MeetingManInfo meetingManInfo = iMeetingMan.getMeetingManInfo(pk, sic);
        return meetingManInfo.getNumber();
    }

}
