package com.kingdee.eas.custom.tiog.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @title DateUtil
 * @description 日期转换工具类
 * @author yacong_liu Email:yacong_liu@qq.com
 * @date 2020-3-24
 */
public class DateUtil {
    private static final String DATE_FORMAT_ALL = "yyyy-MM-dd HH:mm:ss";

    private static final String DATE_FORMAT2_YMD = "yyyy-MM-dd";

    private static ThreadLocal threadLocal_ALL = new ThreadLocal() {
        @Override
        protected Object initialValue() {
            return new SimpleDateFormat(DATE_FORMAT_ALL);
        }
    };

    private static ThreadLocal threadLocal_YMD = new ThreadLocal() {
        @Override
        protected Object initialValue() {
            return new SimpleDateFormat(DATE_FORMAT2_YMD);
        }
    };

    public static DateFormat getDateFormatForAll() {
        return (DateFormat) threadLocal_ALL.get();
    }

    public static DateFormat getDateFormatForYMD() {
        return (DateFormat) threadLocal_YMD.get();
    }

    /**
     * 
     * @title parseALL
     * @description 字符串转日期
     * @param textDate 日期字符串
     * @return Date "yyyy-MM-dd HH:mm:ss"
     * @throws ParseException
     */
    public static Date parseALL(String textDate) throws ParseException {
        return getDateFormatForAll().parse(textDate);
    }

    /**
     * 
     * @title parseYMD
     * @description 字符串转日期
     * @param textDate 日期字符串
     * @return Date "yyyy-MM-dd"
     * @throws ParseException
     */
    public static Date parseYMD(String textDate) throws ParseException {
        return getDateFormatForYMD().parse(textDate);
    }

    /**
     * 
     * @title formatYMD
     * @description 日期转字符串
     * @param date 日期
     * @return String "yyyy-MM-dd"
     * @throws ParseException
     */
    public static String formatYMD(Date date) throws ParseException {
        return getDateFormatForYMD().format(date);
    }

    /**
     * 
     * @title formatALL
     * @description 日期转字符串
     * @param date 日期
     * @return String "yyyy-MM-dd HH:mm:ss"
     * @throws ParseException
     */
    public static String formatALL(Date date) throws ParseException {
        return getDateFormatForAll().format(date);
    }

}
