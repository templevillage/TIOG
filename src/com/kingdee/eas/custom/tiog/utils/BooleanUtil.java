package com.kingdee.eas.custom.tiog.utils;

public class BooleanUtil {

    /**
     * 
     * @title booleanConvertStr
     * @description boolean 转字符串
     * @return true-> "是" false-> "否"
     */
    public static String booleanConvertStr(boolean flag) {
        return (flag == true ? "是" : "否");
    }

}
